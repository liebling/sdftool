      !
      ! Trees.f
      !

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    tree_set_mask:                                                          cc
cc                  Set the masks on all levels/grids w/in the tree           cc
cc                  to facilitate computations such as integrals and norms.   cc
cc                     A point is UNMASKED if it occurs in the finest grid    cc
cc                  for that location, and only occurs in the first instance  cc
cc                  of that location in a level.                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tree_set_mask(tree,masktype)
      implicit none
      integer   tree, masktype
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'trees.inc'
      include 'methods.inc'
      include 'variables.inc'
      integer  li, gi, gl, nextli

      logical     ltrace
      parameter ( ltrace = .false. )

      if (ltrace) write(*,99) myid, 'tree_set_mask: ***tree = ',tree
      if (ltrace) write(*,99) myid, 'tree_set_mask: masktype= ',masktype
      if (ltrace) call tree_dump(tree)
  99  format('[',I3,'] ',A,I5)
  9   format('[',I3,'] ',A,I5)

      !
      ! Loop through levels:
      !
      li     = tree_return_start(tree)
      !
      do while (li.ne.NULL_LEVEL)
         if (ltrace) write(*,99) myid, 'tree_set_mask: li = ',li
         !
         ! Loop over grids:
         !
         gi  = level_return_start(li)
         do while (gi .ne. NULL_GRID)
            if (ltrace) write(*,99) myid, 'tree_set_mask: gi = ',gi
            if (.not.grid_is_local(gi)) then
                if (ltrace) write(*,99) myid, 'tree_set_mask: Not local'
                goto 10
            end if
            if (ltrace) write(*,99) myid, 'tree_set_mask:    Local'
            !
            ! For each grid:
            !    (1) Initialize to unmasked
            !    (2) For each grid on next level, mask
            !    (3) For each grid on this level coming before it,
            !        unmask
            !
            ! (1):
            !
            if (ltrace) write(*,99) myid, 'tree_set_mask:    Unmasking'
            call grid_unmask(gi)
            !
            ! (2):
            !
            nextli = level_return_sibling(li)
            if (ltrace)write(*,99)myid,'tree_set_mask: Child level:'
            if (nextli .ne. NULL_LEVEL) then
               !if(ltrace)write(*,99)myid,'tree_set_mask:Nxtlevel:',nextli
               gl = level_return_start(nextli)
               do while (gl .ne. NULL_GRID)
                  if(ltrace)write(*,9)myid,'               Mask gl =',gl
                  call grid_maskgrid(gi,gl,masktype)
                  gl = grid_return_sibling(gl)
               end do
            end if
            !
            ! (3):
            !
            gl = level_return_start(li)
            if (ltrace)write(*,99)myid,'tree_set_mask: Siblings:'
            do while (gl .ne. NULL_GRID .and. gl .ne. gi)
               if (ltrace)write(*,99)myid,'               Mask gl = ',gl
               call grid_maskgrid(gi,gl,masktype)
               gl = grid_return_sibling(gl)
            end do
            if(ltrace)write(*,99)myid,'tree_set_mask: Done w/ gi=',gi
            !
  10        gi = grid_return_sibling(gi)
         end do
         !
         li = level_return_sibling(li)
      end do

      return
      end           ! END: tree_set_mask

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    tree_free:                                                              cc
cc               Free all levels associated with the tree.                    cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tree_free(tree)
      implicit none
      integer   tree
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'trees.inc'
      include 'methods.inc'
      include 'variables.inc'
      integer  li, prevli, nextli

      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (ltrace2) then
         write(*,99) myid, 'tree_free: tree  = ',tree
  99     format('[',I3,'] ',A,I5)
      end if

      !
      ! Loop through levels and find the one to be removed:
      !
      li     = tree_return_start(tree)
      !
  11  if (li.ne.NULL_LEVEL) then
         nextli = level_return_sibling(li)
         if(ltrace2)write(*,99) myid, 'tree_remove_level: free li:',li
         call level_free(li)
         li     = nextli
         goto 11
      end if

      call tree_set_start(tree, NULL_LEVEL)

      if (ltrace2) then
         call tree_dump(tree)
         write(*,99) myid, 'tree_free: Done. tree  = ',tree
      end if

      ! return tree number to available numbers:
      if (tree .eq. tree_counter - 1) then
         ! Tree to be freed is last one, make it available:
         tree_counter = tree_counter -1
      else
         ! Need to keep free tree numbers:
         tree_numavail                = tree_numavail + 1 
         tree_freenums(tree_numavail) = tree
      end if

      return
      end           ! END: tree_free

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    tree_remove_level:                                                      cc
cc                    Remove a level from a given tree.                       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tree_remove_level(tree,lev)
      implicit none
      integer   tree, lev
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'trees.inc'
      include 'methods.inc'
      include 'variables.inc'
      integer  li, prevli, nextli

      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (ltrace2) then
         write(*,99) myid, 'tree_remove_level: tree  = ',tree
         write(*,99) myid, 'tree_remove_level: lev   = ',lev
  99     format('[',I3,'] ',A,I5)
      end if

      !
      ! Loop through levels and find the one to be removed:
      !
      prevli = NULL_LEVEL
      li     = tree_return_start(tree)
      !
  10  if (li.ne.NULL_LEVEL) then
         nextli = level_return_sibling(li)
         if (li .eq. lev) then
            ! Usage:  level_set_sibling(lev, sibling)
            if (prevli.ne.NULL_LEVEL) then
               !
               ! Level occurs "normally" or at end...
               !
               call level_set_sibling(prevli,nextli)
            else
               !
               ! Level occurs at start of tree
               !
               call tree_set_start(tree, nextli)
            end if
            ! At this point, should free up all the grids and storage
            ! associated with this level, but Jason has not implemented
            ! those routines yet...
            call level_free(lev)
            ! Exit loop:
            goto 20
            !
         end if
         !
         prevli = li
         li     = nextli
         goto 10
      end if

 20   continue

      return
      end           ! END: tree_remove_level


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tree_init()
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer i
         tree_counter = 1

         do i = 1, MAX_NUM_TREES
            tree_level(i)   = NULL_TREE
            tree_sibling(i) = NULL_TREE
            tree_time(i)    = -1.d0
         enddo

         ! keep track of freed trees whose number is available:
         tree_numavail = 0

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    tree_add_level:                                                         cc
cc                    Given some resolution "res" it determines whether       cc
cc                    the given tree contains a level with that resolution,   cc
cc                    and, if so, returns a pointer to that level, else       cc
cc                    returns a pointer to a new level added to that tree.    cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      function tree_add_level(tree, res)
      implicit none
         integer   tree
         real*8    res
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'trees.inc'
      include 'methods.inc'
      include 'variables.inc'
         real*8    lres
         integer   li, last_level, newli
         !Functions:
         !logical   double_equal

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         if (ltrace2) then
            write(*,99) myid, 'tree_add_level: tree = ',tree
            write(*,91) myid, 'tree_add_level: res  = ',res
            write(*,91) myid, 'tree_add_level: Calling tree_dump:'
            call tree_dump(tree)
         end if

         last_level  = NULL_LEVEL
         li          = tree_return_start(tree)
         if(ltrace2)write(*,92)myid,'last_level/li:',last_level,li
         !
         ! Assume levels ordered in decreasing resolution, so
         ! stop when user-input resolution greater than that of
         ! the existing levels:
         !
         do while ( li .ne. NULL_LEVEL )
            lres = level_return_resolution(li)
            if(ltrace2)write(*,91)myid,'tree_add_levelEqual? ',lres,res
            if ( double_equal(lres, res) ) then
               !
               ! Level w/ matching resolution found,
               ! return the pointer and exit:       
               !
               if(ltrace2)write(*,99)myid,'tree_add_level:Yes,equal res'
               if(ltrace2)write(*,99) myid,'tree_add_level:Levfound:',li
               tree_add_level = li
               return
            else if (res .gt. lres) then
               !
               ! Resolution not found,
               ! break from loop and create a new level
               !
               if(ltrace2)write(*,99)myid,'tree_add_level:No,unequalres'
               if(ltrace2)write(*,99)myid,'tree_add_level:Res not found'
               goto 10
            endif
            ! Increment level
            last_level = li
            li         = level_return_sibling(li)
            if(ltrace2)write(*,92)myid,'last_level/li:',last_level,li
         enddo

 10      continue


         newli        = level_get_free_number()
         if (ltrace2) then
            write(*,99) myid,'tree_add_level: newli = ',newli
         end if

         if( last_level .ne. NULL_LEVEL) then
            ! Insert level between two others:
            call level_set_sibling(newli,li)
            call level_set_sibling(last_level, newli)
          call level_set_number(newli,level_return_number(last_level)+1)
           if (ltrace2) then
           write(*,92)myid,'tree_add_level:Insert between:'
           write(*,92)myid,'',last_level,level_return_number(last_level)
           write(*,99)myid,'tree_add_level:And: '
           write(*,92)myid,'',li,         level_return_number(li)
           write(*,99)myid,'tree_add_level:With level number:',
     .                 level_return_number(newli)
           end if
           call level_set_number(li,level_return_number(li)+1)
         else
            ! Put level first
            if (ltrace2) then
            write(*,99) myid,'tree_add_level: First level on this tree'
            end if
            call level_set_sibling(newli, li)
                    ! Confusing argument ordering:
                    !subroutine level_set_sibling(level, sibling)
            call tree_set_start(tree, newli)
            call level_set_number(newli, 1)
         endif

         call level_set_resolution(newli, res)
         call level_set_tree(newli, tree)

         tree_add_level        = newli

         if (ltrace2) then
            write(*,99) myid,'tree_add_level: Calling tree_dump:'
            call tree_dump(tree)
            write(*,99) myid,'tree_add_level:      Done.'
         end if

         return

  91        format('[',I3,'] ',A,5F12.5)
  92        format('[',I3,'] ',A,2I5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F12.5)
  95        format('[',I3,'] ',A,I5,F12.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

      end        ! END: tree_add_level




      subroutine tree_set_sibling(tree, sibling)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer tree
         integer sibling

         tree_sibling(tree) = sibling

         return
      end

      !Return the last level on a certain tree
      integer function tree_return_end(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'

         integer tree

         !Functions:
         integer level_return_sibling
         integer level_get_sibling

         integer li  !Level counter

         li = tree_level(tree) !points ot first level in tree

         !Will increment through till it gets to last sibling
         if( li .ne. NULL_LEVEL ) then
            do while( level_return_sibling(li) .ne. NULL_LEVEL )
               li = level_return_sibling(li)
            enddo

            tree_return_end = li
         else
            tree_return_end = NULL_LEVEL
         endif

         return
      end

      subroutine tree_set_time(tree, time)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer tree
         real*8  time
         tree_time(tree) = time

         return
      end

      double precision function tree_return_time(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc' 
         integer tree
         
         tree_return_time = tree_time(tree)

         return
      end

      subroutine tree_set_reg(tree, reg)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer tree
         integer reg

         tree_on_reg(tree) = reg

         return
      end

      integer function tree_return_reg(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer tree
      
         tree_return_reg = tree_on_reg(tree)
         return
      end

      ! What is this?
      subroutine tree_set_start(tree, level)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer tree
         integer level

         tree_level(tree) = level

         return
      end

      integer function tree_return_sibling(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
 
         integer tree

         tree_return_sibling = tree_sibling(tree)
   
         return
      end

      integer function tree_get_free_number()
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
      
      if (tree_counter .eq. MAX_NUM_TREES) then
         tree_get_free_number = NULL_TREE
         write(*,*) 'tree_get_free_number: No more trees left.'
         write(*,*) 'tree_get_free_number: Stopping here.'
         stop
      else
         tree_get_free_number = tree_counter
         tree_counter         = tree_counter + 1
      end if

         return
      end

      subroutine tree_output(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer        tree
 
         integer        level

         !Functions
         integer tree_return_start
         integer level_return_sibling
  
         !Gives first level in tree
         level = tree_return_start(tree)
         do while( level .ne. NULL_LEVEL )
            call level_output(level)

            !Increment level
            level = level_return_sibling(level)
         enddo

         return
      end

      subroutine tree_amira(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer        tree
 
         integer        level

         !Functions
         integer tree_return_start
         integer level_return_sibling
  
         !Gives first level in tree
         level = tree_return_start(tree)
         do while( level .ne. NULL_LEVEL )
            call amira_level_output(level)

            !Increment level
            level = level_return_sibling(level)
         enddo

         return
      end
 
      integer function tree_return_start(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer tree
   
         tree_return_start = tree_level(tree)
 
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function tree_return_maxradius(tree)
      implicit none
      integer  tree
      include 'class_constants.inc'
      include 'trees.inc'
      integer   li
      real*8    mytmp

      !Functions
      integer   tree_return_start, level_return_sibling
      external  tree_return_start, level_return_sibling
      real*8    level_return_maxradius
      external  level_return_maxradius

      li = tree_return_start( tree )

      tree_return_maxradius = 0.d0
      do while ( li .ne. NULL_LEVEL )
         mytmp = level_return_maxradius(li)
         tree_return_maxradius = max(mytmp,tree_return_maxradius)
         !
         li = level_return_sibling(li)
      end do


      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
 
      integer function tree_get_number_levels(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer   tree

         integer   li
         integer   num_levels

         !Functions
         integer   tree_return_start
         integer   level_return_sibling

         li = tree_return_start( tree )

         if ( li .ne. NULL_LEVEL ) then
            num_levels = 1
         endif

         do while ( level_return_sibling(li) .ne. NULL_LEVEL )
            num_levels = num_levels + 1

            !Increment tree counter
            li = level_return_sibling(li)
         enddo

         tree_get_number_levels = num_levels

         return
      end
 
      
      subroutine tree_dump(tree)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc' 
      include 'variables.inc'
      include 'trees.inc' 
         integer   tree
 
         integer   num_levels
         integer   li
         real*8    t
 
         !Functions
         integer   tree_get_number_levels
         double precision tree_return_time
         integer   level_return_sibling
         integer   tree_return_start

         num_levels = tree_get_number_levels(tree)
         t          = tree_return_time(tree)
        
         write(*,'(A,I3.1,A,A,I3.1,A,I3.1,A,F12.4)') "[",myid,"]",
     *              "  Tree: ", tree, 
     *              "   NumLevels: ", num_levels, 
     *              "   Time: ", t

         li = tree_return_start(tree)
         do while( li .ne. NULL_LEVEL )
            call level_dump(li)

            li = level_return_sibling(li)
         enddo

         return
      end

      integer function tree_return_register(tree)
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer  tree

         tree_return_register = tree_on_reg(tree) 
         return
      end

      subroutine tree_copy( old_tree, new_tree, res )
      implicit none
      include 'class_constants.inc'
      include 'trees.inc'
         integer old_tree
         integer new_tree
         real*8  res

         integer new_level
         integer old_li
         integer shape(6)
         real*8  bbox(6)
         real*8  resolution

         !Functions
         integer tree_add_level
         integer level_return_sibling
         real*8  level_return_resolution
         logical double_equal

         write(*,*) "Entering Tree Copy..."
     
         if ( double_equal(res, 0.d0) ) then

           ! Loop over all level
           old_li = tree_level(old_tree)  ! First level in tree
         
           do while( old_li .ne. NULL_LEVEL )
              ! Grab resolution from old level
              resolution = level_return_resolution(old_li)
              write(*,*) "old_li = ", old_li
              new_level = tree_add_level(new_tree, resolution)
 
              call level_copy( old_li, new_level )

              ! Increment counter
              old_li = level_return_sibling(old_li)
           enddo
         else  
           ! Find resolution to be copied
           old_li = tree_level(old_tree)
         
           do while( old_li .ne. NULL_LEVEL ) 
              resolution = level_return_resolution(old_li)
              write(*,*) "given res = ", res
              write(*,*) "ti res    = ", resolution
              ! Check if this level has resolution specified
              if ( double_equal( res, resolution) ) then
                 goto 20
              endif
           
              ! Increment
              old_li = level_return_sibling(old_li)
           enddo

           write(*,*) "Resolution not found in tree!"
           stop

           ! Create new level and copy it to new tree
20         new_level = tree_add_level(new_tree, resolution)
           call level_copy( old_li, new_level )
         endif

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function tree_return_resolution( tree )
      implicit none
      integer tree
      include 'class_constants.inc'
      include 'trees.inc'
      !include 'methods.inc'
      integer tree_return_start
      real*8  level_return_resolution
      integer li

         li = tree_return_start(tree)
         
         tree_return_resolution = level_return_resolution(li)

         return
      end        ! END: tree_return_resolution

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    tree_valid:  Tests whether a tree  number is valid or not.      cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function tree_valid(lev)
      implicit none
      integer  lev
      include 'class_constants.inc'

      tree_valid = .true.
      if (lev .gt. MAX_NUM_TREES) tree_valid = .false.
      if (lev .le. 0            ) tree_valid = .false.

      return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    tree_getintegral:                                               cc
cc                                                                    cc
cc               Caculate the integral for this tree (a particular    cc
cc         time). Based on register_integrate().                      cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function  tree_getintegral(tree)
      implicit none
      integer tree
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      !include 'methods.inc'
      real*8      LARGENUMBER
      parameter ( LARGENUMBER = 1.0d98 )
      integer rank, ti, li, gi, li2, gi2, shape(3), nr, p
      integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
      real*8  integral, bbox(6), bboxi(6), diff_x, diff_y, diff_z
      real*8  h, real_integral
      real*8  radius, dradius, mradius
      logical  grid_is_local
      external grid_is_local
      integer  level_return_sibling, grid_return_sibling
      external level_return_sibling, grid_return_sibling
      integer  level_return_start, tree_return_start
      external level_return_start, tree_return_start
      real*8   reg_return_maxradius, reg_return_maxresolution
      external reg_return_maxradius, reg_return_maxresolution
      real*8   grid_integrate, tree_return_time
      external grid_integrate, tree_return_time
      logical     ltrace
      parameter ( ltrace = .false. )

          
      if(ltrace) then
         write(*,99)myid,'tree_getintegral: Enter:',tree
         !call tree_dump_info(tree)
      end if

      ti = tree
      if ( ti .ne. NULL_TREE )  then
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            call tree_set_mask(ti,MASK_INTEGRAL)
            ! Go to start of tree
            li = tree_return_start(ti)
            integral = 0.d0
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     !integral =integral + grid_integrate(gi,radius)
                     !integral =integral + grid_integrate(gi,6.d0)
                     integral =integral + grid_integrate(gi,LARGENUMBER)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            ! all processors need to return with the right answer:
            call MPI_AllReduce(integral, real_integral, 1, 
     *                    MPI_DOUBLE_PRECISION, MPI_SUM, 
     *                    MPI_COMM_WORLD, ierr)
!           call MPI_Reduce(integral, real_integral, 1, 
!    *                    MPI_DOUBLE_PRECISION, MPI_SUM, master,
!    *                    MPI_COMM_WORLD, ierr)
            if ( myid .eq. master .and.ltrace) then
                write(*,*) tree_return_time(ti), radius, real_integral
            endif
            tree_getintegral = real_integral
            !
      end if

      if(ltrace)write(*,98)myid,'tree_getintegral:Done',tree_getintegral
  97  format('[',I3,'] ',A,A)
  98  format('[',I3,'] ',A,F18.12)
  99  format('[',I3,'] ',A,I5)

      return
      end             ! END: tree_getintegral

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    tree_calcrho:                                                   cc
cc           Part of the calculation of the helicity of a vector field.c
cc        This routine is adapted from register_carttosphere().       cc
cc        It's a local calculation computed relative to some point    cc
cc        vec{x} as follows:                                          cc
cc             rho(xprime)= vec{B}(xprime) cross (vec{x}-vec{xprime}) cc
cc                                              /|vec{x}-vec{xprime}|^3c
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tree_calcrho(mycoords,trhox, trhoy, trhoz,tBx,tBy,tBz)
      implicit none
      real*8   mycoords(3)
      integer  trhox, trhoy, trhoz, tBx,tBy,tBz
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
      character*32   name, file_name
      integer lrx, lry, lrz, lBx, lBy, lBz
      integer grx, gry, grz, gBx, gBy, gBz
      logical     ltrace
      parameter ( ltrace  = .false. )
      logical     ltrace2
      parameter ( ltrace2 = .false. )

          
      if(ltrace) then
         write(*,99)myid,'tree_calcrho:Enter:',trhox,trhoy,trhoz
         write(*,99)myid,'tree_calcrho:coords:',
     .                       mycoords(1),mycoords(2),mycoords(3)
      end if

      ! Loop over all trees
      if(ltrace)write(*,99)myid,'  trhox,trhoy,trhoz:',trhox,trhoy,trhoz
      if(ltrace)write(*,99)myid,'  tBx,  tBy,  tBz:  ',tBx,tBy,tBz
      ! Go to start of tree
      lrx = tree_return_start(trhox)
      lry = tree_return_start(trhoy)
      lrz = tree_return_start(trhoz)
      lBx = tree_return_start(tBx)
      lBy = tree_return_start(tBy)
      lBz = tree_return_start(tBz)
      ! Loop over all levels
      do while ( lrx .ne. NULL_LEVEL .and.
     *           lry .ne. NULL_LEVEL .and.
     *           lrz .ne. NULL_LEVEL      )
           if(ltrace)write(*,99)myid,'    lrx/y/z: ',lrx,lry,lrz
           if(ltrace)write(*,99)myid,'    lBx/y/z: ',lBx,lBy,lBz
           grx = level_return_start(lrx)
           gry = level_return_start(lry)
           grz = level_return_start(lrz)
           gBx = level_return_start(lBx)
           gBy = level_return_start(lBy)
           gBz = level_return_start(lBz)
           ! Loop over all grids
           do while ( grx .ne. NULL_GRID .and.
     *                gry .ne. NULL_GRID .and.
     *                grz .ne. NULL_GRID       )
               if(ltrace)write(*,99)myid,'A     grx/y/z: ',grx,gry,grz
               if(ltrace)write(*,99)myid,'A     gBx/y/z: ',gBx,gBy,gBz
               if( grid_is_local(grx) .and.
     *             grid_is_local(gry) .and.
     *             grid_is_local(grz)       ) then
                   call grid_calcrho(mycoords,grx,gry,grz,gBx,gBy,gBz)
               endif
               grx = grid_return_sibling(grx)
               gry = grid_return_sibling(gry)
               grz = grid_return_sibling(grz)
               gBx = grid_return_sibling(gBx)
               gBy = grid_return_sibling(gBy)
               gBz = grid_return_sibling(gBz)
           enddo
           lrx = level_return_sibling(lrx)
           lry = level_return_sibling(lry)
           lrz = level_return_sibling(lrz)
           lBx = level_return_sibling(lBx)
           lBy = level_return_sibling(lBy)
           lBz = level_return_sibling(lBz)
      enddo

      if (ltrace) then
         write(*,99)myid,'tree_calcrho: Done.:',trhox,trhoy,trhoz
      end if

  96     format('[',I3,'] ',A,3F13.6)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.12)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: tree_calcrho


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    tree_findgridwpoint:                                            cc
cc           Locate the finest grid in the tree that contains         cc
cc        the given point.                                            cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function tree_findgridwpoint(tree, coords)
      implicit none
      integer  tree
      real*8   coords(*)
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      !include 'methods.inc'
      integer   tree_return_start, level_return_start
      external  tree_return_start, level_return_start
      logical   grid_containspt, grid_containsptint
      external  grid_containspt, grid_containsptint
      integer   grid_return_sibling, level_return_sibling
      external  grid_return_sibling, level_return_sibling
      real*8    grid_return_resolution
      external  grid_return_resolution
      integer level
      integer grid
      real*8  myh, tmpmyh
      real*8      LARGENUMBER
      parameter ( LARGENUMBER = 1.0d98 )
      logical     ltrace
      parameter ( ltrace  = .false. )
      logical     ltrace2
      parameter ( ltrace2 = .false. )

          
      if(ltrace) then
         write(*,99)myid,'tree_findgridwpoint: Enter:',tree
         !write(*,98)myid,'tree_findgridwpoint: coords:',coords
      end if

      if (tree.lt.0) return

      ! Begin assuming it's nowhere:
      tree_findgridwpoint = NULL_GRID
      myh                 = LARGENUMBER

      ! Go to start of tree
      level = tree_return_start(tree)
      ! Loop over all levels
      do while ( level .ne. NULL_LEVEL )
           if(ltrace2)write(*,99)myid,'    level: ',level
           grid = level_return_start(level)
           ! Loop over all grids
           do while ( grid .ne. NULL_GRID )
               if(ltrace2)write(*,99)myid,'A     grid: ',grid
               ! Does this have our point?
               !if (grid_containsptint(grid,coords)) then
               if (grid_containspt(grid,coords)) then
                  if(ltrace)write(*,99)myid,' Grid contains pt',grid
                  tmpmyh = grid_return_resolution(grid)
                  if (tmpmyh .lt. myh) then
                     if(ltrace)write(*,98)myid,' hi-resolution:',tmpmyh
                     tree_findgridwpoint = grid
                     myh                 = tmpmyh
                  end if
               end if
               grid = grid_return_sibling(grid)
           enddo
           level = level_return_sibling(level)
      enddo

      if (ltrace) then
         write(*,99)myid,'tree_findgridwpoint = ',tree_findgridwpoint
         write(*,98)myid,'tree_findgridwpoint: w/ h= ',myh
         write(*,99)myid,'tree_findgridwpoint: Done.:',tree
      end if

  96     format('[',I3,'] ',A,3F13.6)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.12)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: tree_findgridwpoint


#ifndef VECTORS_CPP
#define VECTORS_CPP

/* A simple implementation of vector algebra and calculus. */
#include <cmath>
#include <stdio.h>

struct vec3 {
  double x;
  double y;
  double z;
};

/* Vector algebra. */

bool equals(vec3 v, vec3 w){
  return (v.x == w.x) && (v.y == w.y) && (v.z == w.z);
}

vec3 add(vec3 v, vec3 w){
  vec3 ret;
  ret.x = v.x + w.x;
  ret.y = v.y + w.y;
  ret.z = v.z + w.z;
  return ret;
}

vec3 negative(vec3 v){
  vec3 ret;
  ret.x = 0 - v.x;
  ret.y = 0 - v.y;
  ret.z = 0 - v.z;
  return ret;
}

vec3 subtract(vec3 v, vec3 w){
  return add(v, negative(w));
}

vec3 scalarproduct(vec3 v, double d){
  vec3 ret;
  ret.x = v.x * d;
  ret.y = v.y * d;
  ret.z = v.z * d;
  return ret;
}

double norm(vec3 v){
  return sqrt((v.x*v.x) + (v.y*v.y) + (v.z*v.z));
}

/** Return the unit vector in the direction of v. = v / norm(v) */
vec3 normalize(vec3 v){
  if(v.x == 0 && v.y == 0 && v.z == 0){
    return v;
  }
  return scalarproduct(v, 1 / norm(v));
}

bool equalsepsilon(vec3 v, vec3 w, double epsilon){
  return norm(subtract(v, w)) < epsilon;
}

double dotproduct(double vx, double vy, double vz, double wx, double wy, double wz){
  return (vx * wx) + (vy * wy) + (vz * wz);
}

double dotproduct(vec3 v, vec3 w){
  return dotproduct(v.x, v.y, v.z, w.x, w.y, w.z);
}

vec3 crossproduct(vec3 v, vec3 w){
  vec3 ret;
  ret.x = (v.y * w.z) - (v.z * w.y);
  ret.y = (v.z * w.x) - (v.x * w.z);
  ret.z = (v.x * w.y) - (v.y * w.x);
  return ret;
}

/* Other functions */

double interp(double v0, double v1, double x){
  double fact0 = 1-x;
  double fact1 = x;
  double divideby = fact0 + fact1;
  if(divideby == 0) return (divideby / divideby);
  double ret = (v0 * fact0) + (v1 * fact1);
  return ret / divideby;
}

/* Vector calculus. */

/**
 * Calculate the grad of a scalar field to get a vector field.
 * This algorithm runs in O(n), where n = topx * topy * topz.
 * \param field The original scalar field.
 * \param grad An already-defined matrix to put the grad (vector) field in.
 * \param topx, topy, topz The upper bounds of the matrices field and grad.
*/
void grad(double*** field, double**** grad, int topx, int topy, int topz){
  if(!(topx >= 2 && topy >= 2 && topz >= 2)){
    printf("Error: grad() does not work on a field that has a dimension only one wide!\n");
    return;
  }
  int i, j, k;
  for(i=0; i<topx; i++){
    for(j=0; j<topy; j++){
      for(k=0; k<topz; k++){
        if(i == 0){
          grad[i][j][k][0] = field[i+1][j][k] - field[i][j][k];
        }else if(i == topx-1){
          grad[i][j][k][0] = field[i][j][k] - field[i-1][j][k];
        }else{
          grad[i][j][k][0] = (field[i+1][j][k] - field[i-1][j][k]) * 0.5;
        }
        if(j == 0){
          grad[i][j][k][1] = field[i][j+1][k] - field[i][j][k];
        }else if(j == topy-1){
          grad[i][j][k][1] = field[i][j][k] - field[i][j-1][k];
        }else{
          grad[i][j][k][1] = (field[i][j+1][k] - field[i][j-1][k]) * 0.5;
        }
        if(k == 0){
          grad[i][j][k][2] = field[i][j][k+1] - field[i][j][k];
        }else if(k == topz-1){
          grad[i][j][k][2] = field[i][j][k] - field[i][j][k-1];
        }else{
          grad[i][j][k][2] = (field[i][j][k+1] - field[i][j][k-1]) * 0.5;
        }
      }
    }
  }
}

/**
 * Given a vector field that is the gradient of some scalar field, find the
 * original scalar field.
 * This algorithm runs in O(n) where n = topx * topy * topz.
 * \param grad 4D matrix giving the gradient (vector) field:
 * grad[x][y][z][0/1/2]
 * \param field 3D matrix (already malloc'd) into which gradtoscalar() should
 * put the original scalar field
 * \param topx The length of the arrays grad and field in the X direction.
 * \param topy The length of the arrays grad and field in the Y direction.
 * \param topz The length of the arrays grad and field in the Z direction.
 * \param initx The X-value of the initial condition for the original scalar field.
 * \param inity The Y-value of the initial condition for the original scalar field.
 * \param initz The Z-value of the initial condition for the original scalar field.
 * \param initval The value of the original scalar field at (initx, inity, initz).
*/
void ungrad(double**** grad, double*** field, int topx, int topy, int topz, int initx, int inity, int initz, double initval){
  /*Initial condition*/
  field[initx][inity][initz] = initval;
  /*Variables*/
  int i, j, k;
  double deriv;
  /*X-wise first*/
  for(i=initx + 1; i<topx; i++){
    deriv = interp(grad[i-1][inity][initz][0], grad[i][inity][initz][0], 0.5);
    field[i][inity][initz] = field[i-1][inity][initz] + (1 * deriv);
  }
  for(i=initx - 1; i>=0; i--){
    deriv = interp(grad[i][inity][initz][0], grad[i+1][inity][initz][0], 0.5);
    field[i][inity][initz] = field[i+1][inity][initz] - (1 * deriv);
  }
  /*For every Y*/
  for(j=inity + 1; j<topy; j++){
    deriv = interp(grad[initx][j-1][initz][1], grad[initx][j][initz][1], 0.5);
    field[initx][j][initz] = field[initx][j-1][initz] + (1 * deriv);
    /*X-wise*/
    for(i=initx + 1; i<topx; i++){
      deriv = interp(grad[i-1][j][initz][0], grad[i][j][initz][0], 0.5);
      field[i][j][initz] = field[i-1][j][initz] + (1 * deriv);
    }
    for(i=initx - 1; i>=0; i--){
      deriv = interp(grad[i][j][initz][0], grad[i+1][j][initz][0], 0.5);
      field[i][j][initz] = field[i+1][j][initz] - (1 * deriv);
    }
  }
  for(j=inity - 1; j>=0; j--){
    deriv = interp(grad[initx][j][initz][1], grad[initx][j+1][initz][1], 0.5);
    field[initx][j][initz] = field[initx][j+1][initz] - (1 * deriv);
    for(i=initx + 1; i<topx; i++){
      deriv = interp(grad[i-1][j][initz][0], grad[i][j][initz][0], 0.5);
      field[i][j][initz] = field[i-1][j][initz] + (1 * deriv);
    }
    for(i=initx - 1; i>=0; i--){
      deriv = interp(grad[i][j][initz][0], grad[i+1][j][initz][0], 0.5);
      field[i][j][initz] = field[i+1][j][initz] - (1 * deriv);
    }
  }
  /*For every Z*/
  for(k=initz + 1; k<topz; k++){
    deriv = interp(grad[initx][inity][k-1][2], grad[initx][inity][k][2], 0.5);
    field[initx][inity][k] = field[initx][inity][k-1] + (1 * deriv);
    /*X-wise for Y constant*/
    for(i=initx + 1; i<topx; i++){
      deriv = interp(grad[i-1][inity][k][0], grad[i][inity][k][0], 0.5);
      field[i][inity][k] = field[i-1][inity][k] + (1 * deriv);
    }
    for(i=initx - 1; i>=0; i--){
      deriv = interp(grad[i][inity][k][0], grad[i+1][inity][k][0], 0.5);
      field[i][inity][k] = field[i+1][inity][k] - (1 * deriv);
    }
    /*Y-wise*/
    for(j=inity + 1; j<topy; j++){
      deriv = interp(grad[initx][j-1][k][1], grad[initx][j][k][1], 0.5);
      field[initx][j][k] = field[initx][j-1][k] + (1 * deriv);
      /*X-wise*/
      for(i=initx + 1; i<topx; i++){
        deriv = interp(grad[i-1][j][k][0], grad[i][j][k][0], 0.5);
        field[i][j][k] = field[i-1][j][k] + (1 * deriv);
      }
      for(i=initx - 1; i>=0; i--){
        deriv = interp(grad[i][j][k][0], grad[i+1][j][k][0], 0.5);
        field[i][j][k] = field[i+1][j][k] - (1 * deriv);
      }
    }
    for(j=inity - 1; j>=0; j--){
      deriv = interp(grad[initx][j][k][1], grad[initx][j+1][k][1], 0.5);
      field[initx][j][k] = field[initx][j+1][k] - (1 * deriv);
      for(i=initx + 1; i<topx; i++){
        deriv = interp(grad[i-1][j][k][0], grad[i][j][k][0], 0.5);
        field[i][j][k] = field[i-1][j][k] + (1 * deriv);
      }
      for(i=initx - 1; i>=0; i--){
        deriv = interp(grad[i][j][k][0], grad[i+1][j][k][0], 0.5);
        field[i][j][k] = field[i+1][j][k] - (1 * deriv);
      }
    }
  }
  for(k=initz - 1; k>=0; k--){
    deriv = interp(grad[initx][inity][k][2], grad[initx][inity][k+1][2], 0.5);
    field[initx][inity][k] = field[initx][inity][k+1] - (1 * deriv);
    /*X-wise for Y constant*/
    for(i=initx + 1; i<topx; i++){
      deriv = interp(grad[i-1][inity][k][0], grad[i][inity][k][0], 0.5);
      field[i][inity][k] = field[i-1][inity][k] + (1 * deriv);
    }
    for(i=initx - 1; i>=0; i--){
      deriv = interp(grad[i][inity][k][0], grad[i+1][inity][k][0], 0.5);
      field[i][inity][k] = field[i+1][inity][k] - (1 * deriv);
    }
    /*Y-wise*/
    for(j=inity + 1; j<topy; j++){
      deriv = interp(grad[initx][j-1][k][1], grad[initx][j][k][1], 0.5);
      field[initx][j][k] = field[initx][j-1][k] + (1 * deriv);
      /*X-wise*/
      for(i=initx + 1; i<topx; i++){
        deriv = interp(grad[i-1][j][k][0], grad[i][j][k][0], 0.5);
        field[i][j][k] = field[i-1][j][k] + (1 * deriv);
      }
      for(i=initx - 1; i>=0; i--){
        deriv = interp(grad[i][j][k][0], grad[i+1][j][k][0], 0.5);
        field[i][j][k] = field[i+1][j][k] - (1 * deriv);
      }
    }
    for(j=inity - 1; j>=0; j--){
      deriv = interp(grad[initx][j][k][1], grad[initx][j+1][k][1], 0.5);
      field[initx][j][k] = field[initx][j+1][k] - (1 * deriv);
      for(i=initx + 1; i<topx; i++){
        deriv = interp(grad[i-1][j][k][0], grad[i][j][k][0], 0.5);
        field[i][j][k] = field[i-1][j][k] + (1 * deriv);
      }
      for(i=initx - 1; i>=0; i--){
        deriv = interp(grad[i][j][k][0], grad[i+1][j][k][0], 0.5);
        field[i][j][k] = field[i+1][j][k] - (1 * deriv);
      }
    }
  }
}

/**
 * Calculate the divergence of a vector field to get a scalar field.
 * This algorithm runs in O(n), where n = topx * topy * topz.
 * \param field The original vector field (field[i][j][k][0/1/2]).
 * \param grad An already-defined matrix to put the div (scalar) field in.
 * \param topx, topy, topz The upper bounds of the matrices field and div.
*/
void div(double**** field, double*** div, int topx, int topy, int topz){
  if(!(topx >= 2 && topy >= 2 && topz >= 2)){
    printf("Error: div() does not work on a field that has a dimension only one wide!\n");
    return;
  }
  int i, j, k;
  double sum;
  for(i=0; i<topx; i++){
    for(j=0; j<topy; j++){
      for(k=0; k<topz; k++){
        if(i == 0){
          sum = field[i+1][j][k][0] - field[i][j][k][0];
        }else if(i == topx-1){
          sum = field[i][j][k][0] - field[i-1][j][k][0];
        }else{
          sum = (field[i+1][j][k][0] - field[i-1][j][k][0]) * 0.5;
        }
        if(j == 0){
          sum += field[i][j+1][k][1] - field[i][j][k][1];
        }else if(j == topy-1){
          sum += field[i][j][k][1] - field[i][j-1][k][1];
        }else{
          sum += (field[i][j+1][k][1] - field[i][j-1][k][1]) * 0.5;
        }
        if(k == 0){
          sum += field[i][j][k+1][2] - field[i][j][k][2];
        }else if(k == topz-1){
          sum += field[i][j][k][2] - field[i][j][k-1][2];
        }else{
          sum += (field[i][j][k+1][2] - field[i][j][k-1][2]) * 0.5;
        }
        div[i][j][k] = sum;
      }
    }
  }
}

void div2(double**** field, double*** div, int topx, int topy, int topz){
  if(!(topx >= 2 && topy >= 2 && topz >= 2)){
    printf("Error: div2() does not work on a field that has a dimension only one wide!\n");
    return;
  }
  int i, j, k;
  double sum;
  for(i=0; i<topx; i++){
    for(j=0; j<topy; j++){
      for(k=0; k<topz; k++){
        if(i == topx-1){
          sum = field[i][j][k][0] - field[i-1][j][k][0];
        }else{
          sum = field[i+1][j][k][0] - field[i][j][k][0];
        }
        if(j == topy-1){
          sum += field[i][j][k][1] - field[i][j-1][k][1];
        }else{
          sum += field[i][j+1][k][1] - field[i][j][k][1];
        }
        if(k == topz-1){
          sum += field[i][j][k][2] - field[i][j][k-1][2];
        }else{
          sum += field[i][j][k+1][2] - field[i][j][k][2];
        }
        div[i][j][k] = sum;
      }
    }
  }
}

/**
 * Calculate the curl of a vector field to get a vector field.
 * This algorithm runs in O(n), where n = topx * topy * topz.
 * \param field The original vector field (field[i][j][k][0/1/2]).
 * \param grad An already-defined matrix to put the curl (vector) field in.
 * \param topx, topy, topz The upper bounds of the matrices field and curl.
*/
void curl(double**** field, double**** curl, int topx, int topy, int topz){
  if(!(topx >= 2 && topy >= 2 && topz >= 2)){
    printf("Error: curl() does not work on a field that has a dimension only one wide!\n");
    return;
  }
  double dFxdy, dFydx, dFxdz, dFzdx, dFydz, dFzdy;
  int i, j, k;
  for(i=0; i<topx; i++){
    for(j=0; j<topy; j++){
      for(k=0; k<topz; k++){
        if(i == 0){
          dFydx = field[i+1][j][k][1] - field[i][j][k][1];
          dFzdx = field[i+1][j][k][2] - field[i][j][k][2];
        }else if(i == topx-1){
          dFydx = field[i][j][k][1] - field[i-1][j][k][1];
          dFzdx = field[i][j][k][2] - field[i-1][j][k][2];
        }else{
          dFydx = (field[i+1][j][k][1] - field[i-1][j][k][1]) * 0.5;
          dFzdx = (field[i+1][j][k][2] - field[i-1][j][k][2]) * 0.5;
        }
        if(j == 0){
          dFxdy = field[i][j+1][k][0] - field[i][j][k][0];
          dFzdy = field[i][j+1][k][2] - field[i][j][k][2];
        }else if(j == topy-1){
          dFxdy = field[i][j][k][0] - field[i][j-1][k][0];
          dFzdy = field[i][j][k][2] - field[i][j-1][k][2];
        }else{
          dFxdy = (field[i][j+1][k][0] - field[i][j-1][k][0]) * 0.5;
          dFzdy = (field[i][j+1][k][2] - field[i][j-1][k][2]) * 0.5;
        }
        if(k == 0){
          dFxdz = field[i][j][k+1][0] - field[i][j][k][0];
          dFydz = field[i][j][k+1][1] - field[i][j][k][1];
        }else if(k == topz-1){
          dFxdz = field[i][j][k][0] - field[i][j][k-1][0];
          dFydz = field[i][j][k][1] - field[i][j][k-1][1];
        }else{
          dFxdz = (field[i][j][k+1][0] - field[i][j][k-1][0]) * 0.5;
          dFydz = (field[i][j][k+1][1] - field[i][j][k-1][1]) * 0.5;
        }
        curl[i][j][k][0] = dFzdy - dFydz;
        curl[i][j][k][1] = dFxdz - dFzdx;
        curl[i][j][k][2] = dFydx - dFxdy;
      }
    }
  }
}

void curl2(double**** field, double**** curl, int topx, int topy, int topz){
  if(!(topx >= 2 && topy >= 2 && topz >= 2)){
    printf("Error: curl2() does not work on a field that has a dimension only one wide!\n");
    return;
  }
  double dFxdy, dFydx, dFxdz, dFzdx, dFydz, dFzdy;
  int i, j, k;
  for(i=0; i<topx; i++){
    for(j=0; j<topy; j++){
      for(k=0; k<topz; k++){
        if(i == topx-1){
          dFydx = field[i][j][k][1] - field[i-1][j][k][1];
          dFzdx = field[i][j][k][2] - field[i-1][j][k][2];
        }else{
          dFydx = field[i+1][j][k][1] - field[i][j][k][1];
          dFzdx = field[i+1][j][k][2] - field[i][j][k][2];
        }
        if(j == topy-1){
          dFxdy = field[i][j][k][0] - field[i][j-1][k][0];
          dFzdy = field[i][j][k][2] - field[i][j-1][k][2];
        }else{
          dFxdy = field[i][j+1][k][0] - field[i][j][k][0];
          dFzdy = field[i][j+1][k][2] - field[i][j][k][2];
        }
        if(k == topz-1){
          dFxdz = field[i][j][k][0] - field[i][j][k-1][0];
          dFydz = field[i][j][k][1] - field[i][j][k-1][1];
        }else{
          dFxdz = field[i][j][k+1][0] - field[i][j][k][0];
          dFydz = field[i][j][k+1][1] - field[i][j][k][1];
        }
        curl[i][j][k][0] = dFzdy - dFydz;
        curl[i][j][k][1] = dFxdz - dFzdx;
        curl[i][j][k][2] = dFydx - dFxdy;
      }
    }
  }
}


/**
 * uncurl() involves picking a lot of values that don't matter. For real use
 * there's no reason these can't all be zero or one, but for testing they should
 * be random numbers (to ensure there's no bugs that are slipping through
 * because of choice of number).
*/
double uncurlpick(){
  return -17.3824;
}


/**
 * Given a vector field B, calculate the vector field A such that curl(A) = B.
 * This algorithm runs in O(n) where n=topx*topy*topz, rather than the common
 * one which runs in O(n^2). Also, this algorithm involves calculating no square
 * roots.
 * TODO: This algorithm only calculates up to (xmax-1,ymax-1,zmax-1).
 * 
 * \param B A vector field that already holds the given field. B[topx][topy][topz][3].
 * \param A A vector field to hold the resultant field; must be malloc'd to
 * A[topx][topy][topz][3]
 * \param d A tensor field to hold the calculated partial derivatives of A.
 * This must be already malloc'd to p[topx][topy][topz][3][3].
 * \param topx, topy, topz The upper bounds of the arrays.
 * \param deltax, deltay, deltaz The change in x, y, and z that one unit in the
 * array represents, so that e.g. x_100 = x_000 + (dAxdx_000 * deltax).
*/
void uncurl(double**** B, double**** A, double***** d, int topx, int topy, int topz, double deltax, double deltay, double deltaz){
  int x = 0, y = 0, z = 0;
  double xyring;
  double recipdeltax = 1.0 / deltax;
  double recipdeltay = 1.0 / deltay;
  double recipdeltaz = 1.0 / deltaz;
  A[0][0][0][0] = uncurlpick();
  A[0][0][0][1] = uncurlpick();
  A[0][0][0][2] = uncurlpick();
  for(x=0;x<topx-1;x++){
    for(y=0;y<topy-1;y++){
      for(z=0;z<topz-1;z++){
        //printf("Starting (%d,%d,%d)- ", x, y, z); fflush(stdout);
        //To make sure we have x,y,z here
        if(x>0 && y==0 && z==0){
          d[x-1][y  ][z  ][0][0] = uncurlpick();
          A[x  ][y  ][z  ][0] = A[x-1][y  ][z  ][0] + (d[x-1][y  ][z  ][0][0] * deltax);
        }
        if(x==0 && y>0 && z==0){
          d[x  ][y-1][z  ][1][1] = uncurlpick();
          A[x  ][y  ][z  ][1] = A[x  ][y-1][z  ][1] + (d[x  ][y-1][z  ][1][1] * deltay);
        }
        if(x==0 && y==0 && z>0){
          d[x  ][y  ][z-1][2][2] = uncurlpick();
          A[x  ][y  ][z  ][2] = A[x  ][y  ][z-1][2] + (d[x  ][y  ][z-1][2][2] * deltaz);
        }
        //By curl and partial
        if(x==0){
          d[x  ][y  ][z  ][2][1] = uncurlpick();
          d[x  ][y  ][z  ][1][2] = d[x][y][z][2][1] - B[x][y][z][0];
          A[x  ][y+1][z  ][2]    = A[x][y][z][2]    + d[x][y][z][2][1];
          A[x  ][y  ][z+1][1]    = A[x][y][z][1]    + d[x][y][z][1][2];
        }
        if(y==0){
          d[x  ][y  ][z  ][0][2] = uncurlpick();
          d[x  ][y  ][z  ][2][0] = d[x][y][z][0][2] - B[x][y][z][1];
          A[x  ][y  ][z+1][0]    = A[x][y][z][0]    + d[x][y][z][0][2];
          A[x+1][y  ][z  ][2]    = A[x][y][z][2]    + d[x][y][z][2][0];
        }
        if(z==0){
          d[x  ][y  ][z  ][1][0] = uncurlpick();
          d[x  ][y  ][z  ][0][1] = d[x][y][z][1][0] - B[x][y][z][2];
          A[x+1][y  ][z  ][1]    = A[x][y][z][1]    + d[x][y][z][1][0];
          A[x  ][y+1][z  ][0]    = A[x][y][z][0]    + d[x][y][z][0][1];
        }
        //More non-curl partials (xx, yy, zz)
        if(x>0){
          if(y==0){
            d[x-1][y  ][z+1][0][0] = (A[x  ][y  ][z+1][0] - A[x-1][y  ][z+1][0]) * recipdeltax;
          }
          if(z==0){
            d[x-1][y+1][z  ][0][0] = (A[x  ][y+1][z  ][0] - A[x-1][y+1][z  ][0]) * recipdeltax;
          }
        }
        if(y>0){
          if(z==0){
            d[x+1][y-1][z  ][1][1] = (A[x+1][y  ][z  ][1] - A[x+1][y-1][z  ][1]) * recipdeltay;
          }
          if(x==0){
            d[x  ][y-1][z+1][1][1] = (A[x  ][y  ][z+1][1] - A[x  ][y-1][z+1][1]) * recipdeltay;
          }
        }
        if(z>0){
          if(x==0){
            d[x  ][y+1][z-1][2][2] = (A[x  ][y+1][z  ][2] - A[x  ][y+1][z-1][2]) * recipdeltaz;
          }
          if(y==0){
            d[x+1][y  ][z-1][2][2] = (A[x+1][y  ][z  ][2] - A[x+1][y  ][z-1][2]) * recipdeltaz;
          }
        }
        //The Loop (a loop of functions, not a programming loop)
        d[x  ][y  ][z+1][0][1] = uncurlpick();
        A[x  ][y+1][z+1][0]    =  A[x  ][y  ][z+1][0]    + (d[x  ][y  ][z+1][0][1] * deltay);
        d[x  ][y+1][z  ][0][2] = (A[x  ][y+1][z+1][0]    -  A[x  ][y+1][z  ][0]) * recipdeltaz;
        d[x  ][y+1][z  ][2][0] =  d[x  ][y+1][z  ][0][2] -  B[x  ][y+1][z  ][1];
        A[x+1][y+1][z  ][2]    =  A[x  ][y+1][z  ][2]    + (d[x  ][y+1][z  ][2][0] * deltax);
        d[x+1][y  ][z  ][2][1] = (A[x+1][y+1][z  ][2]    -  A[x+1][y  ][z  ][2]) * recipdeltay;
        d[x+1][y  ][z  ][1][2] =  d[x+1][y  ][z  ][2][1] -  B[x+1][y  ][z  ][0];
        A[x+1][y  ][z+1][1]    =  A[x+1][y  ][z  ][1]    + (d[x+1][y  ][z  ][1][2] * deltaz);
        d[x  ][y  ][z+1][1][0] = (A[x+1][y  ][z+1][1]    -  A[x  ][y  ][z+1][1]) * recipdeltax;

        xyring                 =  d[x  ][y  ][z+1][1][0] -  B[x  ][y  ][z+1][2];
        //This should be zero (or 10e-14 or something)
        //printf("(%d,%d,%d): diff = %f\n", x,y,z, xyring  -  d[x  ][y  ][z+1][0][1]); fflush(stdout);
        //Final non-curl partials
        if(x>0){
          d[x-1][y+1][z+1][0][0] = (A[x  ][y+1][z+1][0] - A[x-1][y+1][z+1][0]) * recipdeltax;
        }
        if(y>0){
          d[x+1][y-1][z+1][1][1] = (A[x+1][y  ][z+1][1] - A[x+1][y-1][z+1][1]) * recipdeltay;
        }
        if(z>0){
          d[x+1][y+1][z-1][2][2] = (A[x+1][y+1][z  ][2] - A[x+1][y+1][z-1][2]) * recipdeltaz;
        }
      }
    }
  }
}



#endif

      !
      ! Tree.inc
      !

      !Pointers
      integer          tree_sibling(MAX_NUM_TREES) !Points to the next tree
      integer          tree_level(MAX_NUM_TREES) !Points to the first level

      !Arrays
      double precision tree_time(MAX_NUM_TREES) !Time for a tree
      integer          tree_on_reg(MAX_NUM_TREES) !Which register the tree is on
      integer          tree_freenums(MAX_NUM_TREES/2) !store available numbers
      integer          tree_numavail
 
      !Counter
      integer          tree_counter   !Tracks number of trees
 
      COMMON           / mem_trees / 
     *                 tree_sibling,
     *                 tree_level,
     *                 tree_time,
     *                 tree_on_reg,
     *                 tree_counter,
     *                 tree_freenums,
     *                 tree_numavail

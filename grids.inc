      !
      ! Grids.inc
      ! 


      !Pointers: 
      integer       grid_field(MAX_NUM_GRIDS)
      integer       grid_coords(MAX_NUM_GRIDS)
      integer       grid_mask(MAX_NUM_GRIDS)
      integer       grid_sibling(MAX_NUM_GRIDS)

      COMMON        / mem_grids / 
     *              grid_field,
     *              grid_coords,
     *              grid_sibling,
     *              grid_mask

      !Arrays:
      integer       grid_owner(MAX_NUM_GRIDS)
      integer       grid_on_level(MAX_NUM_GRIDS)
      integer       grid_rank(MAX_NUM_GRIDS)
      integer       grid_shape(MAX_NUM_GRIDS,3)
      real*8        grid_spacings(MAX_NUM_GRIDS,3)
      real*8        grid_min(MAX_NUM_GRIDS,3)
      real*8        grid_max(MAX_NUM_GRIDS,3)

      !This will increment everytime a new grid is created
      integer       grid_counter      
    
      COMMON        / mem_grids / 
     *              grid_min,
     *              grid_max,
     *              grid_spacings,
     *              grid_shape,
     *              grid_owner,
     *              grid_rank,
     *              grid_on_level,
     *              grid_counter

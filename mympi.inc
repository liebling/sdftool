      !
      ! Common block for myid
      !

      integer      myid
      integer      numprocs

      COMMON     / mem_myid /
     *             myid,
     *             numprocs


      !
      ! MPI variables:
      !
      integer     status(MPI_STATUS_SIZE), ierr, tag
      parameter ( tag = 50 )

      !
      ! Master processor ID (always 0) for MPI:
      !
      integer       MASTER
      parameter   ( MASTER         =  0    )


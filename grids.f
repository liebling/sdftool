      !
      ! Grids.f
      !

      subroutine grid_init()
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer i
         grid_counter = 1
        
         do i = 1, MAX_NUM_GRIDS 
            grid_field(i)   = NULL_GRID
            grid_coords(i)  = NULL_GRID
            grid_sibling(i) = NULL_GRID
            grid_owner(i)   = NULL_GRID
         enddo

         return
      end

      subroutine grid_create(grid,rank)
         integer grid
         integer rank
      write(*,*) 'do not use this routine'
      stop
      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_locmax:                                                            cc
cc                  g2 = g2 - g1                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_locmax( g1, g2)
      implicit none
      integer g1, g2
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  i,  j,  k,    b(3), e(3), o(3)
      integer  ii, jj, kk
      integer  index1, index2

         logical     ltrace
         parameter ( ltrace  = .false. )


      if (ltrace) then
         write(*,9) myid, 'grid_locmax: g1 = ',g1
         write(*,9) myid, 'grid_locmax: g2 = ',g2
  8      format('[',I3,'] ',A,F18.11)
  9      format('[',I3,'] ',A,6I7)
      end if


      do i = 1, grid_rank(g2)
       !
       ! Beginning of intersection:
       !
       b(i)=1+NINT( (grid_min(g1,i)-grid_min(g2,i))/grid_spacings(g2,i))
       if (b(i) .lt. 1               ) b(i) = 1
       if (b(i) .gt. grid_shape(g2,i)) return
       !
       ! End of intersection:
       !
       e(i)=  NINT( (grid_max(g1,i)-grid_min(g2,i))/grid_spacings(g2,i))
       if (e(i) .lt. 0               ) return
       if (e(i) .gt. grid_shape(g2,i)) e(i) = grid_shape(g2,i)
       !
       ! Offset between grids:
       !
       o(i)=  NINT( (grid_min(g2,i)-grid_min(g1,i))/grid_spacings(g2,i))
       !
       if (ltrace) then
           write(*,9) myid,'grid_locmax: i,b,e,o, n1, n2 = ',
     *         i,b(i),e(i),o(i), grid_shape(g1,i), grid_shape(g2,i)
       end if
      end do

       !
       ! Carryout locmaxion:
       !
      if (grid_rank(g2).eq.3) then
         do k = 1, grid_shape(g2,3)
            kk = k + o(3)
            do j = 1, grid_shape(g2,2)
               jj = j + o(2)
               do i = 1, grid_shape(g2,1)
                  ii = i + o(1)
                  index2 = (k -1)*grid_shape(g2,2)*grid_shape(g2,1)
     *                    +(j -1)*grid_shape(g2,1)
     *                    +(i -1)
                  index1 = (kk-1)*grid_shape(g1,2)*grid_shape(g1,1)
     *                    +(jj-1)*grid_shape(g1,1)
     *                    +(ii-1)
                  if (ii.lt.1.or.jj.lt.1.or.kk.lt.1.or.
     *                ii.gt.grid_shape(g1,1).or.
     *                jj.gt.grid_shape(g1,2).or.
     *                kk.gt.grid_shape(g1,3)        ) then
                     q(grid_field(g2)+index2) = 0.d0
                  else
!                    q(grid_field(g2)+index2) = q(grid_field(g2)+index2)
!    *                                         -q(grid_field(g1)+index1)
                     q(grid_field(g2)+index2) =max(
     *                                         q(grid_field(g2)+index2),
     *                                         q(grid_field(g1)+index1))
                  end if
         end do
         end do
         end do
      else if (grid_rank(g2).eq.2) then
            do j = 1, grid_shape(g2,2)
               jj = j + o(2)
               do i = 1, grid_shape(g2,1)
                  ii = i + o(1)
                  index2 = 
     *                    +(j -1)*grid_shape(g2,1)
     *                    +(i -1)
                  index1 =
     *                    +(jj-1)*grid_shape(g1,1)
     *                    +(ii-1)
                  if (ii.lt.1.or.jj.lt.1.or.
     *                ii.gt.grid_shape(g1,1).or.
     *                jj.gt.grid_shape(g1,2)   ) then
                     q(grid_field(g2)+index2) = 0.d0
                  else
                     q(grid_field(g2)+index2) =max(
     *                                         q(grid_field(g2)+index2),
     *                                         q(grid_field(g1)+index1))
                  end if
         end do
         end do
      else if (grid_rank(g2).eq.1) then
               do i = 1, grid_shape(g2,1)
                  ii = i + o(1)
                  index2 = 
     *                    +(i -1)
                  index1 =
     *                    +(ii-1)
                  if (ii.lt.1.or.
     *                ii.gt.grid_shape(g1,1)  ) then
                     q(grid_field(g2)+index2) = 0.d0
                  else
                     q(grid_field(g2)+index2) =max(
     *                                         q(grid_field(g2)+index2),
     *                                         q(grid_field(g1)+index1))
                  end if
         end do
      else 
         write(*,9) myid, 'grid_locmax: Unknown rank: ',grid_rank(g2)
      end if

      if (ltrace) then
         write(*,9) myid, 'grid_locmax: finished'
      end if

      return
      end           ! END: grid_locmax

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_subtract:                                                          cc
cc                  g2 = g2 - g1                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_subtract( g1, g2)
      implicit none
      integer g1, g2
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  i,  j,  k,    b(3), e(3), o(3)
      integer  ii, jj, kk
      integer  index1, index2

         logical     ltrace
         parameter ( ltrace  = .false. )


      if (ltrace) then
         write(*,9) myid, 'grid_subtract: g1 = ',g1
         write(*,9) myid, 'grid_subtract: g2 = ',g2
  8      format('[',I3,'] ',A,F18.11)
  9      format('[',I3,'] ',A,6I7)
      end if


      do i = 1, grid_rank(g2)
       !
       ! Beginning of intersection:
       !
       b(i)=1+NINT( (grid_min(g1,i)-grid_min(g2,i))/grid_spacings(g2,i))
       if (b(i) .lt. 1               ) b(i) = 1
       if (b(i) .gt. grid_shape(g2,i)) return
       !
       ! End of intersection:
       !
       e(i)=  NINT( (grid_max(g1,i)-grid_min(g2,i))/grid_spacings(g2,i))
       if (e(i) .lt. 0               ) return
       if (e(i) .gt. grid_shape(g2,i)) e(i) = grid_shape(g2,i)
       !
       ! Offset between grids:
       !
       o(i)=  NINT( (grid_min(g2,i)-grid_min(g1,i))/grid_spacings(g2,i))
       !
       if (ltrace) then
           write(*,9) myid,'grid_subtract: i,b,e,o, n1, n2 = ',
     *         i,b(i),e(i),o(i), grid_shape(g1,i), grid_shape(g2,i)
       end if
      end do

       !
       ! Carryout subtraction:
       !
      if (grid_rank(g2).eq.3) then
         do k = 1, grid_shape(g2,3)
            kk = k + o(3)
            do j = 1, grid_shape(g2,2)
               jj = j + o(2)
               do i = 1, grid_shape(g2,1)
                  ii = i + o(1)
                  index2 = (k -1)*grid_shape(g2,2)*grid_shape(g2,1)
     *                    +(j -1)*grid_shape(g2,1)
     *                    +(i -1)
                  index1 = (kk-1)*grid_shape(g1,2)*grid_shape(g1,1)
     *                    +(jj-1)*grid_shape(g1,1)
     *                    +(ii-1)
                  if (ii.lt.1.or.jj.lt.1.or.kk.lt.1.or.
     *                ii.gt.grid_shape(g1,1).or.
     *                jj.gt.grid_shape(g1,2).or.
     *                kk.gt.grid_shape(g1,3)        ) then
                     q(grid_field(g2)+index2) = 0.d0
                  else
                     q(grid_field(g2)+index2) = q(grid_field(g2)+index2)
     *                                         -q(grid_field(g1)+index1)
                  end if
         end do
         end do
         end do
      else if (grid_rank(g2).eq.2) then
            do j = 1, grid_shape(g2,2)
               jj = j + o(2)
               do i = 1, grid_shape(g2,1)
                  ii = i + o(1)
                  index2 = 
     *                    +(j -1)*grid_shape(g2,1)
     *                    +(i -1)
                  index1 =
     *                    +(jj-1)*grid_shape(g1,1)
     *                    +(ii-1)
                  if (ii.lt.1.or.jj.lt.1.or.
     *                ii.gt.grid_shape(g1,1).or.
     *                jj.gt.grid_shape(g1,2)   ) then
                     q(grid_field(g2)+index2) = 0.d0
                  else
                     q(grid_field(g2)+index2) = q(grid_field(g2)+index2)
     *                                         -q(grid_field(g1)+index1)
                  end if
         end do
         end do
      else if (grid_rank(g2).eq.1) then
               do i = 1, grid_shape(g2,1)
                  ii = i + o(1)
                  index2 = 
     *                    +(i -1)
                  index1 =
     *                    +(ii-1)
                  if (ii.lt.1.or.
     *                ii.gt.grid_shape(g1,1)  ) then
                     q(grid_field(g2)+index2) = 0.d0
                  else
                     q(grid_field(g2)+index2) = q(grid_field(g2)+index2)
     *                                         -q(grid_field(g1)+index1)
                  end if
         end do
      else 
         write(*,9) myid, 'grid_subtract: Unknown rank: ',grid_rank(g2)
      end if

      if (ltrace) then
         write(*,9) myid, 'grid_subtract: finished'
      end if

      return
      end           ! END: grid_subtract

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_compatible:                                                        cc
cc                  Returns true if grids have same shape, and bbox.          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function grid_compatible(g1, g2)
      implicit none
      integer  g1, g2
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'mem.inc'
      integer  rank1, rank2, i
      logical   double_equal
      external  double_equal

         logical     ltrace
         parameter ( ltrace  = .true. )

      grid_compatible = .false.

      rank1 = grid_rank(g1)
      rank2 = grid_rank(g2)
      if (rank1.ne.rank2) then
        if(ltrace)write(*,*)'grid_compatible:Ranks notequal',rank1,rank2
        return
      end if

      do i = 1, rank1
         !
         ! Strict:
         !
c        if (grid_shape(g1,i).ne.grid_shape(g2,i)) return
c        if (.not.double_equal(grid_min(g1,i),grid_min(g2,i))) return
c        if (.not.double_equal(grid_max(g1,i),grid_max(g2,i))) return
         !
         ! Not so Strict:
         !
         if (.not.double_equal(grid_spacings(g1,i),grid_spacings(g2,i)))
     *       then
            if(ltrace)write(*,*)'grid_compatible:Unequal spacings:',
     *                i,grid_spacings(g1,i),grid_spacings(g2,i)
            return
         end if
         if (.not.double_equal(grid_min(g1,i),grid_min(g2,i))) then
            if(ltrace)write(*,*)'grid_compatible:Unequal mins:',
     *                i,grid_min(g1,i),grid_min(g2,i)
            return
         end if
         if (.not.double_equal(grid_max(g1,i),grid_max(g2,i))) then
            if(ltrace)write(*,*)'grid_compatible:Unequal maxs:',
     *                i,grid_max(g1,i),grid_max(g2,i)
            return
         end if
!        if (.not.double_equal(grid_min(g1,i),grid_min(g2,i)) .and.
!    *       .not.double_equal(grid_max(g1,i),grid_max(g2,i))) return
      end do

      grid_compatible = .true.

      return
      end      ! END: grid_compatible


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_coarsen:                                                           cc
cc                  Select every nth point from the grid in place.            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_coarsen(gi,n)
      implicit none
      integer gi,n
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  i,  j,  k,    index
      integer  ii, jj, kk, newindex
      integer  rank, new_shape(3)

         logical     ltrace
         parameter ( ltrace  = .false. )


      if (ltrace) then
         write(*,9) myid, 'grid_coarsen: gi = ',gi
         write(*,9) myid, 'grid_coarsen: n  = ',n
         call grid_dump(gi)
  8      format('[',I3,'] ',A,F18.11)
  9      format('[',I3,'] ',A,3I10)
      end if

      rank = grid_return_rank(gi)

      if (n.le.0) then
          write(*,*) 'Cannot coarsen with this value of n: ',n
          stop
      end if

      !
      ! Determine new shape:
      !
      do i = 1, rank
         new_shape(i) = (grid_shape(gi,i) - 1 )/ n + 1
         if (ltrace) write(*,9) myid, 'grid_coarsen: i,old,new: ',i,
     *                         grid_shape(gi,i),new_shape(i)
      end do

      !
      ! Coarsen the field using the mask as storage:
      !
      if (rank .eq. 3) then
         do k = 1, new_shape(3)
            kk = n * (k-1) + 1
            do j = 1, new_shape(2)
               jj = n * (j-1) + 1
               do i = 1, new_shape(1)
                  ii = n * (i-1) + 1
                  newindex =   ( k-1)* new_shape(2)   *new_shape(1)
     *                       + ( j-1)* new_shape(1)
     *                       + ( i-1)
                  index    =   (kk-1)*grid_shape(gi,2)*grid_shape(gi,1)
     *                       + (jj-1)*grid_shape(gi,1)
     *                       + (ii-1)
                  q(grid_mask(gi)  +newindex) = q(grid_field(gi) +index)
                  !q(grid_coords(gi)+newindex) = q(grid_coords(gi)+index)
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, new_shape(2)
               jj = n * (j-1) + 1
               do i = 1, new_shape(1)
                  ii = n * (i-1) + 1
                  newindex =   
     *                       + ( j-1)* new_shape(1)
     *                       + ( i-1)
                  index    =  
     *                       + (jj-1)*grid_shape(gi,1)
     *                       + (ii-1)
                  q(grid_mask(gi)+newindex) = q(grid_field(gi)+index)
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, new_shape(1)
                  ii = n * (i-1) + 1
                  newindex =   
     *                       + ( i-1)
                  index    =  
     *                       + (ii-1)
                  q(grid_mask(gi)+newindex) = q(grid_field(gi)+index)
               end do
      else 
         write(*,9) myid, 'grid_coarsen: Unknnown rank: ',rank
      end if

      !
      ! Store new shape:
      !
      do i = 1, rank
         grid_shape(gi,i)    = new_shape(i)
         grid_spacings(gi,i) = grid_spacings(gi,i) * n
         grid_max(gi,i)      =  grid_min(gi,i)
     *                        + grid_spacings(gi,i)*(grid_shape(gi,i)-1)
      end do
   
      !
      ! Do we need to store new coords?
      !

      !
      ! Switch mask and field pointers:
      !
      index          = grid_mask(gi)
      grid_mask(gi)  = grid_field(gi)
      grid_field(gi) = index


      if (ltrace) then
         call grid_dump(gi)
         do i = 1, rank
            write(*,8) myid, 'grid_coarsen: Max: ',grid_max(gi,i)
         end do 
         write(*,9) myid, 'grid_coarsen: finished'
      end if

      return
      end           ! END: grid_coarsen

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_maskgrid:                                                          cc
cc                  Maskoff a region on grid gi where grid gj exists.         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_maskgrid(gi,gj,masktype)
      implicit none
      integer gi, gj, masktype
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  i, j,k, index,  numpoints, begin(3), end(3)

         logical     ltrace
         parameter ( ltrace   = .false. )
         logical     ltrace2
         parameter ( ltrace2  = .false. )

      if (ltrace) then
         write(*,9)myid,'grid_maskgrid: gi/j: ',gi,gj
         write(*,9)myid,'grid_maskgrid: makstype: ',masktype
         write(*,9)myid,'grid_maskgrid: shape:',grid_shape(gi,1),
     *      grid_shape(gi,2), grid_shape(gi,3)
         write(*,7)myid,'grid_maskgrid: h:',grid_spacings(gi,1)
      end if

      do i = 1, grid_rank(gi)
         if (ltrace) then
         write(*,8)myid,'         mins:',i,grid_min(gi,i),grid_min(gj,i)
         write(*,8)myid,'         maxs:',i,grid_max(gi,i),grid_max(gj,i)
         end if
         !
         begin(i) = 1+ NINT( ( grid_min(gj,i) - grid_min(gi,i) ) 
     *                    / grid_spacings(gi,i) )
         !write(*,9)myid,'grid_maskgrid: X: i b:',i,begin(i)
         !write(*,9)myid,'grid_maskgrid: X shape:',grid_shape(gi,i)
         if (begin(i) .lt. 1 ) then
            begin(i) = 1
         else if (begin(i) .gt. grid_shape(gi,i)) then
            if (ltrace)write(*,9)myid,'grid_maskgrid: No intersection'
            return
         end if
         !
         if (masktype .eq. MASK_INTEGRAL ) then
            end(i)   = NINT( ( grid_max(gj,i) - grid_min(gi,i) )
     *                     / grid_spacings(gi,i) )
         else if (masktype .eq. MASK_L2NORM) then
            end(i)   = 1+NINT( ( grid_max(gj,i) - grid_min(gi,i) )
     *                     / grid_spacings(gi,i) )
         else
            write(*,9)myid,'grid_maskgrid: Unknown masktype: ',masktype
         end if
         !write(*,9)myid,'grid_maskgrid: i e:',i,end(i)
         !write(*,9)myid,'grid_maskgrid: i e:',grid_shape(gi,i)
         if (end(i) .le. 1 ) then
            if (ltrace)write(*,9)myid,'grid_maskgrid: No intersection'
            return
         else if (end(i) .gt. grid_shape(gi,i)) then
            end(i) = grid_shape(gi,i)
         end if
         !
      end do

      if (ltrace) then
         do i = 1, grid_rank(gi)
            write(*,9)myid,'grid_maskgrid: i b/e:',i,begin(i),end(i)
         end do
      end if

      if (grid_rank(gi) .eq. 3) then
         do k = begin(3), end(3)
         do j = begin(2), end(2)
         do i = begin(1), end(1)
            index =   (k-1)*grid_shape(gi,1)*grid_shape(gi,2)
     *              + (j-1)*grid_shape(gi,1)
     *              + (i-1)
            q(grid_mask(gi)+index) = GRID_MASKED
         end do
         end do
         end do
      else if (grid_rank(gi) .eq. 2) then
         do j = begin(2), end(2)
         do i = begin(1), end(1)
            index = (j-1)*grid_shape(gi,1) + (i-1)
            q(grid_mask(gi)+index) = GRID_MASKED
         end do
         end do
      else if (grid_rank(gi) .eq. 1) then
         do i = begin(1), end(1)
            q(grid_mask(gi)+i-1) = GRID_MASKED
         end do
      else
         write(*,9) myid, 'grid_maskgrid: Unknown rank'
      end if

  7   format('[',I3,'] ',A,G18.11)
  8   format('[',I3,'] ',A,I5,' ',2G18.11)
  9   format('[',I3,'] ',A,3I10)

      return
      end           ! END: grid_maskgrid

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_unmask:                                                            cc
cc                  Initialize mask to completely unmasked in preparation     cc
cc                  for masking off where finer regions exist.                cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_unmask(gi)
      implicit none
      integer gi
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  i, numpoints

         logical     ltrace
         parameter ( ltrace  = .false. )

      numpoints = 1
      do i = 1, grid_return_rank(gi)
         numpoints = numpoints * grid_shape(gi,i)
      end do

      if (ltrace) then
         write(*,*) 'grid_unmask: Unmasking grid   gi: ',gi
         write(*,*) 'grid_unmask:           numpoints: ',numpoints
         write(*,*) 'grid_unmask:       GRID_UNMASKED: ',GRID_UNMASKED
      end if

      call load_scal1D(q(grid_mask(gi)),GRID_UNMASKED, numpoints)

      return
      end           ! END: grid_unmask

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_free:                                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_free(grid)
      implicit none
      integer grid
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      integer  i, numpoints, owner, rank

         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )


         rank  = grid_rank(grid)
         owner = grid_owner(grid)
         numpoints = 1
         do i = 1, rank
            numpoints = numpoints * grid_shape(grid,i)
         end do

         !
         ! For nonlocal grids, do not need to store data:
         !
         if (owner .eq. myid) then
            call my_free(grid_field(grid),  numpoints)
            call my_free(grid_coords(grid), numpoints)
            call my_free(grid_mask(grid),   numpoints)
         end if

         if(ltrace.or.ltrace2) then
11         FORMAT(A, I4.1, A, A, I12.1)
           write(*,*)  ""
           write(*,11) "[",myid,"]"," grid_free: grid = ", grid
           write(*,*)  ""
         endif
 
         return
      end                ! END: grid_free

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_createB:                                                           cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_createB(grid,rank,shape,owner)
      implicit none
      integer grid, rank, shape(*), owner
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      integer  i, numpoints, numcoords

         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )


         grid_rank(grid)  = rank
         grid_owner(grid) = owner
         numpoints = 1
         numcoords = 0
         do i = 1, rank
            grid_shape(grid,i) = shape(i)
            numpoints = numpoints * grid_shape(grid,i)
            numcoords = numcoords + grid_shape(grid,i)
         end do

         !
         ! For nonlocal grids, do not need to store data:
         !
         if (owner .eq. myid) then
            grid_field(grid)  = my_malloc(numpoints)
            grid_mask(grid)   = my_malloc(numpoints)
            grid_coords(grid) = my_malloc(numcoords)
         end if

         if(ltrace.or.ltrace2) then
11         FORMAT(A, I4.1, A, A, I12.1)
12         FORMAT(A, I4.1, A, A, F6.2)
           write(*,*)  ""
           write(*,11)"[",myid,"]"," grid_createB: grid = ", grid
           do i = 1, rank
           write(*,11)"[",myid,"]"," grid_createB: Ni   = ", shape(i)
           end do
           write(*,11)"[",myid,"]"," grid_createB: numpoints:",numpoints
           !write(*,11)"[",myid,"]"," grid_createB: rank = ",rank
           write(*,11)"[",myid,"]"," grid_createB: rank = ",
     .                     grid_rank(grid)
           write(*,11)"[",myid,"]"," grid_createB: field  ptr =",
     *                     grid_field(grid)
           write(*,11)"[",myid,"]"," grid_createB: coords ptr =",
     *                     grid_coords(grid)
           write(*,*) ""
         endif
 
         return
      end                ! END: grid_createB


      subroutine grid_create_empty(grid)
      implicit none
      include 'class_constants.inc' 
      include 'grids.inc'
         integer grid
 
         integer my_malloc
 
         grid_field(grid) = my_malloc(1)
         return
      end

      integer function grid_get_free_number()
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
      logical     ltrace
      parameter ( ltrace = .false. )

      if (ltrace) then
         write(*,*) 'grid_get_free_number: MAX_NUM_GRIDS=',
     .             MAX_NUM_GRIDS
         write(*,*) 'grid_get_free_number: grid_counter =',
     .             grid_counter
      end if

      if (grid_counter.ge.MAX_NUM_GRIDS) then
         write(*,*) 'grid_get_free_number: No grids left'
         write(*,*) 'grid_get_free_number: Quitting here.'
         stop
      else
         grid_get_free_number = grid_counter
         grid_counter         = grid_counter + 1 
      end if

      if (ltrace) then
         write(*,*) 'grid_get_free_number: Done.'
         write(*,*) 'grid_get_free_number: MAX_NUM_GRIDS=',
     .             MAX_NUM_GRIDS
         write(*,*) 'grid_get_free_number: grid_counter =',
     .             grid_counter
         write(*,*) 'grid_get_free_number: number=',
     .             grid_get_free_number
      end if

      return

      end

      ! Do not use this, the tree stores the rank!
      subroutine grid_set_rank(grid, rank)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
         integer rank
         logical ltrace
         parameter ( ltrace = .false. )

      if(ltrace) write(*,*)'grid_set_rank: grid =',grid
      if(ltrace) write(*,*)'grid_set_rank: rank =',rank
         grid_rank(grid) = rank
      if(ltrace) write(*,*)'grid_set_rank: Done. ',rank

         return
      end

      subroutine grid_set_sibling(grid, sibling)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
         integer sibling

         grid_sibling(grid) = sibling

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_set_bbox:                                                          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_set_bbox(grid, bbox)
      implicit none
      integer grid
      real*8  bbox(*)
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  i, j, numprev
      logical     ltrace
      parameter ( ltrace = .false. )

      !write(*,*) 'grid_set_bbox: rank:',grid_rank(grid)
      numprev = 0
      do i = 1, grid_rank(grid)
         grid_min(grid,i)      = bbox(2*i-1)
         grid_max(grid,i)      = bbox(2*i  )
         grid_spacings(grid,i) =  (grid_max(grid,i)   -grid_min(grid,i))
     *                          / (grid_shape(grid,i) -1               )
         if (ltrace) then
         write(*,*) 'grid_set_bbox: ',
     *           i, bbox(2*i-1), bbox(2*i), grid_spacings(grid,i)
         end if
         ! Some routines need the coords themselves:
         do j = 1, grid_shape(grid,i)
            q(grid_coords(grid)+numprev+(j-1)) = grid_min(grid,i) +
     *                              grid_spacings(grid,i)*(j-1)
         end do
         numprev = numprev + grid_shape(grid,i)
      end do

      if (ltrace ) then
      write(*,*) 'grid_set_bbox:    ',grid
      write(*,*) 'grid_set_bbox: x min/max: ',
     *                     grid_min(grid,1),grid_max(grid,1)
      write(*,*) 'grid_set_bbox: y min/max: ',
     *                     grid_min(grid,2),grid_max(grid,2)
      if (grid_rank(grid).gt.2) then
      write(*,*) 'grid_set_bbox: z min/max: ',
     *                     grid_min(grid,3),grid_max(grid,3)
      end if
      write(*,*) 'grid_set_bbox: x: ',
     *     q(grid_coords(grid)),
     *     q(grid_coords(grid)+grid_shape(grid,1)-1)
      write(*,*) 'grid_set_bbox: y: ',
     *     q(grid_coords(grid)+grid_shape(grid,1)  ),
     *     q(grid_coords(grid)+grid_shape(grid,1)+grid_shape(grid,2)-1)
      if (grid_rank(grid).gt.2) then
      write(*,*) 'grid_set_bbox: z: ',
     *     q(grid_coords(grid)+grid_shape(grid,1)+grid_shape(grid,2)  ),
     *     q(grid_coords(grid)+grid_shape(grid,1)+grid_shape(grid,2) 
     *                                           +grid_shape(grid,3)-1)
      end if
      write(*,*) 'grid_set_bbox: rank:',grid_rank(grid)
      write(*,*) 'grid_set_bbox:  Done.'
      end if
         !grid_min_x(grid) = bbox(1)
         !grid_max_x(grid) = bbox(2)
         !grid_min_y(grid) = bbox(3)
         !grid_max_y(grid) = bbox(4)
         !grid_min_z(grid) = bbox(5)
         !grid_max_z(grid) = bbox(6)

         return
      end         ! END: grid_set_bbox
      
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_bbox:                                                       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_return_bbox(grid, bbox)
      implicit none
      integer grid 
      real*8  bbox(*)
      include 'class_constants.inc'
      include 'grids.inc'
      integer  i

      do i = 1, grid_rank(grid)
         bbox(2*i-1) = grid_min(grid,i)
         bbox(2*i  ) = grid_max(grid,i)
      end do

         return
      end

      subroutine grid_set_shape(grid, shape)
      implicit none
      integer grid
      integer shape(*)
      include 'class_constants.inc'
      include 'grids.inc'
      integer  i, rank
      integer  grid_return_rank
      external grid_return_rank

         rank = grid_return_rank(grid)
         !write(*,*)'grid_set_shape: rank=',rank

         do i = 1, rank
            grid_shape(grid,i) = shape(i)
         end do

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_numpoints:                                                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function  grid_return_numpoints(grid)
      implicit none
      include 'class_constants.inc' 
      include 'grids.inc'
         integer grid
         integer i, rank
      integer  grid_return_rank
      external grid_return_rank
      logical     ltrace
      parameter ( ltrace = .false. )
      
      rank = grid_return_rank(grid)
      
         grid_return_numpoints = 1
         do i = 1, rank
            grid_return_numpoints = grid_return_numpoints
     .                                       *grid_shape(grid,i)
         end do

         if (ltrace) then
            write(*,*) 'grid_return_numpoints: rank = ',rank
            do i = 1, rank
            write(*,*) 'grid_return_numpoints: N(i) = ',
     .                          i,grid_shape(grid,i)
            end do
            write(*,*) 'grid_return_numpoints: num = ',
     .                          grid_return_numpoints
         end if

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_shape:                                                      cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_return_shape(grid, shape)
      implicit none
      include 'class_constants.inc' 
      include 'grids.inc'
         integer grid
         integer shape(*)
         integer i
      
         do i = 1, grid_rank(grid)
            shape(i)  = grid_shape(grid,i)
         end do

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function grid_return_rank(grid)
      implicit none
      !include 'class_constants.inc' 
      !include 'grids.inc' 
         integer grid
      
         integer rank
         integer level
         integer tree
         integer reg
                
         !Functions
         integer  register_return_rank 
         integer  grid_return_level
         integer  level_return_tree
         integer  tree_return_register
         external register_return_rank 
         external grid_return_level
         external level_return_tree
         external tree_return_register
         logical  level_valid, tree_valid, reg_valid
         external level_valid, tree_valid, reg_valid

         logical     ltrace
         parameter ( ltrace = .false. )

         !Find out what register this grid is on       
         if(ltrace)write(*,*) 'grid_return_rank: grid: ',grid
         level = grid_return_level(grid)
         if(ltrace)write(*,*) 'grid_return_rank: level ',level
         if (level_valid(level)) then
            tree  = level_return_tree(level)
         else
             write(*,*) 'grid_return_rank: Invalid level. Stopping.'
             write(*,*) 'grid_return_rank: ',grid,level
             stop
         end if
         if(ltrace)write(*,*) 'grid_return_rank: tree ',tree
         !
         if (tree_valid(tree)) then
            reg   = tree_return_register(tree)
         else
             write(*,*) 'grid_return_rank: Invalid tree. Stopping.'
             write(*,*) 'grid_return_rank: ',grid,level,tree
             stop
         end if
         if(ltrace)write(*,*) 'grid_return_rank: reg ',reg
         !
         if (reg_valid(reg)) then
            rank = register_return_rank(reg)
         else
             write(*,*) 'grid_return_rank: Invalid reg. Stopping.'
             write(*,*) 'grid_return_rank: ',grid,level,tree,reg
             stop
         end if

         if(ltrace)then
           write(*,*) 'grid_return_rank: reg ',reg
           write(*,*) 'grid_return_rank: reg,tree ',reg,tree
           write(*,*) 'grid_return_rank: ',reg,tree,level,grid
           write(*,*) 'grid_return_rank: ',rank
         end if
         grid_return_rank = rank
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_return_time(grid)
      implicit none
      include 'class_constants.inc' 
      include 'grids.inc'
         integer grid        

         integer level
         integer tree
 
         !Functions
         integer grid_return_level
         integer level_return_tree
         double precision tree_return_time

         level = grid_return_level(grid)
         tree  = level_return_tree(level)
         grid_return_time = tree_return_time(tree)
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_return_maxradius(grid)
      implicit none
      integer grid
      include 'class_constants.inc'
      include 'grids.inc'
      integer  i, rank
      real*8   r_wmax, r_wmin
      integer  grid_return_rank
      external grid_return_rank
      
      rank = grid_return_rank(grid)
    
      r_wmax = 0.d0
      r_wmin = 0.d0
      do i = 1, rank
         r_wmax = r_wmax + grid_max(grid,i)**2 
         r_wmin = r_wmin + grid_min(grid,i)**2 
      end do
      r_wmax = sqrt(r_wmax)
      r_wmin = sqrt(r_wmin)

      grid_return_maxradius = max(r_wmax,r_wmin)

      return
      end      ! grid_return_maxradius

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_return_resolution(grid)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
      
         integer level
         
         ! Functions
         integer grid_return_level
         real*8  level_return_resolution
 
         level = grid_return_level(grid)
         grid_return_resolution = level_return_resolution(level)
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_return_name(grid, name)
      implicit none
      include 'class_constants.inc' 
      include 'grids.inc'
         integer        grid
         character*(*)  name
         logical     ltrace
         parameter ( ltrace = .false. )

         integer level
         integer tree
         integer reg
 
         !Functions
         integer          grid_return_level
         integer          level_return_tree
         integer          tree_return_reg

         name = ''
         if (grid.ne.NULL_GRID) then
            level = grid_return_level(grid)
            if (level.ne.NULL_LEVEL) then
               tree  = level_return_tree(level)
               if (tree.ne.NULL_TREE) then
                  reg   = tree_return_reg(tree)
                  if (reg.ne.NULL_REGISTER) then
                     call register_return_myname(reg, name)
                     if(ltrace)write(*,*)'grid_return_name: ',
     .                                 grid,level,tree,reg
                     if(ltrace)write(*,*)'grid_return_name: name: ',
     .                                name(1:20)
                  else
                     if(ltrace)write(*,*)'grid_return_name: Invalid reg'
     .                               ,reg
                  end if
               else
                  if(ltrace)write(*,*)'grid_return_name: Invalid tree:',
     .                               tree
               end if
            else
               if(ltrace)write(*,*)'grid_return_name: Invalid level: ',
     .                               level
            end if
         else
            if(ltrace)write(*,*)'grid_return_name: Invalid grid: ',grid
         end if

         return
      end



cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_set_level(grid, level)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
         integer level

         grid_on_level(grid) = level

         return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function grid_return_level(grid)
      implicit none
      integer grid
      include 'class_constants.inc'
      include 'grids.inc'

         grid_return_level = grid_on_level(grid)

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function grid_return_sibling(grid)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
         grid_return_sibling = grid_sibling(grid)

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function grid_get_coords(grid)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
    
         grid_get_coords = grid_coords(grid)
         return
      end

      integer function grid_get_field(grid)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
    
         grid_get_field = grid_field(grid)
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_output:                                                            cc
cc                 Output grid data to sdf file.                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_output(grid)
      implicit none
      integer  grid
      include 'mpif.h'
      include 'mympi.inc'
      include 'variables.inc'
      include 'class_constants.inc'
      include 'mem.inc'
      include 'grids.inc'
      include 'methods.inc'
 
         integer        gft_rc, rank, i, shape(3)
         integer        gft_out_bbox
         external       gft_out_bbox
         real*8         bbox(6)
         character*128   name

         logical        ltrace
         parameter    ( ltrace = .false. ) 
 

         if ( grid_is_local(grid) ) then
            call grid_return_name(grid,  name)
            call grid_return_bbox(grid,  bbox) 
            rank = grid_return_rank(grid)
            do i = 1, rank
               shape(i) = grid_shape(grid,i)
            end do
            !
            if(ltrace) then
               write(*,80)myid,"grid_output: grid  = ",grid
               write(*,90)myid,"grid_output: name  = ",name(1:20)
               write(*,80)myid,"grid_output: rank  = ",rank
               do i = 1, rank
                 write(*,70)myid,'    i,shape,bbox_min,bbox_max:',
     *              i,shape(i), bbox(2*i-1),bbox(2*i)
               end do
70             format('[',I3,'] ',A,2I5,2F18.10)
80             format('[',I3,'] ',A,I9)
90             format('[',I3,'] ',A,A)
            endif
            !
            if (rank.lt.1.or.rank.gt.3) return
            gft_rc = gft_out_bbox(name,
     *                            grid_return_time(grid),
     *                            shape, rank, bbox,
     *                            q( grid_field(grid) )      )
            if(ltrace)write(*,*) 'grid_output: gft_rc: ',gft_rc
            if (.false.) then
            gft_rc = gft_out_bbox(name//'_mask',
     *                            grid_return_time(grid),
     *                            shape, rank, bbox,
     *                            q( grid_mask(grid) )      )
            end if
         endif

         return
      end          ! END: grid_output

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    amira_grid_output:                                                      cc
cc                 Output amira grid data                                     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine amira_grid_output(grid)
      implicit none
      integer  grid
      include 'mpif.h'
      include 'mympi.inc'
      include 'variables.inc'
      include 'class_constants.inc'
      include 'mem.inc'
      include 'grids.inc'
      include 'methods.inc'
 
         integer        gft_rc, rank, i, shape(3)
         integer        gft_out_bbox
         external       gft_out_bbox
         real*8         bbox(6)
         character*128   name

         logical        ltrace
         parameter    ( ltrace = .false. ) 
         real, dimension(:), allocatable :: f
         character amirastring*128
         character suffix*80
         character timename*128
         integer strlen,strlenB,strlenC
         real*8 time

         if ( grid_is_local(grid) ) then
            call grid_return_name(grid,  name)
            call grid_return_bbox(grid,  bbox) 
            rank = grid_return_rank(grid)
            do i = 1, rank
               shape(i) = grid_shape(grid,i)
            end do
            !
            if(ltrace) then
               write(*,80)myid,"grid_output: grid  = ",grid
               write(*,90)myid,"grid_output: name  = ",name
               write(*,80)myid,"grid_output: rank  = ",rank
               do i = 1, rank
                 write(*,70)myid,'    i,shape,bbox_min,bbox_max:',
     *              i,shape(i), bbox(2*i-1),bbox(2*i)
               end do
70             format('[',I3,'] ',A,2I5,2F18.10)
80             format('[',I3,'] ',A,I3)
90             format('[',I3,'] ',A,A)
            endif
            !
c
c           write out in AMIRA format
c           
c
            if ( rank .eq. 3 ) then
              time = grid_return_time(grid)
              write(unit=suffix,fmt='(F5.2)') time
              i = len(suffix)
              do while (suffix(i:i) .eq. ' ')
                i = i-1
              end do
              strlenB = i

              i = len(name)
              do while (name(i:i) .eq. ' ')
                i = i-1
              end do
              strlen = i
              amirastring = name(1:strlen) // ".hx"
              if ( time .lt. 1.d-6 ) then
                open(59,file=amirastring,status='unknown')
                write(59,110) 'create HxDynamicFileSeriesCtrl',
     *                        ' {TimeSeriesControl}'
                write(59,120) 'TimeSeriesControl init',
     *                        ' -loadCmd {load -raw',
     *                        ' "$FILENAME"',
     *                        ' little xfastest float 1',
     *                        shape(1),shape(2),shape(3),
     *             bbox(1),bbox(2),bbox(3),bbox(4),bbox(5),bbox(6),
     *             ' -header 8} \'
                close(59)
              end if

              timename = name(1:strlen) // suffix(1:strlenB)
              i = len(timename)
              do while (timename(i:i) .eq. ' ')
                i = i-1
              end do
              strlenC = i
   
              open(59,file=amirastring,position='append')
c              write(59,100) 'load -raw ${SCRIPTDIR}/',name(1:strlen),
c     *             ' little xfastest float 1',
c     *             shape(1),shape(2),shape(3),
c     *             bbox(1),bbox(2),bbox(3),bbox(4),bbox(5),bbox(6),
c     *             ' -header 8 ] setLabel ',name(1:strlen)
              write(59,130) '"${SCRIPTDIR}/',timename(1:strlenC),'" ' 
              close(59)

100           format('[ ',A,A,A,I3,I3,I3,
     *               F9.3,F9.3,F9.3,F9.3,F9.3,F9.3,
     *               A,A)
110           format(A,A)
120           format(A,A,A,A,I3,I3,I3,
     *               F9.3,F9.3,F9.3,F9.3,F9.3,F9.3,
     *               A)
130           format(A,A,A,$)

              allocate( f(shape(1)*shape(2)*shape(3)) )
              do i=1,shape(1)*shape(2)*shape(3)
                f(i) = q( grid_field(grid) + (i-1) )
              end do
              open(60,file=timename,status='unknown',
     *                             form='unformatted')
              write(60) f
              close(60)
              deallocate(f)
            else
              write(*,*) 'AMIRA output not set up for 2-d output yet'
            end if

            if (.false.) then
            gft_rc = gft_out_bbox(name//'_mask',
     *                            grid_return_time(grid),
     *                            shape, rank, bbox,
     *                            q( grid_mask(grid) )      )
            end if
         endif

         return
      end          ! END: grid_output


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_dump:                                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_dump(grid)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'grids.inc'
      include 'methods.inc'
      include 'mem.inc'
         real*8      l2norm1D
         external    l2norm1D
         integer   grid, rank, level, tree, reg

         real*8    bbox(6)
         integer   shape(3)

         call grid_return_shape(grid, shape)
         call grid_return_bbox(grid, bbox) 
         rank = grid_return_rank(grid)

         level = grid_return_level(grid)
         tree  = level_return_tree(level)
         reg   = tree_return_register(tree)
 9       FORMAT(A,I3,A,I4,A,I4,A,I4,A,I4)
         write(*,9)"[",myid,"] reg: ",reg,' tree: ',tree,' level:',level

         if (.false.) then
            write(*,*) rank, level, shape, bbox
            write(*,*) myid, grid, grid_return_owner(grid)
         end if

         if (rank.eq. 3) then
11       FORMAT(A,I3.1,A,A,I5.1,A,I3.1,A,I3.1,A,I3.1,A,F6.2,A,F6.2,A,A,
     *          F6.2,A,F6.2,A,A,F6.2,A,F6.2,A,A,I3.1)
            write(*,11) "[",myid,"]","     Grid:",grid, " Shape:",
     *              shape(1),",",shape(2),",",shape(3), 
     *              "[", bbox(1), ",", bbox(2), "]",
     *              "[", bbox(3), ",", bbox(4), "]",
     *              "[", bbox(5), ",", bbox(6), "]",
     *              "Own=", grid_return_owner(grid)
         else if (rank .eq. 2) then
12       FORMAT(A,I3,A,A,I5,A,I3,A,I3,A,F6.2,A,F6.2,A,A,
     *          F6.2,A,F6.2,A,A,I3)
            write(*,12) "[",myid,"]","     Grid:",grid, " Shape:",
     *              shape(1),",",shape(2),
     *              "[", bbox(1), ",", bbox(2), "]",
     *              "[", bbox(3), ",", bbox(4), "]",
     *              "Own=", grid_return_owner(grid)
         else if (rank .eq. 1) then
13       FORMAT(A,I3,A,A,I5,A,I3,A,A,F6.2,A,F6.2,A,A,I3)
            write(*,13) "[",myid,"]","     Grid:",grid, " Shape:",
     *              shape(1),",",
     *              "[", bbox(1), ",", bbox(2), "]",
     *              "Own=", grid_return_owner(grid)
         else 
14       FORMAT(A,I3,A,3I3)
            write(*,15) "[",myid,"] grid_dump: Unknown rank: ",grid,rank
         end if
15       FORMAT(A,I3,A,2I7)

         write(*,*)'gridptr: ',grid_get_field(grid)
         if (rank.eq.1) then
         write(*,*)'l2norm: ',l2norm1D(q(grid_get_field(grid)),shape(1))
         else if (rank.eq.2) then
         write(*,*)'l2norm: ',l2norm1D(q(grid_get_field(grid)),
     .                             shape(1)*shape(2))
         else if (rank.eq.3) then
         write(*,*)'l2norm: ',l2norm1D(q(grid_get_field(grid)),
     .                             shape(1)*shape(2)*shape(3))
         end if

         return
      end

      subroutine grid_copy( old_grid, new_grid )
      implicit none
      include 'class_constants.inc'
      include 'mem.inc'
      include 'grids.inc' 
         integer old_grid
         integer new_grid

         integer shape(3)

         !write(*,*) "Entering grid copy..."

         call grid_return_shape( old_grid, shape )

         call vector_copy( q( grid_field(old_grid) ), 
     *                     q( grid_field(new_grid) ),
     *                     shape(1) * shape(2) * shape(3) )
  
         return
      end


      subroutine grid_set_owner( grid, owner ) 
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid
         integer owner

         grid_owner( grid ) = owner
         return
      end
      

      integer function grid_return_owner( grid ) 
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer grid

         grid_return_owner = grid_owner( grid )
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_number_grids:                                               cc
cc             Count how many grids in the register local to this proc.       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      function grid_return_number_grids(reg_num)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'grids.inc'
      include 'methods.inc'
         integer reg_num

         integer gi, num_grids, ti, li
         integer tree, reg

         num_grids = 0

         ti = register_return_start(reg_num)
         do while(ti .ne. NULL_TREE)
          li = tree_return_start(ti) 
           do while(li .ne. NULL_LEVEL)
            gi = level_return_start(li)
             do while(gi .ne. NULL_GRID)
                !
                if( grid_owner(gi) .eq. myid) then
                   num_grids = num_grids + 1
                endif
                !
                gi = grid_return_sibling(gi)
             enddo
            li = level_return_sibling(li)
           enddo
          ti = tree_return_sibling(ti) 
         enddo

         grid_return_number_grids = num_grids
 
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_send_dataB:                                                        cc
cc                     Send data to master process for merging.               cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_send_dataB( grid )
      implicit none 
      integer grid
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'  
      include 'grids.inc'
      include 'methods.inc'
      include 'mem.inc'
      include 'variables.inc'
      integer numpoints, gridfield, i
          
         ! MPI
         logical     ltrace
         parameter ( ltrace = .false. )

  96        format('[',I3,'] ',A,F15.8)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I10)

         numpoints = 1
         do i = 1, grid_return_rank(grid)
            numpoints = numpoints * grid_shape(grid,i)
         end do

         gridfield = grid_get_field(grid)

         if (ltrace) then
            write(*,99)myid,'grid_send_dataB: grid:      ',grid
            write(*,99)myid,'grid_send_dataB: Numpoints: ',numpoints
            write(*,99)myid,'grid_send_dataB: To:        ',master
            write(*,99)myid,'grid_send_dataB: gridfield: ',gridfield
            write(*,99)myid,'grid_send_dataB: master:    ',master
         end if

         call MPI_Send( q(gridfield), numpoints,
     *                  MPI_DOUBLE_PRECISION, master, tag,
     *                  MPI_COMM_WORLD, ierr ) 

         if (ltrace) then
            write(*,99)myid,'grid_send_dataB: ierr:    ',ierr
            write(*,97)myid,'grid_send_dataB: Data sent'
         end if

         return
      end
      
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_recv_dataB( gi, gm )
      implicit none 
      integer gi, gm
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'  
      include 'grids.inc'
      include 'methods.inc'
      include 'variables.inc'
      include 'mem.inc'
      integer     numpoints, tmpstore

         integer     l_shape(3), reg, g_shape(3), source
         real*8      global_bbox(6)
         real*8      l2norm1D
         external    l2norm1D
         integer gmfield, gifield,
     *           indexm,  indexi, rank
 
         real*8      local_bbox(6), sdf_time, h
         integer     tree, oi, oj, ok, i, j, k
         integer     offset(3)

         logical     ltrace
         parameter ( ltrace = .false. )

  96        format('[',I3,'] ',A,F15.8)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I10)

         ! Get resolution from old grid (gi)
         h    = grid_return_resolution(gi)
         rank = grid_return_rank(gi)

         numpoints = 1
         do i = 1, rank
            numpoints = numpoints * grid_shape(gi,i)
         end do
         tmpstore  = my_malloc(numpoints)
         source    = grid_return_owner(gi)

         if (ltrace) then
            write(*,99)myid,'grid_recv_dataB: From:      ',source
            write(*,99)myid,'grid_recv_dataB: gi:        ',gi
            write(*,99)myid,'grid_recv_dataB: gm:        ',gm
            write(*,99)myid,'grid_recv_dataB: Numpoints: ',numpoints
            write(*,99)myid,'grid_recv_dataB: tmpstore:  ',tmpstore
         end if

         call MPI_Recv( q(tmpstore), numpoints,
     *                  MPI_DOUBLE_PRECISION, source, tag, 
     *                  MPI_COMM_WORLD, status, ierr ) 

         if (ltrace) then
            write(*,99)myid,'grid_recv: received, ierr:',ierr
            write(*,99)myid,'grid_recv: from: ', status(MPI_SOURCE)
            write(*,99)myid,'grid_recv: error:', status(MPI_ERROR)
            write(*,99)myid,'grid_recv: tag:  ', status(MPI_TAG)
            write(*,96)myid,'grid_recv: received',
     *                       l2norm1D(q(tmpstore), numpoints)
         end if

         call grid_return_bbox( gi, local_bbox ) 
         call grid_return_bbox( gm, global_bbox ) 
         !
         call grid_return_shape( gi, l_shape)
         call grid_return_shape( gm, g_shape)
         !
         do i = 1, rank
            offset(i) = NINT((local_bbox(2*i-1) - global_bbox(2*i-1))/h)
         end do
         !oi =  NINT( ( local_bbox(1) - global_bbox(1) ) / h  )
         !oj =  NINT( ( local_bbox(3) - global_bbox(3) ) / h  )
         !ok =  NINT( ( local_bbox(5) - global_bbox(5) ) / h  )

         !
         ! Copy values into place:
         !
         gmfield = grid_get_field(gm)
         gifield = tmpstore
         if (rank .eq. 3) then
            do k = 1, l_shape(3)
             do j = 1, l_shape(2)
              do i = 1, l_shape(1)
                 indexi = (k-1)*l_shape(1)*l_shape(2)
     *                   +(j-1)*l_shape(1)
     *                   +(i-1)
                 indexm = (k+offset(3)-1)*g_shape(1)*g_shape(2)
     *                   +(j+offset(2)-1)*g_shape(1)
     *                   +(i+offset(1)-1)
                 q(gmfield+indexm) = q(gifield+indexi)
              enddo 
             enddo
            enddo
         else if (rank .eq. 2) then
             do j = 1, l_shape(2)
              do i = 1, l_shape(1)
                 indexi = 
     *                   +(j-1)*l_shape(1)
     *                   +(i-1)
                 indexm =
     *                   +(j+offset(2)-1)*g_shape(1)
     *                   +(i+offset(1)-1)
                 q(gmfield+indexm) = q(gifield+indexi)
              enddo 
             enddo 
         else if (rank .eq. 1) then
              do i = 1, l_shape(1)
                 indexi = 
     *                   +(i-1)
                 indexm = 
     *                   +(i+offset(1)-1)
                 q(gmfield+indexm) = q(gifield+indexi)
              enddo 
         else 
           write(*,*) 'grid_recv_dataB: Unknown rank: ',rank
         end if

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_merge_local:                                                       cc
cc                      Merge local grid gi with the uniform mesh             cc
cc                      grid gm.                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_merge_local( gi, gm)
      implicit none
      integer gi, gm
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc' 
      include 'grids.inc'  
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         integer reg, l_shape(3), g_shape(3)
         integer gmfield, gifield,
     *           indexm,  indexi, rank
         real*8  global_bbox(6)
         real*8  local_bbox(6), sdf_time, h
         integer tree, grid, oi, oj, ok, i, j, k
       logical     ltrace
       parameter ( ltrace = .false. )
      
         !
         ! Get info on grid to be merged:
         !
         h    = grid_return_resolution(gi)
         rank = grid_return_rank(gi)
         !
         call grid_return_bbox( gi, local_bbox  )
         call grid_return_bbox( gm, global_bbox ) 
         !
         call grid_return_shape( gi, l_shape)
         call grid_return_shape( gm, g_shape)
         !
         oi =  NINT( ( local_bbox(1) - global_bbox(1) ) / h  )
         oj =  NINT( ( local_bbox(3) - global_bbox(3) ) / h  )
         ok =  NINT( ( local_bbox(5) - global_bbox(5) ) / h  )

         if (ltrace) then
            write(*,99) myid, 'grid_merge_local: gi    =',gi
            write(*,99) myid, 'grid_merge_local: gm    =',gm
            write(*,99) myid, 'grid_merge_local: rank  =',rank
            write(*,99) myid, 'grid_merge_local: g_shape=',
     *            g_shape(1),g_shape(2),g_shape(3)
            write(*,99) myid, 'grid_merge_local: l_shape=',
     *            l_shape(1),l_shape(2),l_shape(3)
            write(*,99) myid, 'grid_merge_local: oi/j/k=',oi,oj,ok
            write(*,96) myid, 'grid_merge_local: g_bbox=',
     *            global_bbox(1),global_bbox(3),global_bbox(5)
            write(*,96) myid, 'grid_merge_local: l_bbox=',
     *            local_bbox(1),local_bbox(3),local_bbox(5)
         end if

         !
         ! Copy values into place:
         !
         gmfield = grid_get_field(gm)
         gifield = grid_get_field(gi)
         if (rank.eq.3) then
            do k = 1, l_shape(3)
             do j = 1, l_shape(2)
              do i = 1, l_shape(1)
                 indexi = (k-1)*l_shape(1)*l_shape(2)
     *                +(j-1)*l_shape(1)
     *                +(i-1)
                 indexm = (k+ok-1)*g_shape(1)*g_shape(2)
     *                +(j+oj-1)*g_shape(1)
     *                +(i+oi-1)
                 q(gmfield+indexm) = q(gifield+indexi)
              enddo 
             enddo
            enddo
         else if (rank.eq.2) then
            do j = 1, l_shape(2)
             do i = 1, l_shape(1)
                indexi =  (j-1)*l_shape(1)
     *                   +(i-1)
                indexm =  (j+oj-1)*g_shape(1)
     *                   +(i+oi-1)
                q(gmfield+indexm) = q(gifield+indexi)
             enddo 
            enddo
         else if (rank.eq.1) then
            do i = 1, l_shape(1)
               indexi = (i-1)
               indexm = (i+oi-1)
               q(gmfield+indexm) = q(gifield+indexi)
            enddo 
         else 
            write(*,*) 'grid_merge_local: Unknown rank: ',rank
         end if

  96        format('[',I3,'] ',A,3F15.8)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,3I5)

         return
      end          ! END: grid_merge_local


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_set_mask(gi, value)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer gi, value
 
         grid_mask(gi) = value
         return
      end

      integer function grid_return_mask(gi)
      implicit none
      include 'class_constants.inc'
      include 'grids.inc'
         integer gi
  
         grid_return_mask = grid_mask(gi)
         return
      end

      !
      ! Switches grid1 with grid2
      !
      subroutine grid_switch( grid1, grid2 )
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc'
      include 'methods.inc'
      include 'variables.inc'
         integer grid1
         integer grid2

         real*8  bbox1(6), bbox2(6)
         integer shape1(3), shape2(3)
         integer grid1_owner, grid2_owner
         integer grid1_field, grid2_field

         ! Switch bbox
         call grid_return_bbox(grid1, bbox1)
         call grid_return_bbox(grid2, bbox2)        
         call grid_set_bbox(grid1, bbox2)
         call grid_set_bbox(grid2, bbox1)

         ! Switch shape
         call grid_return_shape(grid1, shape1)
         call grid_return_shape(grid2, shape2)
         call grid_set_shape(grid1, shape2)
         call grid_set_shape(grid2, shape1)

         ! Switch owner
         grid1_owner = grid_return_owner(grid1)
         grid2_owner = grid_return_owner(grid2)
         call grid_set_owner(grid1, grid2_owner)
         call grid_set_owner(grid2, grid1_owner)

         ! Switch sibling
         !call grid_set_sibling(grid1, grid_return_sibling(grid2))
         !call grid_set_sibling(grid2, grid1) 

         ! Switch pointer in memory   
         grid1_field = grid_get_field(grid1)
         grid2_field = grid_get_field(grid2) 
         grid_field(grid1) = grid2_field
         grid_field(grid2) = grid1_field
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_find_halfmax:                                                      cc
cc                    Finds the max and its location on a given grid          cc
cc                    for half the domain for:                                cc
cc                             x_anum < 0     if hplane<0                     cc
cc                       or    x_anum > 0     if hplane>0                     cc
cc                       where anum = 1, 2, 3                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_find_halfmax(gi,max,coords,anum,hplane)
      implicit none
      integer  gi, anum, hplane
      real*8   max, coords(*)
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  rank, length, shape(3), i,j,k
      integer  grid_return_rank
      real*8   value, x, y, z, mycoord

      logical     ltrace2
      parameter ( ltrace2 = .false. )
  
      if (ltrace2) then
         write(*,*) 'grid_find_halfmax:     gi=',gi
         write(*,*) 'grid_find_halfmax:   anum=',anum
         write(*,*) 'grid_find_halfmax: hplane=',hplane
      end if

      call grid_return_shape(gi, shape)

      rank   = grid_return_rank(gi)
      length = shape(1)

      max    = q(grid_field(gi)) - 1.d0

      if (rank.eq.1) then
         do i = 1, shape(1)
            value = q(grid_field(gi)+(i-1))
            x     = q(grid_coords(gi)+(i-1))
            if (x.ge.0 .and. hplane.gt.0 
     *                 .or.
     *          x.lt.0 .and. hplane.lt.0 ) then
               if (value .gt. max) then
                  max       = value
                  coords(1) = x
               end if
            end if
         end do
      else if (rank.eq.2) then
         do j = 1, shape(2)
         do i = 1, shape(1)
            value = q(grid_field(gi)+(j-1)*shape(1)+(i-1))
            x     = q(grid_coords(gi)+(i-1))
            y     = q(grid_coords(gi)+shape(1)+(j-1))
            if (anum .eq. 1) then
            ! Determine which coord to look at:
               mycoord = x
            else
               mycoord = y
            endif
            if (mycoord.ge.0 .and. hplane.gt.0 
     *                 .or.
     *          mycoord.lt.0 .and. hplane.lt.0 ) then
               if (value .gt. max) then
                  max       = value
                  coords(1) = x
                  coords(2) = y
                  !write(*,*) 'grid_max_halfplane: new max:',value,x,y
               end if
            end if
         end do
         end do
      else if (rank.eq.3) then
         do k = 1, shape(3)
         do j = 1, shape(2)
         do i = 1, shape(1)
            value = q(grid_field(gi)
     *                    +(k-1)*shape(2)*shape(1)+(j-1)*shape(1)+(i-1))
            x     = q(grid_coords(gi)+(i-1))
            y     = q(grid_coords(gi)+shape(1)+(j-1))
            z     = q(grid_coords(gi)+shape(1)+shape(2)+(k-1))
            if (anum .eq. 1) then
            ! Determine which coord to look at:
               mycoord = x
            elseif (anum .eq. 2) then
               mycoord = y
            else
               mycoord = z
            endif
            if (mycoord.ge.0 .and. hplane.gt.0 
     *                 .or.
     *          mycoord.lt.0 .and. hplane.lt.0 ) then
               if (value .gt. max) then
                  max       = value
                  coords(1) = x
                  coords(2) = y
                  coords(3) = z
               end if
            end if
         end do
         end do
         end do
      else
         write(*,*) 'grid_find_halfmax: Unknown rank',rank
         return
      end if

      return
      end       ! END: grid_find_halfmax

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_find_max:                                                          cc
cc                    Finds the max and its location on a given grid          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_find_max(gi,max,coords)
      implicit none
      integer  gi
      real*8   max, coords(*)
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  rank, length, shape(3), i,j,k
      integer  grid_return_rank
      real*8   value

      logical     ltrace2
      parameter ( ltrace2 = .false. )
  
      if (ltrace2) write(*,*) 'grid_find_max: gi= ',gi

      call grid_return_shape(gi, shape)

      rank   = grid_return_rank(gi)
      length = shape(1)

      max    = q(grid_field(gi)) - 1.d0

      if (rank.eq.1) then
         do i = 1, shape(1)
            value = q(grid_field(gi)+(i-1))
            if (value .gt. max) then
               max       = value
               coords(1) = q(grid_coords(gi)+(i-1))
            end if
         end do
      else if (rank.eq.2) then
         do j = 1, shape(2)
         do i = 1, shape(1)
            value = q(grid_field(gi)+(j-1)*shape(1)+(i-1))
            if (value .gt. max) then
               max       = value
               coords(1) = q(grid_coords(gi)+(i-1))
               coords(2) = q(grid_coords(gi)+shape(1)+(j-1))
            end if
         end do
         end do
      else if (rank.eq.3) then
         do k = 1, shape(3)
         do j = 1, shape(2)
         do i = 1, shape(1)
            value = q(grid_field(gi)
     *                    +(k-1)*shape(2)*shape(1)+(j-1)*shape(1)+(i-1))
            if (value .gt. max) then
               max       = value
               coords(1) = q(grid_coords(gi)+(i-1))
               coords(2) = q(grid_coords(gi)+shape(1)+(j-1))
               coords(3) = q(grid_coords(gi)+shape(1)+shape(2)+(k-1))
            end if
         end do
         end do
         end do
      else
         write(*,*) 'grid_find_max: Unknown rank',rank
         return
      end if

      return
      end       ! END: grid_find_max

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_max:                                                        cc
cc                     Return the maximum on the grid.                        cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_return_max(gi)
      implicit none
      integer  gi
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  rank, length, shape(3), i
      integer  grid_return_rank
      real*8   findmax1D
      logical     ltrace2
      parameter ( ltrace2 = .false. )
  
      if (ltrace2) write(*,*) 'grid_return_max: gi= ',gi

      call grid_return_shape(gi, shape)

      rank   = grid_return_rank(gi)
      length = shape(1)
      if (ltrace2) write(*,*) 'grid_return_max: shape(i)=',shape(1)
      do i = 2, rank
         length = length * shape(i)
         if (ltrace2) write(*,*) 'grid_return_max: shape(i)=',shape(i)
      end do

      grid_return_max = findmax1D(q(grid_field(gi)),length)

      if (ltrace2) then
         write(*,*) 'grid_return_max: length = ',length
         write(*,*) 'grid_return_max: rank   = ',rank
      end if

      return
      end       ! END: grid_return_max

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_maxabs:                                                     cc
cc                     Return the maximum of the absolute value of the field  cc
cc                     on the grid.                                           cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_return_maxabs(gi)
      implicit none
      integer  gi
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  rank, length, shape(3), i
      integer  grid_return_rank
      real*8   findmaxabs1D
      logical     ltrace2
      parameter ( ltrace2 = .false. )
  
      if (ltrace2) write(*,*) 'grid_return_maxabs: gi= ',gi

      call grid_return_shape(gi, shape)

      rank   = grid_return_rank(gi)
      length = shape(1)
      if (ltrace2) write(*,*) 'grid_return_maxabs: shape(i)=',shape(1)
      do i = 2, rank
         length = length * shape(i)
         if (ltrace2)write(*,*) 'grid_return_maxabs: shape(i)=',shape(i)
      end do

      grid_return_maxabs = findmaxabs1D(q(grid_field(gi)),length)

      if (ltrace2) then
         write(*,*) 'grid_return_maxabs: length = ',length
         write(*,*) 'grid_return_maxabs: rank   = ',rank
      end if

      return
      end       ! END: grid_return_maxabs

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_find_minimums                                                      cc
cc                    Finds the minimum along with the minimums on the        cc
cc                    various half-planes, along with their locations.        cc
cc                    Assumes minimums already values in it (if no other      cc
cc                    data, then set to a large number before calling).       cc
         !       global min == index 1
         !       min neg x  == index 2
         !       max neg x  == index 3
         !       min neg y  == index 4
         !       max neg y  == index 5
         !       min neg z  == index 6
         !       max neg z  == index 7
cc                    NB: locations() is densely pack w/ the coordinates      cc
cc                        dependent on the rank of the data.                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_find_minimums(gi,minimums,locations)
      implicit none
      integer  gi
      real*8   minimums(7),locations(3*7)
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  rank, length, shape(3), i,j,k
      integer  nx,ny,nz
      integer  grid_return_rank
      real*8   value, x,y,z

      logical     ltrace2
      parameter ( ltrace2 = .false. )
  
      if (ltrace2) write(*,*) 'grid_find_minimums: gi=',gi

      call grid_return_shape(gi, shape)
      rank   = grid_return_rank(gi)

      if (rank.eq.1) then
         nx = shape(1)
         do i = 1, nx
            value = q(grid_field(gi)+(i-1))
            x     = q(grid_coords(gi)+(i-1))
            if (value .lt. minimums(1)) then
               ! Global minimum:
               minimums(1)  = value
               locations(1) = x
            end if
            if (x .lt. 0) then
               if (value .lt. minimums(2)) then
                  ! Half-plane minimum:
                  minimums(2)  = value
                  locations(2) = x
               end if
            else
               if (value .lt. minimums(3)) then
                  ! Half-plane minimum:
                  minimums(3)  = value
                  locations(3) = x
               end if
            end if
         end do
      else if (rank.eq.2) then
         nx = shape(1)
         ny = shape(2)
         do j = 1, ny
         do i = 1, nx
            value = q(grid_field(gi)+(j-1)*shape(1)+(i-1))
            x     = q(grid_coords(gi)+(i-1))
            y     = q(grid_coords(gi)+shape(1)+(j-1))
            if (value .lt. minimums(1)) then
               ! Global minimum:
               minimums(1)  = value
               locations(1) = x
               locations(2) = y
            end if
            ! X-half-planes
            if (x .lt. 0) then
               if (value .lt. minimums(2)) then
                  ! Half-plane minimum:
                  minimums(2)  = value
                  locations(3) = x
                  locations(4) = y
               end if
            else
               if (value .lt. minimums(3)) then
                  ! Half-plane minimum:
                  minimums(3)  = value
                  locations(5) = x
                  locations(6) = y
               end if
            end if
            ! Y-half-planes
            if (y .lt. 0) then
               if (value .lt. minimums(4)) then
                  ! Half-plane minimum:
                  minimums(4)  = value
                  locations(7) = x
                  locations(8) = y
               end if
            else
               if (value .lt. minimums(5)) then
                  ! Half-plane minimum:
                  minimums(5)  = value
                  locations(9) = x
                  locations(10)= y
               end if
            end if
         end do
         end do
      else if (rank.eq.3) then
         nx = shape(1)
         ny = shape(2)
         nz = shape(3)
         do k = 1, nz
         do j = 1, ny
         do i = 1, nx
            value = q(grid_field(gi)
     *                    +(k-1)*shape(2)*shape(1)+(j-1)*shape(1)+(i-1))
            x     = q(grid_coords(gi)+(i-1))
            y     = q(grid_coords(gi)+shape(1)+(j-1))
            z     = q(grid_coords(gi)+shape(1)+shape(2)+(k-1))
            if (value .lt. minimums(1)) then
               ! Global minimum:
               minimums(1)  = value
               locations(1) = x
               locations(2) = y
               locations(3) = z
            end if
            ! X-half-planes
            if (x .lt. 0) then
               if (value .lt. minimums(2)) then
                  ! Half-plane minimum:
                  minimums(2)  = value
                  locations(4) = x
                  locations(5) = y
                  locations(6) = z
               end if
            else
               if (value .lt. minimums(3)) then
                  ! Half-plane minimum:
                  minimums(3)  = value
                  locations(7) = x
                  locations(8) = y
                  locations(9) = z
               end if
            end if
            ! Y-half-planes
            if (y .lt. 0) then
               if (value .lt. minimums(4)) then
                  ! Half-plane minimum:
                  minimums(4)  = value
                  locations(10)= x
                  locations(11)= y
                  locations(12)= z
               end if
            else
               if (value .lt. minimums(5)) then
                  ! Half-plane minimum:
                  minimums(5)  = value
                  locations(13)= x
                  locations(14)= y
                  locations(15)= z
               end if
            end if
            ! Z-half-planes
            if (z .lt. 0) then
               if (value .lt. minimums(6)) then
                  ! Half-plane minimum:
                  minimums(6)  = value
                  locations(16)= x
                  locations(17)= y
                  locations(18)= z
               end if
            else
               if (value .lt. minimums(7)) then
                  ! Half-plane minimum:
                  minimums(7)  = value
                  locations(19)= x
                  locations(20)= y
                  locations(21)= z
               end if
            end if
         end do
         end do
         end do
      else
         write(*,*) 'grid_find_max: Unknown rank',rank
         return
      end if

      return
      end       ! END: grid_find_minimums

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_min:                                                        cc
cc                     Return the minimum on the grid.                        cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_return_min(gi)
      implicit none
      integer  gi
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      integer  rank, length, shape(3), i
      integer  grid_return_rank
      real*8   findmin1D
      logical     ltrace2
      parameter ( ltrace2 = .false. )
  
      if (ltrace2) write(*,*) 'grid_return_min: gi= ',gi

      call grid_return_shape(gi, shape)

      rank   = grid_return_rank(gi)
      length = shape(1)
      if (ltrace2) write(*,*) 'grid_return_min: shape(i)=',shape(1)
      do i = 2, rank
         length = length * shape(i)
         if (ltrace2) write(*,*) 'grid_return_min: shape(i)=',shape(i)
      end do

      grid_return_min = findmin1D(q(grid_field(gi)),length)

      if (ltrace2) then
         write(*,*) 'grid_return_min: length = ',length
         write(*,*) 'grid_return_min: rank   = ',rank
      end if

      return
      end       ! END: grid_return_min


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_containspt                                                         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function grid_containspt(grid,coords)
      implicit none
      integer  grid        
      real*8   coords(*)
      include 'mpif.h'
      include 'mympi.inc'
      integer  rank, i
      real*8   bbox(6)
      integer    grid_return_rank
      external   grid_return_rank

      rank = grid_return_rank(grid)
      call grid_return_bbox(grid,  bbox)

      grid_containspt = .true.
      do i = 1, rank
         !write(*,*) i, coords(i),bbox(2*i-1),bbox(2*i)
         if(coords(i).lt.bbox(2*i-1).or.coords(i).gt.bbox(2*i)) then
            grid_containspt = .false.
         end if
      end do

      return
      end        ! END: grid_containspt

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_containsptint                                                      cc
cc            Point must be strictly within interior of grid, not on boundary cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function grid_containsptint(grid,coords)
      implicit none
      integer  grid        
      real*8   coords(*)
      include 'mpif.h'
      include 'mympi.inc'
      integer  rank, i
      real*8   bbox(6), h, tmp
      real*8   grid_return_resolution
      external grid_return_resolution
      integer    grid_return_rank
      external   grid_return_rank
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-8)

      rank = grid_return_rank(grid)
      call grid_return_bbox(grid,  bbox)
      h    = grid_return_resolution(grid)
      tmp  = h*SMALLNUMBER

      grid_containsptint = .true.
      do i = 1, rank
         !write(*,*) i, coords(i),bbox(2*i-1),bbox(2*i)
!        if( (coords(i).lt.bbox(2*i-1)+SMALLNUMBER)
!    .                  .or.
!    .       (coords(i).gt.bbox(2*i  )-SMALLNUMBER) ) then
         if( (coords(i).lt.(bbox(2*i-1)+tmp))
     .                  .or.
     .       (coords(i).gt.(bbox(2*i  )-tmp)) ) then
            grid_containsptint = .false.
         end if
      end do

      return
      end        ! END: grid_containsptint

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_is_local:                                                          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function grid_is_local(grid)
      implicit none
      integer  grid        
      include 'mpif.h'
      include 'mympi.inc'
      !include 'variables.inc'
      integer  grid_return_owner

      if (grid.lt.1) then
         grid_is_local = .false.
      else
         grid_is_local = grid_return_owner(grid) .eq. myid
      end if

      return
      end        ! END: grid_is_local

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_l2norm:                                                            cc
cc                    Return the l2norm   of all points not masked off.       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_l2norm(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dx, dy, dz, dV
      integer  numpoints
      integer  index, indexip1, indexjp1, indexkp1,
     *         indexip1jp1, indexip1kp1, indexjp1kp1,
     *         indexip1jp1kp1
      !
      integer    grid_return_rank
      external   grid_return_rank
      logical    double_equal
      external   double_equal
      !
      logical    ltrace
      parameter (ltrace = .false. )

      if (ltrace) then
         write(*,99) myid, 'grid_l2norm: gi   = ',gi
      end if

      rank      = grid_return_rank(gi)
      numpoints = 1
      do i = 1, rank
         numpoints = numpoints * grid_shape(gi,i)
      end do

      if (ltrace) then
         write(*,99) myid, 'grid_l2norm: rank = ',rank
         write(*,99) myid, 'grid_l2norm: numpoints = ',numpoints
      end if

      grid_l2norm = 0.d0
      do i = 1, numpoints
         if (double_equal(q(grid_mask(gi)+i-1),GRID_UNMASKED)) then
            grid_l2norm =   grid_l2norm
     *                    + q(grid_field(gi)+i-1)**2
         end if
      end do
      grid_l2norm = sqrt( grid_l2norm / numpoints )

      if (ltrace) then
         write(*,99) myid, 'grid_l2norm: gi   = ',gi
         write(*,99) myid, 'grid_l2norm: rank = ',rank
         write(*,98) myid, 'grid_l2norm: l2norm  = ',grid_l2norm
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_l2norm

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_integrate:                                                         cc
cc                    Return the integral of all points not masked off.       cc
cc                NB: Integrate onntegral of all points not masked off.       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function grid_integrate(gi,radius)
      implicit none
      integer  gi
      real*8   radius
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dx, dy, dz, dV, x,y,z, myr
      integer  index, indexip1, indexjp1, indexkp1,
     *         indexip1jp1, indexip1kp1, indexjp1kp1,
     *         indexip1jp1kp1
      !
      integer    grid_return_rank
      external   grid_return_rank
      logical    double_equal
      external   double_equal
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)
      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)

      if (ltrace) then
         write(*,*) 'grid_integrate: gi, dx, r:',gi,grid_spacings(gi,1),
     .                      radius
      end if

      !
      ! Compute volume element:
      !
      dV = 1.d0
      do i = 1, rank
         dV = dV * grid_spacings(gi,i)*0.5d0
         !write(*,98) myid, 'grid_integrate: dx   = ',grid_spacings(gi,i)
      end do

      grid_integrate = 0.d0

      if (rank.eq.3) then
         do k = 1, nz-1
         do j = 1, ny-1
         do i = 1, nx-1
            index          = (k-1)*ny*nx + (j-1)*nx + (i-1)
            indexip1       = (k-1)*ny*nx + (j-1)*nx + (i  )
            indexjp1       = (k-1)*ny*nx + (j  )*nx + (i-1)
            indexkp1       = (k  )*ny*nx + (j-1)*nx + (i-1)
            !
            indexip1jp1    = (k-1)*ny*nx + (j  )*nx + (i  )
            indexip1kp1    = (k  )*ny*nx + (j-1)*nx + (i  )
            indexjp1kp1    = (k  )*ny*nx + (j  )*nx + (i-1)
            !
            indexip1jp1kp1 = (k  )*ny*nx + (j  )*nx + (i  )
            !
            x      = q(grid_coords(gi)       +(i-1) )
            y      = q(grid_coords(gi)+nx    +(j-1) )
            z      = q(grid_coords(gi)+nx+ny +(k-1) )
            !
            myr    = sqrt(x**2+y**2+z**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED).and.
     .          myr .lt. radius) then
            !if (double_equal(q(grid_mask(gi)+index),GRID_UNMASKED)) then
            !if (q(grid_mask(gi)+index) .eq. GRID_UNMASKED) then
               grid_integrate = grid_integrate + dV * 
     *                        (   q(grid_field(gi)+index         )
     *                          + q(grid_field(gi)+indexip1      )
     *                          + q(grid_field(gi)+indexjp1      )
     *                          + q(grid_field(gi)+indexkp1      )
     *                          + q(grid_field(gi)+indexip1jp1   )
     *                          + q(grid_field(gi)+indexip1kp1   )
     *                          + q(grid_field(gi)+indexjp1kp1   )
     *                          + q(grid_field(gi)+indexip1jp1kp1)
     *                          )

            end if
         end do
         end do
         end do
      else if (rank.eq.2) then
      !else if (rank.eq.1) then
         do j = 1, ny-1
         do i = 1, nx-1
            index          =               (j-1)*nx + (i-1)
            indexip1       =               (j-1)*nx + (i  )
            indexjp1       =               (j  )*nx + (i-1)
            !
            indexip1jp1    =               (j  )*nx + (i  )
            !
            x      = q(grid_coords(gi)       +(i-1) )
            y      = q(grid_coords(gi)+nx    +(j-1) )
            !
            myr    = sqrt(x**2+y**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED).and.
     .          myr .lt. radius) then
               grid_integrate = grid_integrate + dV * 
     *                        (   q(grid_field(gi)+index         )
     *                          + q(grid_field(gi)+indexip1      )
     *                          + q(grid_field(gi)+indexjp1      )
     *                          + q(grid_field(gi)+indexip1jp1   )
     *                          )

            end if
         end do
         end do
      else 
         write(*,99) myid, 'grid_integrate: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_integrate: gi   = ',gi
         write(*,99) myid, 'grid_integrate: rank = ',rank
         write(*,99) myid, 'grid_integrate: nx   = ',nx,ny,nz
         write(*,98) myid, 'grid_integrate: dV   = ',dV
         write(*,98) myid, 'grid_integrate: radius=',radius
         write(*,98) myid, 'grid_integrate: Int  = ',grid_integrate
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_integrate

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    l2norm1D:                                                               cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function l2norm1D(field, nx)
      implicit none
      integer  nx        
      real*8   field(nx)
      integer  i
      logical     ltrace
      parameter ( ltrace = .false. )

      l2norm1D = 0.d0

      if (nx.le.0) return

      if (ltrace) write(*,*) 'l2norm1D: nx = ',nx
      do i = 1, nx
         l2norm1D = l2norm1D + field(i)**2
      end do
      l2norm1D = sqrt( l2norm1D / nx )

      return
      end        ! END: l2norm1D

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    findmin1D:                                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function findmin1D(field, nx)
      implicit none
      integer  nx        
      real*8   field(nx)
      integer  i
      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (nx.le.0) return

      if (ltrace2) write(*,*) 'findmin1D: nx = ',nx
      findmin1D = field(1)
      if (ltrace2) write(*,*) 'findmin1D: findmin1D=',findmin1D
      do i = 2, nx
         !write(*,*) i, field(i)
         if (field(i).lt.findmin1D) then
            findmin1D = field(i)
            if (ltrace2) write(*,*) 'findmin1D: findmin1D=',findmin1D
         end if
      end do

      return
      end        ! END: findmin1D

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    findmax1D:                                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function findmax1D(field, nx)
      implicit none
      integer  nx        
      real*8   field(nx)
      integer  i
      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (nx.le.0) return

      if (ltrace2) write(*,*) 'findmax1D: nx = ',nx
      findmax1D = field(1)
      if (ltrace2) write(*,*) 'findmax1D: findmax1D=',findmax1D
      do i = 2, nx
         !write(*,*) i, field(i)
         if (field(i).gt.findmax1D) then
            findmax1D = field(i)
            if (ltrace2) write(*,*) 'findmin1D: findmax1D=',findmax1D
         end if
      end do

      return
      end        ! END: findmax1D

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    findmaxabs1D                                                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function findmaxabs1D(field, nx)
      implicit none
      integer  nx        
      real*8   field(nx)
      integer  i
      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (nx.le.0) return

      findmaxabs1D = abs(field(1))
      do i = 2, nx
         if (abs(field(i)).gt.findmaxabs1D) then
            findmaxabs1D = abs(field(i))
         end if
      end do

      return
      end        ! END: findmaxabs1D

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_times:                                                             cc
cc                    Multiply by coordinate                                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_times(gi,dir)
      implicit none
      integer gi, dir
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   coord
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_times: Grid/dir: ',gi,dir
      end if

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
            do i = 1, nx
              !
              if (dir.eq.1) then
                 coord = q(grid_coords(gi)+(i-1))
              else if (dir.eq.2) then
                 coord = q(grid_coords(gi)+nx+(j-1))
              else if (dir.eq.3) then
                 coord = q(grid_coords(gi)+nx+ny+(k-1))
              else 
                 write(*,*) 'grid_times: Unknown direction'
              end if
              index = (k-1)*nx*ny + (j-1)*nx + (i-1)
              q(grid_field(gi)+index) = coord * q(grid_field(gi)+index)
              !
            end do
            end do
         end do
      else if (rank .eq. 2) then
         do j = 1, ny
            do i = 1, nx
               !
               if (dir.eq.1) then
                  coord = q(grid_coords(gi)+(i-1))
               else if (dir.eq.2) then
                  coord = q(grid_coords(gi)+nx+(j-1))
               else 
                  write(*,*) 'grid_times: Unknown direction'
               end if
               index = (j-1)*nx + (i-1)
               q(grid_field(gi)+index) = coord * q(grid_field(gi)+index)
               !
            end do
         end do
      else if (rank .eq. 1) then
            do i = 1, nx
               !
               if (dir.eq.1) then
                  coord = q(grid_coords(gi)+(i-1))
               else 
                  write(*,*) 'grid_times: Unknown direction'
               end if
               index = (i-1)
               q(grid_field(gi)+index) = coord * q(grid_field(gi)+index)
               !
            end do
      else
        write(*,*) 'grid_times: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_times: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_times

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_curl:                                                              cc
cc                    Compute the curl of a vector field                      cc
cc               NB: adapting to also work for 2D data producing              cc
cc                   just the z-component. In that case, we ignore            cc
cc                   gz, cx, and cy.                                          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_curl(gx,gy,gz, cx,cy,cz)
      implicit none
      integer gx,gy,gz, cx,cy,cz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      real*8   dFz_dx, dFz_dy, dFz_dz
      real*8   dFy_dx, dFy_dy, dFy_dz
      real*8   dFx_dx, dFx_dy, dFx_dz
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1
      integer         index_ip2, index_im2,
     *                index_jp2, index_jm2,
     *                index_kp2, index_km2
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gx)

      if (rank.eq.3) then
         if (.not.grid_compatible(gx,gy) .or.
     .       .not.grid_compatible(gy,gz) ) then
            write(*,99) myid, 'grid_curl: Grids not compatible',gx,gy,gz
            return
         end if
      else if (rank.eq.2) then
         if (.not.grid_compatible(gx,gy) ) then
            write(*,99) myid, 'grid_curl: 2D Grids not compatible',gx,gy
            return
         end if
      else if (rank.eq.1) then
         if (.not.grid_compatible(gx,cx) ) then
            write(*,99) myid, 'grid_curl: 1D Grids not compatible',gx,cx
            return
         end if
      else 
         write(*,99)myid,'grid_curl: Nothing implemented for rank:',rank
      end if

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_curl: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_curl: cx/y/z: ',cx,cy,cz
      end if

      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)
      !
      cxptr = grid_field(cx)
      cyptr = grid_field(cy)
      czptr = grid_field(cz)

      if (rank .eq. 3) then
         call load_scal1d( q(cxptr),    0.d0, nx*ny*nz)
         call load_scal1d( q(cyptr),    0.d0, nx*ny*nz)
         call load_scal1d( q(czptr),    0.d0, nx*ny*nz)
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  index_im1 = (k-1)*nx*ny + (j-1)*nx + (i-1-1)
                  index_im2 = (k-1)*nx*ny + (j-1)*nx + (i-1-2)
                  index_ip1 = (k-1)*nx*ny + (j-1)*nx + (i-1+1)
                  index_ip2 = (k-1)*nx*ny + (j-1)*nx + (i-1+2)
                  index_jm1 = (k-1)*nx*ny + (j-1-1)*nx + (i-1)
                  index_jm2 = (k-1)*nx*ny + (j-1-2)*nx + (i-1)
                  index_jp1 = (k-1)*nx*ny + (j-1+1)*nx + (i-1)
                  index_jp2 = (k-1)*nx*ny + (j-1+2)*nx + (i-1)
                  index_km1 = (k-1-1)*nx*ny + (j-1)*nx + (i-1)
                  index_km2 = (k-1-2)*nx*ny + (j-1)*nx + (i-1)
                  index_kp1 = (k-1+1)*nx*ny + (j-1)*nx + (i-1)
                  index_kp2 = (k-1+2)*nx*ny + (j-1)*nx + (i-1)
                  !
                  if (i.eq.1) then
                     ! 2nd order accurate forward difference:
                     dFz_dx =(  -3.d0*q(gzptr+index)
     .                          +4.d0*q(gzptr+index_ip1)
     .                          -1.d0*q(gzptr+index_ip2)        )/2.d0/h
                     dFy_dx =(  -3.d0*q(gyptr+index)
     .                          +4.d0*q(gyptr+index_ip1)
     .                          -1.d0*q(gyptr+index_ip2)        )/2.d0/h
                  else if (i.eq.nx) then
                     ! 2nd order accurate backward difference:
                     dFz_dx =(  +3.d0*q(gzptr+index)
     .                          -4.d0*q(gzptr+index_im1)
     .                          +1.d0*q(gzptr+index_im2)        )/2.d0/h
                     dFy_dx =(  +3.d0*q(gyptr+index)
     .                          -4.d0*q(gyptr+index_im1)
     .                          +1.d0*q(gyptr+index_im2)        )/2.d0/h
                  else
                  dFz_dx =(q(gzptr+index_ip1)-q(gzptr+index_im1))/2.d0/h
                  dFy_dx =(q(gyptr+index_ip1)-q(gyptr+index_im1))/2.d0/h
                  end if
                  if (j.eq.1) then
                     ! 2nd order accurate forward difference:
                     dFz_dy =(  -3.d0*q(gzptr+index)
     .                          +4.d0*q(gzptr+index_jp1)
     .                          -1.d0*q(gzptr+index_jp2)        )/2.d0/h
                     dFx_dy =(  -3.d0*q(gxptr+index)
     .                          +4.d0*q(gxptr+index_jp1)
     .                          -1.d0*q(gxptr+index_jp2)        )/2.d0/h
                  else if (j.eq.ny) then
                     ! 2nd order accurate backward difference:
                     dFz_dy =(  +3.d0*q(gzptr+index)
     .                          -4.d0*q(gzptr+index_jm1)
     .                          +1.d0*q(gzptr+index_jm2)        )/2.d0/h
                     dFx_dy =(  +3.d0*q(gxptr+index)
     .                          -4.d0*q(gxptr+index_jm1)
     .                          +1.d0*q(gxptr+index_jm2)        )/2.d0/h
                  else
                  dFz_dy =(q(gzptr+index_jp1)-q(gzptr+index_jm1))/2.d0/h
                  dFx_dy =(q(gxptr+index_jp1)-q(gxptr+index_jm1))/2.d0/h
                  end if
                  if (k.eq.1) then
                     ! 2nd order accurate forward difference:
                     dFy_dz =(  -3.d0*q(gyptr+index)
     .                          +4.d0*q(gyptr+index_kp1)
     .                          -1.d0*q(gyptr+index_kp2)        )/2.d0/h
                     dFx_dz =(  -3.d0*q(gxptr+index)
     .                          +4.d0*q(gxptr+index_kp1)
     .                          -1.d0*q(gxptr+index_kp2)        )/2.d0/h
                  else if (k.eq.nz) then
                     ! 2nd order accurate backward difference:
                     dFy_dz =(  +3.d0*q(gyptr+index)
     .                          -4.d0*q(gyptr+index_km1)
     .                          +1.d0*q(gyptr+index_km2)        )/2.d0/h
                     dFx_dz =(  +3.d0*q(gxptr+index)
     .                          -4.d0*q(gxptr+index_km1)
     .                          +1.d0*q(gxptr+index_km2)        )/2.d0/h
                  else
                  dFy_dz =(q(gyptr+index_kp1)-q(gyptr+index_km1))/2.d0/h
                  dFx_dz =(q(gxptr+index_kp1)-q(gxptr+index_km1))/2.d0/h
                  end if
                  !
                  q(cxptr+index) = dFz_dy - dFy_dz
                  q(cyptr+index) = dFx_dz - dFz_dx
                  q(czptr+index) = dFy_dx - dFx_dy
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
        !write(*,99)myid,'grid_curl: Rank not implemented ', rank
         call load_scal1d( q(czptr),    0.d0, nx*ny*nz)
            do j = 1, ny
               do i = 1, nx
                  index     =               (j-1)*nx + (i-1)
                  index_im1 =               (j-1)*nx + (i-1-1)
                  index_im2 =               (j-1)*nx + (i-1-2)
                  index_ip1 =               (j-1)*nx + (i-1+1)
                  index_ip2 =               (j-1)*nx + (i-1+2)
                  index_jm1 =               (j-1-1)*nx + (i-1)
                  index_jm2 =               (j-1-2)*nx + (i-1)
                  index_jp1 =               (j-1+1)*nx + (i-1)
                  index_jp2 =               (j-1+2)*nx + (i-1)
                  !
                  if (i.eq.1) then
                     ! 2nd order accurate forward difference:
                     dFy_dx =(  -3.d0*q(gyptr+index)
     .                          +4.d0*q(gyptr+index_ip1)
     .                          -1.d0*q(gyptr+index_ip2)        )/2.d0/h
                  else if (i.eq.nx) then
                     ! 2nd order accurate backward difference:
                     dFy_dx =(  +3.d0*q(gyptr+index)
     .                          -4.d0*q(gyptr+index_im1)
     .                          +1.d0*q(gyptr+index_im2)        )/2.d0/h
                  else
                  dFy_dx =(q(gyptr+index_ip1)-q(gyptr+index_im1))/2.d0/h
                  end if
                  if (j.eq.1) then
                     ! 2nd order accurate forward difference:
                     dFx_dy =(  -3.d0*q(gxptr+index)
     .                          +4.d0*q(gxptr+index_jp1)
     .                          -1.d0*q(gxptr+index_jp2)        )/2.d0/h
                  else if (j.eq.ny) then
                     ! 2nd order accurate backward difference:
                     dFx_dy =(  +3.d0*q(gxptr+index)
     .                          -4.d0*q(gxptr+index_jm1)
     .                          +1.d0*q(gxptr+index_jm2)        )/2.d0/h
                  else
                  dFx_dy =(q(gxptr+index_jp1)-q(gxptr+index_jm1))/2.d0/h
                  end if
                  !
                  q(czptr+index) = dFy_dx - dFx_dy
                  !
               end do
            end do
      else if (rank .eq. 1) then
        !write(*,99)myid,'grid_curl: Rank not implemented ', rank
               do i = 1, nx
                  index     =               (i-1  )
                  index_im1 =               (i-1-1)
                  index_im2 =               (i-1-2)
                  index_ip1 =               (i-1+1)
                  index_ip2 =               (i-1+2)
                  !
                  if (i.eq.1) then
                     ! 2nd order accurate forward difference:
                     dFx_dx =(  -3.d0*q(gxptr+index)
     .                          +4.d0*q(gxptr+index_ip1)
     .                          -1.d0*q(gxptr+index_ip2)        )/2.d0/h
                  else if (i.eq.nx) then
                     ! 2nd order accurate backward difference:
                     dFx_dx =(  +3.d0*q(gxptr+index)
     .                          -4.d0*q(gxptr+index_im1)
     .                          +1.d0*q(gxptr+index_im2)        )/2.d0/h
                  else
                  dFx_dx =(q(gxptr+index_ip1)-q(gxptr+index_im1))/2.d0/h
                  end if
                  !
                  q(cxptr+index) = dFx_dx
                  !
            end do
      else
        write(*,99)myid,'grid_curl: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_curl: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_curl

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_uncurl:                                                            cc
cc                Translating Louis' code from his vectors.cpp                cc
cc                to here. Will need to be changed appropriately              cc
cc                for AMR and domain-decomposed grids.                        cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_uncurl(gx,gy,gz, cx,cy,cz)
      implicit none
      integer gx,gy,gz, cx,cy,cz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      real*8   dFz_dx, dFz_dy, dFz_dz
      real*8   dFy_dx, dFy_dy, dFy_dz
      real*8   dFx_dx, dFx_dy, dFx_dz
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1, index_ip1jp1,
     *         index_im1kp1, index_im1jp1, index_ip1jm1,index_jm1kp1,
     *         index_jp1km1, index_ip1km1, index_jp1kp1,index_ip1jm1kp1,
     *         index_im1jp1kp1, index_ip1kp1, index_ip1jp1km1,
     *         index_im2, index_jm2, index_km2
      ! pointer to derivatives array D(x,y,z,[xyz],[xyz])
      integer  D, num, 
     .         dAxx, dAyy, dAzz,
     .         dAxy, dAyx,
     .         dAzy, dAyz,
     .         dAxz, dAzx
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   bbox(6), time
      real*8   grid_return_time
      external grid_return_time
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  my_malloc
      external my_malloc
      !
      logical    ltrace
      parameter (ltrace = .false. )
      ! Output the partial derivatives of A (stored in temporary storage):
      logical    dtrace
      parameter (dtrace = .true. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_uncurl: Grids not compatible',gx,gy,gz
         return
      end if

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
      num  = nx*ny*nz
   
      if (ltrace) then
         write(*,99) myid, 'grid_uncurl: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_uncurl: cx/y/z: ',cx,cy,cz
      end if

      ! Pointers to the x/y/z components of magnetic field:
      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)
      !
      ! Pointers to the Ax/y/z components to be computed and stored:
      cxptr = grid_field(cx)
      cyptr = grid_field(cy)
      czptr = grid_field(cz)
      !
      ! Allocate storage for partial derivatives of A:
      !
      D = my_malloc(num*3*3)

      if (rank .eq. 3) then
         ! Initialize Vector potential:
         call load_scal1d( q(cxptr), -0.d0, num)
         call load_scal1d( q(cyptr), -0.d0, num)
         call load_scal1d( q(czptr), -0.d0, num)
         call load_scal1d( q(D),     +0.d0, num*3*3)
         !
         ! Louis uses C-type ordering...the ordering appears
         ! to be quite apparent (but still does not work perfectly):
         !
         !do k = 1, nz-1
            !do j = 1, ny-1
               !do i = 1, nx-1
               do i = 1, nx-1
            do j = 1, ny-1
         do k = 1, nz-1
                  ! Pointers to points relative to (i,j,k):
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  index_im1 = (k-1)*nx*ny + (j-1)*nx + (i-1-1)
                  index_ip1 = (k-1)*nx*ny + (j-1)*nx + (i-1+1)
                  index_jm1 = (k-1)*nx*ny + (j-1-1)*nx + (i-1)
                  index_jp1 = (k-1)*nx*ny + (j-1+1)*nx + (i-1)
                  index_km1 = (k-1-1)*nx*ny + (j-1)*nx + (i-1)
                  index_kp1 = (k-1+1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  index_ip1jp1 = (k-1)*nx*ny + (j  )*nx + (i  )
                  index_im1jp1 = (k-1)*nx*ny + (j  )*nx + (i-2)
                  index_ip1jm1 = (k-1)*nx*ny + (j-2)*nx + (i  )
                  index_ip1kp1 = (k  )*nx*ny + (j-1)*nx + (i  )
                  index_im1kp1 = (k  )*nx*ny + (j-1)*nx + (i-2)
                  index_ip1km1 = (k-2)*nx*ny + (j-1)*nx + (i  )
                  index_jp1kp1 = (k  )*nx*ny + (j  )*nx + (i-1)
                  index_jm1kp1 = (k  )*nx*ny + (j-2)*nx + (i-1)
                  index_jp1km1 = (k-2)*nx*ny + (j  )*nx + (i-1)
                  !
                  index_im1jp1kp1 = (k  )*nx*ny + (j  )*nx + (i-2)
                  index_ip1jm1kp1 = (k  )*nx*ny + (j-2)*nx + (i  )
                  index_ip1jp1km1 = (k-2)*nx*ny + (j  )*nx + (i  )
                  !
                  ! D(A_x,x) = 0*nx*ny*nz+1....1*nx*ny*nz
                  ! D(A_x,y) = 1*nx*ny*nz+1....2*nx*ny*nz
                  ! D(A_x,z) = 2*nx*ny*nz+1....3*nx*ny*nz
                  ! D(A_y,x) = 3*nx*ny*nz+1....4*nx*ny*nz
                  ! D(A_y,y) = 4*nx*ny*nz+1....5*nx*ny*nz
                  ! D(A_y,z) = 5*nx*ny*nz+1....6*nx*ny*nz
                  ! D(A_z,x) = 6*nx*ny*nz+1....7*nx*ny*nz
                  ! D(A_z,y) = 7*nx*ny*nz+1....8*nx*ny*nz
                  ! D(A_z,z) = 8*nx*ny*nz+1....9*nx*ny*nz
                  !
                  dAxx = 0*3*num + 0*num 
                  dAxy = 0*3*num + 1*num
                  dAxz = 0*3*num + 2*num
                  !
                  dAyx = 1*3*num + 0*num
                  dAyy = 1*3*num + 1*num
                  dAyz = 1*3*num + 2*num
                  !
                  dAzx = 2*3*num + 0*num
                  dAzy = 2*3*num + 1*num
                  dAzz = 2*3*num + 2*num
                  !
                  if (i.gt.1 .and. j.eq.1 .and. k.eq.1) then
                     q(cxptr+index) = q(cxptr+index_im1)
     .                                + h*q(dAxx+index_im1)
                  end if
                  if (i.eq.1 .and. j.gt.1 .and. k.eq.1) then
                     q(cyptr+index) = q(cyptr+index_jm1)
     .                                + h*q(dAyy+index_jm1)
                  end if
                  if (i.eq.1 .and. j.eq.1 .and. k.gt.1) then
                     q(czptr+index) = q(czptr+index_km1)
     .                                + h*q(dAzz+index_km1)
                  end if
                  if (i.eq.1) then
                     q(dAyz+index)      = q(dAzy+index) - q(gxptr+index)
                     q(czptr+index_jp1) = q(czptr+index)+h*q(dAzy+index)
                     q(cyptr+index_kp1) = q(cyptr+index)+h*q(dAyz+index)
                  end if
                  if (j.eq.1) then
                     q(dAzx+index)      = q(dAxz+index) - q(gyptr+index)
                     q(cxptr+index_kp1) = q(cxptr+index)+h*q(dAxz+index)
                     q(czptr+index_ip1) = q(czptr+index)+h*q(dAzx+index)
                  end if
                  if (k.eq.1) then
                     q(dAxy+index)      = q(dAyx+index) - q(gzptr+index)
                     q(cyptr+index_ip1) = q(cyptr+index)+h*q(dAyx+index)
                     q(cxptr+index_jp1) = q(cxptr+index)+h*q(dAxy+index)
                  end if
                  !
                  if (i.gt.1) then
                     if (j.eq.1) then
                        q(dAxx+index_im1kp1) = ( q(cxptr+index_kp1)
     .                                       -q(cxptr+index_im1kp1) )/h
                     end if
                     if (k.eq.1) then
                        q(dAxx+index_im1jp1) = ( q(cxptr+index_jp1)
     .                                       -q(cxptr+index_im1jp1) )/h
                     end if
                  end if
                  if (j.gt.1) then
                     if (k.eq.1) then
                        q(dAyy+index_ip1jm1) = ( q(cyptr+index_ip1)
     .                                       -q(cyptr+index_ip1jm1) )/h
                     end if
                     if (i.eq.1) then
                        q(dAyy+index_jm1kp1) = ( q(cyptr+index_kp1)
     .                                -q(cyptr+index_jm1kp1) )/h
                     end if
                  end if
                  if (k.gt.1) then
                     if (i.eq.1) then
                        q(dAzz+index_jp1km1) = ( q(czptr+index_jp1)
     .                                -q(czptr+index_jp1km1) )/h
                     end if
                     if (j.eq.1) then
                        q(dAzz+index_ip1km1) = ( q(czptr+index_ip1)
     .                                -q(czptr+index_ip1km1) )/h
                     end if
                  end if
                  !
                  ! The Loop
                  !
                  q(cxptr+index_jp1kp1) = q(cxptr+index_kp1)
     .                                   +h*q(dAxy+index_kp1)
                  q(dAxz+index_jp1)     =(q(cxptr+index_jp1kp1)
     .                                   -q(cxptr+index_jp1   ) )/h
                  q(dAzx+index_jp1)     = q(dAxz+index_jp1)
     *                                   -q(gyptr+index_jp1)
                  !
                  q(czptr+index_ip1jp1) = q(czptr+index_jp1)
     .                                   +h*q(dAzx+index_jp1)
                  q(dAzy+index_ip1)     =(q(czptr+index_ip1jp1)
     .                                   -q(czptr+index_ip1   ) )/h
                  q(dAyz+index_ip1)     = q(dAzy+index_ip1)
     *                                   -q(gxptr+index_ip1)
                  !
                  q(cyptr+index_ip1kp1) = q(cyptr+index_ip1)
     .                                   +h*q(dAyz+index_ip1)
                  q(dAyx+index_kp1)     =(q(cyptr+index_ip1kp1)
     .                                   -q(cyptr+index_kp1   ) )/h
                  !
                  if (i.gt.1) then
                     q(dAxx+index_im1jp1kp1)=(q(cxptr+index_jp1kp1)
     .                                      -q(cxptr+index_im1jp1kp1))/h
                  end if
                  if (j.gt.1) then
                     q(dAyy+index_ip1jm1kp1)=(q(cyptr+index_ip1kp1)
     .                                      -q(cyptr+index_ip1jm1kp1))/h
                  end if
                  if (k.gt.1) then
                     q(dAzz+index_ip1jp1km1)=(q(czptr+index_ip1jp1)
     .                                      -q(czptr+index_ip1jp1km1))/h
                  end if
                  !
               end do
            end do
         end do
         ! Extrapolate boundary values:
         !    (linear extrapolation for now)
         i = nx
         do j = 1, ny
            do k = 1, nz
               index     = (k-1)*nx*ny + (j-1)*nx + (i-1  )
               index_im1 = (k-1)*nx*ny + (j-1)*nx + (i-1-1)
               index_im2 = (k-1)*nx*ny + (j-1)*nx + (i-1-2)
               q(cxptr+index) = 2.d0*q(cxptr+index_im1)
     .                             - q(cxptr+index_im2)
               q(cyptr+index) = 2.d0*q(cyptr+index_im1)
     .                             - q(cyptr+index_im2)
               q(czptr+index) = 2.d0*q(czptr+index_im1)
     .                             - q(czptr+index_im2)
            end do
         end do
         j = ny
         do i = 1, nx
            do k = 1, nz
               index     = (k-1)*nx*ny + (j-1  )*nx + (i-1)
               index_jm1 = (k-1)*nx*ny + (j-1-1)*nx + (i-1)
               index_jm2 = (k-1)*nx*ny + (j-1-2)*nx + (i-1)
               q(cxptr+index) = 2.d0*q(cxptr+index_jm1)
     .                             - q(cxptr+index_jm2)
               q(cyptr+index) = 2.d0*q(cyptr+index_jm1)
     .                             - q(cyptr+index_jm2)
               q(czptr+index) = 2.d0*q(czptr+index_jm1)
     .                             - q(czptr+index_jm2)
            end do
         end do
         k = nz
         do i = 1, nx
            do j = 1, ny
               index     = (k-1  )*nx*ny + (j-1)*nx + (i-1)
               index_km1 = (k-1-1)*nx*ny + (j-1)*nx + (i-1)
               index_km2 = (k-1-2)*nx*ny + (j-1)*nx + (i-1)
               q(cxptr+index) = 2.d0*q(cxptr+index_km1)
     .                             - q(cxptr+index_km2)
               q(cyptr+index) = 2.d0*q(cyptr+index_km1)
     .                             - q(cyptr+index_km2)
               q(czptr+index) = 2.d0*q(czptr+index_km1)
     .                             - q(czptr+index_km2)
            end do
         end do
      else if (rank .eq. 2) then
        write(*,99)myid,'grid_uncurl: Rank not implemented ', rank
      else if (rank .eq. 1) then
        write(*,99)myid,'grid_uncurl: Rank not implemented ', rank
      else
        write(*,99)myid,'grid_uncurl: Unknown rank', rank
      end if

      if (dtrace) then
!     subroutine field_out3d(field,time, name,mix,max,miy,may,
!    *                       miz,maz,nx,ny,nz,id)
         call grid_return_bbox(gx,  bbox)
         time = grid_return_time(gx)
         call field_out3d(q(dAxx),time,'dAxx', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         call field_out3d(q(dAxy),time,'dAxy', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         call field_out3d(q(dAxz),time,'dAxz', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         !
         call field_out3d(q(dAyx),time,'dAyx', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         call field_out3d(q(dAyy),time,'dAyy', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         call field_out3d(q(dAyz),time,'dAyz', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         !
         call field_out3d(q(dAzx),time,'dAzx', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         call field_out3d(q(dAzy),time,'dAzy', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
         call field_out3d(q(dAzz),time,'dAzz', bbox(1),bbox(2),
     *              bbox(3),bbox(4),bbox(5),bbox(6),nx,ny,nz,myid)
      end if

      ! Free temporary storage:
      call my_free(D, num*3*3)

      if (ltrace) then
         write(*,99) myid, 'grid_uncurl: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_uncurl

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_cross:                                                             cc
cc                    Compute the cross product of two vector fields          cc
cc                    and replace the second vector with the result           cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_cross(gx,gy,gz, cx,cy,cz)
      implicit none
      integer gx,gy,gz, cx,cy,cz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   ax, ay, az, bx, by, bz
      real*8   h
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_cross: Grids not compatible',gx,gy,gz
         return
      end if

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_cross: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_cross: cx/y/z: ',cx,cy,cz
      end if

      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)
      !
      cxptr = grid_field(cx)
      cyptr = grid_field(cy)
      czptr = grid_field(cz)

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  ax = q(gxptr+index)
                  ay = q(gyptr+index)
                  az = q(gzptr+index)
                  !
                  bx = q(cxptr+index)
                  by = q(cyptr+index)
                  bz = q(czptr+index)
                  !
                  q(cxptr+index) = ay * bz - az * by
                  q(cyptr+index) = az * bx - ax * bz
                  q(czptr+index) = ax * by - ay * bx
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
        !write(*,99)myid,'grid_cross: 2D cross producing vector in z-dir'
            do j = 1, ny
               do i = 1, nx
                  index     =               (j-1)*nx + (i-1)
                  !
                  ax = q(gxptr+index)
                  ay = q(gyptr+index)
                  az = q(gzptr+index)
                  !
                  bx = q(cxptr+index)
                  by = q(cyptr+index)
                  bz = q(czptr+index)
                  !
                  q(cxptr+index) = ay * bz - az * by
                  q(cyptr+index) = az * bx - ax * bz
                  q(czptr+index) = ax * by - ay * bx
                  !
               end do
            end do
      else if (rank .eq. 1) then
        write(*,99)myid,'grid_cross: Rank not implemented ', rank
      else
        write(*,99)myid,'grid_cross: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_cross: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_cross

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_grad:                                                              cc
cc                    Compute gradient   of a scalar.                         cc
cc                    (in a Cartesian basis)                                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_grad(gs,gx, gy,gz)
      implicit none
      integer  gs, gx,gy,gz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  gsptr
      integer  gxptr, gyptr, gzptr
      real*8   h, ds_dx, ds_dy, ds_dz
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_grad: Grids not compatible',gx,gy,gz
         return
      end if

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_grad: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_grad: gs:     ',gs
      end if

      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)
      !
      gsptr = grid_field(gs)

      if (rank .eq. 3) then
         do k = 2, nz-1
            do j = 2, ny-1
               do i = 2, nx-1
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  index_im1 = (k-1)*nx*ny + (j-1)*nx + (i-1-1)
                  index_ip1 = (k-1)*nx*ny + (j-1)*nx + (i-1+1)
                  index_jm1 = (k-1)*nx*ny + (j-1-1)*nx + (i-1)
                  index_jp1 = (k-1)*nx*ny + (j-1+1)*nx + (i-1)
                  index_km1 = (k-1-1)*nx*ny + (j-1)*nx + (i-1)
                  index_kp1 = (k-1+1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  ds_dx =(q(gsptr+index_ip1)-q(gsptr+index_im1))/2.d0/h
                  ds_dy =(q(gsptr+index_jp1)-q(gsptr+index_jm1))/2.d0/h
                  ds_dz =(q(gsptr+index_kp1)-q(gsptr+index_km1))/2.d0/h
                  !
                  q(gxptr+index) = ds_dx
                  q(gyptr+index) = ds_dy
                  q(gzptr+index) = ds_dz
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
        !write(*,99)myid,'grid_grad: Rank not implemented ', rank
            do j = 2, ny-1
               do i = 2, nx-1
                  index     = (j-1)*nx + (i-1)
                  index_im1 = (j-1)*nx + (i-1-1)
                  index_ip1 = (j-1)*nx + (i-1+1)
                  index_jm1 = (j-1-1)*nx + (i-1)
                  index_jp1 = (j-1+1)*nx + (i-1)
                  !
                  ds_dx =(q(gsptr+index_ip1)-q(gsptr+index_im1))/2.d0/h
                  ds_dy =(q(gsptr+index_jp1)-q(gsptr+index_jm1))/2.d0/h
                  !
                  q(gxptr+index) = ds_dx
                  q(gyptr+index) = ds_dy
                  q(gzptr+index) = 0.d0
                  !
               end do
            end do
      else if (rank .eq. 1) then
        write(*,99)myid,'grid_grad: Rank not implemented ', rank
      else
        write(*,99)myid,'grid_grad: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_grad: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_grad

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_div:                                                               cc
cc                    Compute divergence of a Cartesian vector.               cc
cc                    (in a Cartesian basis)                                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_div(gs,gx, gy,gz)
      implicit none
      integer  gs, gx,gy,gz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  gsptr
      integer  gxptr, gyptr, gzptr
      real*8   h, dgx_dx, dgy_dy, dgz_dz
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_div: Grids not compatible',gx,gy,gz
         return
      end if

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_div: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_div: gs:     ',gs
      end if

      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)
      !
      gsptr = grid_field(gs)

      if (rank .eq. 3) then
         do k = 2, nz-1
            do j = 2, ny-1
               do i = 2, nx-1
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  index_im1 = (k-1)*nx*ny + (j-1)*nx + (i-1-1)
                  index_ip1 = (k-1)*nx*ny + (j-1)*nx + (i-1+1)
                  index_jm1 = (k-1)*nx*ny + (j-1-1)*nx + (i-1)
                  index_jp1 = (k-1)*nx*ny + (j-1+1)*nx + (i-1)
                  index_km1 = (k-1-1)*nx*ny + (j-1)*nx + (i-1)
                  index_kp1 = (k-1+1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  dgx_dx =(q(gxptr+index_ip1)-q(gxptr+index_im1))/2.d0/h
                  dgy_dy =(q(gyptr+index_jp1)-q(gyptr+index_jm1))/2.d0/h
                  dgz_dz =(q(gzptr+index_kp1)-q(gzptr+index_km1))/2.d0/h
                  !
                  q(gsptr+index) = dgx_dx+dgy_dy+dgz_dz
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
        write(*,99)myid,'grid_div: Rank not implemented ', rank
      else if (rank .eq. 1) then
        write(*,99)myid,'grid_div: Rank not implemented ', rank
      else
        write(*,99)myid,'grid_div: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_div: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_div

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_dot:                                                               cc
cc                    Compute dot product of two vectors                      cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_dot(gx, gy,gz, ggx, ggy, ggz)
      implicit none
      integer  gx,gy,gz, ggx,ggy,ggz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  ggxptr, ggyptr, ggzptr
      integer   gxptr,  gyptr,  gzptr
      real*8   h
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_dot: Grids not compatible',gx,gy,gz
         return
      end if

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_dot: gx/y/z: ',gx,gy,gz
      end if

      gxptr  = grid_field(gx)
      gyptr  = grid_field(gy)
      gzptr  = grid_field(gz)
      !
      ggxptr = grid_field(ggx)
      ggyptr = grid_field(ggy)
      ggzptr = grid_field(ggz)
      !

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(ggxptr+index) = q(gxptr+index)*q(ggxptr+index)
     .                             +q(gyptr+index)*q(ggyptr+index)
     .                             +q(gzptr+index)*q(ggzptr+index)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
        !write(*,99)myid,'grid_div: Rank not implemented ', rank
            do j = 1, ny
               do i = 1, nx
                  index     =               (j-1)*nx + (i-1)
                  !
                  q(ggxptr+index) = q(gxptr+index)*q(ggxptr+index)
     .                             +q(gyptr+index)*q(ggyptr+index)
     .                             +q(gzptr+index)*q(ggzptr+index)
                  !
               end do
            end do
      else if (rank .eq. 1) then
        write(*,99)myid,'grid_div: Rank not implemented ', rank
      else
        write(*,99)myid,'grid_div: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_div: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_div

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_vecmag:                                                            cc
cc                    Compute the magnitude of a vector                       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_vecmag(gx, gy,gz, gmag)
      implicit none
      integer  gx,gy,gz, gmag
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  gmagptr
      integer   gxptr,  gyptr,  gzptr
      real*8   h
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_vecmag: Grids not compatible',gx,gy,gz
         return
      end if

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_vecmag: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_vecmag: gmag:   ',gmag
      end if

      gxptr  = grid_field(gx)
      gyptr  = grid_field(gy)
      gzptr  = grid_field(gz)
      !
      gmagptr = grid_field(gmag)
      !

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gmagptr+index) = q(gxptr+index)**2
     .                              +q(gyptr+index)**2
     .                              +q(gzptr+index)**2
                  q(gmagptr+index) = sqrt(q(gmagptr+index))
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  index     =               (j-1)*nx + (i-1)
                  !
                  q(gmagptr+index) = q(gxptr+index)**2
     .                              +q(gyptr+index)**2
     .                              +q(gzptr+index)**2
                  q(gmagptr+index) = sqrt(q(gmagptr+index))
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  index     =                          (i-1)
                  !
                  q(gmagptr+index) = q(gxptr+index)**2
     .                              +q(gyptr+index)**2
     .                              +q(gzptr+index)**2
                  q(gmagptr+index) = sqrt(q(gmagptr+index))
                  !
               end do
      else
        write(*,99)myid,'grid_vecmag: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_vecmag: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_vecmag


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_cart2sphere:                                                       cc
cc                    Transform from Cartesian basis to Spherical one         cc
cc                 NB: Updated to compute transformation relative to          cc
cc                     an arbitrary origin.                                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_cart2sphere(center, gx,gy,gz, cx,cy,cz,axis)
      implicit none
      real*8  center(3)
      integer gx,gy,gz, cx,cy,cz, axis
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      ! Cartesian vector components:
      real*8   a_x, a_y, a_z
      real*8     x,   y,   z
      real*8    xc,  yc,  zc
      real*8     radius, theta, phi
      real*8     rho, costheta, sintheta, cosphi,sinphi
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      real(kind=8) cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-13)
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) ) then
         write(*,99) myid, 'grid_cart2sphere: Grids not compatible',
     .                     gx,gy,gz
         return
      end if

      ! Use the point passed in the arguments
      ! as the origin:
      xc   = center(1)
      yc   = center(2)
      zc   = center(3)

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gy,2)
      nz   = grid_shape(gz,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_cart2sphere: gx/y/z: ',gx,gy,gz
         write(*,99) myid, 'grid_cart2sphere: cx/y/z: ',cx,cy,cz
         write(*,99) myid, 'grid_cart2sphere: axis:   ',axis
      end if

      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)
      !
      cxptr = grid_field(cx)
      cyptr = grid_field(cy)
      czptr = grid_field(cz)

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  a_x    = q(gxptr+index)
                  a_y    = q(gyptr+index)
                  a_z    = q(gzptr+index)
                  !
                  x      = q(grid_coords(gx)       +(i-1) )
                  y      = q(grid_coords(gx)+nx    +(j-1) )
                  z      = q(grid_coords(gx)+nx+ny +(k-1) )
                  !
                  radius = sqrt((x-xc)**2+(y-yc)**2+(z-zc)**2)
                  rho    = sqrt((x-xc)**2+(y-yc)**2)
                  !radius = sqrt(x**2+y**2+z**2)
                  !rho    = sqrt(x**2+y**2)
                  if (radius .gt. SMALLNUMBER) then
                     theta    = acos((z-zc)/radius)
                     costheta = (z-zc)/radius
                     !theta  = acos(z/radius)
                     !costheta = z/radius
                     sintheta = rho/radius
                  else
                     theta    = 0.d0
                     costheta = 0.d0
                     sintheta = 0.d0
                  end if
                  if (rho .gt. SMALLNUMBER) then
                     cosphi   = (x-xc)/rho
                     sinphi   = (y-yc)/rho
                     !cosphi   = x/rho
                     !sinphi   = y/rho
                  else
                     cosphi   = 0.d0
                     sinphi   = 0.d0
                  end if
                  !
                  ! Radial component:
                  !write(*,88) x,y,z,phi,theta,sinphi,cosphi
                  q(cxptr+index) =   sintheta  *cosphi  *a_x
     *                             + sintheta  *sinphi  *a_y
     *                             + costheta           *a_z
                  ! Theta  component:
                  q(cyptr+index) =   costheta  *cosphi  *a_x
     *                             + costheta  *sinphi  *a_y
     *                             - sintheta           *a_z
                  ! Phi    component:
                     !cosphi   = x/rho
                     !sinphi   = y/rho
                  q(czptr+index) = -            sinphi  *a_x
     *                             +            cosphi  *a_y
                  !q(czptr+index) = 3.33d0
                  if (.false.) then
                  !if (abs(a_x).gt.0.000001.or.abs(a_y).gt.0.00001)then
                  !if (abs(q(czptr+index)).gt.0.0000001) then
!                 write(*,87) x,y,phi,sinphi,cosphi,a_x,a_y,
!    .                              q(czptr+index)
                  write(*,86) ' 2: ',
     *                             -            sinphi  *a_x,
     *                             +            cosphi  *a_y,
     *                             -            sinphi  *a_x
     *                             +            cosphi  *a_y,
     *                             +            sinphi  *a_x
     *                             +            cosphi  *a_y,
     *                                        q(czptr+index)
   86            format(A,G10.3,' ',G10.3,' ',G10.3,' ',G10.3,' ',G10.3)
   87             format(8G10.1)
   88             format(7G10.2)
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         !do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     =               (j-1)*nx + (i-1)
                  !index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  a_x    = q(gxptr+index)
                  a_y    = q(gyptr+index)
                  a_z    = q(gzptr+index)
                  !
                  ! For 2D data, we need to decide how
                  ! to interpret the coordinates:
                  !    if axis==1    x = 0
                  !                  y = first coord
                  !                  z = first coord
                  !    if axis==2    x = first coord
                  !                  y = 0
                  !                  z = first coord
                  !    if axis==3    x = first coord
                  !                  y = first coord
                  !                  z = 0
                  !
                  if (axis.eq.1) then
                     x   = 0.d0
                     y   = q(grid_coords(gx)       +(i-1) )
                     z   = q(grid_coords(gx)+nx    +(j-1) )
                  else if (axis.eq.2) then
                     x   = q(grid_coords(gx)       +(i-1) )
                     y   = 0.d0
                     z   = q(grid_coords(gx)+nx    +(j-1) )
                  else if (axis.eq.3.or.axis.eq.0) then
                     ! Default behavior
                     x   = q(grid_coords(gx)       +(i-1) )
                     y   = q(grid_coords(gx)+nx    +(j-1) )
                     z   = 0.d0
                  else 
                     write(*,99)myid,'grid_cart2sphere:Something wrong',
     .                                  axis
                  end if
                  !
                  radius = sqrt((x-xc)**2+(y-yc)**2+(z-zc)**2)
                  rho    = sqrt((x-xc)**2+(y-yc)**2)
                  if (radius .gt. SMALLNUMBER) then
                     theta    = acos((z-zc)/radius)
                     costheta = (z-zc)/radius
                     sintheta = rho/radius
                  else
                     theta    = 0.d0
                     costheta = 0.d0
                     sintheta = 0.d0
                  end if
                  if (rho .gt. SMALLNUMBER) then
                     cosphi   = (x-xc)/rho
                     sinphi   = (y-yc)/rho
                  else
                     cosphi   = 0.d0
                     sinphi   = 0.d0
                  end if
                  !
                  ! Radial component:
                  !write(*,88) x,y,z,phi,theta,sinphi,cosphi
                  q(cxptr+index) =   sintheta  *cosphi  *a_x
     *                             + sintheta  *sinphi  *a_y
     *                             + costheta           *a_z
                  ! Theta  component:
                  q(cyptr+index) =   costheta  *cosphi  *a_x
     *                             + costheta  *sinphi  *a_y
     *                             - sintheta           *a_z
                  ! Phi    component:
                  q(czptr+index) = -            sinphi  *a_x
     *                             +            cosphi  *a_y
                  if (.false.) then
                     write(*,86) ' 2: ',
     *                             -            sinphi  *a_x,
     *                             +            cosphi  *a_y,
     *                             -            sinphi  *a_x
     *                             +            cosphi  *a_y,
     *                             +            sinphi  *a_x
     *                             +            cosphi  *a_y,
     *                                        q(czptr+index)
                  end if
                  !
               end do
            end do
         !end do
        !write(*,99)myid,'grid_cart2sphere: Rank not implemented ', rank
      else if (rank .eq. 1) then
         !do k = 1, nz
            !do j = 1, ny
               do i = 1, nx
                  index     =                          (i-1)
                  !index     =               (j-1)*nx + (i-1)
                  !index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  a_x    = q(gxptr+index)
                  a_y    = q(gyptr+index)
                  a_z    = q(gzptr+index)
                  !
                  x      = q(grid_coords(gx)       +(i-1) )
                  y      = q(grid_coords(gx)+nx    +(j-1) )
                  y      = 0.d0
                  !z      = q(grid_coords(gx)+nx+ny +(k-1) )
                  z      = 0.d0
                  !
                  radius = sqrt((x-xc)**2+(y-yc)**2+(z-zc)**2)
                  rho    = sqrt((x-xc)**2+(y-yc)**2)
                  if (radius .gt. SMALLNUMBER) then
                     theta    = acos((z-zc)/radius)
                     costheta = (z-zc)/radius
                     sintheta = rho/radius
                  else
                     theta    = 0.d0
                     costheta = 0.d0
                     sintheta = 0.d0
                  end if
                  if (rho .gt. SMALLNUMBER) then
                     cosphi   = (x-xc)/rho
                     sinphi   = (y-yc)/rho
                  else
                     cosphi   = 0.d0
                     sinphi   = 0.d0
                  end if
                  !
                  ! Radial component:
                  !write(*,88) x,y,z,phi,theta,sinphi,cosphi
                  q(cxptr+index) =   sintheta  *cosphi  *a_x
     *                             + sintheta  *sinphi  *a_y
     *                             + costheta           *a_z
                  ! Theta  component:
                  q(cyptr+index) =   costheta  *cosphi  *a_x
     *                             + costheta  *sinphi  *a_y
     *                             - sintheta           *a_z
                  ! Phi    component:
                  q(czptr+index) = -            sinphi  *a_x
     *                             +            cosphi  *a_y
                  if (.false.) then
                     write(*,86) ' 2: ',
     *                             -            sinphi  *a_x,
     *                             +            cosphi  *a_y,
     *                             -            sinphi  *a_x
     *                             +            cosphi  *a_y,
     *                             +            sinphi  *a_x
     *                             +            cosphi  *a_y,
     *                                        q(czptr+index)
                  end if
                  !
               end do
            !end do
         !end do
        !write(*,99)myid,'grid_cart2sphere: Rank not implemented ', rank
      else
        write(*,99)myid,'grid_cart2sphere: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_cart2sphere: Done gx/y/z   = ',gx,gy,gz
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_cart2sphere

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_divide:                                                            cc
cc                    Divide g1 by g2 and store in g1                         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_divide(g1, g2)
      implicit none
      integer  g1, g2
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer   g1ptr,  g2ptr
      real*8   h, denom
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-13)
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(g1,g2) ) then
         write(*,99) myid, 'grid_divide: Grids not compatible',g1,g2
         return
      end if

      rank = grid_return_rank(g1)

      h    = grid_return_resolution(g2)

      nx   = grid_shape(g1,1)
      ny   = grid_shape(g1,2)
      nz   = grid_shape(g1,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_divide: g1/2: ',g1,g2
      end if

      g1ptr  = grid_field(g1)
      g2ptr  = grid_field(g2)
      !

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  denom          = q(g2ptr+index)
                  if (abs(denom).lt.SMALLNUMBER) then
                     q(g1ptr+index) = 0.d0
                  else
                     q(g1ptr+index) = q(g1ptr+index) / denom
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  index     =               (j-1)*nx + (i-1)
                  !
                  denom          = q(g2ptr+index)
                  if (abs(denom).lt.SMALLNUMBER) then
                     q(g1ptr+index) = 0.d0
                  else
                     q(g1ptr+index) = q(g1ptr+index) / denom
                  end if
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  index     =                          (i-1)
                  !
                  denom          = q(g2ptr+index)
                  if (abs(denom).lt.SMALLNUMBER) then
                     q(g1ptr+index) = 0.d0
                  else
                     q(g1ptr+index) = q(g1ptr+index) / denom
                  end if
                  !
               end do
      else
        write(*,99)myid,'grid_divide: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_divide: Done g1/2     = ',g1,g2
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_divide

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_add:                                                               cc
cc                    Add      two registers and place in third               cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_add(g1,g2,go)
      implicit none
      integer  g1, g2, go
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      real*8   dFz_dx, dFz_dy, dFz_dz
      real*8   dFy_dx, dFy_dy, dFy_dz
      real*8   dFx_dx, dFx_dy, dFx_dz
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(g1,g2) .or.
     .    .not.grid_compatible(g1,go) ) then
         write(*,99)myid,'grid_add: Grids not compatible',g1,g2,go
         return
      end if

      rank = grid_return_rank(g1)

      h    = grid_return_resolution(g1)

      nx   = grid_shape(g1,1)
      ny   = grid_shape(g2,2)
      nz   = grid_shape(go,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_add: g1/2/o: ',g1,g2,go
      end if

      gxptr = grid_field(g1)
      gyptr = grid_field(g2)
      gzptr = grid_field(go)

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gzptr+index) = q(gxptr+index) + q(gyptr+index)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         k = 1
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gzptr+index) = q(gxptr+index) + q(gyptr+index)
                  !
               end do
            end do
      else if (rank .eq. 1) then
         k = 1
            j = 1
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gzptr+index) = q(gxptr+index) + q(gyptr+index)
                  !
               end do
      else
        write(*,99)myid,'grid_add: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_add: Done gx/y/z   = ',g1,g2,go
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_add

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_multiply:                                                          cc
cc                    Multiply two registers and place in third               cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_multiply(g1,g2,go)
      implicit none
      integer  g1, g2, go
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      real*8   dFz_dx, dFz_dy, dFz_dz
      real*8   dFy_dx, dFy_dy, dFy_dz
      real*8   dFx_dx, dFx_dy, dFx_dz
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(g1,g2) .or.
     .    .not.grid_compatible(g1,go) ) then
         write(*,99)myid,'grid_multiply: Grids not compatible',g1,g2,go
         return
      end if

      rank = grid_return_rank(g1)

      h    = grid_return_resolution(g1)

      nx   = grid_shape(g1,1)
      ny   = grid_shape(g2,2)
      nz   = grid_shape(go,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_multiply: g1/2/o: ',g1,g2,go
      end if

      gxptr = grid_field(g1)
      gyptr = grid_field(g2)
      gzptr = grid_field(go)

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gzptr+index) = q(gxptr+index) * q(gyptr+index)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         k = 1
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gzptr+index) = q(gxptr+index) * q(gyptr+index)
                  !
               end do
            end do
      else if (rank .eq. 1) then
         k = 1
            j = 1
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  q(gzptr+index) = q(gxptr+index) * q(gyptr+index)
                  !
               end do
      else
        write(*,99)myid,'grid_multiply: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_multiply: Done gx/y/z   = ',g1,g2,go
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_multiply
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_multdv:                                                            cc
cc                    Multiply by product of its grid spacings                cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_multdv(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_multdv: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = dv * q(grid_field(gi)+index)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = dv * q(grid_field(gi)+index)
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  q(grid_field(gi)+index) = dv * q(grid_field(gi)+index)
                  !
               end do
      else
        write(*,*) 'grid_multdv: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_multdv: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_multdv

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_addconstant:                                                       cc
cc                    Add         a constant.                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_addconstant(multiplier,gi)
      implicit none
      real*8  multiplier
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_addconstant: Grid: ',gi,multiplier
      end if
      dv = multiplier

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = dv + q(grid_field(gi)+index)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = dv + q(grid_field(gi)+index)
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  !
                  index = (i-1)
                  q(grid_field(gi)+index) = dv + q(grid_field(gi)+index)
                  !
               end do
      else
        write(*,*) 'grid_addconstant: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_addconstant: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_multconstant

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_multconstant:                                                      cc
cc                    Multiply by a constant.                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_multconstant(multiplier,gi)
      implicit none
      real*8  multiplier
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_multconstant: Grid: ',gi,multiplier
      end if
      dv = multiplier

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = dv * q(grid_field(gi)+index)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = dv * q(grid_field(gi)+index)
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  !
                  index = (i-1)
                  q(grid_field(gi)+index) = dv * q(grid_field(gi)+index)
                  !
               end do
      else
        write(*,*) 'grid_multconstant: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_multconstant: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_multconstant

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_mask:                                                              cc
cc               Convert field to a mask based on value of target+/-epsilon   cc
cc                    Adapted from grid_sqrt.                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_maskOP(mytarget,myepsilon,gi)
      implicit none
      real*8  mytarget, myepsilon
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   lowerbnd, upperbnd, field
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_maskOP: Grid: ',gi,h
      end if

      upperbnd = mytarget + myepsilon
      lowerbnd = mytarget - myepsilon
      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  field = q(grid_field(gi)+index)
                  if (lowerbnd .le.field .and. field.le.upperbnd) then
                     q(grid_field(gi)+index) =1.d0
                  else
                     q(grid_field(gi)+index) =0.d0
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  field = q(grid_field(gi)+index)
                  if (lowerbnd .le.field .and. field.le.upperbnd) then
                     q(grid_field(gi)+index) =1.d0
                  else
                     q(grid_field(gi)+index) =0.d0
                  end if
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  !
                  index = (i-1)
                  field = q(grid_field(gi)+index)
                  if (lowerbnd .le.field .and. field.le.upperbnd) then
                     q(grid_field(gi)+index) =1.d0
                  else
                     q(grid_field(gi)+index) =0.d0
                  end if
                  !
               end do
      else
        write(*,*) 'grid_maskOP: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_maskOP: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_maskOP

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_maskOP2:                                                           cc
cc               Convert field to a mask based on value of target             cc
cc            Differs from maskOP because here anything greater than          cc
cc            than the target gets set to 1.                                  cc
cc                    Adapted from grid_sqrt.                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_maskOP2(mytarget,gi)
      implicit none
      real*8  mytarget
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz, gridptr
      real*8   lowerbnd, upperbnd, field
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_maskOP2: Grid: ',gi, rank
         write(*,98) myid, 'grid_maskOP2: mytarget: ',mytarget
      end if

      gridptr = grid_field(gi)
      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  field = q(gridptr+index)
                  if (field .ge. mytarget) then
                     q(gridptr+index) =1.d0
                  else
                     q(gridptr+index) =0.d0
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  field = q(gridptr+index)
                  if (field .ge. mytarget) then
                     q(gridptr+index) =1.d0
                  else
                     q(gridptr+index) =0.d0
                  end if
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  !
                  index = (i-1)
                  field = q(gridptr+index)
                  if (field .ge. mytarget) then
                     q(gridptr+index) =1.d0
                  else
                     q(gridptr+index) =0.d0
                  end if
                  !
               end do
      else
        write(*,*) 'grid_maskOP: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_maskOP2: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,5F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_maskOP2


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_sqrt:                                                              cc
cc                    Squareroot of a field.                                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_sqrt(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_sqrt: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) =sqrt(q(grid_field(gi)+index))
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) =sqrt(q(grid_field(gi)+index))
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  q(grid_field(gi)+index) =sqrt(q(grid_field(gi)+index))
                  !
               end do
      else
        write(*,*) 'grid_sqrt: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_sqrt: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_sqrt

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_abs:                                                               cc
cc                    Absolute value of a field.                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_abs(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h, tmp
      logical  double_equal
      external double_equal
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_abs: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  q(grid_field(gi)+index) = abs(tmp)
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  q(grid_field(gi)+index) = abs(tmp)
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  q(grid_field(gi)+index) = abs(tmp)
                  !
               end do
      else
        write(*,*) 'grid_abs: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_abs: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_abs

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_floor:                                                             cc
cc                      floor of a field.                                     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_floor(floor,gi)
      implicit none
      real*8  floor
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h, tmp
      logical  double_equal
      external double_equal
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_floor: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (tmp.le.floor) then
                     q(grid_field(gi)+index) = floor
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (tmp.le.floor) then
                     q(grid_field(gi)+index) = floor
                  end if
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (tmp.le.floor) then
                     q(grid_field(gi)+index) = floor
                  end if
                  !
               end do
      else
        write(*,*) 'grid_floor: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_floor: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_floor

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_unfloor:                                                           cc
cc                    unfloor of a field.                                     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_unfloor(floor,gi)
      implicit none
      real*8  floor
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h, tmp
      logical  double_equal
      external double_equal
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_unfloor: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (tmp.le.floor) then
                     q(grid_field(gi)+index) = 0.d0
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (tmp.le.floor) then
                     q(grid_field(gi)+index) = 0.d0
                  end if
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (tmp.le.floor) then
                     q(grid_field(gi)+index) = 0.d0
                  end if
                  !
               end do
      else
        write(*,*) 'grid_unfloor: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_unfloor: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_unfloor

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_inverse:                                                           cc
cc                    Inverse of a field.                                     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_inverse(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h, tmp
      logical  double_equal
      external double_equal
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_inverse: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (double_equal(tmp,0.d0)) then
                     q(grid_field(gi)+index) = -1.d0
                  else
                     q(grid_field(gi)+index) = 1.d0/tmp
                  end if
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (double_equal(tmp,0.d0)) then
                     q(grid_field(gi)+index) = -1.d0
                  else
                     q(grid_field(gi)+index) = 1.d0/tmp
                  end if
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  tmp                     = q(grid_field(gi)+index)
                  if (double_equal(tmp,0.d0)) then
                     q(grid_field(gi)+index) = -1.d0
                  else
                     q(grid_field(gi)+index) = 1.d0/tmp
                  end if
                  !
               end do
      else
        write(*,*) 'grid_inverse: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_inverse: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_inverse

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_arcsin:                                                            cc
cc                    arcsin a field.                                         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_arcsin(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_arcsin: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
            do i = 1, nx
              !
              index = (k-1)*nx*ny + (j-1)*nx + (i-1)
              q(grid_field(gi)+index) = asin(q(grid_field(gi)+index))
              !
            end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
             do i = 1, nx
              !
              index = (j-1)*nx + (i-1)
              q(grid_field(gi)+index) = asin(q(grid_field(gi)+index))
              !
            end do
            end do
      else if (rank .eq. 1) then
         dv = h
           do i = 1, nx
             !
             index = (i-1)
             q(grid_field(gi)+index) = asin(q(grid_field(gi)+index))
             !
           end do
      else
        write(*,*) 'grid_arcsin: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_arcsin: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_arcsin

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_square:                                                            cc
cc                    Square a field.                                         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_square(gi)
      implicit none
      integer gi
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dv, h
      integer  index
      real*8   grid_return_resolution
      external grid_return_resolution
      integer  grid_return_rank
      external grid_return_rank
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)

      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
   
      h  = grid_return_resolution(gi)

      if (ltrace) then
         write(*,99) myid, 'grid_square: Grid: ',gi,h
      end if

      if (rank .eq. 3) then
         dv = h**3
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = q(grid_field(gi)+index)**2
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
         dv = h**2
            do j = 1, ny
               do i = 1, nx
                  !
                  index = (j-1)*nx + (i-1)
                  q(grid_field(gi)+index) = q(grid_field(gi)+index)**2
                  !
               end do
            end do
      else if (rank .eq. 1) then
         dv = h
               do i = 1, nx
                  !
                  index = (i-1)
                  q(grid_field(gi)+index) = q(grid_field(gi)+index)**2
                  !
               end do
      else
        write(*,*) 'grid_square: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_square: Done gi   = ',gi
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_square


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_r:                                                                 cc
cc             Extract the radius of a field.                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_r(g1,go)
      implicit none
      integer  g1, go
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      real*8   x,y,z, r
      real*8   x2,y2,z2, r2
      integer  index, index_ip1, index_im1,
     *                index_jp1, index_jm1,
     *                index_kp1, index_km1
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      !
      logical    ltrace
      parameter (ltrace = .false. )
      logical    ltrace2
      parameter (ltrace2 = .false. )


      if ( .not.grid_compatible(g1,go) ) then
         write(*,99)myid,'grid_r: Grids not compatible',g1,go
         return
      end if

      rank = grid_return_rank(g1)

      h    = grid_return_resolution(g1)

      nx   = grid_shape(g1,1)
      ny   = grid_shape(g1,2)
      nz   = grid_shape(g1,3)
   
      if (ltrace) then
         write(*,99) myid, 'grid_r: g1/o: ',g1,go
         write(*,99) myid, 'grid_r: rank: ',rank
         write(*,99) myid, 'grid_r: nx/y/z: ',nx,ny,nz
      end if

      gxptr = grid_field(g1)
      gzptr = grid_field(go)

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  !x = q(grid_coords(gxptr)+(i-1))
                  !y = q(grid_coords(gxptr)+nx+(j-1))
                  !z = q(grid_coords(gxptr)+nx+ny+(k-1))
                  x2= grid_min(g1,1) + (i-1)*grid_spacings(g1,1)
                  y2= grid_min(g1,2) + (j-1)*grid_spacings(g1,2)
                  z2= grid_min(g1,3) + (k-1)*grid_spacings(g1,3)
                  !r = sqrt(x**2 + y**2 + z**2)
                  r2= sqrt(x2**2 + y2**2 + z2**2)
                  !if(ltrace2)write(*,66)'grid_r: ',x,y,z,r
                  if(ltrace2)write(*,66)'grid_r:2',x2,y2,z2,r2
                  q(gzptr+index) = r2
                  !
               end do
            end do
         end do
 66   format(A,4G17.5)
      else if (rank .eq. 2) then
         k = 1
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  !x = q(grid_coords(gxptr)+(i-1))
                  !y = q(grid_coords(gxptr)+nx+(j-1))
                  x = grid_min(g1,1) + (i-1)*grid_spacings(g1,1)
                  y = grid_min(g1,2) + (j-1)*grid_spacings(g1,2)
                  r = sqrt(x**2 + y**2)
                  q(gzptr+index) = r
                  !
               end do
            end do
      else if (rank .eq. 1) then
         k = 1
            j = 1
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  !x = q(grid_coords(gxptr)+(i-1))
                  x = grid_min(g1,1) + (i-1)*grid_spacings(g1,1)
                  r = sqrt(x**2)
                  q(gzptr+index) = r
                  !
               end do
      else
        write(*,99)myid,'grid_r: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_r: Done g1/o   = ',g1,go
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_r


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_getspeccoords:                                                     cc
cc            Get specific coordinates indicated by indices.                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_getspeccoords(mycoords,gi,i,j,k)
      implicit none
      real*8  mycoords(*)
      integer gi, i,j,k
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  rank
      logical     ltrace
      parameter ( ltrace  = .false. )


      if (ltrace) then
         write(*,9) myid, 'grid_getspeccoords: gi = ',gi
         call grid_dump(gi)
  8      format('[',I3,'] ',A,F18.11)
  9      format('[',I3,'] ',A,3I10)
      end if

      rank = grid_return_rank(gi)

      mycoords(1) = q( grid_coords(gi) + (i-1) )
      mycoords(2) = q( grid_coords(gi) + (j-1)
     .                                 + grid_shape(gi,1) )
      mycoords(3) = q( grid_coords(gi) + (k-1)
     .                                 + grid_shape(gi,2)
     .                                 + grid_shape(gi,1) )

      return
      end           ! END: grid_getspeccoords

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_setspecpoint:                                                      cc
cc            Set the value for a point specified by integer indices.         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_setspecpoint(tmpvalue,gi,i,j,k)
      implicit none
      real*8  tmpvalue
      integer gi, i,j,k
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc' 
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  rank, index, nx,ny,nz
      logical     ltrace
      parameter ( ltrace  = .false. )


      if (ltrace) then
         write(*,9) myid, 'grid_setspecpoint: gi = ',gi
         call grid_dump(gi)
  8      format('[',I3,'] ',A,F18.11)
  9      format('[',I3,'] ',A,3I10)
      end if

      rank = grid_return_rank(gi)
      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
      index= (k-1)*nx*ny + (j-1)*nx + (i-1)

      q(grid_field(gi)+index) = tmpvalue

      return
      end           ! END: grid_setspecpoint

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_calcrho:                                                           cc
cc          Computes part of the helicity on a grid. Takes as input           cc
cc          some particular point, vec{x} and then does the local calc:       cc
cc             rho(xprime)= vec{B}(xprime) cross (vec{x}-vec{xprime})         cc
cc                                              /|vec{x}-vec{xprime}|^3       cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_calcrho(coords,grx,gry,grz,gBx,gBy,gBz)
      implicit none
      real*8   coords(3)
      integer  grx,gry,grz,gBx,gBy,gBz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  grxptr, gryptr, grzptr
      integer  gBxptr, gByptr, gBzptr
      real*8   h
      ! Cartesian vector components:
      real*8   Bx, By, Bz
      real*8   xx, yy, zz
      real*8   xp, yp, zp
      real*8   rx, ry, rz
      real*8     radius, rcubed
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution, grid_l2norm
      external grid_return_resolution, grid_l2norm
      real(kind=8) cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-10)
      !
      logical    ltrace
      parameter (ltrace = .false. )


      if (.not.grid_compatible(grx,gry) .or.
     .    .not.grid_compatible(gry,grz) ) then
         write(*,99) myid, 'grid_calcrho: Grids not compatible',
     .                     grx,gry,grz
         return
      end if

      rank = grid_return_rank(grx)

      h    = grid_return_resolution(grx)

      nx   = grid_shape(grx,1)
      ny   = grid_shape(gry,2)
      nz   = grid_shape(grz,3)
   
      grxptr = grid_field(grx)
      gryptr = grid_field(gry)
      grzptr = grid_field(grz)
      !
      gBxptr = grid_field(gBx)
      gByptr = grid_field(gBy)
      gBzptr = grid_field(gBz)

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  ! Magnetic field components:
                  Bx    = q(gBxptr+index)
                  By    = q(gByptr+index)
                  Bz    = q(gBzptr+index)
                  !
                  ! vec{x}:
                  xx    = coords(1)
                  yy    = coords(2)
                  zz    = coords(3)
                  !
                  ! vec{xprime}:
                  xp     = q(grid_coords(grx)       +(i-1) )
                  yp     = q(grid_coords(grx)+nx    +(j-1) )
                  zp     = q(grid_coords(grx)+nx+ny +(k-1) )
                  !
                  rx     = xx-xp
                  ry     = yy-yp
                  rz     = zz-zp
                  !
                  radius = sqrt(rx**2+ry**2+rz**2)
                  rcubed = radius**3
                  !
                  if (rcubed .gt. SMALLNUMBER) then
                     q(grxptr+index) =  (By * rz - Bz * ry) /rcubed
                     q(gryptr+index) =  (Bz * rx - Bx * rz) /rcubed
                     q(grzptr+index) =  (Bx * ry - By * rx) /rcubed
                  else
                     q(grxptr+index) =  0.d0
                     q(gryptr+index) =  0.d0
                     q(grzptr+index) =  0.d0
                  end if
                  !write(*,90)myid,'calcrho: ',Bx,By,Bz,rx,ry,rz
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
        write(*,99)myid,'grid_calcrho:     Rank not implemented ', rank
      else if (rank .eq. 1) then
        write(*,99)myid,'grid_calcrho:     Rank not implemented ', rank
      else
        write(*,99)myid,'grid_calcrho:     Unknown rank', rank
      end if

      if (ltrace) then
      write(*,93)myid,'grid_calcrho:l2norm|rhox/y/z|:',grid_l2norm(grx),
     *                             grid_l2norm(gry),grid_l2norm(grz)
         write(*,99) myid, 'grid_calcrho: Done grx/y/z   = ',grx,gry,grz
      end if

  90  format('[',I3,'] ',A,6G10.2)
  91  format('[',I3,'] ',A,G10.2)
  93  format('[',I3,'] ',A,3G10.2)
  97  format('[',I3,'] ',A,A)
  98  format('[',I3,'] ',A,F18.11)
  99  format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_calcrho

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  field_out1d:                                                              cc
cc                    Quickly output a field to SDF.                          cc
cc                    Used for debugging, else use grid_output_sdf().         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine field_out1d(field,time, name,mix,max,nx,id)
      implicit      none
      integer       nx, id
      real(kind=8)  field(nx),time
      real(kind=8)  mix, max
      character*(*) name
      !
      character(18) prefix, gft_name
      integer       rank
      parameter (   rank = 1)
      integer       gft_rc, gft_shape(rank), lenpre
      real(kind=8)  bbox(2*rank)

      integer       gft_out_bbox, mystringlength
      external      gft_out_bbox, mystringlength

      logical      ltrace
      parameter (  ltrace = .false. )

      gft_shape(1) = nx

      bbox(1) = mix
      bbox(2) = max

      call int2str(id,prefix)

      lenpre              = mystringlength(prefix)
      gft_name(1:lenpre)  = prefix(1:lenpre)
      gft_name(lenpre+1:) = name

      if (ltrace) then
          write(*,*) 'field_out1d: name:     ',name
          write(*,*) 'field_out1d: id:       ',id
          write(*,*) 'field_out1d: time:     ',time
          write(*,*) 'field_out1d: prefix:   ',prefix
          write(*,*) 'field_out1d: lenpre:   ',lenpre
          write(*,*) 'field_out1d: gft_name: ',gft_name
          write(*,*) 'field_out1d: nx:       ',nx
          write(*,*) 'field_out1d: mi/max:   ',mix,max
      end if

      if (ltrace) write(*,*) 'field_out1d: using default method:'
      gft_rc       = gft_out_bbox(gft_name, time,     gft_shape,
     *                            rank, bbox, field)

      if (ltrace) then
          write(*,*) 'field_out1d: Done.     ',gft_rc
      end if

      return
      end        ! END: field_out1d

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  field_out2d:                                                              cc
cc                    Quickly output a field to SDF.                          cc
cc                    Used for debugging, else use grid_output_sdf().         cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine field_out2d(field,time, name,mix,max,miy,may, 
     *                       nx,ny,id)
      implicit      none
      integer       nx,ny, id
      real(kind=8)  field(nx,ny),time
      real(kind=8)  mix, max, miy, may
      character*(*) name
      !
      character(18) prefix, gft_name
      integer       rank
      parameter (   rank = 2)
      integer       gft_rc, gft_shape(rank), lenpre
      real(kind=8)  bbox(2*rank)

      integer       gft_out_bbox, mystringlength
      external      gft_out_bbox, mystringlength

      logical      ltrace
      parameter (  ltrace = .false. )

      gft_shape(1) = nx
      gft_shape(2) = ny

      bbox(1) = mix
      bbox(2) = max
      bbox(3) = miy
      bbox(4) = may

      call int2str(id,prefix)

      lenpre              = mystringlength(prefix)
      gft_name(1:lenpre)  = prefix(1:lenpre)
      gft_name(lenpre+1:) = name

      if (ltrace) then
          write(*,*) 'field_out2d: name:     ',name
          write(*,*) 'field_out2d: id:       ',id
          write(*,*) 'field_out2d: time:     ',time
          write(*,*) 'field_out2d: prefix:   ',prefix
          write(*,*) 'field_out2d: lenpre:   ',lenpre
          write(*,*) 'field_out2d: gft_name: ',gft_name
      end if

      gft_rc       = gft_out_bbox(gft_name, time,     gft_shape,
     *                            rank, bbox, field)

      return
      end        ! END: field_out2d

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  field_out3d:                                                              cc
cc                    Quickly output a field to SDF.                          cc
cc                    Used for debugging, else use grid_output_sdf().         cc
cc                    Does not record coordinate info, just maps to 0..1.     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine field_out3d(field,time, name,mix,max,miy,may, 
     *                       miz,maz,nx,ny,nz,id)
      implicit      none
      integer       nx,ny,nz, id
      real(kind=8)  field(nx,ny,nz),time
      real(kind=8)  mix, max, miy, may, miz, maz
      character*(*) name
      !
      character(18) prefix, gft_name
      integer       rank
      parameter (   rank = 3)
      integer       gft_rc, gft_shape(rank), lenpre
      real(kind=8)  bbox(2*rank)

      integer       gft_out_bbox, mystringlength
      external      gft_out_bbox, mystringlength

      logical      ltrace
      parameter (  ltrace = .false. )

      gft_shape(1) = nx
      gft_shape(2) = ny
      gft_shape(3) = nz

      bbox(1) = 0.d0
      bbox(2) = 1.d0
      bbox(3) = 0.d0
      bbox(4) = 1.d0
      bbox(5) = 0.d0
      bbox(6) = 1.d0
      bbox(1) = mix
      bbox(2) = max
      bbox(3) = miy
      bbox(4) = may
      bbox(5) = miz
      bbox(6) = maz

      call int2str(id,prefix)

      !Does not work for some reason:
      !gft_name            = prefix//name
      !
      !lenpre              = len(prefix)
      lenpre              = mystringlength(prefix)
      gft_name(1:lenpre)  = prefix(1:lenpre)
      gft_name(lenpre+1:) = name

      if (ltrace) then
          write(*,*) 'field_out3d: name:     ',name
          write(*,*) 'field_out3d: id:       ',id
          write(*,*) 'field_out3d: time:     ',time
          write(*,*) 'field_out3d: prefix:   ',prefix
          write(*,*) 'field_out3d: lenpre:   ',lenpre
          write(*,*) 'field_out3d: gft_name: ',gft_name
      end if

      gft_rc       = gft_out_bbox(gft_name, time,     gft_shape,
     *                            rank, bbox, field)

      return
      end        ! END: field_out3d

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_interp_pt:                                                         cc
cc                  Find interpolated field value at given point              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_interp_pt(gi,myvalue, coords)
      implicit none
      integer  gi
      real*8   myvalue, coords(*)
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      include 'methods.inc'
      integer  rank, length, shape(3), l
      !integer  grid_return_rank
      real*8   bbox(6)
      real*8   h, dx,dy,dz, left, right, mytmp(4),mytmp2(2)
      !real*8   grid_return_resolution
      !external grid_return_resolution
      integer  myindex(3), indexleft, indexright

      logical     ltrace2
      parameter ( ltrace2 = .false. )
      logical     ltrace3
  
      ltrace3 = .false.
      if(ltrace2) then
         write(*,99)myid,'grid_interp_pt: ******** gi= ',gi
         call grid_dump(gi)
         write(*,98)myid,'grid_interp_pt: l2norm(gi): ',grid_l2norm(gi)
      end if

      call grid_return_shape(gi, shape)
      h = grid_return_resolution(gi)
      call grid_return_bbox(gi, bbox)

      rank   = grid_return_rank(gi)
      if(ltrace2) then
         do l = 1, rank
            write(*,*)'     grid_interp_pt: For pt: ',l,coords(l)
         end do
      end if

      myvalue = -1d99

      ! Find nearest points with which to interpolate:
      do l = 1, rank
        myindex(l) = INT((coords(l)-grid_min(gi,l))/h)+1
        !myindex(l) = (coords(l)-grid_min(gi,l))/h+1
        if (myindex(l).lt.1) then
           write(*,*)'grid_interp_pt: index negative, cannot interp',l
           stop
           !return
        else if (myindex(l).gt.shape(l)) then
           write(*,*)'grid_interp_pt: index too large,cannot interp',l
           stop
           !return
        else if (myindex(l).eq.shape(l)) then
           ! Need the "inside" index to interpolate to last point:
           myindex(l) = shape(l)-1
           if (l.eq.1) ltrace3    = .true.
        !else if (myindex(l).eq.1) then
           !ltrace3    = .true.
        end if
      end do
      if(ltrace2 .and. rank.eq. 3) then
         write(*,99)myid,'grid_interp_pt: myindex:',myindex(1),
     .     myindex(2), myindex(3)
         write(*,98)myid,'grid_interp_pt: corr. to',
     .     (myindex(1)-1)*h+grid_min(gi,1),
     .     (myindex(2)-1)*h+grid_min(gi,2),
     .     (myindex(3)-1)*h+grid_min(gi,3)
         write(*,98)myid,'grid_interp_pt: Next  to',
     .     (myindex(1)  )*h+grid_min(gi,1),
     .     (myindex(2)  )*h+grid_min(gi,2),
     .     (myindex(3)  )*h+grid_min(gi,3)
         indexleft  =   (myindex(3)-1)*shape(1)*shape(2)
     .                + (myindex(2)-1)*shape(1)
     .                + (myindex(1)-1)
         write(*,98)myid,'grid_interp_pt: (i,  j,  k  ) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)-1)*shape(1)*shape(2)
     .                + (myindex(2)-1)*shape(1)
     .                + (myindex(1)  )
         write(*,98)myid,'grid_interp_pt: (i+1,j,  k  ) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)-1)*shape(1)*shape(2)
     .                + (myindex(2)  )*shape(1)
     .                + (myindex(1)-1)
         write(*,98)myid,'grid_interp_pt: (i,  j+1,k  ) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)-1)*shape(1)*shape(2)
     .                + (myindex(2)  )*shape(1)
     .                + (myindex(1)  )
         write(*,98)myid,'grid_interp_pt: (i+1,j+1,k  ) value:',
     .                q(grid_field(gi)+indexleft)
         !
         indexleft  =   (myindex(3)  )*shape(1)*shape(2)
     .                + (myindex(2)-1)*shape(1)
     .                + (myindex(1)-1)
         write(*,98)myid,'grid_interp_pt: (i,  j,  k+1) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)  )*shape(1)*shape(2)
     .                + (myindex(2)-1)*shape(1)
     .                + (myindex(1)  )
         write(*,98)myid,'grid_interp_pt: (i+1,j,  k+1) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)  )*shape(1)*shape(2)
     .                + (myindex(2)  )*shape(1)
     .                + (myindex(1)-1)
         write(*,98)myid,'grid_interp_pt: (i,  j+1,k+1) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)  )*shape(1)*shape(2)
     .                + (myindex(2)  )*shape(1)
     .                + (myindex(1)  )
         write(*,98)myid,'grid_interp_pt: (i+1,j+1,k+1) value:',
     .                q(grid_field(gi)+indexleft)
         indexleft  =   (myindex(3)  )*shape(1)*shape(2)
     .                + (myindex(2)  )*shape(1)
     .                + (myindex(1)  )
         write(*,98)myid,'grid_interp_pt: Upper value:',
     .                q(grid_field(gi)+indexleft)
       else if(ltrace2 .and. rank.eq. 2) then
         write(*,99)myid,'grid_interp_pt: myindex:',myindex(1),
     .     myindex(2)
      end if

      !if (rank.lt.3) myindex(3)=1
      !if (rank.lt.2) myindex(2)=1
      if (rank.eq.1) then
         ! linearly interpolate in x-direction to plane:
         !     lower  left: (i, j)
         dx         =  (coords(1) - (myindex(1)-1)*h-grid_min(gi,1))/h
         indexleft  =   (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
         !     lower right: (i+1, j)
         indexright =  (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(1) = (1.d0-dx)*left + (dx)*right
         !
         ! linearly interpolate in x-direction to line:
         myvalue    = mytmp(1)
         !
      else if (rank.eq.2) then
         ! linearly interpolate in x-direction to plane:
         !     lower  left: (i, j)
         dx         =  (coords(1) - (myindex(1)-1)*h-grid_min(gi,1))/h
         indexleft  =   
     .             + (myindex(2)-1)*shape(1)
     .             + (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
         !     lower right: (i+1, j)
         indexright =  
     .             + (myindex(2)-1)*shape(1)
     .             + (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(1) = (1.d0-dx)*left + (dx)*right
      !     upper left: (i,j+1)
         indexleft  =   
     .             + (myindex(2)-1+1)*shape(1)
     .             + (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
      !     upper right: (i+1,j+1)
         indexright =  
     .             + (myindex(2)-1+1)*shape(1)
     .             + (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(3) = (1.d0-dx)*left + (dx)*right
         !
         ! linearly interpolate in y-direction to line:
         !     upper:  (k+1)
         dy         =  (coords(2) - (myindex(2)-1)*h-grid_min(gi,2))/h
         left       = mytmp(1)
         right      = mytmp(3)
         myvalue    = (1.d0-dy)*left + (dy)*right
         !
      else if (rank.eq.3) then            
         ! linearly interpolate in x-direction to plane:
         !     upper  left: (j,k+1)
         dx         =  (coords(1) - (myindex(1)-1)*h-grid_min(gi,1))/h
         indexleft  =   (myindex(3)-1+1)*shape(1)*shape(2)
     .             + (myindex(2)-1)*shape(1)
     .             + (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
         indexright =   (myindex(3)-1+1)*shape(1)*shape(2)
     .             + (myindex(2)-1)*shape(1)
     .             + (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(1) = (1.d0-dx)*left + (dx)*right
         !     upper  right: (j+1,k+1)
         indexleft  =   (myindex(3)-1+1)*shape(1)*shape(2)
     .             + (myindex(2)-1+1)*shape(1)
     .             + (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
         indexright =   (myindex(3)-1+1)*shape(1)*shape(2)
     .             + (myindex(2)-1+1)*shape(1)
     .             + (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(2) = (1.d0-dx)*left + (dx)*right
      !     bottom left: (j,k)
         indexleft  =   (myindex(3)-1)*shape(1)*shape(2)
     .             + (myindex(2)-1)*shape(1)
     .             + (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
         indexright =   (myindex(3)-1)*shape(1)*shape(2)
     .             + (myindex(2)-1)*shape(1)
     .             + (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(3) = (1.d0-dx)*left + (dx)*right
         !     bottom right: (j+1,k)
         indexleft  =   (myindex(3)-1)*shape(1)*shape(2)
     .             + (myindex(2)-1+1)*shape(1)
     .             + (myindex(1)-1)
         left       = q(grid_field(gi)+indexleft)
         indexright =   (myindex(3)-1)*shape(1)*shape(2)
     .             + (myindex(2)-1+1)*shape(1)
     .             + (myindex(1)-1+1)
         right      = q(grid_field(gi)+indexright)
         mytmp(4) = (1.d0-dx)*left + (dx)*right
         !
         ! linearly interpolate in y-direction to line:
         !     upper:  (k+1)
         dy         =  (coords(2) - (myindex(2)-1)*h-grid_min(gi,2))/h
         left       = mytmp(1)
         right      = mytmp(2)
         mytmp2(1)= (1.d0-dy)*left + (dy)*right
         !     bottom: (k)
         left       = mytmp(3)
         right      = mytmp(4)
         mytmp2(2)= (1.d0-dy)*left + (dy)*right
         ! linearly interpolate in z-direction to point:
         dz      =  (coords(3) - (myindex(3)-1)*h-grid_min(gi,3))/h
         right   = mytmp2(1)
         left    = mytmp2(2)
         myvalue = (1.d0-dz)*left + (dz)*right
      end if

         !if(ltrace3) myvalue = 9.d0

      if(ltrace3.and. .false.)then
         write(*,98)myid,'grid_interp_pt: h:      ',h
         write(*,98)myid,'grid_interp_pt: dx/y/z: ',dx,dy
         write(*,98)myid,'grid_interp_pt: coords: ',coords(1),coords(2)
         write(*,98)myid,'grid_interp_pt: bbox::  ',bbox(1),bbox(2),
     .                                              bbox(3),bbox(4)
         write(*,98)myid,'grid_interp_pt: myindex',myindex(1),myindex(2)
         write(*,98)myid,'grid_interp_pt: shape: ',shape(1),shape(2)
         write(*,98)myid,'grid_interp_pt: after x-interp:',mytmp(1),
     .       mytmp(3)
         write(*,98)myid,'grid_interp_pt: Final value: ',myvalue
      end if
      if(ltrace2)then
         write(*,98)myid,'grid_interp_pt: h:      ',h
         write(*,98)myid,'grid_interp_pt: dx/y/z: ',dx,dy,dz
         write(*,98)myid,'grid_interp_pt: after x-interp:',mytmp(1),
     .       mytmp(2),mytmp(3),mytmp(4)
         write(*,98)myid,'grid_interp_pt: after y-interp:',mytmp2(1),
     .       mytmp2(2)
         write(*,98)myid,'grid_interp_pt: Final value: ',myvalue
      end if
      return

  90  format('[',I3,'] ',A,6G10.2)
  91  format('[',I3,'] ',A,G10.2)
  93  format('[',I3,'] ',A,3G10.2)
  97  format('[',I3,'] ',A,A)
  98  format('[',I3,'] ',A,5G14.6)
  99  format('[',I3,'] ',A,5I5)

      end       ! END: grid_interp_pt

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_newcenter:                                                         cc
cc                    Transform the coordinates of a field to those           cc
cc                 about a new center.                                        cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_newcenter(center, gx)
      implicit none
      real*8  center(3)
      integer gx
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  cxptr, cyptr, czptr
      integer  gxptr, gyptr, gzptr
      real*8   h
      ! Cartesian vector components:
      real*8   a_x, a_y, a_z
      real*8     x,   y,   z
      real*8    xc,  yc,  zc
      real*8    bbox(6)
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      real(kind=8) cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-13)
      !
      logical    ltrace
      parameter (ltrace = .false. )


      ! Use the point passed in the arguments
      ! as the origin:
      xc   = center(1)
      yc   = center(2)
      zc   = center(3)

      rank = grid_return_rank(gx)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gx,1)
      ny   = grid_shape(gx,2)
      nz   = grid_shape(gx,3)
   
      gxptr = grid_field(gx)
      call grid_return_bbox(gx, bbox)

      if (ltrace) then
         write(*,99) myid, 'grid_newcenter: gx: ',gx
         write(*,98) myid, 'grid_newcenter: newcenter: ',xc,yc,zc
         write(*,99) myid, 'grid_newcenter: rank: ',rank
         write(*,99) myid, 'grid_newcenter: Original bbox:'
         do i = 1, rank
             write(*,98) myid, 'grid_newcenter: ',bbox(2*i-1),bbox(2*i)
         end do
      end if

      if (rank .eq. 3) then
         bbox(1) = bbox(1) - xc
         bbox(2) = bbox(2) - xc
         bbox(3) = bbox(3) - yc
         bbox(4) = bbox(4) - yc
         bbox(5) = bbox(5) - zc
         bbox(6) = bbox(6) - zc
         if(ltrace)write(*,*)'newcenter: x:',bbox(1),bbox(2)
         if(ltrace)write(*,*)'newcenter: y:',bbox(3),bbox(4)
         if(ltrace)write(*,*)'newcenter: z:',bbox(5),bbox(6)
      else if (rank .eq. 2) then
         bbox(1) = bbox(1) - xc
         bbox(2) = bbox(2) - xc
         bbox(3) = bbox(3) - yc
         bbox(4) = bbox(4) - yc
         if(ltrace)write(*,*)'newcenter: x:',bbox(1),bbox(2)
         if(ltrace)write(*,*)'newcenter: y:',bbox(3),bbox(4)
      else if (rank .eq. 1) then
         bbox(1) = bbox(1) - xc
         bbox(2) = bbox(2) - xc
         if(ltrace)write(*,*)'newcenter: x:',bbox(1),bbox(2)
      else
        write(*,99)myid,'grid_newcenter: Unknown rank', rank
      end if

      call grid_set_bbox(gx, bbox)

      if (ltrace) then
         write(*,99) myid, 'grid_newcenter: Done gx = ',gx
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,5F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_newcenter

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_polar:                                                             cc
cc                    Computes the polar     angle of a field gg              cc
cc                    and store it in field gx.                               cc
cc                NB: derives from grid_radvector.                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_polar(gg,gx)
      implicit none
      integer gg, gx
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  gxptr, gyptr, gzptr, ggptr
      real*8   h, phi, rho
      ! Cartesian vector components:
      real*8   a_x, a_y, a_z
      real*8     x,   y,   z, r
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      real(kind=8) cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-13)
      !
      logical    ltrace
      parameter (ltrace = .false. )

      rank = grid_return_rank(gg)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gg,1)
      ny   = grid_shape(gg,2)
      nz   = grid_shape(gg,3)
   
      if ( .not.grid_compatible(gg,gx) ) then
         write(*,99) myid, 'grid_polar: Grids not compatible',
     .                     gg,gx
         return
      end if

      ggptr = grid_field(gg)
      gxptr = grid_field(gx)

      if (ltrace) then
         write(*,99) myid, 'grid_polar: gg: ',gg
         write(*,99) myid, 'grid_polar: rank: ',rank
         write(*,99) myid, 'grid_polar: Original bbox:'
      end if

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = q(grid_coords(gg)+nx    +(j-1) )
                  z      = q(grid_coords(gg)+nx+ny +(k-1) )
                  !
                  r   = sqrt( x**2 + y**2 + z**2 )
                  rho = sqrt( x**2 + y**2)
                  if (rho .lt. SMALLNUMBER .and. z.gt.SMALLNUMBER) then
                     phi = 0.0d0
                  elseif (rho.lt. SMALLNUMBER.and.z.lt.-SMALLNUMBER)then
                     phi = cpi
                  elseif (abs(z).le.SMALLNUMBER) then
                     phi = 0.5d0*cpi
                  elseif (z.le.-SMALLNUMBER) then
                     phi = cpi + atan(rho/z)
                  else
                     phi = atan(rho/z)
                  end if
                  q(gxptr+index) = phi
                  !
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  index     = (j-1)*nx + (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = q(grid_coords(gg)+nx    +(j-1) )
                  z      = 0.d0
                  !
                  rho = sqrt( x**2 + y**2)
                  if (rho .gt. SMALLNUMBER.and.y.ge.0) then
                     phi = acos(x/rho)
                  elseif (rho .gt. SMALLNUMBER.and.y.lt.0) then
                     phi = cpi + acos(-x/rho)
                  else
                     phi = 0.d0
                  end if
                  q(gxptr+index) = phi
                  !
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  index     = (i-1)
                  q(gxptr+index) = 0.d0
                  !
                  !
               end do
      else
        write(*,99)myid,'grid_polar: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_polar: Done gg = ',gg
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,5F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_polar

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_azimuth:                                                           cc
cc                    Computes the azimuthal angle of a field gg              cc
cc                    and store it in field gx.                               cc
cc                NB: derives from grid_radvector.                            cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_azimuth(gg,gx)
      implicit none
      integer gg, gx
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  gxptr, gyptr, gzptr, ggptr
      real*8   h, phi, rho
      ! Cartesian vector components:
      real*8   a_x, a_y, a_z
      real*8     x,   y,   z, r
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      real(kind=8) cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-13)
      !
      logical    ltrace
      parameter (ltrace = .false. )

      rank = grid_return_rank(gg)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gg,1)
      ny   = grid_shape(gg,2)
      nz   = grid_shape(gg,3)
   
      if ( .not.grid_compatible(gg,gx) ) then
         write(*,99) myid, 'grid_azimuth: Grids not compatible',
     .                     gg,gx
         return
      end if

      ggptr = grid_field(gg)
      gxptr = grid_field(gx)

      if (ltrace) then
         write(*,99) myid, 'grid_azimuth: gg: ',gg
         write(*,99) myid, 'grid_azimuth: rank: ',rank
         write(*,99) myid, 'grid_azimuth: Original bbox:'
      end if

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = q(grid_coords(gg)+nx    +(j-1) )
                  z      = q(grid_coords(gg)+nx+ny +(k-1) )
                  !
                  r   = sqrt( x**2 + y**2 + z**2 )
                  rho = sqrt( x**2 + y**2)
                  if (rho .gt. SMALLNUMBER.and.y.ge.0) then
                     phi = acos(x/rho)
                  elseif (rho .gt. SMALLNUMBER.and.y.lt.0) then
                     phi = cpi + acos(-x/rho)
                  else
                     phi = 0.d0
                  end if
                  q(gxptr+index) = phi
                  !
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  index     = (j-1)*nx + (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = q(grid_coords(gg)+nx    +(j-1) )
                  z      = 0.d0
                  !
                  rho = sqrt( x**2 + y**2)
                  if (rho .gt. SMALLNUMBER.and.y.ge.0) then
                     phi = acos(x/rho)
                  elseif (rho .gt. SMALLNUMBER.and.y.lt.0) then
                     phi = cpi + acos(-x/rho)
                  else
                     phi = 0.d0
                  end if
                  q(gxptr+index) = phi
                  !
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  index     = (i-1)
                  q(gxptr+index) = 0.d0
                  !
                  !
               end do
      else
        write(*,99)myid,'grid_azimuth: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_azimuth: Done gg = ',gg
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,5F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_azimuth

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_radvector:                                                         cc
cc                    Computes the components of the unit radial vector.      cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_radvector(gg,gx,gy,gz)
      implicit none
      integer gg, gx, gy, gz
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      integer  gxptr, gyptr, gzptr, ggptr
      real*8   h
      ! Cartesian vector components:
      real*8   a_x, a_y, a_z
      real*8     x,   y,   z, r
      integer  index
      integer  grid_return_rank
      external grid_return_rank
      logical    grid_compatible
      external   grid_compatible
      real*8   grid_return_resolution
      external grid_return_resolution
      real(kind=8) cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-13)
      !
      logical    ltrace
      parameter (ltrace = .false. )

      rank = grid_return_rank(gg)

      h    = grid_return_resolution(gx)

      nx   = grid_shape(gg,1)
      ny   = grid_shape(gg,2)
      nz   = grid_shape(gg,3)
   
      if (.not.grid_compatible(gx,gy) .or.
     .    .not.grid_compatible(gy,gz) .or.
     .    .not.grid_compatible(gg,gx)          ) then
         write(*,99) myid, 'grid_radvector: Grids not compatible',
     .                     gg,gx,gy,gz
         return
      end if

      ggptr = grid_field(gg)
      gxptr = grid_field(gx)
      gyptr = grid_field(gy)
      gzptr = grid_field(gz)

      if (ltrace) then
         write(*,99) myid, 'grid_radvector: gg: ',gg
         write(*,99) myid, 'grid_radvector: rank: ',rank
         write(*,99) myid, 'grid_radvector: Original bbox:'
      end if

      if (rank .eq. 3) then
         do k = 1, nz
            do j = 1, ny
               do i = 1, nx
                  index     = (k-1)*nx*ny + (j-1)*nx + (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = q(grid_coords(gg)+nx    +(j-1) )
                  z      = q(grid_coords(gg)+nx+ny +(k-1) )
                  !
                  r = sqrt( x**2 + y**2 + z**2 )
                  if (r.gt.SMALLNUMBER) then
                     q(gxptr+index) = x / r
                     q(gyptr+index) = y / r
                     q(gzptr+index) = z / r
                  else
                     q(gxptr+index) = 0.d0
                     q(gyptr+index) = 0.d0
                     q(gzptr+index) = 0.d0
                  end if
                  !
                  !
               end do
            end do
         end do
      else if (rank .eq. 2) then
            do j = 1, ny
               do i = 1, nx
                  index     = (j-1)*nx + (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = q(grid_coords(gg)+nx    +(j-1) )
                  z      = 0.d0
                  !
                  r = sqrt( x**2 + y**2 + z**2 )
                  if (r.gt.SMALLNUMBER) then
                     q(gxptr+index) = x / r
                     q(gyptr+index) = y / r
                  else
                     q(gxptr+index) = 0.d0
                     q(gyptr+index) = 0.d0
                  end if
                  q(gzptr+index) = 0.d0
                  !
                  !
               end do
            end do
      else if (rank .eq. 1) then
               do i = 1, nx
                  index     = (i-1)
                  !
                  x      = q(grid_coords(gg)       +(i-1) )
                  y      = 0.d0
                  z      = 0.d0
                  !
                  r = sqrt( x**2 + y**2 + z**2 )
                  if (r.gt.SMALLNUMBER) then
                     q(gxptr+index) = x / r
                  else
                     q(gxptr+index) = 0.d0
                  end if
                  q(gyptr+index) = 0.d0
                  q(gzptr+index) = 0.d0
                  !
                  !
               end do
      else
        write(*,99)myid,'grid_radvector: Unknown rank', rank
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_radvector: Done gg = ',gg
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,5F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_radvector

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_comhalfplanes:                                                     cc
cc                    Add the appropriate integrals over this grid to         cc
cc                the existing values for the centers of mass.                cc
cc                    The axis argument indicates which axis [1,2, or 3]      cc
cc                over while we split the domain for the two objects.         cc
cc        NOTE:        We need to normalize by the mass to get back           cc
cc                coordinates for the COM. You have to do this                cc
cc                outside this routine when all grids have been dealt with.   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_comhalfplanes(gi,comn,comp,mn,mp,axis)
      implicit none
      integer  gi, axis
      real*8   comn(3),comp(3)
      real*8     mn,     mp
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dx, dy, dz, dV, x,y,z, myr, temp
      real*8   x_ip1,y_jp1, z_kp1
      real*8   temp_i, temp_j, temp_k
      integer  index, indexip1, indexjp1, indexkp1,
     *         indexip1jp1, indexip1kp1, indexjp1kp1,
     *         indexip1jp1kp1
      !
      integer    grid_return_rank
      external   grid_return_rank
      logical    double_equal
      external   double_equal
      !
      logical    ltrace
      parameter (ltrace = .false. )
      logical    ltrace2
      parameter (ltrace2 = .false. )


      rank = grid_return_rank(gi)
      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)

      if (ltrace) then
         write(*,80) gi,q(grid_coords(gi)),q(grid_coords(gi)+nx-1),
     .                  q(grid_coords(gi)+nx),q(grid_coords(gi)+nx+ny-1)
         write(*,80) axis,comn(1),   comn(2),    comp(1),   comp(2)
         !write(*,80) comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
      end if
      if (ltrace2) then
         write(*,*) 'grid_comhalfplanes: gi, dx, r:',gi,
     .                          grid_spacings(gi,1)
         write(*,*) 'grid_comhalfplanes: rank: ',rank
         write(*,*) 'grid_comhalfplanes: axis: ',axis
      end if

      !
      ! Compute volume element:
      !
      dV = 1.d0
      do i = 1, rank
         ! Include factor (1/2) for averaging over i and ip1 points
         !    in each direction:
         dV = dV * grid_spacings(gi,i)*0.5d0
         if(ltrace2)write(*,98) myid, 'grid_integrate: dx   = ',
     .                                grid_spacings(gi,i)
      end do

      if (rank.eq.3) then
         do k = 1, nz-1
         do j = 1, ny-1
         do i = 1, nx-1
            index          = (k-1)*ny*nx + (j-1)*nx + (i-1)
            indexip1       = (k-1)*ny*nx + (j-1)*nx + (i  )
            indexjp1       = (k-1)*ny*nx + (j  )*nx + (i-1)
            indexkp1       = (k  )*ny*nx + (j-1)*nx + (i-1)
            !
            indexip1jp1    = (k-1)*ny*nx + (j  )*nx + (i  )
            indexip1kp1    = (k  )*ny*nx + (j-1)*nx + (i  )
            indexjp1kp1    = (k  )*ny*nx + (j  )*nx + (i-1)
            !
            indexip1jp1kp1 = (k  )*ny*nx + (j  )*nx + (i  )
            !
            x      = q(grid_coords(gi)       +(i-1) )
            y      = q(grid_coords(gi)+nx    +(j-1) )
            z      = q(grid_coords(gi)+nx+ny +(k-1) )
            x_ip1  = q(grid_coords(gi)       +(i  ) )
            y_jp1  = q(grid_coords(gi)+nx    +(j  ) )
            z_kp1  = q(grid_coords(gi)+nx+ny +(k  ) )
            !
            myr    = sqrt(x**2+y**2+z**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED))then
               temp    = dV * (
     *                            q(grid_field(gi)+index         )
     *                          + q(grid_field(gi)+indexip1      )
     *                          + q(grid_field(gi)+indexjp1      )
     *                          + q(grid_field(gi)+indexkp1      )
     *                          + q(grid_field(gi)+indexip1jp1   )
     *                          + q(grid_field(gi)+indexip1kp1   )
     *                          + q(grid_field(gi)+indexjp1kp1   )
     *                          + q(grid_field(gi)+indexip1jp1kp1)
     *                          )
               temp_i  = dV * (
     *                            q(grid_field(gi)+index         )*x
     *                          + q(grid_field(gi)+indexip1      )*x_ip1
     *                          + q(grid_field(gi)+indexjp1      )*x
     *                          + q(grid_field(gi)+indexkp1      )*x
     *                          + q(grid_field(gi)+indexip1jp1   )*x_ip1
     *                          + q(grid_field(gi)+indexip1kp1   )*x_ip1
     *                          + q(grid_field(gi)+indexjp1kp1   )*x    
     *                          + q(grid_field(gi)+indexip1jp1kp1)*x_ip1
     *                          )
               temp_j  = dV * (
     *                            q(grid_field(gi)+index         )*y
     *                          + q(grid_field(gi)+indexip1      )*y
     *                          + q(grid_field(gi)+indexjp1      )*y_jp1
     *                          + q(grid_field(gi)+indexkp1      )*y
     *                          + q(grid_field(gi)+indexip1jp1   )*y_jp1
     *                          + q(grid_field(gi)+indexip1kp1   )*y
     *                          + q(grid_field(gi)+indexjp1kp1   )*y_jp1
     *                          + q(grid_field(gi)+indexip1jp1kp1)*y_jp1
     *                          )
               temp_k  = dV * (
     *                            q(grid_field(gi)+index         )*z
     *                          + q(grid_field(gi)+indexip1      )*z
     *                          + q(grid_field(gi)+indexjp1      )*z
     *                          + q(grid_field(gi)+indexkp1      )*z_kp1
     *                          + q(grid_field(gi)+indexip1jp1   )*z
     *                          + q(grid_field(gi)+indexip1kp1   )*z_kp1
     *                          + q(grid_field(gi)+indexjp1kp1   )*z_kp1
     *                          + q(grid_field(gi)+indexip1jp1kp1)*z_kp1
     *                          )
               if (axis.eq.1) then
                  if (x .lt. 0) then
                     comn(1) = comn(1) + temp_i
                     comn(2) = comn(2) + temp_j
                     comn(3) = comn(3) + temp_k
                     mn      = mn      + temp
                  else
                     comp(1) = comp(1) + temp_i
                     comp(2) = comp(2) + temp_j
                     comp(3) = comp(3) + temp_k
                     mp      = mp      + temp
                  end if
               else if (axis.eq.2) then
                  if (y .lt. 0) then
                     comn(1) = comn(1) + temp_i
                     comn(2) = comn(2) + temp_j
                     comn(3) = comn(3) + temp_k
                     mn      = mn      + temp
                  else
                     comp(1) = comp(1) + temp_i
                     comp(2) = comp(2) + temp_j
                     comp(3) = comp(3) + temp_k
                     mp      = mp      + temp
                  end if
               else if (axis.eq.3) then
                  if (z .lt. 0) then
                     comn(1) = comn(1) + temp_i
                     comn(2) = comn(2) + temp_j
                     comn(3) = comn(3) + temp_k
                     mn      = mn      + temp
                  else
                     comp(1) = comp(1) + temp_i
                     comp(2) = comp(2) + temp_j
                     comp(3) = comp(3) + temp_k
                     mp      = mp      + temp
                  end if
               else 
                  write(*,*)'grid_comhalfplanes: Somethingvery wrong!'
                  write(*,*)'grid_comhalfplanes: axis = ',axis
               end if
            end if
         end do
         end do
         end do
      else if (rank.eq.2) then
         do j = 1, ny-1
         do i = 1, nx-1
            index          =               (j-1)*nx + (i-1)
            indexip1       =               (j-1)*nx + (i  )
            indexjp1       =               (j  )*nx + (i-1)
            !
            indexip1jp1    =               (j  )*nx + (i  )
            !
            x      = q(grid_coords(gi)       +(i-1) )
            y      = q(grid_coords(gi)+nx    +(j-1) )
            x_ip1  = q(grid_coords(gi)       +(i  ) )
            y_jp1  = q(grid_coords(gi)+nx    +(j  ) )
            !
            myr    = sqrt(x**2+y**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED))then
               temp    = dV * (
     *                            q(grid_field(gi)+index         )
     *                          + q(grid_field(gi)+indexip1      )
     *                          + q(grid_field(gi)+indexjp1      )
     *                          + q(grid_field(gi)+indexkp1      )
     *                          )
               temp_i  = dV * (
     *                            q(grid_field(gi)+index         )*x
     *                          + q(grid_field(gi)+indexip1      )*x_ip1
     *                          + q(grid_field(gi)+indexjp1      )*x
     *                          + q(grid_field(gi)+indexkp1      )*x
     *                          )
               temp_j  = dV * (
     *                            q(grid_field(gi)+index         )*y
     *                          + q(grid_field(gi)+indexip1      )*y
     *                          + q(grid_field(gi)+indexjp1      )*y_jp1
     *                          + q(grid_field(gi)+indexkp1      )*y
     *                          )
               if (axis.eq.1) then
                  if (x .lt. 0) then
                     comn(1) = comn(1) + temp_i
                     comn(2) = comn(2) + temp_j
                     comn(3) = comn(3) + temp_k
                     mn      = mn      + temp
                  else
                     comp(1) = comp(1) + temp_i
                     comp(2) = comp(2) + temp_j
                     comp(3) = comp(3) + temp_k
                     mp      = mp      + temp
                  end if
               else if (axis.eq.2) then
                  if (y .lt. 0) then
                     comn(1) = comn(1) + temp_i
                     comn(2) = comn(2) + temp_j
                     comn(3) = comn(3) + temp_k
                     mn      = mn      + temp
                  else
                     comp(1) = comp(1) + temp_i
                     comp(2) = comp(2) + temp_j
                     comp(3) = comp(3) + temp_k
                     mp      = mp      + temp
                  end if
               else if (axis.eq.3) then
                  write(*,*)'grid_comhalfplanes: This value of axis'
                  write(*,*)'grid_comhalfplanes: does not make sense'
                  write(*,*)'grid_comhalfplanes: for 2D data'
                  write(*,*)'grid_comhalfplanes: axis = ',axis
               else 
                  write(*,*)'grid_comhalfplanes: Somethingvery wrong!'
                  write(*,*)'grid_comhalfplanes: axis = ',axis
               end if
            end if
         end do
         end do
      else 
         write(*,99) myid, 'grid_comhalfplanes: Unknown rank'
      end if

      if (ltrace) then
         !write(*,80) comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
         write(*,80) axis, comn(1),   comn(2),    comp(1),   comp(2)
         !write(*,80) axis,comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
      end if
      if (ltrace2) then
         write(*,80) axis,comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
         write(*,99) myid, 'grid_comhalfplanes: gi   = ',gi
         write(*,99) myid, 'grid_comhalfplanes: rank = ',rank
         write(*,99) myid, 'grid_comhalfplanes: nx   = ',nx,ny,nz
         write(*,98) myid, 'grid_comhalfplanes: dV   = ',dV
      end if

  80     format(I2,4G15.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_comhalfplanes


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_moments:                                                           cc
cc                 Compute various moments w/r/t a specific COM               cc
cc      Compute the moments of a given field D, in particular,        cc
cc      the first:                                                    cc
cc                I^j  = \int D x^j d^3x                              cc
cc      and second moments:                                           cc
cc                I^jk = \int D x^j x^k d^3x                          cc
cc      Moments assumed to be packed into a single array:             cc
cc                     (  I^xx    I^xy   I^xz  )                      cc
cc                     (  I^x     I^yy   I^yz  )                      cc
cc                     (  I^y     I^z    I^zz  )                      cc
cc      Adapted from HAD:src/amr/grid3.F->grid_momentfind()           cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_moments(gi,momsn,momsp,comn,comp,mn,mp,axis)
      implicit none
      integer  gi, axis
      real*8   comn(3),comp(3), momsn(3,3),momsp(3,3)
      real*8     mn,     mp
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz
      real*8   dx, dy, dz, dV, x,y,z, myr, temp
      real*8   x_ip1,  y_jp1,  z_kp1
      real*8   xn,     yn,     zn
      real*8   xn_ip1, yn_jp1, zn_kp1
      real*8   xp,     yp,     zp
      real*8   xp_ip1, yp_jp1, zp_kp1
      real*8   temp_i,   temp_j,   temp_k
      real*8   temp_in,  temp_jn,  temp_kn
      real*8   temp_ip,  temp_jp,  temp_kp
      real*8   temp_iin, temp_jjn, temp_kkn
      real*8   temp_iip, temp_jjp, temp_kkp
      real*8   temp_ijp, temp_ikp, temp_jkp
      real*8   temp_ijn, temp_ikn, temp_jkn
      integer  index, indexip1, indexjp1, indexkp1,
     *         indexip1jp1, indexip1kp1, indexjp1kp1,
     *         indexip1jp1kp1
      !
      integer    grid_return_rank
      external   grid_return_rank
      logical    double_equal
      external   double_equal
      !
      logical    wrtorigin
      logical    ltrace
      parameter (ltrace = .false. )
      logical    ltrace2
      parameter (ltrace2 = .false. )


      rank = grid_return_rank(gi)
      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)

      if (abs(comn(1)).lt.1d-12 .and.
     .    abs(comn(2)).lt.1d-12 .and.
     .    abs(comn(3)).lt.1d-12 .and.
     .    abs(comp(1)).lt.1d-12 .and.
     .    abs(comp(2)).lt.1d-12 .and.
     .    abs(comp(3)).lt.1d-12      ) then
         wrtorigin = .true.
         if(ltrace)write(*,*)'grid_moments: wrtorigin!!'
      else
         wrtorigin = .false.
      end if
      if (ltrace) then
!        write(*,80) gi,q(grid_coords(gi)),q(grid_coords(gi)+nx-1),
!    .                  q(grid_coords(gi)+nx),q(grid_coords(gi)+nx+ny-1)
         write(*,80) axis,comn(1),   comn(2),    comp(1),   comp(2)
      end if
      if (ltrace2) then
         write(*,*) 'grid_moments: gi, dx, r:',gi,
     .                          grid_spacings(gi,1)
         write(*,*) 'grid_moments: rank: ',rank
         write(*,*) 'grid_moments: axis: ',axis
      end if

      !
      ! Compute volume element:
      !
      dV = 1.d0
      do i = 1, rank
         ! Include factor (1/2) for averaging over i and ip1 points
         !    in each direction:
         dV = dV * grid_spacings(gi,i)*0.5d0
         if(ltrace2)write(*,98) myid, 'grid_integrate: dx   = ',
     .                                grid_spacings(gi,i)
      end do

      if (rank.eq.3) then
         do k = 1, nz-1
         do j = 1, ny-1
         do i = 1, nx-1
            index          = (k-1)*ny*nx + (j-1)*nx + (i-1)
            indexip1       = (k-1)*ny*nx + (j-1)*nx + (i  )
            indexjp1       = (k-1)*ny*nx + (j  )*nx + (i-1)
            indexkp1       = (k  )*ny*nx + (j-1)*nx + (i-1)
            !
            indexip1jp1    = (k-1)*ny*nx + (j  )*nx + (i  )
            indexip1kp1    = (k  )*ny*nx + (j-1)*nx + (i  )
            indexjp1kp1    = (k  )*ny*nx + (j  )*nx + (i-1)
            !
            indexip1jp1kp1 = (k  )*ny*nx + (j  )*nx + (i  )
            !
            ! Use new coordinates relative to COMs:
            !
            !
            x      = q(grid_coords(gi)       +(i-1) )
            y      = q(grid_coords(gi)+nx    +(j-1) )
            z      = q(grid_coords(gi)+nx+ny +(k-1) )
            x_ip1  = q(grid_coords(gi)       +(i  ) )
            y_jp1  = q(grid_coords(gi)+nx    +(j  ) )
            z_kp1  = q(grid_coords(gi)+nx+ny +(k  ) )
            !
            xn      = q(grid_coords(gi)       +(i-1) )-comn(1)
            yn      = q(grid_coords(gi)+nx    +(j-1) )-comn(2)
            zn      = q(grid_coords(gi)+nx+ny +(k-1) )-comn(3)
            xn_ip1  = q(grid_coords(gi)       +(i  ) )-comn(1)
            yn_jp1  = q(grid_coords(gi)+nx    +(j  ) )-comn(2)
            zn_kp1  = q(grid_coords(gi)+nx+ny +(k  ) )-comn(3)
            !
            xp      = q(grid_coords(gi)       +(i-1) )-comp(1)
            yp      = q(grid_coords(gi)+nx    +(j-1) )-comp(2)
            zp      = q(grid_coords(gi)+nx+ny +(k-1) )-comp(3)
            xp_ip1  = q(grid_coords(gi)       +(i  ) )-comp(1)
            yp_jp1  = q(grid_coords(gi)+nx    +(j  ) )-comp(2)
            zp_kp1  = q(grid_coords(gi)+nx+ny +(k  ) )-comp(3)
            !
            myr    = sqrt(x**2+y**2+z**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED))then
               temp_in  = dV*(
     *                  q(grid_field(gi)+index         )*xn
     *                + q(grid_field(gi)+indexip1      )*xn_ip1
     *                + q(grid_field(gi)+indexjp1      )*xn
     *                + q(grid_field(gi)+indexkp1      )*xn
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1
     *                + q(grid_field(gi)+indexjp1kp1   )*xn
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1
     *                )
               temp_ip  = dV*(
     *                  q(grid_field(gi)+index         )*xp
     *                + q(grid_field(gi)+indexip1      )*xp_ip1
     *                + q(grid_field(gi)+indexjp1      )*xp
     *                + q(grid_field(gi)+indexkp1      )*xp
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1
     *                + q(grid_field(gi)+indexjp1kp1   )*xp
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1
     *                )
               temp_jn  = dV*(
     *                  q(grid_field(gi)+index         )*yn
     *                + q(grid_field(gi)+indexip1      )*yn
     *                + q(grid_field(gi)+indexjp1      )*yn_jp1
     *                + q(grid_field(gi)+indexkp1      )*yn
     *                + q(grid_field(gi)+indexip1jp1   )*yn_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*yn
     *                + q(grid_field(gi)+indexjp1kp1   )*yn_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*yn_jp1
     *                )
               temp_jp  = dV*(
     *                  q(grid_field(gi)+index         )*yp
     *                + q(grid_field(gi)+indexip1      )*yp
     *                + q(grid_field(gi)+indexjp1      )*yp_jp1
     *                + q(grid_field(gi)+indexkp1      )*yp
     *                + q(grid_field(gi)+indexip1jp1   )*yp_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*yp
     *                + q(grid_field(gi)+indexjp1kp1   )*yp_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*yp_jp1
     *                )
               temp_kn  = dV*(
     *                  q(grid_field(gi)+index         )*zn
     *                + q(grid_field(gi)+indexip1      )*zn
     *                + q(grid_field(gi)+indexjp1      )*zn_kp1
     *                + q(grid_field(gi)+indexkp1      )*zn
     *                + q(grid_field(gi)+indexip1jp1   )*zn
     *                + q(grid_field(gi)+indexip1kp1   )*zn_kp1
     *                + q(grid_field(gi)+indexjp1kp1   )*zn_kp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*zn_kp1
     *                )
               temp_kp  = dV*(
     *                  q(grid_field(gi)+index         )*zp
     *                + q(grid_field(gi)+indexip1      )*zp
     *                + q(grid_field(gi)+indexjp1      )*zp
     *                + q(grid_field(gi)+indexkp1      )*zp_kp1
     *                + q(grid_field(gi)+indexip1jp1   )*zp
     *                + q(grid_field(gi)+indexip1kp1   )*zp_kp1
     *                + q(grid_field(gi)+indexjp1kp1   )*zp_kp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*zp_kp1
     *                )
               temp_iin = dV*(
     *                  q(grid_field(gi)+index         )*xn**2
     *                + q(grid_field(gi)+indexip1      )*xn_ip1**2
     *                + q(grid_field(gi)+indexjp1      )*xn**2
     *                + q(grid_field(gi)+indexkp1      )*xn**2
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1**2
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1**2
     *                + q(grid_field(gi)+indexjp1kp1   )*xn**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1**2
     *                )
               temp_iip = dV*(
     *                  q(grid_field(gi)+index         )*xp**2
     *                + q(grid_field(gi)+indexip1      )*xp_ip1**2
     *                + q(grid_field(gi)+indexjp1      )*xp**2
     *                + q(grid_field(gi)+indexkp1      )*xp**2
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1**2
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1**2
     *                + q(grid_field(gi)+indexjp1kp1   )*xp**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1**2
     *                )
               temp_jjn = dV*(
     *                  q(grid_field(gi)+index         )*yn**2
     *                + q(grid_field(gi)+indexip1      )*yn**2
     *                + q(grid_field(gi)+indexjp1      )*yn_jp1**2
     *                + q(grid_field(gi)+indexkp1      )*yn**2
     *                + q(grid_field(gi)+indexip1jp1   )*yn_jp1**2
     *                + q(grid_field(gi)+indexip1kp1   )*yn**2
     *                + q(grid_field(gi)+indexjp1kp1   )*yn_jp1**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*yn_jp1**2
     *                )
               temp_jjp = dV*(
     *                  q(grid_field(gi)+index         )*yp**2
     *                + q(grid_field(gi)+indexip1      )*yp**2
     *                + q(grid_field(gi)+indexjp1      )*yp_jp1**2
     *                + q(grid_field(gi)+indexkp1      )*yp**2
     *                + q(grid_field(gi)+indexip1jp1   )*yp_jp1**2
     *                + q(grid_field(gi)+indexip1kp1   )*yp**2
     *                + q(grid_field(gi)+indexjp1kp1   )*yp_jp1**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*yp_jp1**2
     *                )
               temp_kkn = dV*(
     *                  q(grid_field(gi)+index         )*zn**2
     *                + q(grid_field(gi)+indexip1      )*zn**2
     *                + q(grid_field(gi)+indexjp1      )*zn**2
     *                + q(grid_field(gi)+indexkp1      )*zn_kp1**2
     *                + q(grid_field(gi)+indexip1jp1   )*zn**2
     *                + q(grid_field(gi)+indexip1kp1   )*zn_kp1**2
     *                + q(grid_field(gi)+indexjp1kp1   )*zn_kp1**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*zn_kp1**2
     *                )
               temp_kkp = dV*(
     *                  q(grid_field(gi)+index         )*zp**2
     *                + q(grid_field(gi)+indexip1      )*zp**2
     *                + q(grid_field(gi)+indexjp1      )*zp**2
     *                + q(grid_field(gi)+indexkp1      )*zp_kp1**2
     *                + q(grid_field(gi)+indexip1jp1   )*zp**2
     *                + q(grid_field(gi)+indexip1kp1   )*zp_kp1**2
     *                + q(grid_field(gi)+indexjp1kp1   )*zp_kp1**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*zp_kp1**2
     *                )
               temp_ijn = dV*(
     *                  q(grid_field(gi)+index         )*xn*yn
     *                + q(grid_field(gi)+indexip1      )*xn_ip1*yn
     *                + q(grid_field(gi)+indexjp1      )*xn*yn_jp1
     *                + q(grid_field(gi)+indexkp1      )*xn*yn
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1*yn_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1*yn
     *                + q(grid_field(gi)+indexjp1kp1   )*xn*yn_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1*yn_jp1
     *                )
               temp_ijp = dV*(
     *                  q(grid_field(gi)+index         )*xp*yp
     *                + q(grid_field(gi)+indexip1      )*xp_ip1*yp
     *                + q(grid_field(gi)+indexjp1      )*xp*yp_jp1
     *                + q(grid_field(gi)+indexkp1      )*xp*yp
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1*yp_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1*yp
     *                + q(grid_field(gi)+indexjp1kp1   )*xp*yp_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1*yp_jp1
     *                )
               temp_ikn = dV*(
     *                  q(grid_field(gi)+index         )*xn*zn
     *                + q(grid_field(gi)+indexip1      )*xn_ip1*zn
     *                + q(grid_field(gi)+indexjp1      )*xn*zn
     *                + q(grid_field(gi)+indexkp1      )*xn*zn_kp1
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1*zn
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1*zn_kp1
     *                + q(grid_field(gi)+indexjp1kp1   )*xn*zn_kp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1*zn_kp1
     *                )
               temp_ikp = dV*(
     *                  q(grid_field(gi)+index         )*xp*zp
     *                + q(grid_field(gi)+indexip1      )*xp_ip1*zp
     *                + q(grid_field(gi)+indexjp1      )*xp*zp
     *                + q(grid_field(gi)+indexkp1      )*xp*zp_kp1
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1*zp
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1*zp_kp1
     *                + q(grid_field(gi)+indexjp1kp1   )*xp*zp_kp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1*zp_kp1
     *                )
               temp_jkn = dV*(
     *                  q(grid_field(gi)+index         )*yn*zn
     *                + q(grid_field(gi)+indexip1      )*yn*zn
     *                + q(grid_field(gi)+indexjp1      )*yn_jp1*zn
     *                + q(grid_field(gi)+indexkp1      )*yn*zn_kp1
     *                + q(grid_field(gi)+indexip1jp1   )*yn_jp1*zn
     *                + q(grid_field(gi)+indexip1kp1   )*yn*zn_kp1
     *                + q(grid_field(gi)+indexjp1kp1   )*yn_jp1*zn_kp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*yn_jp1*zn_kp1
     *                )
               temp_jkp = dV*(
     *                  q(grid_field(gi)+index         )*yp*zp
     *                + q(grid_field(gi)+indexip1      )*yp*zp
     *                + q(grid_field(gi)+indexjp1      )*yp_jp1*zp
     *                + q(grid_field(gi)+indexkp1      )*yp*zp_kp1
     *                + q(grid_field(gi)+indexip1jp1   )*yp_jp1*zp
     *                + q(grid_field(gi)+indexip1kp1   )*yp*zp_kp1
     *                + q(grid_field(gi)+indexjp1kp1   )*yp_jp1*zp_kp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*yp_jp1*zp_kp1
     *                )
               if (wrtorigin) then
                     ! just compute using the "n" storage
                     ! and later just duplicate result to "p"
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(3,3) = momsn(3,3) + temp_kkn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(3,2) = momsn(3,2) + temp_kn
                     momsn(1,2) = momsn(1,2) + temp_ijn
                     momsn(1,3) = momsn(1,3) + temp_ikn
                     momsn(2,3) = momsn(2,3) + temp_jkn
               else if (axis.eq.1) then
                  if (x .lt. 0) then
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(3,3) = momsn(3,3) + temp_kkn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(3,2) = momsn(3,2) + temp_kn
                     momsn(1,2) = momsn(1,2) + temp_ijn
                     momsn(1,3) = momsn(1,3) + temp_ikn
                     momsn(2,3) = momsn(2,3) + temp_jkn
                  else
                     momsp(1,1) = momsp(1,1) + temp_iip
                     momsp(2,2) = momsp(2,2) + temp_jjp
                     momsp(3,3) = momsp(3,3) + temp_kkp
                     momsp(2,1) = momsp(2,1) + temp_ip
                     momsp(3,1) = momsp(3,1) + temp_jp
                     momsp(3,2) = momsp(3,2) + temp_kp
                     momsp(1,2) = momsp(1,2) + temp_ijp
                     momsp(1,3) = momsp(1,3) + temp_ikp
                     momsp(2,3) = momsp(2,3) + temp_jkp
                  end if
               else if (axis.eq.2) then
                  if (y .lt. 0) then
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(3,3) = momsn(3,3) + temp_kkn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(3,2) = momsn(3,2) + temp_kn
                     momsn(1,2) = momsn(1,2) + temp_ijn
                     momsn(1,3) = momsn(1,3) + temp_ikn
                     momsn(2,3) = momsn(2,3) + temp_jkn
                  else
                     momsp(1,1) = momsp(1,1) + temp_iip
                     momsp(2,2) = momsp(2,2) + temp_jjp
                     momsp(3,3) = momsp(3,3) + temp_kkp
                     momsp(2,1) = momsp(2,1) + temp_ip
                     momsp(3,1) = momsp(3,1) + temp_jp
                     momsp(3,2) = momsp(3,2) + temp_kp
                     momsp(1,2) = momsp(1,2) + temp_ijp
                     momsp(1,3) = momsp(1,3) + temp_ikp
                     momsp(2,3) = momsp(2,3) + temp_jkp
                  end if
               else if (axis.eq.3) then
                  if (z .lt. 0) then
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(3,3) = momsn(3,3) + temp_kkn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(3,2) = momsn(3,2) + temp_kn
                     momsn(1,2) = momsn(1,2) + temp_ijn
                     momsn(1,3) = momsn(1,3) + temp_ikn
                     momsn(2,3) = momsn(2,3) + temp_jkn
                  else
                     momsp(1,1) = momsp(1,1) + temp_iip
                     momsp(2,2) = momsp(2,2) + temp_jjp
                     momsp(3,3) = momsp(3,3) + temp_kkp
                     momsp(2,1) = momsp(2,1) + temp_ip
                     momsp(3,1) = momsp(3,1) + temp_jp
                     momsp(3,2) = momsp(3,2) + temp_kp
                     momsp(1,2) = momsp(1,2) + temp_ijp
                     momsp(1,3) = momsp(1,3) + temp_ikp
                     momsp(2,3) = momsp(2,3) + temp_jkp
                  end if
               else 
                  write(*,*)'grid_comhalfplanes: Somethingvery wrong!'
                  write(*,*)'grid_comhalfplanes: axis = ',axis
               end if
            end if
         end do
         end do
         end do
      else if (rank.eq.2) then
         do j = 1, ny-1
         do i = 1, nx-1
            index          =               (j-1)*nx + (i-1)
            indexip1       =               (j-1)*nx + (i  )
            indexjp1       =               (j  )*nx + (i-1)
            !
            indexip1jp1    =               (j  )*nx + (i  )
            !
            x      = q(grid_coords(gi)       +(i-1) )
            y      = q(grid_coords(gi)+nx    +(j-1) )
            x_ip1  = q(grid_coords(gi)       +(i  ) )
            y_jp1  = q(grid_coords(gi)+nx    +(j  ) )
            !
            xn      = q(grid_coords(gi)       +(i-1) )-comn(1)
            yn      = q(grid_coords(gi)+nx    +(j-1) )-comn(2)
            xn_ip1  = q(grid_coords(gi)       +(i  ) )-comn(1)
            yn_jp1  = q(grid_coords(gi)+nx    +(j  ) )-comn(2)
            !
            xp      = q(grid_coords(gi)       +(i-1) )-comp(1)
            yp      = q(grid_coords(gi)+nx    +(j-1) )-comp(2)
            xp_ip1  = q(grid_coords(gi)       +(i  ) )-comp(1)
            yp_jp1  = q(grid_coords(gi)+nx    +(j  ) )-comp(2)
            !
            myr    = sqrt(x**2+y**2+z**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED))then
               temp_in  = dV*(
     *                  q(grid_field(gi)+index         )*xn
     *                + q(grid_field(gi)+indexip1      )*xn_ip1
     *                + q(grid_field(gi)+indexjp1      )*xn
     *                + q(grid_field(gi)+indexkp1      )*xn
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1
     *                + q(grid_field(gi)+indexjp1kp1   )*xn
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1
     *                )
               temp_ip  = dV*(
     *                  q(grid_field(gi)+index         )*xp
     *                + q(grid_field(gi)+indexip1      )*xp_ip1
     *                + q(grid_field(gi)+indexjp1      )*xp
     *                + q(grid_field(gi)+indexkp1      )*xp
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1
     *                + q(grid_field(gi)+indexjp1kp1   )*xp
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1
     *                )
               temp_jn  = dV*(
     *                  q(grid_field(gi)+index         )*yn
     *                + q(grid_field(gi)+indexip1      )*yn
     *                + q(grid_field(gi)+indexjp1      )*yn_jp1
     *                + q(grid_field(gi)+indexkp1      )*yn
     *                + q(grid_field(gi)+indexip1jp1   )*yn_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*yn
     *                + q(grid_field(gi)+indexjp1kp1   )*yn_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*yn_jp1
     *                )
               temp_jp  = dV*(
     *                  q(grid_field(gi)+index         )*yp
     *                + q(grid_field(gi)+indexip1      )*yp
     *                + q(grid_field(gi)+indexjp1      )*yp_jp1
     *                + q(grid_field(gi)+indexkp1      )*yp
     *                + q(grid_field(gi)+indexip1jp1   )*yp_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*yp
     *                + q(grid_field(gi)+indexjp1kp1   )*yp_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*yp_jp1
     *                )
               temp_iin = dV*(
     *                  q(grid_field(gi)+index         )*xn**2
     *                + q(grid_field(gi)+indexip1      )*xn_ip1**2
     *                + q(grid_field(gi)+indexjp1      )*xn**2
     *                + q(grid_field(gi)+indexkp1      )*xn**2
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1**2
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1**2
     *                + q(grid_field(gi)+indexjp1kp1   )*xn**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1**2
     *                )
               temp_iip = dV*(
     *                  q(grid_field(gi)+index         )*xp**2
     *                + q(grid_field(gi)+indexip1      )*xp_ip1**2
     *                + q(grid_field(gi)+indexjp1      )*xp**2
     *                + q(grid_field(gi)+indexkp1      )*xp**2
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1**2
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1**2
     *                + q(grid_field(gi)+indexjp1kp1   )*xp**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1**2
     *                )
               temp_jjn = dV*(
     *                  q(grid_field(gi)+index         )*yn**2
     *                + q(grid_field(gi)+indexip1      )*yn**2
     *                + q(grid_field(gi)+indexjp1      )*yn_jp1**2
     *                + q(grid_field(gi)+indexkp1      )*yn**2
     *                + q(grid_field(gi)+indexip1jp1   )*yn_jp1**2
     *                + q(grid_field(gi)+indexip1kp1   )*yn**2
     *                + q(grid_field(gi)+indexjp1kp1   )*yn_jp1**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*yn_jp1**2
     *                )
               temp_jjp = dV*(
     *                  q(grid_field(gi)+index         )*yp**2
     *                + q(grid_field(gi)+indexip1      )*yp**2
     *                + q(grid_field(gi)+indexjp1      )*yp_jp1**2
     *                + q(grid_field(gi)+indexkp1      )*yp**2
     *                + q(grid_field(gi)+indexip1jp1   )*yp_jp1**2
     *                + q(grid_field(gi)+indexip1kp1   )*yp**2
     *                + q(grid_field(gi)+indexjp1kp1   )*yp_jp1**2
     *                + q(grid_field(gi)+indexip1jp1kp1)*yp_jp1**2
     *                )
               temp_ijn = dV*(
     *                  q(grid_field(gi)+index         )*xn*yn
     *                + q(grid_field(gi)+indexip1      )*xn_ip1*yn
     *                + q(grid_field(gi)+indexjp1      )*xn*yn_jp1
     *                + q(grid_field(gi)+indexkp1      )*xn*yn
     *                + q(grid_field(gi)+indexip1jp1   )*xn_ip1*yn_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*xn_ip1*yn
     *                + q(grid_field(gi)+indexjp1kp1   )*xn*yn_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*xn_ip1*yn_jp1
     *                )
               temp_ijp = dV*(
     *                  q(grid_field(gi)+index         )*xp*yp
     *                + q(grid_field(gi)+indexip1      )*xp_ip1*yp
     *                + q(grid_field(gi)+indexjp1      )*xp*yp_jp1
     *                + q(grid_field(gi)+indexkp1      )*xp*yp
     *                + q(grid_field(gi)+indexip1jp1   )*xp_ip1*yp_jp1
     *                + q(grid_field(gi)+indexip1kp1   )*xp_ip1*yp
     *                + q(grid_field(gi)+indexjp1kp1   )*xp*yp_jp1
     *                + q(grid_field(gi)+indexip1jp1kp1)*xp_ip1*yp_jp1
     *                )
            !
               if (wrtorigin) then
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(1,2) = momsn(1,2) + temp_ijn
               elseif (axis.eq.1) then
                  if (x .lt. 0) then
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(1,2) = momsn(1,2) + temp_ijn
                  else
                     momsp(1,1) = momsp(1,1) + temp_iip
                     momsp(2,2) = momsp(2,2) + temp_jjp
                     momsp(2,1) = momsp(2,1) + temp_ip
                     momsp(3,1) = momsp(3,1) + temp_jp
                     momsp(1,2) = momsp(1,2) + temp_ijp
                  end if
               else if (axis.eq.2) then
                  if (y .lt. 0) then
                     momsn(1,1) = momsn(1,1) + temp_iin
                     momsn(2,2) = momsn(2,2) + temp_jjn
                     momsn(2,1) = momsn(2,1) + temp_in
                     momsn(3,1) = momsn(3,1) + temp_jn
                     momsn(1,2) = momsn(1,2) + temp_ijn
                  else
                     momsp(1,1) = momsp(1,1) + temp_iip
                     momsp(2,2) = momsp(2,2) + temp_jjp
                     momsp(2,1) = momsp(2,1) + temp_ip
                     momsp(3,1) = momsp(3,1) + temp_jp
                     momsp(1,2) = momsp(1,2) + temp_ijp
                  end if
               else if (axis.eq.3) then
                  write(*,*)'grid_comhalfplanes: This value of axis'
                  write(*,*)'grid_comhalfplanes: does not make sense'
                  write(*,*)'grid_comhalfplanes: for 2D data'
                  write(*,*)'grid_comhalfplanes: axis = ',axis
               else 
                  write(*,*)'grid_comhalfplanes: Somethingvery wrong!'
                  write(*,*)'grid_comhalfplanes: axis = ',axis
               end if
            end if
         end do
         end do
      else 
         write(*,99) myid, 'grid_comhalfplanes: Unknown rank'
      end if

      if (wrtorigin) then
         if(ltrace)write(*,*)'moments: w/r/t origin'
                     momsp(1,1) = momsn(1,1)
                     momsp(2,2) = momsn(2,2)
                     momsp(3,3) = momsn(3,3)
                     momsp(2,1) = momsn(2,1)
                     momsp(3,1) = momsn(3,1)
                     momsp(3,2) = momsn(3,2)
                     momsp(1,2) = momsn(1,2)
                     momsp(1,3) = momsn(1,3)
                     momsp(2,3) = momsn(2,3)
      end if
      if (ltrace) then
         !write(*,80) comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
         write(*,80) axis, comn(1),   comn(2),    comp(1),   comp(2)
         !write(*,80) axis,comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
      end if
      if (ltrace2) then
         !write(*,80) axis,comn(1)/mn,comn(2)/mn, comp(1)/mp,comp(2)/mp
         write(*,99) myid, 'grid_comhalfplanes: gi   = ',gi
         write(*,99) myid, 'grid_comhalfplanes: rank = ',rank
         write(*,99) myid, 'grid_comhalfplanes: nx   = ',nx,ny,nz
         write(*,98) myid, 'grid_comhalfplanes: dV   = ',dV
      end if

  80     format(I4,4G15.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_moments

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_histo:                                                             cc
cc                    Return the integral of all points not masked off        cc
cc                but add the result into the appropriate bin of the          cc
cc                histogram. Essentially doing a bunch of integrals,          cc
cc                where the contribution gets binned based on a second field. cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_histo(gi,gi2,hist,bins,nbins)
      implicit none
      integer  gi,nbins, gi2
      real*8   hist(nbins),bins(nbins)
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      integer  i,  j,  k, rank
      integer  nx, ny, nz, mybin
      integer  nx2,ny2,nz2,rank2
      integer  gridptr, gridptr2
      real*8   dx, dy, dz, dV, x,y,z, myr, contrib
      real*8   binvalue
      integer  index, indexip1, indexjp1, indexkp1,
     *         indexip1jp1, indexip1kp1, indexjp1kp1,
     *         indexip1jp1kp1
      !
      integer    grid_return_rank
      external   grid_return_rank
      logical    double_equal
      external   double_equal
      !
      logical    ltrace
      parameter (ltrace = .false. )


      rank = grid_return_rank(gi)
      nx   = grid_shape(gi,1)
      ny   = grid_shape(gi,2)
      nz   = grid_shape(gi,3)
      rank2= grid_return_rank(gi2)
      nx2  = grid_shape(gi2,1)
      ny2  = grid_shape(gi2,2)
      nz2  = grid_shape(gi2,3)

      if (rank.ne.rank2) then
         write(*,*) 'grid_histo: Grids do not have same rank',rank,rank2
         return
      else
         do i = 1, rank
            if (grid_shape(gi,i).ne.grid_shape(gi2,i)) then
               write(*,*) 'grid_histo: Not same dimension:',i,
     .                   grid_shape(gi,i),grid_shape(gi2,i)
               return
            end if
         end do
      end if

      if (ltrace) then
         write(*,*) 'grid_histo: gi, dx:',gi,grid_spacings(gi,1)
         write(*,*) 'grid_histo: nbins: ',nbins
         do i = 1, nbins
            write(*,*) 'grid_histo: Bin: ',i,bins(i),bins(i+1),hist(i)
         end do
      end if

      !
      ! Compute volume element:
      !
      dV = 1.d0
      do i = 1, rank
         dV = dV * grid_spacings(gi,i)*0.5d0
      end do

      !grid_integrate = 0.d0
      ! No need to initialize to zero, since other grids will contribute:

      gridptr  = grid_field(gi)
      gridptr2 = grid_field(gi2)
      if (rank.eq.3) then
         do k = 1, nz-1
         do j = 1, ny-1
         do i = 1, nx-1
            index          = (k-1)*ny*nx + (j-1)*nx + (i-1)
            indexip1       = (k-1)*ny*nx + (j-1)*nx + (i  )
            indexjp1       = (k-1)*ny*nx + (j  )*nx + (i-1)
            indexkp1       = (k  )*ny*nx + (j-1)*nx + (i-1)
            !
            indexip1jp1    = (k-1)*ny*nx + (j  )*nx + (i  )
            indexip1kp1    = (k  )*ny*nx + (j-1)*nx + (i  )
            indexjp1kp1    = (k  )*ny*nx + (j  )*nx + (i-1)
            !
            indexip1jp1kp1 = (k  )*ny*nx + (j  )*nx + (i  )
            !
            !x      = q(grid_coords(gi)       +(i-1) )
            !y      = q(grid_coords(gi)+nx    +(j-1) )
            !z      = q(grid_coords(gi)+nx+ny +(k-1) )
            !
            !myr    = sqrt(x**2+y**2+z**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED))then
               contrib = dV * 
     *                        (   q(gridptr+index         )
     *                          + q(gridptr+indexip1      )
     *                          + q(gridptr+indexjp1      )
     *                          + q(gridptr+indexkp1      )
     *                          + q(gridptr+indexip1jp1   )
     *                          + q(gridptr+indexip1kp1   )
     *                          + q(gridptr+indexjp1kp1   )
     *                          + q(gridptr+indexip1jp1kp1)
     *                          )
               binvalue = q(gridptr2+index)
               ! find where to add the contribution:
               if (binvalue .lt. bins(1) .or. 
     *             binvalue .gt. bins(nbins) ) then
                  ! can ignore this value
               else
                  mybin = INT((binvalue-bins(1))/(bins(2)-bins(1)))+1
                  if (mybin.ge.1 .and. mybin .le.nbins) then
                     hist(mybin) = hist(mybin) + contrib
                  else
                     write(*,*)'grid_histo: big problem, mybin=',mybin
                  end if
               end if
               !
            end if
         end do
         end do
         end do
      else if (rank.eq.2) then
         do j = 1, ny-1
         do i = 1, nx-1
            index          =               (j-1)*nx + (i-1)
            indexip1       =               (j-1)*nx + (i  )
            indexjp1       =               (j  )*nx + (i-1)
            !
            indexip1jp1    =               (j  )*nx + (i  )
            !
            !x      = q(grid_coords(gi)       +(i-1) )
            !y      = q(grid_coords(gi)+nx    +(j-1) )
            !
            !myr    = sqrt(x**2+y**2)
            !
            if (NINT(q(grid_mask(gi)+index)).eq.NINT(GRID_UNMASKED))then
               contrib = dV * 
     *                        (   q(gridptr+index         )
     *                          + q(gridptr+indexip1      )
     *                          + q(gridptr+indexjp1      )
     *                          + q(gridptr+indexip1jp1   )
     *                          )
               binvalue = q(gridptr2+index)
               if(ltrace)write(*,*)'grid_histo: binvalue:',binvalue
               ! find where to add the contribution:
               if (binvalue .lt. bins(1) .or. 
     *             binvalue .gt. bins(nbins) ) then
                  ! can ignore this value
               else
                  mybin = INT((binvalue-bins(1))/(bins(2)-bins(1)))+1
                  if(ltrace)then
                      write(*,*)'grid_histo: mybin',mybin
                      write(*,*)'grid_histo: binvalue-bins(1):',
     *                   binvalue-bins(1)
                      write(*,*)'grid_histo: bins(2)-bins(1):',
     *                   bins(2)-bins(1)
                  end if
                  if (mybin.ge.1 .and. mybin .le.nbins) then
                     hist(mybin) = hist(mybin) + contrib
                  else
                     write(*,*)'grid_histo: big problem, mybin=',mybin
                  end if
               end if
               !
            end if
         end do
         end do
      else 
         write(*,99) myid, 'grid_histo: Unknown rank'
      end if

      if (ltrace) then
         write(*,99) myid, 'grid_histo: gi   = ',gi
         write(*,99) myid, 'grid_histo: gi2  = ',gi2
         write(*,99) myid, 'grid_histo: rank = ',rank
         write(*,99) myid, 'grid_histo: nx   = ',nx,ny,nz
         write(*,98) myid, 'grid_histo: dV   = ',dV
         write(*,*) 'grid_histo: nbins: ',nbins
         do i = 1, nbins
            write(*,*) 'grid_histo: Bin: ',i,bins(i),bins(i+1),hist(i)
         end do
         write(*,98) myid, 'grid_histo: Done.'
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

      return
      end          ! END: grid_histo

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_interp_twotrees:                                                   cc
cc                  At each grid point, interpolate from two different trees  cc
cc             and operate on both of them w/ user-inputted operation.        cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_interp_twotrees(gi,tree1,tree2,operation)
      implicit none
      integer  gi,tree1,tree2,operation
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'grids.inc'
      include 'mem.inc'
      include 'methods.inc'
      include 'variables.inc'
      real*8   value1, value2, bbox(6), coords(3)
      integer  i,j,k, rank, shape(3), numpoints
      integer  tg1, tg2, gridptr
     

      logical     ltrace2
      parameter ( ltrace2 = .false. )
      logical     ltrace3
      parameter ( ltrace3 = .false. )
  
      numpoints =  grid_return_numpoints(gi)
      call grid_return_bbox(gi, bbox)
      gridptr   = grid_get_field(gi)
      if(ltrace2) then
         write(*,99)myid,'grid_interp_twotrees: args: ',
     .                             gi,tree1,tree2,operation
         write(*,99)myid,'grid_interp_twotrees: numpts:',numpoints
      end if

      do i = 1, numpoints
         !
         if(ltrace3)write(*,*)'finding coords:'
         call grid_return_coordspt(gi,i, coords)
         if(ltrace3)then
            write(*,*)'coords:',coords(1),coords(2),coords(3)
         end if
         !
         ! Initialize point to 0 and only update
         ! if we can find the point in both trees:
         !
         q(gridptr + i-1) = 0.d0
         if(ltrace3)write(*,*)'finding first grid'
         tg1 =  tree_findgridwpoint(tree1,coords)
         if (grid_is_local(tg1)) then
            if(ltrace3)write(*,*)'grid_interp_pt 1 ',tg1
            call grid_interp_pt(tg1, value1, coords)
            !
            if(ltrace3)write(*,*)'finding second grid'
            tg2 =  tree_findgridwpoint(tree2,coords)
            if (grid_is_local(tg2)) then
               if(ltrace3)write(*,*)'grid_interp_pt 2 ',tg2
               call grid_interp_pt(tg2, value2, coords)
               !
               if (operation.eq.OP_SUBTRACTGEN) then
                  q(gridptr + i-1) = value1 - value2
                  !q(gridptr + i-1) = 1.d0
                  !q(gridptr + i-1) = value1
               else
                  write(*,99)myid,'grid_interp_twotrees:Op not '
                  stop
               end if
            end if
         end if
         ! Just for debugging:
         !write(*,*) coords(1),bbox(2)
         !if (double_equal(coords(1),bbox(2))) then
         !       q(gridptr + i-1) = 1.2d0
         !       write(*,*) '***************'
         !       write(*,*) 'setting to 1.2:',coords(1),bbox(2)
         !end if
      end do

      if(ltrace2) then
         write(*,99)myid,'grid_interp_twotrees: Done. ',
     .                             gi,tree1,tree2,operation
      end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I9)

      return
      end          ! END: grid_interp_twotrees

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    grid_return_coordspt:                                                   cc
cc               Return the coordinates of a point denoted by an integer      cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine grid_return_coordspt(grid, num, coords)
      implicit none
      include 'class_constants.inc' 
      include 'grids.inc'
      include 'mem.inc'
         integer grid, num, numcheck
         real*8  coords(*)
         integer rank, i,j,k, shape(3), coordsptr
         integer tmpint
         logical     ltrace 
         parameter ( ltrace = .false. )
         integer  grid_return_rank, grid_get_coords
         external grid_return_rank, grid_get_coords
      logical   double_equal
      external  double_equal
         real*8  bbox(6)
     
      call grid_return_bbox(grid, bbox)
      rank   = grid_return_rank(grid)
      call grid_return_shape(grid, shape)
      tmpint = shape(1)*shape(2)
      i = 1
      j = 1
      k = 1
      if (rank .eq. 1) then
         i         = num
      else if (rank .eq. 2) then
         !i         = mod(num,shape(1))
         j         = int((num-1)/shape(1)) + 1
         !j         = int(num/shape(1)) + 1
         i         = num - (j-1)*shape(1)
      else if (rank .eq. 3) then
         ! Untested:
         !k         = int(num/tmpint) + 1
         k         = int((num-1)/tmpint) + 1
         j         = int(num-(k-1)*tmpint/shape(1)) + 1
         i         = num - (k-1)*tmpint - (j-1)*shape(1)
      end if
      numcheck = (k-1)*tmpint +(j-1)*shape(1)+i
      if (numcheck .ne. num) then
         write(*,*) 'coordspt: Problem',numcheck,num
         stop
      end if
      coordsptr = grid_get_coords(grid)
      coords(1) = q(coordsptr + i                       -1)
      coords(2) = q(coordsptr + shape(1) + j            -1)
      coords(3) = q(coordsptr + shape(1) + shape(2) + k -1)

      !write(*,*) 'coordspt:',num,i,shape(1),coords(1),bbox(2)
      if (ltrace) then
         write(*,*) 'coordspt:',rank,num,i,j,k
         write(*,*) 'coordspt:coords:',coords(1),coords(2),coords(3)
      end if
      !if (i.eq.shape(1).or.j.eq.shape(2)) then
         !write(*,*)'coordspt:',num,i,shape(1),j,shape(2),numcheck
      !end if
      if (i.eq.1) then
         if (.not.double_equal(coords(1),bbox(1))) then
             write(*,*) 'coordspt:WRONG small x:',coords(1),bbox(1)
            write(*,*) 'coordspt:',rank,num,i,j,k
            write(*,*) 'coordspt:coords:',coords(1),coords(2),coords(3)
            write(*,*) 'coordspt:shape:',shape(1),shape(2),shape(3)
            stop
         end if
      end if
      if (i.eq.shape(1)) then
         if (.not.double_equal(coords(1),bbox(2))) then
             write(*,*) 'coordspt:WRONG big x:',coords(1),bbox(1)
            write(*,*) 'coordspt:',rank,num,i,j,k
            write(*,*) 'coordspt:coords:',coords(1),coords(2),coords(3)
            write(*,*) 'coordspt:shape:',shape(1),shape(2),shape(3)
            stop
         end if
      end if
      if (j.eq.1) then
         if (.not.double_equal(coords(2),bbox(3))) then
             write(*,*) 'coordspt:WRONG small y:',coords(2),bbox(3)
            write(*,*) 'coordspt:',rank,num,i,j,k
            write(*,*) 'coordspt:coords:',coords(1),coords(2),coords(3)
            write(*,*) 'coordspt:shape:',shape(1),shape(2),shape(3)
            stop
         end if
      end if
      if (j.eq.shape(2)) then
         if (.not.double_equal(coords(2),bbox(4))) then
             write(*,*) 'coordspt:WRONG big y:',coords(2),bbox(4)
            write(*,*) 'coordspt:',rank,num,i,j,k
            write(*,*) 'coordspt:coords:',coords(1),coords(2),coords(3)
            write(*,*) 'coordspt:shape:',shape(1),shape(2),shape(3)
            stop
         end if
      end if

         return
      end ! END: grid_return_coordspt


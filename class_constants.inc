      !
      ! Constants for sdf classes(registers, trees, levels, grids)
      !

      integer      MAX_NUM_REG
      integer      MAX_NUM_TREES
      integer      MAX_NUM_LEVELS
      integer      MAX_NUM_GRIDS
      integer      NULL_REGISTER
      integer      NULL_TREE
      integer      NULL_LEVEL
      integer      NULL_GRID

      parameter   ( MAX_NUM_REG    = 101    )
      parameter   ( MAX_NUM_TREES  = 10000 )
      parameter   ( MAX_NUM_LEVELS = 10000 )
      parameter   ( MAX_NUM_GRIDS  = 100000 )
      parameter   ( NULL_REGISTER  = -1    )
      parameter   ( NULL_TREE      = -1    )
      parameter   ( NULL_LEVEL     = -1    )
      parameter   ( NULL_GRID      = -1    )

      real*8       GRID_MASKED
      real*8       GRID_UNMASKED
      parameter   ( GRID_MASKED    = -1.d0    )
      parameter   ( GRID_UNMASKED  =  0.d0    )

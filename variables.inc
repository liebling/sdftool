      ! 
      ! glabal constants for sdftool operations
      ! 
      
      integer      num_params          ! What is this variable?
      integer      MAX_OPERATIONS
      integer      MAX_DATA_SETS
      integer      MAX_TIME_LEVELS
      

      parameter  ( num_params      = 20    )
      parameter  ( MAX_OPERATIONS  = 400   )
      parameter  ( MAX_DATA_SETS   = 16    )
      parameter  ( MAX_TIME_LEVELS = 100   )


      !
      ! global constants for operations
      !

      integer      OP_GENERATE
      integer      OP_MAXIMUM
      integer      OP_MINIMUM
      integer      OP_INPUT
      integer      OP_OUTPUT
      integer      OP_FINISH
      integer      OP_REG_DUMP
      integer      OP_FILTER
      integer      OP_FILTERH
      integer      OP_FILTERT
      integer      OP_FILTERL
      integer      OP_RENAME
      integer      OP_UNI_MERGE
      integer      OP_SUBTRACT
      integer      OP_SUBTRACTGEN
      integer      OP_RESTRICT
      integer      OP_INTEGRATE
      integer      OP_TIMES
      integer      OP_MAXABS
      integer      OP_COARSEN
      integer      OP_L2NORM
      integer      OP_AMIRA
      integer      OP_READONE
      integer      OP_CURL
      integer      OP_DIV
      integer      OP_CROSS
      integer      OP_DOT
      integer      OP_VECMAG
      integer      OP_CART2SPHERE
      integer      OP_COPY
      integer      OP_DIVIDE
      integer      OP_LOOPPROC
      integer      OP_MULTDV
      integer      OP_MULTIPLY
      integer      OP_MULTIPLYCONSTANT
      integer      OP_SQRT
      integer      OP_ARCSIN
      integer      OP_ADD
      integer      OP_ADDCONSTANT
      integer      OP_INVERSE
      integer      OP_R
      integer      OP_SQUARE
      integer      OP_ERASE 
      integer      OP_ABS 
      integer      OP_LOCMAX
      integer      OP_MEMSTAT
      integer      OP_HISTO
      integer      OP_REGLIST
      integer      OP_UNFLOOR
      integer      OP_FLOOR
      integer      OP_GRAD
      integer      OP_HELICITY
      integer      OP_MAXHALFPLANE
      integer      OP_UNCURL
      integer      OP_SETPREFAXIS
      integer      OP_MASK
      integer      OP_FLINES
      integer      OP_INTERPLINE
      integer      OP_NEWCENTER
      integer      OP_RADVECTOR
      integer      OP_SUBTRACTUNO
      integer      OP_MASK2
      integer      OP_INTERPTOGRID
      integer      OP_GENSPHEREGRID
      integer      OP_QUADRUPOLE
      integer      OP_EXTRACTAZI  
      integer      OP_ATPOINT    
      integer      OP_EXTRACTPOLAR

      parameter  ( OP_GENERATE     = 1  ) 
      parameter  ( OP_MAXIMUM      = 2  ) 
      parameter  ( OP_MINIMUM      = 3  ) 
      parameter  ( OP_INPUT        = 4  ) 
      parameter  ( OP_OUTPUT       = 5  ) 
      parameter  ( OP_FINISH       = 6  )
      parameter  ( OP_REG_DUMP     = 7  )
      parameter  ( OP_FILTER       = 8  ) 
      parameter  ( OP_RENAME       = 9  ) 
      parameter  ( OP_UNI_MERGE    = 10 ) 
      parameter  ( OP_SUBTRACT     = 11 )
      parameter  ( OP_RESTRICT     = 12 )
      parameter  ( OP_INTEGRATE    = 13 )
      parameter  ( OP_MAXABS       = 14 )
      parameter  ( OP_FILTERH      = 15 ) 
      parameter  ( OP_FILTERT      = 16 ) 
      parameter  ( OP_FILTERL      = 17 ) 
      parameter  ( OP_COARSEN      = 18 ) 
      parameter  ( OP_L2NORM       = 19 ) 
      parameter  ( OP_AMIRA        = 20 ) 
      parameter  ( OP_TIMES        = 21 )
      parameter  ( OP_READONE      = 22 )
      parameter  ( OP_CURL         = 23 )
      parameter  ( OP_DIV          = 24 )
      parameter  ( OP_CROSS        = 25 )
      parameter  ( OP_DOT          = 26 )
      parameter  ( OP_VECMAG       = 27 )
      parameter  ( OP_CART2SPHERE  = 28 )
      parameter  ( OP_COPY         = 29 )
      parameter  ( OP_DIVIDE       = 30 )
      parameter  ( OP_LOOPPROC     = 31 )
      parameter  ( OP_MULTDV       = 32 )
      parameter  ( OP_MULTIPLY     = 33 )
      parameter  ( OP_MULTIPLYCONSTANT=34 )
      parameter  ( OP_SQRT         = 35 )
      parameter  ( OP_ARCSIN       = 36 )
      parameter  ( OP_ADD          = 37 )
      parameter  ( OP_ADDCONSTANT  = 38 )
      parameter  ( OP_INVERSE      = 39 )
      parameter  ( OP_R            = 40 )
      parameter  ( OP_SQUARE       = 41 )
      parameter  ( OP_ERASE        = 42 )
      parameter  ( OP_ABS          = 43 )
      parameter  ( OP_LOCMAX       = 44 )
      parameter  ( OP_MEMSTAT      = 45 )
      parameter  ( OP_HISTO        = 46 )
      parameter  ( OP_REGLIST      = 47 )
      parameter  ( OP_UNFLOOR      = 48 )
      parameter  ( OP_FLOOR        = 49 )
      parameter  ( OP_GRAD         = 50 )
      parameter  ( OP_HELICITY     = 51 )
      parameter  ( OP_MAXHALFPLANE = 52 )
      parameter  ( OP_UNCURL       = 53 )
      parameter  ( OP_SETPREFAXIS  = 54 )
      parameter  ( OP_MASK         = 55 )
      parameter  ( OP_FLINES       = 56 )
      parameter  ( OP_INTERPLINE   = 57 )
      parameter  ( OP_NEWCENTER    = 58 )
      parameter  ( OP_RADVECTOR    = 59 )
      parameter  ( OP_SUBTRACTUNO  = 60 )
      parameter  ( OP_MASK2        = 61 )
      parameter  ( OP_INTERPTOGRID = 62 )
      parameter  ( OP_GENSPHEREGRID= 63 )
      parameter  ( OP_QUADRUPOLE   = 64 )
      parameter  ( OP_EXTRACTAZI   = 65 )
      parameter  ( OP_ATPOINT      = 66 )
      parameter  ( OP_EXTRACTPOLAR = 67 )
      parameter  ( OP_SUBTRACTGEN  = 68 )

      !
      ! global constants for parameters_real
      !    (these are not the values themselves, but
      !     instead, the indices to the arrays which
      !     hold the various parameters input by the user)
      !

      integer     P_time
      integer     P_x_length
      integer     P_y_length
      integer     P_z_length
      integer     P_amplitude
      integer     P_dt
      integer     P_max_time
      integer     P_resolution

      parameter  ( P_time         = 1 )
      parameter  ( P_x_length     = 2 )
      parameter  ( P_y_length     = 3 )
      parameter  ( P_z_length     = 4 )
      parameter  ( P_amplitude    = 5 )
      parameter  ( P_dt           = 6 )
      parameter  ( P_max_time     = 7 )
      parameter  ( P_resolution   = 8 )


      !
      ! global constants for parameters_integer
      !
      
      integer     P_nx
      integer     P_ny
      integer     P_nz
      integer     P_time_levels
      integer     P_register 
      integer     P_num_ops
      integer     P_my_id
      integer     P_number_procs
      integer     P_reg_target
      integer     P_register2
      integer     P_out_t_by_t
      integer     P_absolute_value_flag
      integer     P_n
      integer     P_register3
      integer     P_reg_target2
      integer     P_reg_target3
      integer     P_level
       
      parameter  ( P_nx                   = 1  )
      parameter  ( P_ny                   = 2  )
      parameter  ( P_nz                   = 3  )
      parameter  ( P_time_levels          = 4  )
      parameter  ( P_register             = 5  )
      parameter  ( P_num_ops              = 6  )
      parameter  ( P_my_id                = 7  )
      parameter  ( P_number_procs         = 8  )
      parameter  ( P_reg_target           = 9  )
      parameter  ( P_register2            = 10 )
      parameter  ( P_out_t_by_t           = 11 )
      parameter  ( P_absolute_value_flag  = 12 )
      parameter  ( P_n                    = 13 )
      parameter  ( P_register3            = 14 )
      parameter  ( P_reg_target2          = 15 )
      parameter  ( P_reg_target3          = 16 )
      parameter  ( P_level                = 17 )

      !
      ! Types of masking that can be done on a tree:
      !
      integer      MASK_L2NORM
      integer      MASK_INTEGRAL
      parameter  ( MASK_L2NORM            = 1 )
      parameter  ( MASK_INTEGRAL          = 2 )

      !
      ! Coordinate directions:
      !
      integer      XDIR
      integer      YDIR
      integer      ZDIR
      parameter  ( XDIR = 1 )
      parameter  ( YDIR = 2 )
      parameter  ( ZDIR = 3 )

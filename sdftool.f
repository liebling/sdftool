      program SDFtool
      implicit none

      include 'mpif.h'
      include 'mympi.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'class_constants.inc'     
 
      !gft
      integer           step 
      integer           gft_rc
      
      ! Mode which treats each data set individually
      ! because of memory limitations
      logical           readonemode
      ! Mode which loops over files output from a variety
      ! of processors
      logical           loopprocmode
      integer           loopprocnum

      !Parameters: these are arrays for real and integer parameters
      real*8            params_real(MAX_OPERATIONS, num_params)
      integer           params_int(MAX_OPERATIONS, num_params)
      character*128     params_chr
      integer           operations(MAX_OPERATIONS)
      character*64      file_name(MAX_OPERATIONS)
      character*64      params_file    !Name of parameter file
      
      integer           i, rc, globalrc, iter, proc
      
      !Functions
      integer           gft_close_all

      logical           ltrace
      parameter (       ltrace = .false. ) 

      mem  = 0 
      ! iteration counter through SDF files
      iter = 1
      ! the process number to prefix for reading in data:
      proc = myid
      ! Set return codes to successful (+1)
      globalrc = 1
      rc       = 1

      readonemode  = .false.
      loopprocmode = .false.

      !
      ! Initialization: 
      !
      if (ltrace) write(*,*) 'main: Initializing'
      call initialize()     
 
      !
      ! Read in parameters: 
      !      
      if( myid .eq. MASTER) then
         call get_params_file( params_file )
         call params_read( params_real, params_int, params_chr,
     *                  operations,  file_name, params_file  )
      endif
      
      !
      ! Broadcast the parameters to all procs:
      !
      if (ltrace) write(*,*) 'main: numprocs = ',numprocs
      if (ltrace) write(*,*) 'main: Bcasting params'
      call params_bcast( params_real, params_int, params_chr,
     *                   operations, file_name      )

 
      !
      ! Main Loop: loop over operations
      !
      if (ltrace) write(*,*) 'main: Looping over operations'
 5    continue
      do i = 1, params_int(1, P_num_ops)
        !!!!!
        if(ltrace) then
11        FORMAT(A, I4.1, A, A, I4)
          !write(*,*)  ""
          write(*,11) "[",myid,"]"," main: operation = ", operations(i)
          !write(*,*) "[",myid,"]"," main: operation = ", operations(i)
          !write(*,*)  ""
        endif
        call execute_op( operations(i), params_real, params_int,
     *                   params_chr, file_name(i),  i, iter, proc, rc )
        if(ltrace) write(*,11) "[",myid,"]"," main: rc = ", rc
        ! Detect if ever negative:
        globalrc = min(globalrc,rc)
        if(ltrace)write(*,11)"[",myid,"]"," main: globalrc = ",globalrc
        if (operations(i) .eq. OP_READONE) then
           if(ltrace.and. .not.readonemode)
     *        write(*,*)  "main: Using READONE mode..."
           readonemode = .true.
        else if (operations(i) .eq. OP_LOOPPROC) then
            if (numprocs .eq. 1.and. .not.loopprocmode) then
               loopprocnum  = params_int(i,P_register2)
               if(ltrace.and. .not.loopprocmode)
     *              write(*,*) 'main: loopprocnum = ',loopprocnum
               loopprocmode = .true.
            else if (.not.loopprocmode) then
               write(*,*) 'main: If running on more than one process'
               write(*,*) 'main: then cannot use loopproc. Ignoring..'
            end if
        end if
        !!!!!
      enddo 
      
      if (readonemode) then
         if(ltrace)write(*,*)  "main: Readone mode:", iter
         if(ltrace)write(*,*)  "main: Erasing current data"
         call erase_allregisters()
         if(ltrace)write(*,*)  "main: Checking whether more data"
         if (globalrc .le. 0) then
            ! Reset this so that future loops work okay:
            globalrc = 1
            if(ltrace)write(*,*)"main: No more data"
         else
            !if(ltrace)Call reg_dump_info(1)
            !if(ltrace)Call reg_dump_info(2)
            !if(ltrace)Call reg_dump_info(3)
            if(ltrace)write(*,*)  "main: and beginning again"
            iter = iter + 1
            goto 5
         end if
      end if

      if (ltrace) write(*,*)'main: Is loopprocmode set?'
      if (loopprocmode) then
         proc = proc + 1
         if (ltrace) write(*,*)'main: Now considering next proc:',proc
         iter = 1
         if (ltrace) write(*,*)'main: Resetting iter counter',iter
         if (ltrace) write(*,*)'main: Closing all SDF files...'
         gft_rc = gft_close_all()
         if (ltrace) call mem_stat()
         if (ltrace) write(*,*)'main: Looping up to:',loopprocnum
         if (proc .lt. loopprocnum) then
            if (.true.)write(*,*)'main: Now considering next proc:',proc
            goto 5
         end if
      end if

      !
      ! Finishing: 
      !
      if (ltrace) write(*,*)'main: Quitting...'
      gft_rc = gft_close_all()
      call MPI_FINALIZE( ierr )

      stop
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine initialize()
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'variables.inc'
      include 'mem.inc'

         call MPI_INIT( ierr )
         call MPI_COMM_RANK( MPI_COMM_WORLD, myid,     ierr )
         call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
         call reg_init()
         call tree_init()
         call level_init()
         call grid_init()

         return
      end


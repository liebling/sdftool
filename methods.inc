      ! 
      ! Functions for grids, levels, trees and registers
      !

      !
      ! Integer
      !
      integer register_add_tree
      integer register_return_end
      integer register_return_start
      integer register_get_number_trees
      integer register_return_rank
      integer register_create_grid
      integer register_getprefaxis

      integer tree_add_level
      integer tree_return_end
      integer tree_return_reg
      integer tree_return_sibling
      integer tree_get_free_number
      integer tree_return_start
      integer tree_get_number_levels
      integer tree_return_register
      integer tree_findgridwpoint

      integer level_add_grid
      integer level_get_free_number
      integer level_return_end
      integer level_return_tree
      integer level_return_sibling
      integer level_return_start
      integer level_get_number_grids
      integer level_return_number
 
      integer grid_get_free_number
      integer grid_return_rank
      integer grid_return_level
      integer grid_return_sibling
      integer grid_return_owner
      integer grid_get_field
      integer grid_get_coords
      integer grid_return_number_grids
      integer grid_return_mask
      integer grid_return_numpoints

      integer my_malloc 

      !
      ! Real*8
      !
      real*8  tree_return_time
      real*8  tree_return_resolution
      real*8  tree_getintegral

      real*8  reg_return_resolution

      real*8  level_return_resolution

      real*8  grid_return_time
      real*8  grid_return_resolution
      real*8  grid_return_min
      real*8  grid_return_max
      real*8  grid_return_maxabs
      real*8  grid_integrate
      real*8  grid_l2norm

      real*8  find_maximum
      real*8  find_minimum
      real*8  integrate

      !
      ! Logical
      !
      logical register_store_rank
      logical grid_is_local
      logical double_equal
      logical grid_compatible

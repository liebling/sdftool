.IGNORE: 

#
# Libraries:
#
#   bbhutil--- needed for SDF data routines
#   mpi    --- mpi library (either -lmpi or -lmpich)
#   sv     --- "scivis" routines linked to by bbhutil in some implementations 
#


#
# Environment variables which need to be defined to compile (define in .cshrc):
#
#     LSV       = -lsv   (if installed)
#     LOCALIB   = -L/usr/local/lib  or -L/usr/local/lib32
#     F77FLAGS  = Compiler/processor specific (with PG compilers use -Mrecursive)
#     CCFLAGS   = Compiler/processor specific
#
#

#STATIC       = -static
STATIC       = 

FLIBS        = $(LMPI) -lbbhutil -lrnpl $(LSV)
F77_         = $(MPIF77) $(F77FLAGS) $(LOCALINC)
MPIF77_LOAD  = $(MPIF77) $(STATIC) $(DEBUG) $(MPIOPTIONS) $(F77FLAGS)  $(LOCALIB)
OBJECTS      = sdftool.o sdfroutines.o levels.o grids.o trees.o registers.o


.f.o:
	$(F77_) -c $*.f
.c.o:
	$(CC_)  -c $*.c

sdftool: $(OBJECTS) variables.inc
	$(MPIF77_LOAD) sdftool.o sdfroutines.o levels.o grids.o trees.o registers.o -o sdftool $(FLIBS) 

testsdfmpi: testsdfmpi.o
	$(MPIF77_LOAD) testsdfmpi.o -o testsdfmpi $(FLIBS) 

install: sdftool
	/bin/cp sdftool ${HOME}/bin

clean: 
	rm -f sdftool
	rm -f *.o

export: clean
	(cd ..; tar cvzf sdftool.tar.gz --exclude CVS sdftool)
	scp ../sdftool.tar.gz relativity.liu.edu:~/public_html/export


      !
      ! Registers.f
      !
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_getprefaxis:                                           cc
cc                   Returns a preferred axis.                        cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function register_getprefaxis(reg)
      implicit none
      integer  reg, axis
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'

      logical        ltrace
      parameter (    ltrace = .false. )

      register_getprefaxis = reg_axis(reg)

      if (ltrace) then
         write(*,9) myid, 'register_getprefaxis: reg =',reg,
     .                      register_getprefaxis
      end if

   9  format('[',I3,'] ',A,5I5)

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_setprefaxis:                                           cc
cc                   Sets a preferred axis.                           cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_setprefaxis(reg,axis)
      implicit none
      integer  reg, axis
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      integer  ti, li, gi
      real(kind=8) time, h, bbox(6)
      integer  tree, level, grid, rank, shape(3), rrank

      logical        ltrace
      parameter (    ltrace = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_setprefaxis: reg =',reg
         write(*,9) myid, 'register_setprefaxis: axis=',axis
      end if

      if (axis.ge.1 .and. axis.le.3) then
         reg_axis(reg) = axis
      else
         write(*,9)myid,'register_setprefaxis:Nonsensical value axis=',
     .                     axis
      end if

   9  format('[',I3,'] ',A,5I5)

      return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_copy:                                                  cc
cc                   Copy a register to a new one.                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_copy(from, to)
      implicit none
      integer  from, to
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      integer  ti, li, gi
      real(kind=8) time, h, bbox(6)
      integer  tree, level, grid, rank, shape(3), rrank

      logical        ltrace
      parameter (    ltrace = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_copy: from/to = ',from,to
      end if

      !call register_copy_proc(from, to)
      reg_proc(to) = reg_proc(from)

      rrank = register_return_rank(from)
      call register_set_rank(to, rrank)
      if(ltrace)write(*,9)myid,'register_copy: rrank=',rrank
      if(ltrace) call reg_dump_info(from)
      ! Get first tree in register 
      ti = register_return_start(from)
  6   if (ti .ne. NULL_TREE) then
         li    = tree_return_start(ti)
         time  = tree_return_time(ti)
         tree  = register_add_tree(to, time)
  7      if (li .ne. NULL_LEVEL) then
            h     = level_return_resolution(li)
            level = tree_add_level(tree, h)
            gi    = level_return_start(li)
  8         if (gi .ne. NULL_GRID) then
               grid  = grid_get_free_number()
               rank  = grid_return_rank(gi)
               call grid_return_shape(gi, shape)
               call grid_createB(grid, rank, shape, myid)
               call grid_return_bbox(gi, bbox)
               call grid_set_bbox(grid, bbox)
               call grid_set_level(grid, level)
               call level_add_gridB(grid, level)
               call grid_copy( gi, grid )
               if (ltrace) then 
                  write(*,9)myid,'register_copy: gi/li =',gi,li
                  write(*,9)myid,'register_copy: grid/rank = ',grid,rank
                  write(*,9)myid,'register_copy: grid_return_level = ',
     .                                       grid_return_level(grid)
                  write(*,9)myid,'register_copy: level_return_tree = ',
     .                        level_return_tree(grid_return_level(grid))
                  write(*,9)myid,'register_copy: tree_return_register=',
     .  tree_return_register(level_return_tree(grid_return_level(grid)))
                  write(*,9)myid,'register_copy: grid_return_rank = ',
     .                                       grid_return_rank(grid)
                  write(*,9)myid,'register_copy: shape: ',shape(1),
     .                        shape(2),shape(3)
                  !call grid_dump(grid)
               end if
               !
               gi = grid_return_sibling(gi)
               if(ltrace)write(*,9)myid,'register_copy:gi->sib:',gi
               goto 8
            end if
            li = level_return_sibling(li)
            if(ltrace)write(*,9)myid,'register_copy:lev->sib:',li
            goto 7
         end if
         ti = tree_return_sibling(ti)
         if(ltrace)write(*,9)myid,'register_copy:tree->sib:',ti
         goto 6
      end if

      if(ltrace)write(*,9)myid,'register_copy: Done.'
   9  format('[',I3,'] ',A,5I5)

      return
      end        ! register_copy

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_coarsen:                                               cc
cc                   Select every nth point.                          cc
cc                   This routine is a good TEMPLATE for doing        cc
cc                   something to every grid in a register.           cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_coarsen(nth, reg)
      implicit none
      integer  nth, reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      integer  ti, li, gi

      logical        ltrace
      parameter (    ltrace = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_coarsen: nth  =',nth
         write(*,9) myid, 'register_coarsen: reg  =',reg
      end if

      ! Get first tree in register 
      ti = register_return_start(reg)

      !
      ! Loop over all times/tree:
      !
      do while (ti .ne. NULL_TREE)
         if (ltrace) write(*,9)myid,'Consider ti=',ti
         li = tree_return_start(ti)
         do while (li .ne. NULL_LEVEL)
            if (ltrace) write(*,9)myid,'   Consider li=',li
            gi = level_return_start(li)
            do while (gi .ne. NULL_GRID)
               if (ltrace) write(*,9)myid,'      Consider gi=',gi
               if (grid_is_local(gi)) then
                  if (ltrace) write(*,9)myid,'         *Process  gi=',gi
                  call grid_coarsen(gi,nth)
               end if
               gi = grid_return_sibling(gi)
            end do
            li = level_return_sibling(li)
         end do
         !
         ti = tree_return_sibling(ti)
      end do

      !
      !
      if (ltrace) then
         call reg_dump_info(reg)
         write(*,9) myid, 'register_coarsen: Done'
   9     format('[',I3,'] ',A,I5)
      end if

      return
      end          ! END: register_coarsen


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    reg_valid:  Tests whether a register number is valid or not.    cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function reg_valid(reg)
      implicit none
      integer  reg
      include 'class_constants.inc'

      reg_valid = .true.
      if (reg .gt. MAX_NUM_REG) reg_valid = .false.
      if (reg .le. 0          ) reg_valid = .false.

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    reg_return_resolution:                                          cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function reg_return_resolution(reg)
      implicit none
      integer  reg
      !include 'methods.inc'
      real*8   tree_return_resolution
      integer  register_return_start 

      reg_return_resolution =  tree_return_resolution(
     *                           register_return_start(reg) )

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_filterl:                                               cc
cc                   Remove all levels from a register except that    cc
cc                   given by the user (indexed from 0)               cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_filterl(reg, lev)
      implicit none
      integer  reg, lev
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      integer  ti, li, lnext, level_index

      logical        ltrace2
      parameter (    ltrace2 = .false. )


      if(ltrace2)write(*,99)myid,'Register_filterl: retain level:',lev

      ! Get first tree in register 
      ti = register_return_start(reg)

      !
      ! Loop over all times/tree:
      !
  10  if (ti .ne. NULL_TREE) then
         if (ltrace2) write(*,99)myid,'Consider ti=',ti
         !
         ! Loop over all resolutions/levels:
         !
         ! Count the level:
         !    (level numbers are just for memory and do not
         !     refer to successive resolutions)               
         level_index = 0
         li = tree_return_start(ti)
  20     if (li .ne. NULL_LEVEL) then
            !if (ltrace2) write(*,99)myid,'   Consider li=',li
            if (ltrace2) write(*,99)myid,'   Consider lev=',level_index
            ! store this in case we have to remove current level
            lnext = level_return_sibling(li)
            if ( level_index .ne. lev) then
               !
               ! Remove this level:
               !
               if(ltrace2)write(*,99)myid,'    Removing li=',level_index
               call tree_remove_level(ti,li)
            else
               if(ltrace2)write(*,99)myid,'    Keeping  li=',level_index
            end if
            !
            ! Use stored value of sibling in case level was removed:
            li = lnext
            level_index = level_index + 1
            goto 20
         end if
         !
         ti = tree_return_sibling(ti)
         goto 10
      end if

      if (ltrace2) then
         write(*,99) myid, 'reg_filterl: reg = ',reg
  99     format('[',I3,'] ',A,I5)
      end if

      return
      end          ! END: register_filterl

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    register_remove_tree:                                                   cc
cc                    Remove a tree  from a given register.                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_remove_tree(reg,tree)
      implicit none
      integer   reg, tree
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'trees.inc'
      include 'methods.inc'
      include 'variables.inc'
      integer  ti, prevti, nextti

      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (ltrace2) then
         write(*,99) myid, 'register_remove_tree: reg   = ',reg
         write(*,99) myid, 'register_remove_tree: tree  = ',tree
  99     format('[',I3,'] ',A,I5)
      end if

      !
      ! Loop through tree and find the one to be removed:
      !
      prevti = NULL_TREE
      ti     = register_return_start(reg)
      !
  10  if (ti.ne.NULL_TREE) then
         nextti = tree_return_sibling(ti)
         if (ti .eq. tree) then
            if (prevti.ne.NULL_TREE) then
               !
               ! Tree occurs "normally" or at end...
               !
               call tree_set_sibling(prevti,nextti)
            else
               !
               ! Tree occurs at start of tree
               !
               call register_set_start(reg, nextti)
            end if
            ! At this point, should free up all the grids and storage
            ! associated with this level, but Jason has not implemented
            ! those routines yet...
            call tree_free(tree)
            ! Exit loop:
            goto 20
            !
         end if
         !
         prevti = ti
         ti     = nextti
         goto 10
      end if

 20   continue

      return
      end           ! END: register_remove_tree

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    register_free:                                                          cc
cc                    Free memory associated with a register                  cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_free(reg)
      implicit none
      integer   reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'trees.inc'
      include 'methods.inc'
      include 'variables.inc'
      integer  ti, nextti

      logical     ltrace2
      parameter ( ltrace2 = .false. )

      if (ltrace2) then
         write(*,99) myid, 'register_free: reg   = ',reg
         call reg_dump_info(reg)
  99     format('[',I3,'] ',A,I5)
      end if

      !
      ! Loop through tree and find the one to be removed:
      !
      ti     = register_return_start(reg)
      !
  11  if (ti.ne.NULL_TREE) then
         nextti = tree_return_sibling(ti)
         if(ltrace2)write(*,99) myid, 'register_free: freeing tree: ',ti
         call tree_free(ti)
         ti     = nextti
         goto 11
      end if

      call register_set_start(reg, NULL_TREE)

      if (ltrace2) then
         call reg_dump_info(reg)
         write(*,99) myid, 'register_free: Done. reg   = ',reg
      end if

      return
      end           ! END: register_free

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_filtert:                                               cc
cc                   Remove all trees  from a register except those   cc
cc                   dictated by the given index vector.              cc
cc                   Uses Matts index vector notiation.               cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_filtert(reg, cline)
      implicit none
      integer  reg
      character*(*) cline
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      integer  ti, tc, i
      !
      !   Index Vector Stuff
      !
      ! Max size of index vector storage:
      integer     mxliv
      parameter ( mxliv = 10000 )
      ! The index vector itself:
      integer  iv(mxliv)
      ! Size being used here:
      integer  nv
      ! Next tree to retain
      integer  nextiv, ivcount
      ! The functions:
      integer     irdivn
      external    irdivn

      logical        ltrace2
      parameter (    ltrace2 = .false. )
      logical        ltrace3
      parameter (    ltrace3 = .false. )

      if( irdivn(iv,nv,mxliv,1,mxliv,cline) .lt. -1 ) then
          if (ltrace2)
     *          write(*,*) 'register_filtert: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,nv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      if (ltrace2) then
         !do i = 1, nv
            !write(*,*) i, iv(i)
         !end do
         write(*,99) myid, 'reg_filtert: nv     = ',nv
         write(*,99) myid, 'reg_filtert: reg    = ',reg
         write(*,97) myid, 'reg_filtert: cline  = ',cline
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)
      end if

      ! Get first tree in register 
      ti      = register_return_start(reg)
      ! Count times/tree
      tc      = 0
      ! First tree to retain:
      ivcount = 1
      nextiv  = iv(ivcount)

  10  if (ti .ne. NULL_TREE) then
         tc = tc + 1
         if (ltrace3) write(*,99) myid, 'reg_filtert:     ti = ',ti
         if (tc .ne. nextiv) then
            if (myid.eq.3) then
            if (ltrace2) write(*,99) myid, 'reg_filtert:     Remove ',ti
            end if
            !
            ! Remove this tree:
            !
            call register_remove_tree(reg,ti)
         else
            if (ltrace3) write(*,99) myid, 'reg_filtert:     Retain ',ti
            ivcount = ivcount + 1
            if (ivcount .gt. nv) then
               !
               ! No more trees to retain
               !
               nextiv = 0
            else
               nextiv  = iv(ivcount)
            end if
         end if
         !
         ti = tree_return_sibling(ti)
         goto 10
      end if

 800  continue

      if (ltrace2) then
         if (myid.eq. 3) call reg_dump_info(reg)
      end if

      return
      end          ! END: register_filtert


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_filterh:                                               cc
cc                   Remove all levels from a register except those   cc
cc                   with the given resolution.                       cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_filterh(reg, res)
      implicit none
      integer  reg
      real*8   res
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      integer  ti, li, lnext
      real*8   lres
      !logical  double_equal 
      !external double_equal 

      logical        ltrace2
      parameter (    ltrace2 = .false. )


      ! Get first tree in register 
      ti = register_return_start(reg)

      !
      ! Loop over all times/tree:
      !
  10  if (ti .ne. NULL_TREE) then
         if (ltrace2) write(*,99)myid,'Consider ti=',ti
         !
         ! Loop over all resolutions/levels:
         !
         li = tree_return_start(ti)
  20     if (li .ne. NULL_LEVEL) then
            if (ltrace2) write(*,99)myid,'   Consider li=',li
            lres  = level_return_resolution(li)
            if (ltrace2) write(*,98)myid,'         lres =',lres
            if (ltrace2) write(*,98)myid,'          res =', res
            ! store this in case we have to remove current level
            lnext = level_return_sibling(li)
            if ( abs(lres-res).gt.0.0001d0) then
            !if ( abs(lres-res).lt.0.0001d0) then
            !if ( .not.double_equal(lres,res) ) then
               !
               ! Remove this level:
               !
               if (ltrace2) write(*,98)myid,'   Resolution==',lres
               call tree_remove_level(ti,li)
            else 
               if (ltrace2) write(*,98)myid,'   Res diff=',abs(lres-res)
            end if
            !
            ! Use stored value of sibling in case level was removed:
            !li = level_return_sibling(li)
            li = lnext
            goto 20
         end if
         !
         ti = tree_return_sibling(ti)
         goto 10
      end if

      if (ltrace2) then
         write(*,99) myid, 'reg_filterh: reg = ',reg
         write(*,98) myid, 'reg_filterh: res = ',res
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)
      end if

      return
      end          ! END: register_filterh

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    reg_dump_info:                                                  cc
cc                   Dumps out all information about a register.      cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine reg_dump_info(reg)
      implicit none
      integer  reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  ti, li, gi, shape(3), i, rank
      real*8   bbox(6)

      logical     brief
      parameter ( brief = .true. )

      write(*,97) myid,'////////    reg_dump_info \\\\\\\\\\\\\\\\\\'
      write(*,99) myid,'               Register: ',reg
      write(*,98) myid,'               Myname:   ',reg_myname(reg)
      write(*,99) myid,'               Rank:     ',reg_rank(reg)
      !write(*,98) myid,'               Name:     ',reg_name(reg)
      !write(*,99) myid,'               Reg_tree: ',reg_tree(reg)
      if (.not.brief) then
      write(*,97) myid,'               Memory Status:'
      write(*,90) myid,'                  Arena: ',arena
      write(*,90) myid,'                  Mem  : ',mem
      write(*,91) myid,'                  %Used: ',100.d0*mem/arena
      end if
    
      !
      ! Dump whole structure:
      !
      ti = register_return_start(reg)
 10   if (ti .ne. NULL_TREE) then
         write(*,85) myid,'Tree: ',ti, tree_return_time(ti)
         li = tree_return_start(ti)
 20      if (li .ne. NULL_LEVEL) then
            write(*,96)myid,'   Level: ',li, level_return_resolution(li)
            gi = level_return_start(li)
 30         if (gi .ne. NULL_GRID) then
               call grid_return_shape(gi, shape)
               call grid_return_bbox(gi, bbox)
               rank = grid_return_rank(gi)
               if (rank .eq. 3) then
                  write(*,83)myid,'    Grid:',gi, ' Own:',
     *                                  grid_return_owner(gi), 
     *                                  shape(1),shape(2),shape(3)
                  write(*,73)myid,      bbox(1),bbox(3),bbox(5),
     *                                  bbox(2),bbox(4),bbox(6)
               else if (rank .eq. 2) then
                  write(*,82)myid,'    Grid:',gi, ' Own:',
     *                                  grid_return_owner(gi), 
     *                                  shape(1),shape(2)
                  write(*,72)myid,      bbox(1),bbox(3),
     *                                  bbox(2),bbox(4)
               else if (rank .eq. 1) then
                  write(*,81)myid,'    Grid:',gi, ' Own:',
     *                                  grid_return_owner(gi), 
     *                                  shape(1)
                  write(*,71)myid,      bbox(1),
     *                                  bbox(2)
               end if
               write(*,89)myid,'    L2norm:', grid_l2norm(gi)
               gi = grid_return_sibling(gi)
               goto 30
            end if
            li = level_return_sibling(li)
            goto 20
         end if
         ti = tree_return_sibling(ti)
         goto 10
      end if

      write(*,97) myid,'\\\\\\\\\\\\\\\\    ............. //////////'

  83  format('[',I3,'] ',A,I4,A,I4,' (',I4,',',I4,',',I4,')')
  73  format('[',I3,'] ','             ','(',F8.3,',',F8.3,',',F8.3,
     *        ') (',F8.3,',',F8.3,',',F8.3,')')
  82  format('[',I3,'] ',A,I4,A,I4,' (',I4,',',I4,')')
  72  format('[',I3,'] ','             ','(',F8.3,',',F8.3,
     *        ') (',F8.3,',',F8.3,')')
  81  format('[',I3,'] ',A,I4,A,I4,' (',I7,')')
  71  format('[',I3,'] ','             ','(',F8.3,
     *        ') (',F8.3,')')

  89        format('[',I3,'] ',A,G11.3)
  90        format('[',I3,'] ',A,I16)
  91        format('[',I3,'] ',A,F5.2,'%')
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  85        format('[',I3,'] ',A,I5,' Time: ',F10.5)
  96        format('[',I3,'] ',A,I5,' Res:  ',F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

      call mem_stat()

      return
      end       ! END: reg_dump_info

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine reg_init()
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer i

         reg_counter  = 1

         do i = 1, MAX_NUM_REG
            reg_tree(i)  = NULL_REGISTER
         enddo

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_generate(reg_num, name, bbox, shape,
     *                             t, d_t, end_time, amp)
      implicit none
!     include 'mpif.h'
!     include 'mympi.inc'
!     include 'class_constants.inc'
!     include 'mem.inc'
!     include 'variables.inc'
!     include 'registers.inc'

      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc' 
      include 'methods.inc'
      include 'mem.inc'

         integer        reg_num
         integer        shape(3)
         real*8         bbox(6)
         character*(*)  name
         real*8         t
         real*8         d_t
         real*8         amp
         real*8         end_time
         real*8         time          ! no idea what this is

         integer        tree
         integer        level
         integer        grid
         real*8         ti
         real*8         h
 
         character*2    my_rank_string
 
         !Functions  
         !integer        register_add_tree
         !integer        tree_add_level
         !integer        level_add_grid
         !integer        grid_get_field
         character      register_return_name

         logical        ltrace
         parameter (    ltrace = .false. )
         logical        lltrace
         parameter (    lltrace = .false. )

         if (lltrace) then
            write(*,*) 'register_generate: reg_num = ',reg_num
            write(*,*) 'register_generate: t       = ',t
            write(*,*) 'register_generate: d_t     = ',d_t
            write(*,*) 'register_generate: end_time= ',end_time
            write(*,*) 'register_generate: name    = ',name
         end if

         call register_assign_proc(reg_num, myid)
         !call register_assign_name(reg_num, name)
         call register_assign_namenum(reg_num, name, myid)
         reg_rank(reg_num) = 3
         
         ti    = time

         if(d_t .eq. 0) d_t = 1.0

         do while( ti .le. end_time )
            !grid = 20
            !grid = grid_get_free_number()
            grid = grid_get_free_number()
            call grid_createB(grid, reg_rank(reg_num), shape, myid)
            call grid_set_bbox(grid, bbox)

            tree  = register_add_tree(reg_num, ti)
            if (lltrace) write(*,*) 'register_generate: t=',t
            
            h     = ( bbox(2) - bbox(1) ) / ( shape(1) - 1 )
             
            level = tree_add_level(tree, h)
            call grid_set_level(grid, level)
            call level_add_gridB(grid, level)
            !level = tree_add_level(tree, h)
         
            !grid  = level_add_grid(level,  shape, bbox, myid, .false.)

            if(ltrace) then
               write(*,*) "reg_gen: tree = ", tree
               write(*,*) "reg_gen: level = ", level
               write(*,*) "reg_gen: grid = ", grid
               write(*,*) "reg_gen: grid_field = ", grid_get_field(grid)
            endif
 
            call generate_gaussian(shape, bbox,
     *                             amp,
     *                             q( grid_get_field(grid) )  )

            ti    =  ti + d_t
         enddo

         return
      end

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  Register_Add_Tree:                                              !!
!!        Given a time and register will return pointer to either   !!
!!    a newly created tree or pointer to tree with the same         !!
!!    time.                                                         !!
!!                                                                  !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      function register_add_tree(reg_num, sdf_time)
      implicit none
         integer     reg_num
         real*8      sdf_time
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'methods.inc'
      include 'variables.inc'
         real*8      ttime
         integer     ti, i, newti
         integer     last_tree
         ! Functions
         !logical   double_equal

         logical     ltrace2
         parameter ( ltrace2 = .false. )


         if (ltrace2) then
            write(*,99) myid, 'register_add_tree: reg_num  = ',reg_num
            write(*,91) myid, 'register_add_tree: sdf_time = ',sdf_time
            call reg_dump_info(reg_num)
         end if

         !
         ! Loop over trees to see if there is a tree with said time
         !
         last_tree = NULL_TREE
         ti        = register_return_start(reg_num)
         do while ( ti .ne. NULL_TREE ) 
            ttime = tree_return_time(ti)
            if (ltrace2)write(*,95)myid,'times: ',ttime,sdf_time
            !write(*,*) ttime,sdf_time, abs(ttime-sdf_time)
            if (double_equal(ttime,sdf_time))then
               if (ltrace2) then
               write(*,'(A,I4.1,A,F6.3)') "[",myid,
     *                     "] ADD_TREE: tree found time = ",sdf_time
               endif
               !
               ! Existing tree has the time we need,
               !  so return this tree pointer:
               !
               register_add_tree = ti
               return
            else if (ttime .gt. sdf_time) then
               !
               ! Time not found,
               ! break from loop and insert new tree:
               !
               if (ltrace2)write(*,99)myid,'Got to large times',ti
               goto 10
            endif
            ! Increment tree
            last_tree = ti
            ti        = tree_return_sibling(ti)
         enddo

         if (ltrace2)write(*,99)myid,'Add tree at end'

 10      continue
         
        newti      = tree_get_free_number()

        if( last_tree .ne. NULL_TREE ) then
            if (ltrace2) then
               write(*,99)myid,'register_add_tree:Insert btw:',last_tree
               write(*,99)myid,'register_add_tree: And: ',ti
            endif
            call tree_set_sibling(newti,     ti    )
            call tree_set_sibling(last_tree, newti )
        else
            if (ltrace2) then
               write(*,97)myid,'register_add_tree: Put at beginnning'
            endif
           ! put tree first:
           call register_set_start(reg_num, newti)
        endif

        call tree_set_time(newti, sdf_time)
        call tree_set_reg(newti, reg_num)
         
        register_add_tree        = newti

        if (ltrace2) then
           write(*,97)myid,'register_add_tree: Now show reg_dump:'
           call reg_dump_info(reg_num)
           write(*,97)myid,'register_add_tree: Done.'
        end if

        return

  91        format('[',I3,'] ',A,F10.5)
  92        format('[',I3,'] ',A,2I5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

      end           ! END: reg_add_tree


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc   register_assign_proc:                                            cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_assign_proc(reg, proc)
      implicit none
      integer        reg, proc
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'

      logical        ltrace2
      parameter (    ltrace2 = .false. )

      if (ltrace2) then
         write(*,99)myid,'register_assign_proc: reg,proc=',reg,proc
  99        format('[',I3,'] ',A,3I9)
      end if

         reg_proc(reg) = myid

      if (ltrace2) then
         write(*,99)myid,'register_assign_proc:reg_proc=',reg_proc(reg)
      end if

      return
      end
 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc   register_assign_name:                                            cc
cc                          Store the name of the register            cc
cc                          along with the name used for SDF output,  cc
cc                          in particular the register name prepended cc
cc                          with the processor number, eg. 0phi.sdf   cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_assign_name(reg, register_name)
      implicit none
      integer        reg
      character*(*)  register_name
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      character*32   str_myid
      character*128  tmp_str
      integer        length
      integer        mystringlength

      logical        ltrace2
      parameter (    ltrace2 = .false. )

      if (ltrace2) then
         write(*,99)myid,'register_assign_name: reg=',reg
         write(*,98)myid,'register_assign_name: register_name:',
     .                     register_name
         write(*,97)myid,'register_assign_name: reg_proc:',reg_proc(reg)
      end if

         reg_name(reg)    = register_name

         call int2str( reg_proc(reg), str_myid )
         !call int2str( myid, str_myid )
         tmp_str(1:)         = str_myid
         length              = mystringlength(tmp_str)
         tmp_str(length+1:)  = reg_name(reg)
         reg_myname(reg)     = tmp_str

         if (ltrace2) then
            write(*,*) length
            write(*,98) myid, 'register_assign_name: str_myid=',
     *                str_myid
            write(*,98) myid, 'register_assign_name: tmp_str=',
     *                tmp_str
            write(*,98) myid, 'register_assign_name: myname =',
     *                reg_myname(reg)
  97        format('[',I3,'] ',A,I9)
  98        format('[',I3,'] ',A,A)
  99        format('[',I3,'] ',A,3I9)
         end if

         return
      end
 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc   register_assign_namenum:                                         cc
cc                          Store the name of the register            cc
cc                          along with the name used for SDF output,  cc
cc                          in particular the register name prepended cc
cc                          with the processor number, eg. 0phi.sdf   cc
cc                    NB: this version takes a number as an argument  cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_assign_namenum(reg, register_name, num)
      implicit none
      integer        reg, num
      character*(*)  register_name
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      character*32   str_myid
      character*128  tmp_str
      integer        length
      integer        mystringlength

      logical        ltrace2
      parameter (    ltrace2 = .false. )

         reg_name(reg)    = register_name

         reg_proc(reg)       = num
         call int2str(  num, str_myid )
         !call int2str( myid, str_myid )
         tmp_str(1:)         = str_myid
         length              = mystringlength(tmp_str)
         tmp_str(length+1:)  = reg_name(reg)
         reg_myname(reg)     = tmp_str

         if (ltrace2) then
            write(*,*) length
            write(*,98) myid, 'register_assign_name: str_myid=',
     *                str_myid
            write(*,98) myid, 'register_assign_name: tmp_str=',
     *                tmp_str
            write(*,98) myid, 'register_assign_name: myname =',
     *                reg_myname(reg)
  98        format('[',I3,'] ',A,A)
         end if

         return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_return_name(reg, name)     
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer       reg
         character*(*) name
         
          name = reg_name(reg)

         return
      end
 

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function reg_return_maxresolution(reg)
      implicit none
      integer  reg
      include 'class_constants.inc' 
      include 'registers.inc'
      integer  ti
      real*8   mytmp
      real*8   tree_return_maxresolution
      integer  tree_return_sibling
      external tree_return_sibling
      real*8   tree_return_resolution
      external tree_return_resolution

         ti = reg_tree(reg)  !points to first tree in register
         
         reg_return_maxresolution = 0.d0
         do while( ti .ne. NULL_TREE )
            !tmp = tree_return_maxresolution(ti)
            mytmp = tree_return_resolution(ti)
            reg_return_maxresolution=max(mytmp,reg_return_maxresolution)
            ti = tree_return_sibling(ti)
         end do

         return
      end         ! reg_return_maxresolution

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function reg_return_maxradius(reg)
      implicit none
      integer  reg
      include 'class_constants.inc' 
      include 'registers.inc'
      integer  ti
      integer  tree_return_sibling
      external tree_return_sibling
      real*8   tree_return_maxradius
      external tree_return_maxradius

         ti = reg_tree(reg)  !points to first tree in register
         
         do while( ti .ne. NULL_TREE )
            reg_return_maxradius = tree_return_maxradius(ti)
            ti = tree_return_sibling(ti)
         end do

         return
      end         ! reg_return_maxradius

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_return_myname(reg, name)
      implicit none
      include 'class_constants.inc' 
      include 'registers.inc'
         integer       reg
         character*(*) name

         name = reg_myname(reg)
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function register_return_end(reg_num)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer     reg_num
         
         integer     ti   !Tree counter
         logical     ltrace
         parameter ( ltrace = .false. )

         !Functions 
         integer     tree_return_sibling
         integer     tree_get_sibling
         
         if (ltrace) write(*,*) "Finding end of register..."
         ti = reg_tree(reg_num)  !points to first tree in register
         
         !Increments through all the siblings till it gets to the last tree
         if( ti .ne. NULL_TREE) then
            do while( tree_return_sibling(ti) .ne. NULL_TREE )
               ti = tree_return_sibling(ti)
            enddo
        
            register_return_end = ti
         else
            !
            ! Should not this be NULL_TREE?
            !
            register_return_end = NULL_REGISTER
         endif
 
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_set_start(reg, tree)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer reg
         integer tree

         reg_tree(reg) = tree

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_output(reg)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer   reg

         integer   tree
         logical   ltrace2
         parameter(ltrace2 = .false.)

         !Functions:
         integer   register_return_start
         integer   tree_return_sibling

         if (ltrace2) then
            write(*,*) "Entering Output: name = ", reg_name(reg) 
            write(*,*) "                 rank = ", reg_rank(reg)
            call reg_dump_info(reg)
         endif

         !Gives first tree in register
         tree = register_return_start(reg)
         do while( tree .ne. NULL_TREE)
            call tree_output(tree)

            !Increment tree
            tree = tree_return_sibling(tree)
         enddo

         return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_amira(reg)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer   reg

         integer   tree
         logical   ltrace2
         parameter(ltrace2 = .false.)

         !Functions:
         integer   register_return_start
         integer   tree_return_sibling

         if (ltrace2) then
            write(*,*) "Entering Output: name = ", reg_name(reg) 
            write(*,*) "                 rank = ", reg_rank(reg)
         endif

         !Gives first tree in register
         tree = register_return_start(reg)
         do while( tree .ne. NULL_TREE)
            call tree_amira(tree)

            !Increment tree
            tree = tree_return_sibling(tree)
         enddo

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function register_return_start(reg)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer reg
         
         register_return_start = reg_tree(reg)
         return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_readonesdf:                                            cc
cc                      Reads one dataset at a time from an SDF file. cc
cc                      num---the number prefix for which data to readcc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_readonesdf(reg_num, file_name, step, num,rc)
      implicit none
      integer        reg_num, step, rc, num
      character*(*)  file_name
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'

         integer        tree
         integer        level 
         integer        grid
         integer        rank
         integer        gft_rc
         integer        owner
         integer        shape(3), i
         character*64   name
         logical        file_exists

         logical        ltrace2
         parameter (    ltrace2 = .false. )

         ! Functions
         integer        gft_read_shape
         integer        register_read

         ! set return code to successful
         rc = 1

         if (ltrace2) then
            write(*,99)myid,'register_readonesdf: reg_num   =',reg_num
            write(*,98)myid,'register_readonesdf: file_name =',file_name
            write(*,99)myid,'register_readonesdf: num       =',num
  99        format('[',I3,'] ',A,3I9)
  98        format('[',I3,'] ',A,A)
         end if

         ! Assign name and rank to register variables
         if (ltrace2)write(*,97)myid,'register_readonesdf: assign name'
         !call register_assign_name(reg_num, file_name)
         call register_assign_proc(reg_num, num)
         call register_assign_namenum(reg_num, file_name,num)
         file_exists =  register_store_rank(reg_num)
         if (.not. file_exists) then
                if(ltrace2)write(*,99)myid,
     *                  'register_readonesdf:cannot read rank'
                rc = -1
                goto 800
         end if
         call register_return_myname(reg_num, name)
         if (ltrace2)write(*,98)myid,'register_readonesdf:name: ', name
         
         if (file_exists) then
            rank   = reg_rank(reg_num)
            if(ltrace2)write(*,99)myid,'register_readonesdf:rank =',rank
            if(ltrace2)write(*,99)myid,'register_readonesdf:step =',step
            gft_rc = gft_read_shape(name, step, shape)
            if (gft_rc .le. 0) then
                if(ltrace2)write(*,99)myid,
     *                  'register_readonesdf:cannot read shape'
                rc = -1
                goto 800
            end if
            if (ltrace2) then
                do i = 1, rank
                   write(*,99)myid,'register_readonesdf:shp',shape(i)
                end do
            end if
            gft_rc = register_read(reg_num, name, shape,rank,step)
            if (gft_rc .le. 0) then
                rc = -1
                goto 800
            end if
         end if

         if (ltrace2) then
            write(*,97)myid,'register_readonesdf:rc: ',rc
            write(*,97)myid,'register_readonesdf:About to call reg_comm'
  97        format('[',I3,'] ',A)
         end if

         call register_communicate(reg_num)

  800    continue

         if (ltrace2) call reg_dump_info(reg_num)

         return
      end              ! END: register_readonesdf


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_readsdf:                                               cc
cc                      Main routine to read in SDF from file.        cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_readsdf(reg_num, file_name)
      implicit none
      integer        reg_num
      character*(*)  file_name
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'

         integer        tree
         integer        level 
         integer        grid
         integer        step
         integer        rank
         integer        gft_rc
         integer        owner
         integer        shape(3), i
         character*64   name
         logical        file_exists

         logical        ltrace2
         parameter (    ltrace2 = .false. )

         ! Functions
         integer        gft_read_shape
         integer        register_read
         !integer        grid_get_free_number
         !external       grid_get_free_number

         if (ltrace2) then
            write(*,99) myid, 'register_readsdf: reg_num   = ',reg_num
            write(*,98) myid, 'register_readsdf: file_name = ',file_name
  99        format('[',I3,'] ',A,I9)
  98        format('[',I3,'] ',A,A)
         end if

         ! Assign name and rank to register variables
         if (ltrace2) write(*,97) myid, 'register_readsdf: assign name'
         !call register_assign_name(reg_num, file_name)
         call register_assign_proc(reg_num, myid)
         call register_assign_namenum(reg_num, file_name,myid)
         file_exists =  register_store_rank(reg_num)
         call register_return_myname(reg_num, name)
         if(ltrace2)write(*,98)myid,'register_readsdf: name:',name(1:10)
         
         step     = 1
         if (file_exists) then
            rank   = reg_rank(reg_num)
            if(ltrace2)write(*,99)myid,'register_readsdf: rank = ', rank
            gft_rc = 1
            do while (gft_rc .gt. 0)
                gft_rc = gft_read_shape(name, step, shape)
                if (ltrace2) then
                   write(*,99)myid,'register_readsdf: step = ', step
                   do i = 1, rank
                      write(*,99)myid,'register_readsdf:shp=',shape(i)
                   end do
                end if
                gft_rc = register_read(reg_num, name, shape,rank,step)
                if(ltrace2)write(*,99)myid,'register_readsdf:gft_rc = ',
     .                                                       gft_rc
                step = step + 1
            end do
         end if

         if (ltrace2) then
            write(*,97) myid, 'register_readsdf: About to call reg_comm'
  97        format('[',I3,'] ',A)
         end if

         call register_communicate(reg_num)

         if (ltrace2) call reg_dump_info(reg_num)

         return
      end              ! END: register_readsdf

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_read:                                                  cc
cc                   Read in a single data set of any rank in 1..3    cc
cc                   Returns return code from gft_read()              cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function  register_read(reg_num, name, shape, rank, step)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc' 
      include 'methods.inc'
      include 'mem.inc'
         integer       reg_num
         integer       rank
         integer       step
         integer       shape(rank)
         character*(*) name

         real*8        sdf_time, h
         real*8        bbox(6)
         real*8        l2norm1D
         external      l2norm1D
         character*128 cnames
         integer       tree, grid, i, level
         integer       gft_read_full
         integer       coordsptr
         external      gft_read_full
         integer       owner, index
         logical       is_it_empty
         logical       ltrace2
 
         parameter   ( ltrace2 = .false. )

         if (ltrace2) then
            write(*,99) myid, 'register_read: reg_num= ',reg_num
            write(*,98) myid, 'register_read: name   = ',name
            write(*,99) myid, 'register_read: rank   = ',rank
            do i = 1, rank
               write(*,99) myid, 'register_read: shape=   ',shape(i)
            end do
  89        format('[',I3,'] ',A,3I18)
  91        format('[',I3,'] ',A,F12.5)
  92        format('[',I3,'] ',A,2I5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,1p,2F12.5)
  95        format('[',I3,'] ',A,I5,F12.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)
         end if

         ! Create new grid:
         grid      = grid_get_free_number()
         call grid_createB(grid, rank, shape, myid)

         coordsptr = grid_get_coords(grid)

         if (ltrace2) then
          write(*,99)myid,'register_read: read grid=',grid
          write(*,89)myid,'register_read: coordsptr=',coordsptr
          !write(*,99)myid,'register_read: coords=',grid_get_coords(grid)
          write(*,89)myid,'register_read: field =',grid_get_field(grid)
            write(*,99)myid,'register_read: step = ',step
            write(*,99)myid,'register_read: rank = ',rank
            write(*,98)myid,'register_read: name = ',name
         end if

         register_read = gft_read_full( name, step, shape,
     *                           cnames, rank, sdf_time,
     *                           q(coordsptr),
     *                           q(grid_get_field(grid))   )
         if (register_read .le. 0) then
            if(ltrace2)write(*,99)myid,'register_read:not read'
            return
         elseif (ltrace2) then
            !write(*,99)myid,'register_read: ||grid||=',grid_l2norm(grid)
            write(*,99)myid,'register_read: rc   = ',register_read
            write(*,91)myid,'register_read: ||grid||=',
     .      l2norm1D(q(grid_get_field(grid)),shape(1)*shape(2)*shape(3))
         end if

         !
         ! Extract the bounding box:
         !
         index  = 0
         do i = 1, rank
           bbox(2*i-1)  = q(coordsptr+index)
           bbox(2*i  )  = q(coordsptr+index+shape(i)-1)
           index        = index + shape(i)
           if(ltrace2)write(*,*)2*i-1, 2*i, index, bbox(2*i-1),bbox(2*i)
         end do
         !
         ! Store the bounding box:
         !
         call grid_set_bbox(grid, bbox)
         if (ltrace2) then
            do i = 1, rank
             write(*,94)myid,'register_read: bbox:',
     *              shape(i),bbox(2*i-1),bbox(2*i)
            end do
         end if

         ! Now create the tree, level, and grid
         if (ltrace2) then
            write(*,91)myid,'register_read: sdf_time:', sdf_time
            write(*,91)myid,'register_read: Calling register_add_tree'
         end if
         tree        = register_add_tree(reg_num, sdf_time)
           
         ! What happens if data set is not uniform resolution?
         h           = ( bbox(2) - bbox(1) )/( shape(1)*1.d0 - 1.d0 )
         level       = tree_add_level(tree, h)
         call grid_set_level(grid, level)

         ! Add new grid to the level
         call level_add_gridB(grid, level)
         if (ltrace2) then
            write(*,92)myid,'register_read: tree/level=',tree,level
            write(*,91)myid,'register_read:           h=',h
            write(*,91)myid,'register_read: Calling tree dump:'
            call tree_dump(tree)
            !write(*,91)myid,'register_read: Calling grid dump:'
            !call grid_dump(grid)
         end if
         
         return
      end             ! END: register_read


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_communicate:                                           cc
cc                         Talks to other processors and tells        cc
cc                         them about grids on this proc as well      cc
cc                         as listens and stores their info.          cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_communicate(reg_num)
      implicit none
      integer     reg_num
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc' 
      include 'registers.inc'
      include 'methods.inc'
         integer     num_grids, i, j, master_rank, rank
         real*8      transfer_array(9,MAX_NUM_GRIDS)
         !real*8      transfer_array(MAX_NUM_GRIDS, 9)
         logical     ltrace2
         parameter  (ltrace2=.false.)

  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,3I5)

         if(ltrace2) then
            write(*,99) myid, 'register_communicate: reg_num:',reg_num
         endif
           
         ! Communication
         do i = 0, numprocs - 1
            ! First figure out how many grids we are going to transfer
            ! Then send that info to all procs
            if(ltrace2)write(*,99)myid,' i = ', i 
            !
            if ( i .eq. myid ) then
               num_grids = grid_return_number_grids(reg_num)
            endif
            !
            if(ltrace2)write(*,99)myid,'reg_com:num_grids/i',num_grids,i
            call MPI_BCAST( num_grids, 1, MPI_INTEGER, i, 
     *                      MPI_COMM_WORLD, ierr )
            if(ltrace2)write(*,99)myid,'reg_com: num_grids=',num_grids
            if(ltrace2)write(*,99)myid,'reg_com: reg_num  =',reg_num
            !
            ! Routine loops over all grids and sets up trans_array 
            if ( i .eq. myid ) then
               call register_trans_setup( reg_num, transfer_array,
     *                                 num_grids )
            endif
            !
            if(ltrace2)write(*,97)myid,'reg_com: Get grid info'
            call MPI_BCAST( transfer_array, num_grids * 9, 
     *                      MPI_DOUBLE_PRECISION, i, MPI_COMM_WORLD, 
     *                      ierr )
            !
            if ( i .ne. myid ) then
               if(ltrace2)write(*,99)myid,'Create num_grids=',num_grids
               call register_grid_create( reg_num, transfer_array,
     *                                    num_grids, i )
            endif
         enddo

         ! If there is no information on the master proc then 
         ! its rank is zero because it has no data but the master
         ! procs register should have the same rank as the other procs
         ! at least I think it should.  This fixes the 
         ! the problems with merge if the master proc does not 
         ! have data. 
         
         ! If master does not have a rank this will give it one
         ! Must be a better way to do this
         rank = register_return_rank(reg_num)
         call MPI_Reduce(rank, master_rank,
     *                   1, MPI_INTEGER, MPI_MAX, master, 
     *                   MPI_COMM_WORLD, ierr)

         if ( myid .eq. master ) then
            call register_assign_rank(reg_num, master_rank) 
         endif
         return
      end     ! END: register_communicate

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc   register_grid_create:                                            cc
cc               Create grids communicated from other procs           cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_grid_create( reg_num, data_array, num_grids,
     *                                 owner)
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'
         integer reg_num, num_grids, owner
         real*8  data_array(9,num_grids)
               
         integer rank, shape(3), grid, ti, li, gi, i
         real*8  bbox(6), sdf_time, h
         logical empty 

         logical     ltrace2
         parameter ( ltrace2 = .false. )

  92        format('[',I3,'] ',A,F10.5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

         ! Loop over all grids in data_array
         do gi = 1, num_grids
            ! Restore data from data_array
            call trans_array_return( data_array, rank, bbox, 
     *                               sdf_time, h, gi, num_grids )

            ! Add tree to register 
            ti = register_add_tree(reg_num, sdf_time)
            li = tree_add_level(ti, h)
  
            do i = 1, rank
               shape(i) = 1 + NINT( ( bbox(2*i) - bbox(2*i-1) ) / h )
           if(ltrace2)write(*,94)myid,' ',shape(i),bbox(2*i),bbox(2*i-1)
            end do
            ! Create new grid:
            grid      = grid_get_free_number()
            call grid_createB(grid, rank, shape, owner)
            !call grid_set_bbox(grid, bbox)
            call grid_set_level(grid, li)
            ! Add new grid to the level
            call level_add_gridB(grid, li)

            if (ltrace2) then
               write(*,99)myid,'reg_grid_create: Creating =',gi
               write(*,99)myid,'reg_grid_create:     grid =',grid
               write(*,99)myid,'reg_grid_create:       ti =',ti
               write(*,99)myid,'reg_grid_create:       li =',li
               write(*,99)myid,'reg_grid_create:     rank =',rank
               write(*,99)myid,'reg_grid_create:    owner =',owner
               write(*,92)myid,'reg_grid_create:        h =',h
               write(*,92)myid,'reg_grid_create: sdf_time =',sdf_time
            end if
         enddo

         return
      end           ! END: register_grid_create


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_trans_setup:                                           cc
cc                                                                    cc
cc      Loops over all local grids and prep info to send to others    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_trans_setup( reg_num, transfer_array, 
     *                                 num_grids )
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc' 
      include 'methods.inc'
         integer reg_num, num_grids
         real*8  transfer_array(9,num_grids)
 
         integer ti, li, gi, rank, i,j
         real*8  sdf_time, h, bbox(6)

         logical     ltrace
         parameter ( ltrace = .false. )


  92        format('[',I3,'] ',A,F10.5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,5I5)

         if (ltrace) then
            write(*,99)myid,'reg_trans_setup:reg_num=',reg_num,num_grids
         end if

         ti = register_return_start(reg_num)
         i  = 1

         do while ( ti .ne. NULL_TREE )
           if(ltrace)write(*,99)myid,'reg_trans_setup: ti=',ti
           li = tree_return_start(ti)
            do while ( li .ne. NULL_LEVEL )
             if(ltrace)write(*,99)myid,'reg_trans_setup: li=',li
             gi = level_return_start(li)
              do while ( gi .ne. NULL_GRID )
                if(ltrace)write(*,99)myid,'reg_trans_setup: gi=',gi
                if (ltrace) call grid_dump(gi)
                if ( grid_is_local(gi) ) then
                  if(ltrace)write(*,99)myid,'reg_trans_setup: i=',i,gi
                  call       trans_array_setup( transfer_array, 
     *                                          num_grids, i, gi)
                  if (ltrace) then
                     write(*,99)myid,'reg_trans_setup: i=',i
                     call grid_dump(gi)
                     do j = 1, 9
                        write(*,92) myid, '     ',transfer_array(j,i)
                     end do
                  end if
                  i = i + 1
                endif
                !
                ! Increment grid
                gi = grid_return_sibling(gi)
              enddo
             ! Increment level
             li = level_return_sibling(li)
            enddo
           ! Increment tree
           ti = tree_return_sibling(ti)
         enddo

         return
      end         ! END: register_trans_setup 


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_list( )
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
         integer      reg

         integer      num_trees
         integer      ti

         !Functions
         integer      register_get_number_trees
         integer      tree_return_sibling
         integer      register_return_start

         logical       ltrace
         parameter   ( ltrace  = .false. )
        
         do reg = 1, MAX_NUM_REG
            ti = register_return_start(reg)
            if (ti.ne.NULL_TREE) then
               write(*,*) 'register_list: ',reg, reg_name(reg)(1:12)
            else
               if(ltrace) write(*,*) 'register_list: No trees',
     .                         reg, reg_name(reg)(1:12),ti
            end if
         enddo

         return
      end 

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_dump( reg )
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
         integer      reg

         integer      num_trees
         integer      ti

         !Functions
         integer      register_get_number_trees
         integer      tree_return_sibling
         integer      register_return_start
        
         !Get number of trees
         num_trees = register_get_number_trees( reg )
         
         !Write register name and its number
         write(*, *) ""

         write(*,'(A,I3.1,A,A,I2.1,A,A8,A,I3.1,A,I3.1)') "[",myid,"]", 
     *                 " Reg: ", reg, 
     *                 " Name: ", reg_name(reg), 
     *                 " NumTrees: ", num_trees,
     *                 " Rank: ", reg_rank(reg)

         ti = register_return_start(reg)

         do while( ti .ne. NULL_TREE )
            call tree_dump(ti)

            ti = tree_return_sibling(ti)
         enddo

         return
      end 
 
      integer function register_get_number_trees(register)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer   register
      
         integer   ti
         integer   num_trees

         !Functions
         integer   register_return_start
         integer   tree_return_sibling

         ti = register_return_start( register )
      
         if ( ti .ne. NULL_TREE ) then
            num_trees = 1
         endif

         do while ( tree_return_sibling(ti) .ne. NULL_TREE )
            num_trees = num_trees + 1
         
            !Increment tree counter
            ti = tree_return_sibling(ti)
         enddo

10       register_get_number_trees = num_trees
         return
      end

 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_set_rank(reg,rank)
      implicit none
      integer reg, rank
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'

         reg_rank(reg) = rank

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function register_return_rank(reg)
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
         integer reg

         register_return_rank = reg_rank(reg) 

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_filter( target_reg, reg, sdf_time, 
     *                                 res, name )
      implicit none
      include 'mpif.h' 
      include 'class_constants.inc'
      include 'registers.inc'  
      include 'variables.inc'
         integer       target_reg
         integer       reg
         real*8        sdf_time
         real*8        res                ! Resolution
         character*(*) name
  
         integer       ti                 ! Tree counter
         integer       ti_sibling
         integer       tree
         integer       rank
 
         real*8        time_fuzz_factor   ! Difference between times
         real*8        level_fuzz_factor  ! Difference between resolutions
         real*8        difference

         logical       ltrace2
         parameter   ( ltrace2 = .false. )

         !Functions
         integer       register_return_start 
         integer       register_return_rank
         integer       register_add_tree
         integer       tree_return_sibling
         real*8        tree_return_time
         logical       my_double_equal

         ! Get first tree in register 
         ti = register_return_start(target_reg)

         ti_sibling       = tree_return_sibling(ti)
         time_fuzz_factor = tree_return_time(ti_sibling) -
     *                      tree_return_time(ti)

         ! Figure out the time fuzz factor
         do while( tree_return_sibling(ti) .ne. NULL_TREE )
            ti_sibling = tree_return_sibling(ti)
            difference = tree_return_time(ti_sibling) - 
     *                   tree_return_time(ti)

            if( difference .lt. time_fuzz_factor ) then
               time_fuzz_factor = difference
            endif

            ! Increment
            ti = tree_return_sibling(ti)
         enddo

         if (ltrace2) then
            write(*,*) "Register_Filter: time        = ", sdf_time
            write(*,*) "Register_Filter: fuzz_factor = ", 
     *                  time_fuzz_factor
         endif


         ti = register_return_start( target_reg )

         ! Find what tree is equal to the time we want
         do while( ti .ne. NULL_TREE )
            write(*,*) "return_time = ", tree_return_time(ti)
            if( my_double_equal(tree_return_time(ti), sdf_time, 
     *                          time_fuzz_factor) ) then
               goto 20
            endif
 
            ! Increment tree
            ti = tree_return_sibling(ti)
         enddo 

         write(*,*) "Reg_Filter: Error time not found in the register"
         stop
         goto 30
 
20       tree = register_add_tree(reg, sdf_time)

         ! Retrieve rank of target register
         rank =  register_return_rank(target_reg)
         
         call register_assign_name(reg, name)
         call register_assign_rank(reg, rank)

         ! ti:    The tree being copied
         ! tree:  New tree
         call   tree_copy( ti, tree, res )

30       return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_assign_rank( register, rank )
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer register
         integer rank

         reg_rank(register) = rank 
   
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical  function register_store_rank(reg)
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
         integer        reg
         integer        step, gft_rc
 
         ! Functions
         integer        gft_read_rank

         logical       ltrace2
         parameter   ( ltrace2 = .false. )

         step = 1

         gft_rc = gft_read_rank(reg_myname(reg),step,reg_rank(reg))

         if (ltrace2) then
            write(*,98)myid, 'register_store_rank:name=',reg_myname(reg)
            write(*,99)myid, 'register_store_rank:rank=',reg_rank(reg)
  99        format('[',I3,'] ',A,I3)
  98        format('[',I3,'] ',A,A)
         end if

         register_store_rank = gft_rc .gt. 0

         return
      end        ! END: register_store_rank

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_rename( reg, name )
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
         integer        reg
         character*(*)  name
         !write(*,*) "[", myid, "] Rename: name = ", name
         call register_assign_name( reg, name )

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_unigrid_merge:                                         cc
cc                                                                    cc
cc         NB: Updated by SLL                                         cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_unigrid_merge( target_reg, name, new_reg)
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'
         integer       target_reg, new_reg
         character*(*) name
         real*8        bbox(6), global_bbox(6)
         integer       i, ti, li, gi, local_shape(3), rank, g_shape(3)
         integer       j, level
         ! New registers variables
         integer       tree, grid, owner
         real*8        sdf_time, h
         
         logical       ltrace
         parameter   ( ltrace  = .false. )
         logical       ltrace2
         parameter   ( ltrace2 = .false. )
        
         if(ltrace) then       
            write(*,99) myid,'Merge: Target reg:     ',target_reg
            write(*,99) myid,'Merge: New    reg:     ',new_reg
         end if

         rank = register_return_rank(target_reg)
         h    = reg_return_resolution(target_reg)

         ! Get global bbox 
         if( myid .eq. master ) then
            call register_find_global_bbox( target_reg, global_bbox ) 
            ! Determine shape:
            g_shape(1) = 1+NINT((global_bbox(2)-global_bbox(1))/h)
            g_shape(2) = 1+NINT((global_bbox(4)-global_bbox(3))/h)
            g_shape(3) = 1+NINT((global_bbox(6)-global_bbox(5))/h)
            !
            call   register_assign_name(new_reg, name)
            call   register_assign_rank(new_reg, rank)
            !
            if(ltrace) then       
               !call reg_dump_info(new_reg)
               write(*,*) "[0] Merge: Global bbox(1) = ", global_bbox(1)
               write(*,*) "[0] Merge: Global bbox(2) = ", global_bbox(2)
               write(*,*) "[0] Merge: Global bbox(3) = ", global_bbox(3)
               write(*,*) "[0] Merge: Global bbox(4) = ", global_bbox(4)
               write(*,*) "[0] Merge: Global bbox(5) = ", global_bbox(5)
               write(*,*) "[0] Merge: Global bbox(6) = ", global_bbox(6)
               write(*,*) "[0] Merge: g_shape(1)     = ", g_shape(1)
               write(*,*) "[0] Merge: g_shape(2)     = ", g_shape(2)
               write(*,*) "[0] Merge: g_shape(3)     = ", g_shape(3)
               write(*,96) myid,'Merge: h:              ',h
               write(*,99) myid,'Merge: Target reg rank:',rank
            end if
         endif
         !
         !if (ltrace) call reg_dump_info(target_reg)
         if (ltrace) call reg_dump_info(new_reg)
         !
         ! For each tree, do the merge:
         !
         ti = register_return_start( target_reg )
         do while ( ti .ne. NULL_TREE ) 
            ! Should only be one level
            li = tree_return_start(ti)
            h  = level_return_resolution(li)
            !
            ! Create new tree  in new register:
            if(myid .eq. master) then
              sdf_time = tree_return_time(ti)
              tree     = register_add_tree(new_reg, sdf_time) 
              level    = tree_add_level(tree, h) 
              grid     = grid_get_free_number()
              call grid_set_level(grid, level)
              call grid_createB(grid, rank, g_shape, myid)
              call grid_set_bbox(grid, global_bbox)
              call level_add_gridB(grid, level)
              if(ltrace) then
                  write(*,99)myid,'Merge:   New tree tree=',tree
                  write(*,96)myid,'Merge:   at time      =',sdf_time
                  write(*,96)myid,'Merge:   New level, h =',h
                  write(*,99)myid,' Calling level_add_grid'
              end if
            endif
            !
            ! Now actually do the merge
            !
            gi = level_return_start(li)
            do while ( gi .ne. NULL_GRID ) 
               owner = grid_return_owner(gi)
               if(ltrace2) then
                   write(*,99) myid, ' Work w/ gi = ',gi,owner
               end if
               if  (owner .eq. myid ) then
                  if(ltrace2) write(*,99) myid, ' We own  = ',gi
                  if (myid .eq. master) then
                     !
                     ! Grid is local:
                     !
                     if(ltrace) write(*,99) myid, ' localgi = ',gi
                     call grid_merge_local(gi, grid)
                  else
                     !
                     ! We are not master and have to send grid data:
                     !
                     if(ltrace) write(*,99) myid, ' Sending gi = ',gi
                     call grid_send_dataB(gi)
                     if(ltrace2) write(*,99) myid, " Sent!: gi = ",gi
                  end if
               else if ( myid  .eq. master) then
                  !
                  ! We are master and have to receive data:
                  !
                  if(ltrace) write(*,99) myid, ' Recv gi = ',gi
                  if(ltrace) write(*,99) myid, ' From own= ',owner
                  call grid_recv_dataB( gi, grid )
                  if(ltrace2) write(*,99) myid, " Recieved: gi = ",gi
               else
                  if(ltrace2) write(*,99) myid, " Ignoring  gi = ",gi
               endif
               ! Increment grid
               gi = grid_return_sibling(gi)
            enddo
            ! Increment tree
            ti = tree_return_sibling(ti)
         enddo

         if (ltrace2) then
            if (myid.eq.master) call reg_dump_info(new_reg)
            write(*,'(A,I4.1,A)') "[",myid,"] Leaving uni merge..."
         end if

  96        format('[',I3,'] ',A,F15.8)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,5I5)

         return
      end           ! END: register_unigrid_merge

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_find_global_bbox(reg, g_bbox)
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
         integer reg
         real*8  g_bbox(6)

         integer ti, li, gi
         real*8  bbox(6)
          
         ! Functions
         integer register_return_start
         integer tree_return_start
         integer level_return_start
         integer tree_return_sibling
         integer level_return_sibling
         integer grid_return_sibling        


         g_bbox(1) = 0
         g_bbox(2) = 0
         g_bbox(3) = 0
         g_bbox(4) = 0
         g_bbox(5) = 0
         g_bbox(6) = 0

         ! Loop over all grids; start by looping over all trees
         ti = register_return_start(reg)
       
         li = tree_return_start(ti)
         do while(li .ne. NULL_LEVEL)
            gi = level_return_start(li)
            do while(gi .ne. NULL_GRID)
               call grid_return_bbox(gi, bbox)

               if(bbox(1) .lt. g_bbox(1)) g_bbox(1) = bbox(1)
               if(bbox(2) .gt. g_bbox(2)) g_bbox(2) = bbox(2)
               if(bbox(3) .lt. g_bbox(3)) g_bbox(3) = bbox(3)
               if(bbox(4) .gt. g_bbox(4)) g_bbox(4) = bbox(4)
               if(bbox(5) .lt. g_bbox(5)) g_bbox(5) = bbox(5)
               if(bbox(6) .gt. g_bbox(6)) g_bbox(6) = bbox(6)
                  
               ! Increment grid
               gi = grid_return_sibling(gi)
            enddo ! End grid loop
            li = level_return_sibling(li)
         enddo ! End level loop
        
         return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_locmax:                                                cc
cc                       Reg2 = max(Reg2, Reg1) at each point.        cc
cc                          replaces register 2.                      cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_locmax( reg1, reg2)
      implicit none
      integer  reg1, reg2
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'methods.inc'
      include 'variables.inc'
      include 'mem.inc'

      integer  t1, l1, g1
      integer  t2, l2, g2

      logical        ltrace
      parameter (    ltrace = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_locmax: reg1  =',reg1
         write(*,9) myid, 'register_locmax: reg2  =',reg2
      end if

      ! Get first tree in register 
      t1 = register_return_start(reg1)
      t2 = register_return_start(reg2)

      !
      ! Loop over all times/tree:
      !
      do while (t2 .ne. NULL_TREE)
         if (ltrace) write(*,9)myid,'Consider t1/2=',t1,t2
         if (t1 .eq. NULL_TREE) then
            write(*,*) 'Registers do not have corresponding trees'
            return
         end if
         if (ltrace) call tree_dump(t1)
         if (ltrace) call tree_dump(t2)
         l1 = tree_return_start(t1)
         l2 = tree_return_start(t2)
         do while (l2 .ne. NULL_LEVEL)
            if (ltrace) write(*,9)myid,'   Consider l1/2=',l1,l2
            if (l1 .eq. NULL_LEVEL) then
               write(*,*) 'Trees do not have corresponding levels'
               return
            end if
            g1 = level_return_start(l1)
            g2 = level_return_start(l2)
            do while (g2 .ne. NULL_GRID)
               if (ltrace) write(*,9)myid,'      Consider g1/2=',g1,g2
               if (g1 .eq. NULL_GRID) then
                  write(*,*) 'Levels do not have corresponding grids'
                  return
               end if
               if (grid_is_local(g1).and.grid_is_local(g2)) then
                  if(ltrace)write(*,9)myid,'      *Process  g1/2=',g1,g2
                  if (grid_compatible(g1,g2) ) then
                     call grid_locmax(g1,g2)
                  else
                     write(*,*)'Grids not compatible: ',g1,g2
                     return
                  end if
               else if (grid_is_local(g1).or.grid_is_local(g2)) then
                  write(*,*) 'Corr. grids must be on same proc'
                  return
               end if
               g1 = grid_return_sibling(g1)
               g2 = grid_return_sibling(g2)
            end do
            l1 = level_return_sibling(l1)
            l2 = level_return_sibling(l2)
         end do
         !
         t1 = tree_return_sibling(t1)
         t2 = tree_return_sibling(t2)
      end do

      !
      !
      if (ltrace) then
         write(*,9) myid, 'register_locmax: Done'
   9     format('[',I3,'] ',A,5I5)
      end if

      return
      end      ! END: register_locmax

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_histo:                                                 cc
cc                 Make a histogram based on a field:                 cc
cc                   numbins -- number of bins to use                 cc
cc                   hmin    -- minimum value for first bin           cc
cc                   hmax    -- maximum value for last  bin           cc
cc                   reg     -- reg number for data to be integrated  cc
cc                   reg2    -- reg number for data to be binned      cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_histo( numbins, hmin, hmax, reg, reg2)
      implicit none
      integer  numbins, reg, reg2
      real*8   hmin, hmax   
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'methods.inc'
      include 'variables.inc'
      include 'mem.inc'
      integer  i, ti,  li,  gi
      integer     ti2, li2, gi2
      real*8        histo(numbins), histobins(numbins+1)
      real*8   real_histo(numbins), dh

      logical        ltrace
      parameter (    ltrace = .false. )
      logical        ltrace2
      parameter (    ltrace2 = .false. )
      logical        ltrace3
      parameter (    ltrace3 = .false. )

      if (ltrace) then
         write(*,*) 'register_histo: Generates a histogram'
         !write(*,*) 'register_histo: not implemented yet!'
         write(*,99)myid,'Entering Integration..reg: ', reg
         write(*,99)myid,'Entering Integration..reg2:', reg2
      end if
      if(ltrace3) call reg_dump_info(reg)

      ! Setup histogram:
      dh = (hmax-hmin)/(numbins  )
      do i = 1, numbins
         histo(i)     = 0.d0
         histobins(i) = hmin +(i-1)*dh
      end do
      histobins(numbins+1) = hmax
      if (ltrace) then
         do i = 1, numbins
            write(*,90)myid,'   Bin: ',i,histobins(i),histobins(i+1)
         end do
      end if

      ti   = register_return_start(reg)
      ti2  = register_return_start(reg2)
      ! Loop over all trees
      do while ( ti .ne. NULL_TREE .and. ti2.ne.NULL_TREE) 
            if(ltrace2)write(*,99)myid,'                     ti: ',ti
            call tree_set_mask(ti,MASK_INTEGRAL)
            ! do not need to mask second field
            ! Go to start of tree
            li  = tree_return_start(ti)
            li2 = tree_return_start(ti2)
            ! Zero out histogram:
            do i = 1, numbins
               !histo(i)     = 0.1d0*i
               histo(i)     = 0.d0
            end do
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL .and. li2.ne.NULL_LEVEL)   
               if(ltrace2)write(*,99)myid,'                    li: ',li
               if(ltrace2)write(*,99)myid,'                   li2: ',li2
               gi = level_return_start(li)
               gi2= level_return_start(li2)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID .and. gi2.ne.NULL_GRID) 
                  if( grid_is_local(gi)  .and.
     .                grid_is_local(gi2)          ) then
                     if(ltrace2)write(*,99)myid,'              gi: ',gi
                     if(ltrace2)write(*,99)myid,'             gi2: ',gi2
                     if (grid_compatible(gi,gi2) ) then
                        if(ltrace)write(*,99)myid,'reg_histo:compat:',
     .                                   gi,gi2
                        call grid_histo(gi,gi2,histo,histobins,numbins)
                     else
                         write(*,99)myid,'reg_histo:Grids incompat:',
     .                                   gi,gi2
                     end if
                  endif
                  gi = grid_return_sibling(gi)
                  gi2= grid_return_sibling(gi2)
               enddo
               li = level_return_sibling(li)
               li2= level_return_sibling(li2)
            enddo
            !
            if(ltrace2)write(*,99)myid,'reg_histo: calling MPI_Reduce'
            call MPI_Reduce(histo, real_histo, numbins, 
     *                    MPI_DOUBLE_PRECISION, MPI_SUM, master,
     *                    MPI_COMM_WORLD, ierr)
            if(ltrace2)write(*,99)myid,'reg_histo: back from MPI_Reduce'
            if ( myid .eq. master ) then
                ! output the histogram:
               do i = 1, numbins
                  write(*,96) i, tree_return_time(ti), 
     *                   histobins(i),histobins(i+1),real_histo(i)
               end do
            endif
            !
            ti = tree_return_sibling(ti)
            ti2= tree_return_sibling(ti2)
      end do

  90     format('[',I3,'] ',A,I5,3F18.11)
  96     format(I5,4G18.11)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,3F18.11)
  99     format('[',I3,'] ',A,3I5)

      return
      end      ! END: register_histo

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_subtract:                                              cc
cc                       Reg2 = Reg2 - Reg 1 In place.                cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_subtract( reg1, reg2)
      implicit none
      integer  reg1, reg2
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'methods.inc'
      include 'variables.inc'
      include 'mem.inc'
      character*32   name, file_name

      integer  t1, l1, g1
      integer  t2, l2, g2

      logical        ltrace
      parameter (    ltrace = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_subtract: reg1  =',reg1
         write(*,9) myid, 'register_subtract: reg2  =',reg2
      end if

      ! Subtraction done in place, and so rename:
      file_name = "subtraction"
      call register_assign_name(reg1, file_name)

      ! Get first tree in register 
      t1 = register_return_start(reg1)
      t2 = register_return_start(reg2)

      !
      ! Loop over all times/tree:
      !
      do while (t2 .ne. NULL_TREE)
         if (ltrace) write(*,9)myid,'Consider t1/2=',t1,t2
         if (t1 .eq. NULL_TREE) then
            write(*,*) 'Registers do not have corresponding trees'
            return
         end if
         if (ltrace) call tree_dump(t1)
         if (ltrace) call tree_dump(t2)
         l1 = tree_return_start(t1)
         l2 = tree_return_start(t2)
         do while (l2 .ne. NULL_LEVEL)
            if (ltrace) write(*,9)myid,'   Consider l1/2=',l1,l2
            if (l1 .eq. NULL_LEVEL) then
               write(*,*) 'Trees do not have corresponding levels'
               return
            end if
            g1 = level_return_start(l1)
            g2 = level_return_start(l2)
            do while (g2 .ne. NULL_GRID)
               if (ltrace) write(*,9)myid,'      Consider g1/2=',g1,g2
               if (g1 .eq. NULL_GRID) then
                  write(*,*) 'Levels do not have corresponding grids'
                  return
               end if
               if (grid_is_local(g1).and.grid_is_local(g2)) then
                  if(ltrace)write(*,9)myid,'      *Process  g1/2=',g1,g2
                  if (grid_compatible(g1,g2) ) then
                     call grid_subtract(g1,g2)
                  else
                     write(*,*)'Grids not compatible: ',g1,g2
                     return
                  end if
               else if (grid_is_local(g1).or.grid_is_local(g2)) then
                  write(*,*) 'Corr. grids must be on same proc'
                  return
               end if
               g1 = grid_return_sibling(g1)
               g2 = grid_return_sibling(g2)
            end do
            l1 = level_return_sibling(l1)
            l2 = level_return_sibling(l2)
         end do
         !
         t1 = tree_return_sibling(t1)
         t2 = tree_return_sibling(t2)
      end do

      !
      !
      if (ltrace) then
         write(*,9) myid, 'register_subtract: Done'
   9     format('[',I3,'] ',A,5I5)
      end if

      return
      end      ! END: register_subtract

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_subtractgen:                                           cc
cc                       Reg3 = Reg2 - Reg 1                          cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_subtractgen( reg1, reg2, reg3)
      implicit none
      integer  reg1, reg2, reg3
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'methods.inc'
      include 'variables.inc'
      include 'mem.inc'
      character*32   name, file_name
      real*8   time1, time2, res1, res2
      real*8   bbox(6), bbox1(6), bbox2(6)
      integer  i, rank, shape(3)
      integer  grid, level, tree

      integer  t1, l1, g1
      integer  t2, l2, g2

         real*8      LARGENUMBER
         parameter ( LARGENUMBER = 1.0d7 )
      logical        ltrace
      parameter (    ltrace = .false. )
      logical        ltrace2
      parameter (    ltrace2 = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_subtractgen: reg1  =',reg1
         write(*,9) myid, 'register_subtractgen: reg2  =',reg2
         write(*,9) myid, 'register_subtractgen: reg3  =',reg3
      end if

      ! Create new tree for the subtraction
      file_name = "subtraction"
      call register_assign_name(reg3, file_name)

      ! Get first common tree in register 
      t1    = register_return_start(reg1)
      time1 = -1.d0
      if (t1 .ne. NULL_TREE) time1 = tree_return_time(t1)
      t2    = register_return_start(reg2)
      time2 = -1.d0
      if (t2 .ne. NULL_TREE) time2 = tree_return_time(t2)

      !do while (time1.ge.0 .and. time2.ge.0)
      do while (t1.ne.NULL_TREE .and. t2.ne.NULL_TREE)
         if(ltrace)write(*,10)myid,"subtractgen: time1/2:",time1,time2
         if (.not.double_equal(time1,time2)) then
            if (time1.lt. time2) then
               t1 =tree_return_sibling(t1)
               if (t1.ne.NULL_TREE) time1 = tree_return_time(t1)
            else
               t2 =tree_return_sibling(t2)
               if (t2.ne.NULL_TREE) time2 = tree_return_time(t2)
            end if
         else
            if(ltrace)write(*,10)myid,"subtractgen: At same time",time1
            !
            ! Same time and so can process
            !
            l1   = tree_return_start(t1)
            res1 = -1.d0
            if (l1.ne.NULL_LEVEL) res1 = level_return_resolution(l1)
            l2   = tree_return_start(t2)
            res2 = -1.d0
            if (l2.ne.NULL_LEVEL) res2 = level_return_resolution(l2)
            do while (l1.ne.NULL_LEVEL .and. l2.ne.NULL_LEVEL)
            !do while (res1.ge.0 .and. res2.ge.0)
               if(ltrace)write(*,10)myid,"subtractgen:res1/2:",res1,res2
               if (.not.double_equal(res1,res2)) then
                  if (res1.lt.res2) then
                     l2 =level_return_sibling(l2)
                     if(l2.ne.NULL_TREE)res2=level_return_resolution(l2)
                  else
                     l1 =level_return_sibling(l1)
                     if(l1.ne.NULL_TREE)res1=level_return_resolution(l1)
                  end if
               else
                  !
                  ! Same time and resolution and can process now:
                  !
                  if(ltrace)write(*,10)myid,"subtractgen:Same res:",res1
                  !
                  ! Determine bounding boxes of each level
                  !     Bounding box of l1:
                      do i = 1, 3
                         bbox1(2*i-1) = LARGENUMBER
                         bbox1(2*i  ) =-LARGENUMBER
                         bbox2(2*i-1) = LARGENUMBER
                         bbox2(2*i  ) =-LARGENUMBER
                      end do
                      if(ltrace)write(*,10)myid,"finding bbox for l1:"
                      g1 = level_return_start(l1)
                      do while (g1.ne.NULL_GRID)
                         !
                         call grid_return_bbox(g1, bbox)
                         do i = 1, 3
                            if (bbox(2*i-1).lt.bbox1(2*i-1) ) then
                               bbox1(2*i-1) = bbox(2*i-1)
                            end if
                            if (bbox(2*i)  .gt.bbox1(2*i)   ) then
                               bbox1(2*i)   = bbox(2*i)
                            end if
                         end do
                         !
                         if(ltrace2)then
                            do i = 1, 3
                               write(*,10)myid,"bbox1: ",
     .                                   1.0d0*i,bbox1(2*i-1),bbox1(2*i)
                            end do
                         end if
                         g1 = grid_return_sibling(g1)
                      end do
                      !
                      if(ltrace)write(*,10)myid,"finding bbox for l2:"
                      g2 = level_return_start(l2)
                      do while (g2.ne.NULL_GRID)
                         !
                         call grid_return_bbox(g2, bbox)
                         do i = 1, 3
                            if (bbox(2*i-1).lt.bbox2(2*i-1) ) then
                               bbox2(2*i-1) = bbox(2*i-1)
                            end if
                            if (bbox(2*i)  .gt.bbox2(2*i)   ) then
                               bbox2(2*i)   = bbox(2*i)
                            end if
                         end do
                         !
                         if(ltrace2)then
                            do i = 1, 3
                               write(*,10)myid,"bbox2: ",
     .                                   1.0d0*i,bbox2(2*i-1),bbox2(2*i)
                            end do
                         end if
                         g2 = grid_return_sibling(g2)
                      end do
                      if(ltrace2)then
                         do i = 1, 3
                            write(*,10)myid,"bbox: ",
     .                        bbox1(2*i-1),bbox1(2*i),
     .                        bbox2(2*i-1),bbox2(2*i)
                         end do
                      end if
                  ! Compute intersection of the two levels
                  !    (and store in bbox())
                  !    (and determine shape)
                  do i = 1, 3
                    bbox(2*i-1) = max(bbox1(2*i-1),bbox2(2*i-1))
                    bbox(2*i)   = min(bbox1(2*i),  bbox2(2*i)  )
                    shape(i)    = NINT((bbox(2*i)-bbox(2*i-1))/res1) + 1
                  end do
                  if(ltrace2)then
                        write(*,10)myid,"res1 :",res1,res2
                     do i = 1, 3
                        write(*,11)myid,"Int. bbox: ",
     .                        shape(i), bbox(2*i-1),bbox(2*i)
                     end do
                  end if
                  !
                  ! Create grid for this intersection
                  !
                  rank       = register_return_rank(reg1)
                  !write(*,*) 'reg3 = ',reg3
                  !write(*,*) 'rank = ',rank
                  call register_set_rank(reg3,rank)
                  if(ltrace)write(*,9)myid,"subtractgen: rank:",rank
                  !
                  grid        = grid_get_free_number()
                  if(ltrace)write(*,9)myid,"subtractgen: grid:",grid
                  call grid_createB(grid, rank, shape, myid)
                  if(ltrace)write(*,9)myid,"subtractgen: Created grid"
!                 call grid_set_rank(grid,rank)
!                 if(ltrace)write(*,9)myid,"subtractgen: Arank=",
!    .                              grid_return_rank(grid)
                  if(ltrace)write(*,9)myid,"subtractgen: Set bbox"
                  call grid_set_bbox(grid, bbox)
!                 if(ltrace)write(*,9)myid,"subtractgen: Brank=",
!    .                              grid_return_rank(grid)
                  tree        = register_add_tree(reg3, time1)
!                 if(ltrace)write(*,9)myid,"subtractgen: Crank=",
!    .                              grid_return_rank(grid)
                  if(ltrace)write(*,9)myid,"subtractgen: tree:",tree
                  level       = tree_add_level(tree, res1)
!                 if(ltrace)write(*,9)myid,"subtractgen: Drank=",
!    .                              grid_return_rank(grid)
                  if(ltrace)write(*,9)myid,"subtractgen: level:",level
                  call grid_set_level(grid, level)
                  call level_add_gridB(grid, level)
                  !call grid_set_rank(grid,rank)
                  !call grid_return_shape(grid, shape)
                  !write(*,*) 'shape:',shape(1),shape(2)
                  !call grid_return_bbox(grid, bbox)
                  !write(*,*) 'bbox::',bbox(1),bbox(2)
                  !write(*,*) 'bbox::',bbox(3),bbox(4)
                  !call grid_dump(grid)
!                 if(ltrace)write(*,9)myid,"subtractgen: Arank=",
!    .                              grid_return_rank(grid)
                  if (ltrace) call grid_dump(grid)
                  ! Add new grid to the level
                  !call level_add_gridB(grid, level)
                  if (ltrace) call level_dump(level)
                  !
                  ! Loop over grid point and add/subtract corresponding points
                  !
                  !
                  if(ltrace)write(*,9)myid,
     .                      "subtractgen: Calling grid_interp_twotrees"
                  call grid_interp_twotrees(grid, t1, t2,OP_SUBTRACTGEN)
                  !
                  !
                  if(ltrace)write(*,10)myid,"subtractgen:Advance lev"
                  l1 =level_return_sibling(l1)
                  if (l1.ne.NULL_TREE) res1 =level_return_resolution(l1)
                  l2 =level_return_sibling(l2)
                  if (l2.ne.NULL_TREE) res2 =level_return_resolution(l2)
                  if(ltrace)write(*,9)myid,"subtractgen:l1/2:",l1,l2
               end if
            end do
            !
            !
            !
            if(ltrace)write(*,10)myid,"subtractgen:Advance to next tree"
            t1 =tree_return_sibling(t1)
            if (t1.ne.NULL_TREE) time1 = tree_return_time(t1)
            t2 =tree_return_sibling(t2)
            if (t2.ne.NULL_TREE) time2 = tree_return_time(t2)
         end if
      end do

      !
      !
      if (ltrace) then
         write(*,9) myid, 'register_subtractgen: Done'
   9     format('[',I3,'] ',A,5I5)
   10    format('[',I3,'] ',A,5G12.6)
   11    format('[',I3,'] ',A,I7,4G14.5)
      end if

      return
      end      ! END: register_subtractgen

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_subtractuno:                                           cc
cc                       Reg2 = Reg2 - Reg 1 In place.                cc
cc                Where Reg 1 has data at only one time               cc
cc                      and this data set gets subtracted from        cc
cc                      all data in Reg. 2.                           cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_subtractuno( reg1, reg2)
      implicit none
      integer  reg1, reg2
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'methods.inc'
      include 'variables.inc'
      include 'mem.inc'
      character*32   name, file_name

      integer  t1, l1, g1
      integer  t2, l2, g2

      logical        ltrace
      parameter (    ltrace = .false. )
      logical        ltrace2
      parameter (    ltrace2 = .false. )

      if (ltrace) then
         write(*,9) myid, 'register_subtractuno: reg1  =',reg1
         write(*,9) myid, 'register_subtractuno: reg2  =',reg2
      end if

      ! Subtraction done in place, and so rename:
      file_name = "subtraction"
      call register_assign_name(reg2, file_name)

      ! Get first tree in register 
      t1 = register_return_start(reg1)
      t2 = register_return_start(reg2)

      !
      ! Loop over all times/tree:
      !
      do while (t2 .ne. NULL_TREE)
         if (ltrace) write(*,9)myid,'Consider t1/2=',t1,t2
         if (ltrace2) call tree_dump(t1)
         if (ltrace2) call tree_dump(t2)
         l1 = tree_return_start(t1)
         l2 = tree_return_start(t2)
         do while (l2 .ne. NULL_LEVEL)
            if (ltrace) write(*,9)myid,'   Consider l1/2=',l1,l2
            if (l1 .eq. NULL_LEVEL) then
               write(*,*) 'Trees do not have corresponding levels'
               return
            end if
            g1 = level_return_start(l1)
            g2 = level_return_start(l2)
            do while (g2 .ne. NULL_GRID)
               if (ltrace) write(*,9)myid,'      Consider g1/2=',g1,g2
               if (g1 .eq. NULL_GRID) then
                  write(*,*) 'Levels do not have corresponding grids'
                  return
               end if
               if (grid_is_local(g1).and.grid_is_local(g2)) then
                  if(ltrace)write(*,9)myid,'      *Process  g1/2=',g1,g2
                  if (grid_compatible(g1,g2) ) then
                     call grid_subtract(g1,g2)
                  else
                     write(*,*)'Grids not compatible: ',g1,g2
                     return
                  end if
               else if (grid_is_local(g1).or.grid_is_local(g2)) then
                  write(*,*) 'Corr. grids must be on same proc'
                  return
               end if
               g1 = grid_return_sibling(g1)
               g2 = grid_return_sibling(g2)
            end do
            l1 = level_return_sibling(l1)
            l2 = level_return_sibling(l2)
         end do
         !
         ! Only a single tree
         !t1 = tree_return_sibling(t1)
         t2 = tree_return_sibling(t2)
      end do
      !
      !
      if (ltrace) then
         write(*,9) myid, 'register_subtractuno: Done'
   9     format('[',I3,'] ',A,5I5)
      end if

      return
      end      ! END: register_subtractuno

!
! Register_create_grid: Creates a new register or uses an existing one and then creates
!                       a tree or uses an existing tree and then same for level and same
!                       for grid and will finally return a grid which has already
!                       had its memory allocated.
! 

      function register_create_grid( new_reg, rank, name, sdf_time, h,
     *                               shape, bbox )
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc' 
      include 'methods.inc'
      include 'variables.inc'
         integer       new_reg, shape(3), rank
         real*8        sdf_time, h, bbox(6)
         character*(*) name
         logical       ltrace2
         parameter   ( ltrace2 = .false. )

         integer       tree, grid, li
         !integer       tree, level, grid
        
         if (ltrace2) then
            write(*,*) "Reg_Create_Grid: reg      = ", new_reg
            write(*,*) "Reg_Create_Grid: name     = ", name
            write(*,*) "Reg_Create_Grid: time     = ", sdf_time
            write(*,*) "Reg_Create_Grid: h        = ", h
            write(*,*) "Reg_Create_Grid: shape(1) = ", shape(1)
            write(*,*) "Reg_Create_Grid: shape(2) = ", shape(2)
            write(*,*) "Reg_Create_Grid: shape(3) = ", shape(3)
            write(*,*) "Reg_Create_Grid: bbox(1)  = ", bbox(1)
            write(*,*) "Reg_Create_Grid: bbox(2)  = ", bbox(2)
            write(*,*) "Reg_Create_Grid: bbox(3)  = ", bbox(3)
            write(*,*) "Reg_Create_Grid: bbox(4)  = ", bbox(4)
            write(*,*) "Reg_Create_Grid: bbox(5)  = ", bbox(5)
            write(*,*) "Reg_Create_Grid: bbox(6)  = ", bbox(6)
         endif
           
         call    register_assign_name(new_reg, name)         
         call    register_assign_rank(new_reg, rank)
         tree  = register_add_tree(new_reg, sdf_time)
         li    = tree_add_level(tree, h)
         write(*,*) 'Something very wrong if this is called'
         write(*,*) 'deprecated. Quitting...'
         stop
         !grid  = level_add_grid(li, shape, bbox, myid, .false.)

         register_create_grid = grid
         return
      end



cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_restrict( reg, new_reg, name )
      implicit none
      include 'mpif.h' 
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc' 
      include 'methods.inc'
      include 'mem.inc'
         integer reg, new_reg

         integer ti, li, gi, new_grid
         integer rank, shape(3), shape_coarse(3)
         real*8  h, bbox(6)
         character*(*) name
           
         ti   = register_return_start(reg)
         rank = register_return_rank(reg)

         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
          ! Go to start of tree
          li = tree_return_start(ti)

          ! Loop over all levels
          do while ( li .ne. NULL_LEVEL )   
           gi = level_return_start(li)

           ! Loop over all grids
           do while ( gi .ne. NULL_GRID .and. 
     *                grid_return_owner(gi) .eq. myid )
             call grid_return_shape(gi, shape)
             call grid_return_bbox(gi, bbox)
         
             ! Restrict the shape
             shape_coarse(1) = ((shape(1)-1) / 2 ) + 1
             shape_coarse(2) = ((shape(2)-1) / 2 ) + 1
             shape_coarse(3) = ((shape(3)-1) / 2 ) + 1
             h               = (bbox(2) - bbox(1)) /
     *                         (shape_coarse(1) - 1)
 
             new_grid = register_create_grid(new_reg, rank, 
     *                                       name, 
     *                                       tree_return_time(ti),
     *                                       h, shape_coarse, bbox)
            
             call restriction_operator(q(grid_get_field(gi)), shape,
     *                        q(grid_get_field(new_grid)), shape_coarse)
 
             gi = grid_return_sibling(gi)
           enddo
           li = level_return_sibling(li)
          enddo
          ti = tree_return_sibling(ti)
         enddo

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_l2norm:                                                cc
cc                                                                    cc
cc               Modeled after the integrate routine but requires     cc
cc         a different mask.                                          cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_l2norm(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  integral, bbox(6), bboxi(6), diff_x, diff_y, diff_z
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering Integration..reg: ', reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            call tree_set_mask(ti,MASK_L2NORM)
            ! Go to start of tree
            li = tree_return_start(ti)
            integral = 0.d0
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     integral =integral + grid_l2norm(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            call MPI_Reduce(integral, real_integral, 1, 
     *                    MPI_DOUBLE_PRECISION, MPI_SUM, master,
     *                    MPI_COMM_WORLD, ierr)
            if ( myid .eq. master ) then
                write(*,*) tree_return_time(ti), real_integral
            endif
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_l2norm


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_integrate:                                             cc
cc                                                                    cc
cc               Caculate the integral as a function of time for      cc
cc         the register using the finest points available at any      cc
cc         given spatial location.                                    cc
cc               This is achieved by creating a mask on all grids     cc
cc         so that the a given point only contributes to the          cc
cc         if the mask indicates that this is both the finest data    cc
cc         available there and if it is the "first" occurrence of     cc
cc         this point in the level.                                   cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_integrate(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         real*8      LARGENUMBER
         parameter ( LARGENUMBER = 1.0d98 )

         integer rank, ti, li, gi, li2, gi2, shape(3), nr, p
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  integral, bbox(6), bboxi(6), diff_x, diff_y, diff_z
         real*8  h, real_integral
         real*8  radius, dradius, mradius
         real*8   reg_return_maxradius, reg_return_maxresolution
         external reg_return_maxradius, reg_return_maxresolution

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering Integration..reg: ', reg
            call reg_dump_info(reg)
         end if

         ! Loop for different radii:
         radius  = 0.d0
         dradius = 5.d0*reg_return_maxresolution(reg)
         if (dradius .le. 0) dradius = 1.d0
         mradius = reg_return_maxradius(reg)
         nr      = mradius/dradius + 1
         if (.false.) then
            write(*,99)myid,'register_integrate:     nr = ',nr
            write(*,98)myid,'register_integrate:mradius = ',mradius
            write(*,98)myid,'register_integrate:dradius = ',dradius
         end if
         do p = 1, nr
         !
         radius = radius + dradius
         !
         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            call tree_set_mask(ti,MASK_INTEGRAL)
            ! Go to start of tree
            li = tree_return_start(ti)
            integral = 0.d0
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     integral =integral + grid_integrate(gi,radius)
                     !integral =integral + grid_integrate(gi,6.d0)
                     !integral =integral + grid_integrate(gi,LARGENUMBER)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            call MPI_Reduce(integral, real_integral, 1, 
     *                    MPI_DOUBLE_PRECISION, MPI_SUM, master,
     *                    MPI_COMM_WORLD, ierr)
            if ( myid .eq. master ) then
                write(*,*) tree_return_time(ti), radius, real_integral
                !write(*,*) tree_return_time(ti), real_integral
            endif
            !
            ti = tree_return_sibling(ti)
         enddo 
         !
         end do   ! end loop over larger and larger radii

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_integrate


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_minimum( reg ) 
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         integer     reg

         integer     ti, li, gi, shape(3), i, rank, proc_min
         !   Now compute up to 7 different minimums:
         !       global min == index 1
         !       min neg x  == index 2
         !       max neg x  == index 3
         !       min neg y  == index 4
         !       max neg y  == index 5
         !       min neg z  == index 6
         !       max neg z  == index 7
         !   NB: coords stores the locations of these
         !       minimas and is densely packed when rank<3
         real*8      local_min(7), true_min(7), temp_min(7), coords(21)
         !real*8      local_min, true_min, temp_min

         real*8      LARGENUMBER
         parameter ( LARGENUMBER = 1.0d98 )

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         if (ltrace2) then
            write(*,'(A,I4.1,A,I4.1)')"[",myid,
     *                      "] Enter Register Minimum",reg
         end if

         rank      = register_return_rank(reg)
         ! Loop over all grids
         ti        = register_return_start(reg)
         if (ltrace2) write(*,*) 'register_minimum: ti = ',ti

         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            do i = 1, 1+2*rank
               local_min(i) = LARGENUMBER
            end do
            li        = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if (ltrace2) write(*,*) 'register_minimum: li = ',li
               gi        = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if (ltrace2) write(*,*)'register_minimum: gi = ',gi
                     call grid_find_minimums(gi,local_min,coords)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            ! Get global minimum for this tree/time:
            if(ltrace2)write(*,*)'register_minimum: Let all procs know'
            call MPI_AllReduce(local_min, true_min, 1+2*rank,
     *                    MPI_DOUBLE_PRECISION, MPI_MIN,
     *                    MPI_COMM_WORLD, ierr)
            ! All processes now have the true_minimums,
            ! now we have to get the coordinates for each:
            !
            !   Coordinates for global min:
            do i = 1, 1+2*rank
               !
               if(ltrace2)write(*,*)'register_minimum: min(i),i=',
     .                 true_min(i),i
               if (double_equal(true_min(i),local_min(i))) then
                  call MPI_AllReduce(myid,proc_min,1,MPI_INTEGER,
     *                         MPI_MIN,MPI_COMM_WORLD,ierr)
                  if(ltrace2)write(*,*)myid,' I have the true max',i
               else
                  call MPI_AllReduce(numprocs,proc_min,1,MPI_INTEGER,
     *                         MPI_MIN,MPI_COMM_WORLD,ierr)
                  if(ltrace2)write(*,*)myid,'I DONT have the true max',i
               end if
               ! When the minimum is infinity can have problems
               if (proc_min.lt.0 .or. proc_min.ge.numprocs) then
                  proc_min = 0
               end if
               if(ltrace2)write(*,*)myid,'Proc w/ the min is',proc_min
               call MPI_BCAST(coords(rank*(i-1)+1),  rank,
     &          MPI_DOUBLE_PRECISION, proc_min, MPI_COMM_WORLD, ierr)
               if(ltrace2) then
                  if (rank.eq.1) then
                     write(*,*) 'min: ',i,true_min(i),
     &              coords(rank*(i-1)+1)
                  else if (rank.eq.2) then
                     write(*,*) 'min: ',i,true_min(i),
     &              coords(rank*(i-1)+1),coords(rank*(i-1)+2)
                  else 
                     write(*,*) 'min: ',i,true_min(i),
     &              coords(rank*(i-1)+1),coords(rank*(i-1)+2),
     &              coords(rank*(i-1)+3)
                  end if
               end if
            end do
            !
            if(myid .eq. MASTER) then
                if (.true.) then
                ! Simple output:
                write(*,'(2F16.9)')
     *                  tree_return_time(ti), true_min(1)
                else
               if (rank.eq.1) then
               write(*,'(3F16.9)') tree_return_time(ti), true_min(1),
     *               true_min(2),true_min(3)
               elseif (rank.eq.2) then
               write(*,'(5F16.9)') tree_return_time(ti), true_min(1),
     *                true_min(2),true_min(3),true_min(4),true_min(5)
               else
               write(*,'(7F11.4)') tree_return_time(ti), true_min(1),
     *               true_min(2),true_min(3),true_min(4),true_min(5),
     *               true_min(6),true_min(7)
               end if
               end if
               ! File output:
  93     format(G16.9,' ',G16.9,' ',G16.9)
  94     format(G14.7, ' ',G14.7, ' ',G14.7, ' ',G14.7)
  95     format(G12.5, ' ',G12.5, ' ',G12.5, ' ',G12.5,' ',G12.5)
               open(61,file='.globmin',position='append')
               if (rank.eq.1) then
               write(61,93) tree_return_time(ti), true_min(1),
     *               coords(1)
               elseif (rank.eq.2) then
               write(61,94) tree_return_time(ti), true_min(1),
     *               coords(1),coords(2)
               else
               write(61,95) tree_return_time(ti), true_min(1),
     *               coords(1),coords(2),coords(3)
               end if
               close(61)
               open(61,file='.negxmin',position='append')
               if (rank.eq.1) then
               write(61,93) tree_return_time(ti), true_min(2),
     *               coords(2)
               elseif (rank.eq.2) then
               write(61,94) tree_return_time(ti), true_min(2),
     *               coords(3),coords(4)
               else
               write(61,95) tree_return_time(ti), true_min(2),
     *               coords(4),coords(5),coords(6)
               end if
               close(61)
               open(61,file='.posxmin',position='append')
               if (rank.eq.1) then
               write(61,93) tree_return_time(ti), true_min(3),
     *               coords(3)
               elseif (rank.eq.2) then
               write(61,94) tree_return_time(ti), true_min(3),
     *               coords(5),coords(6)
               else
               write(61,95) tree_return_time(ti), true_min(3),
     *               coords(7),coords(8),coords(9)
               end if
               close(61)
               if (rank.gt.1)then
               open(61,file='.negymin',position='append')
               if (rank.eq.2) then
               write(61,94) tree_return_time(ti), true_min(4),
     *               coords(7),coords(8)
               else
               write(61,95) tree_return_time(ti), true_min(4),
     *               coords(10),coords(11),coords(12)
               end if
               close(61)
               open(61,file='.posymin',position='append')
               if (rank.eq.2) then
               write(61,94) tree_return_time(ti), true_min(5),
     *               coords(9),coords(10)
               else
               write(61,95) tree_return_time(ti), true_min(5),
     *               coords(13),coords(14),coords(15)
               end if
               close(61)
               end if
               if (rank.gt.2)then
               open(61,file='.negzmin',position='append')
               write(61,95) tree_return_time(ti), true_min(6),
     *               coords(16),coords(17),coords(18)
               close(61)
               open(61,file='.poszmin',position='append')
               write(61,95) tree_return_time(ti), true_min(7),
     *               coords(19),coords(20),coords(21)
               close(61)
               end if
            end if
            ! Go to next tree/time:
            ti = tree_return_sibling(ti)
         enddo
         
         return
      end         ! END: register_minimum

      
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_maxhalfplane( reg,axisnum,halfplane ) 
      implicit none
      integer     reg, axisnum, halfplane
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer     ti, li, gi, shape(3), rank, i
         ! Identity of processor which contains the max:
         integer     proc_min
         real*8      local_max, true_max, temp_max
         ! Coords of maximum:
         real*8      coords(3),lcoords(3)

         real*8      LARGENUMBER
         parameter ( LARGENUMBER = 1.0d98 )

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         if (ltrace2) then
            write(*,'(A,I4.1,A)')"[",myid,"]Enter Register MaxHalfPlane"
            write(*,99) myid,'       reg: ',reg
            write(*,99) myid,'   axisnum: ',axisnum
            write(*,99) myid,' halfplane: ',halfplane
  95        format('[',I3,'] ',A,3G13.3)
  96        format('[',I3,'] ',A,I5,G13.3)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)
            !call reg_dump_info(reg)
         end if

         ! Loop over all grids
         ti        = register_return_start(reg)
         rank      = register_return_rank(reg)
         if(ltrace2)write(*,99)myid,'register_halfmax: ti   = ',ti
         if(ltrace2)write(*,99)myid,'register_halfmax: rank = ',rank

         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            local_max = -LARGENUMBER
            li        = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace2)write(*,99)myid,'register_halfmax: li = ',li
               gi        = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace2)write(*,99)myid,'register_halfmax:gi',gi
                     !call grid_find_max(gi,temp_max,coords)
                     call grid_find_halfmax(gi,temp_max,coords,
     *                              axisnum,halfplane)
                     !temp_max =grid_return_max(gi)
                     if(ltrace2) write(*,95)
     *                    myid,'register_halfmax: tmpmax=',temp_max
                     if ( temp_max .gt. local_max ) then
                        local_max = temp_max
                        lcoords(1) = coords(1)
                        lcoords(2) = coords(2)
                        lcoords(3) = coords(3)
                        if(ltrace2)write(*,95)
     *                     myid,'register_halfmax: max= ',local_max
                     end if
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            ! Get global minimum for this tree/time:
            if (ltrace2) then
               write(*,95) myid, ' My local max = ',local_max
               do i = 1, rank
                  write(*,96) myid, ' i Local coords: ',i,lcoords(i)
               end do
            end if
            call MPI_Reduce(local_max, true_max, 1,
     *                    MPI_DOUBLE_PRECISION, MPI_MAX, MASTER,
     *                    MPI_COMM_WORLD, ierr)
            call MPI_BCAST(true_max, 1, MPI_DOUBLE_PRECISION,
     &                                  master, MPI_COMM_WORLD, ierr)
            if(ltrace2)write(*,95) myid,'true_max = ',true_max
            ! All procs have the true_max, now figure out which
            ! proc has it so we can get coordinates:
            !write(*,95)myid,'local/true max = ',local_max,true_max
            if (double_equal(local_max,true_max)) then
              call MPI_Reduce(myid,proc_min,1,MPI_INTEGER,
     *                         MPI_MIN,master,MPI_COMM_WORLD,ierr)
               if(ltrace2)write(*,99)myid,' I have the true max'
            else
                  call MPI_Reduce(numprocs,proc_min,1,MPI_INTEGER,
     *                         MPI_MIN,master,MPI_COMM_WORLD,ierr)
               if(ltrace2)write(*,99)myid,' I do NOT have the true max'
            end if
            ! Let everyone know that proc_min will broadcast coords:
            call MPI_BCAST(proc_min, 1, MPI_INTEGER,
     &                                  master, MPI_COMM_WORLD, ierr)
            ! Everyone now gets the coords of the min from proc_min:
            if(ltrace2)write(*,*)myid,' The proc w/ the max is',proc_min
            call MPI_BCAST(lcoords,  rank, MPI_DOUBLE_PRECISION,
     &                            proc_min, MPI_COMM_WORLD, ierr)
            if (ltrace2) then
               do i = 1, rank
                  write(*,96) myid, ' i/coords: ',i,lcoords(i)
               end do
            end if
            !
            if (myid.eq. MASTER) then
               !write(*,*) 'register_maximum: proc_min: ',proc_min
               if (rank .eq. 1) then
                  !write(*,'(3G18.11)') tree_return_time(ti), true_max,
                  write(*,83) tree_return_time(ti), true_max,
     *              lcoords(1)
               else if (rank .eq. 2) then
                  write(*,84) tree_return_time(ti), true_max,
     *              lcoords(1), lcoords(2)
               else if (rank .eq. 3) then
                  write(*,85) tree_return_time(ti), true_max,
     *              lcoords(1), lcoords(2), lcoords(3)
               else 
                  write(*,*) 'register_halfmax: Unknown rank'
               end if
            end if
            if(ltrace2)write(*,99)myid,'register_halfmax: Done w:',ti
            ! Go to next tree/time:
            ti = tree_return_sibling(ti)
         enddo
         
         if(ltrace2)write(*,99)myid,'register_halfmax: Done.'
  83     format(G18.11,' ',G18.11,' ',G18.11)
  84     format(G18.11,' ',G18.11,' ',G18.11,' ',G18.11)
  85     format(G18.11,' ',G18.11,' ',G18.11,' ',G18.11,' ',G18.11)
         return
      end         ! END: register_halfmax

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_maximum( reg ) 
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         integer     reg

         integer     ti, li, gi, shape(3), rank, i
         ! Identity of processor which contains the max:
         integer     proc_min
         real*8      local_max, true_max, temp_max
         ! Coords of maximum:
         real*8      coords(3), lcoords(3)

         real*8      LARGENUMBER
         parameter ( LARGENUMBER = 1.0d98 )

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         if (ltrace2) then
            write(*,'(A,I4.1,A)')"[",myid,"] Entering Register Maximum"
  95        format('[',I3,'] ',A,3E10.3)
  96        format('[',I3,'] ',A,I5,E10.3)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)
            call reg_dump_info(reg)
         end if

         ! Loop over all grids
         ti        = register_return_start(reg)
         rank      = register_return_rank(reg)
         if(ltrace2)write(*,99)myid,'register_maximum: ti   = ',ti
         if(ltrace2)write(*,99)myid,'register_maximum: rank = ',rank

         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            local_max = -LARGENUMBER
            li        = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace2)write(*,99)myid,'register_maximum: li = ',li
               gi        = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace2)write(*,99)myid,'register_maximum:gi',gi
                     call grid_find_max(gi,temp_max,coords)
                     !temp_max =grid_return_max(gi)
                     if(ltrace2) write(*,95)
     *                    myid,'register_maximum: tmpmax=',temp_max
                     if ( temp_max .gt. local_max ) then
                        local_max = temp_max
                        lcoords(1)= coords(1)
                        lcoords(2)= coords(2)
                        lcoords(3)= coords(3)
                        if(ltrace2)write(*,95)
     *                     myid,'register_maximum: max= ',local_max
                     end if
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            ! Get global minimum for this tree/time:
            if (ltrace2) then
               write(*,95) myid, ' My local max = ',local_max
               do i = 1, rank
                  write(*,96) myid, ' i Local coords: ',i,lcoords(i)
               end do
            end if
            call MPI_Reduce(local_max, true_max, 1,
     *                    MPI_DOUBLE_PRECISION, MPI_MAX, MASTER,
     *                    MPI_COMM_WORLD, ierr)
            call MPI_BCAST(true_max, 1, MPI_DOUBLE_PRECISION,
     &                                  master, MPI_COMM_WORLD, ierr)
            if(ltrace2)write(*,95) myid,'true_max = ',true_max
            ! All procs have the true_max, now figure out which
            ! proc has it so we can get coordinates:
            !write(*,95)myid,'local/true max = ',local_max,true_max
            if (double_equal(local_max,true_max)) then
              call MPI_Reduce(myid,proc_min,1,MPI_INTEGER,
     *                         MPI_MIN,master,MPI_COMM_WORLD,ierr)
               if(ltrace2)write(*,99)myid,' I have the true max'
            else
                  call MPI_Reduce(numprocs,proc_min,1,MPI_INTEGER,
     *                         MPI_MIN,master,MPI_COMM_WORLD,ierr)
               if(ltrace2)write(*,99)myid,' I do NOT have the true max'
            end if
            ! Let everyone know that proc_min will broadcast coords:
            call MPI_BCAST(proc_min, 1, MPI_INTEGER,
     &                                  master, MPI_COMM_WORLD, ierr)
            ! Everyone now gets the coords of the min from proc_min:
            if(ltrace2)write(*,*)myid,' The proc w/ the max is',proc_min
            call MPI_BCAST(lcoords,  rank, MPI_DOUBLE_PRECISION,
     &                            proc_min, MPI_COMM_WORLD, ierr)
            if (ltrace2) then
               do i = 1, rank
                  write(*,96) myid, ' i/coords: ',i,lcoords(i)
               end do
            end if
            !
            if (myid.eq. MASTER) then
               !write(*,*) 'register_maximum: proc_min: ',proc_min
               if (rank .eq. 1) then
                  write(*,83) tree_return_time(ti), true_max,
     *              lcoords(1)
               else if (rank .eq. 2) then
                  write(*,84) tree_return_time(ti), true_max,
     *              lcoords(1), lcoords(2)
               else if (rank .eq. 3) then
                  write(*,85) tree_return_time(ti), true_max,
     *              lcoords(1), lcoords(2), lcoords(3)
               else 
                  write(*,*) 'register_maximum: Unknown rank'
               end if
            end if
            if(ltrace2)write(*,99)myid,'register_maximum: Done w:',ti
            ! Go to next tree/time:
            ti = tree_return_sibling(ti)
         enddo
         
         if(ltrace2)write(*,99)myid,'register_maximum: Done.'
  83     format(G18.11,' ',G18.11,' ',G18.11)
  84     format(G18.11,' ',G18.11,' ',G18.11,' ',G18.11)
  85     format(G18.11,' ',G18.11,' ',G18.11,' ',G18.11,' ',G18.11)
         return
      end         ! END: register_maximum

      
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_maxabs( reg ) 
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         integer     reg

         integer     ti, li, gi, shape(3)
         real*8      local_max, true_max, temp_max
         !real*8      local_min, true_min, temp_min

         real*8      LARGENUMBER
         parameter ( LARGENUMBER = 1.0d98 )

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         if (ltrace2) then
            write(*,'(A,I4.1,A)')"[",myid,"] Entering Register Maximum"
         end if

         ! Loop over all grids
         ti        = register_return_start(reg)
         if (ltrace2) write(*,*) 'register_maximum: ti = ',ti

         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            local_max = -LARGENUMBER
            li        = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if (ltrace2) write(*,*) 'register_maximum: li = ',li
               gi        = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if (ltrace2) write(*,*)'register_maximum: gi = ',gi
                     temp_max =grid_return_maxabs(gi)
                     if (ltrace2)
     *                    write(*,*)'register_maximum: tmpmax=',temp_max
                     if ( temp_max .gt. local_max ) then
                        local_max = temp_max
                        if (ltrace2)
     *                     write(*,*)'register_maximum: max= ',local_max
                     end if
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            ! Get global minimum for this tree/time:
            if (numprocs .gt. 1) then
               call MPI_Reduce(local_max, true_max, 1,
     *                    MPI_DOUBLE_PRECISION, MPI_MAX, MASTER,
     *                    MPI_COMM_WORLD, ierr)
            else
               true_max = local_max
            end if
            if(myid .eq. MASTER) write(*,'(2G18.11)') 
     *                  tree_return_time(ti), true_max
            ! Go to next tree/time:
            ti = tree_return_sibling(ti)
         enddo
         
         return
      end         ! END: register_maxabs

      
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_times:                                                 cc
cc           Multiply a field by one of its coordinates               cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_times(reg, dir)
      implicit none
      integer reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering times..reg: ', reg
            write(*,99)myid,'Entering times..dir: ', dir
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_times(gi,dir)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_times

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_curl:                                                  cc
cc           Compute curl of a vector field.                          cc
cc       NB: adapting so that it works with 2D data                   cc
cc           producing just a z-component of the curl.                cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_curl(regx,regy,regz,curlx,curly,curlz)
      implicit none
      integer  regx, regy, regz, curlx, curly, curlz
      !integer  reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_curl: regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_curl:curlx/y/z:',curlx,curly,curlz
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
            call reg_dump_info(regy)
            call reg_dump_info(regz)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation of curl
         !
         call register_copy(regx, curlx)
         call register_copy(regy, curly)
         call register_copy(regz, curlz)

         file_name = "curlx"
         call register_assign_name(curlx, file_name)
         file_name = "curly"
         call register_assign_name(curly, file_name)
         file_name = "curlz"
         call register_assign_name(curlz, file_name)
         call register_return_myname(curlx, name)
         if(ltrace) then
            write(*,97)myid,'register_curl: name: ',name
         end if

         t1   = register_return_start(regx)
         t2   = register_return_start(regy)
         t3   = register_return_start(regz)
         tx   = register_return_start(curlx)
         ty   = register_return_start(curly)
         tz   = register_return_start(curlz)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              tx .ne. NULL_TREE )
!    *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 lx .ne. NULL_LEVEL  )
!    *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    gx .ne. NULL_GRID  )
!    *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(gx) ) then
!    *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     call grid_curl(g1,g2,g3, gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_curl

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_uncurl:                                                cc
cc           Implement Louis' uncurl algorithm within sdftool.        cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_uncurl(regBx,regBy,regBz,Ax,Ay,Az)
      implicit none
      integer  regBx, regBy, regBz, Ax, Ay, Az
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
          write(*,99)myid,'register_uncurl:regBx/y/z:',regBx,regBy,regBz
          write(*,99)myid,'register_uncurl:   Ax/y/z:',Ax,Ay,Az
         end if
         if(ltrace2) then
            call reg_dump_info(regBx)
            call reg_dump_info(regBy)
            call reg_dump_info(regBz)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation of curl
         !
         call register_copy(regBx, Ax)
         call register_copy(regBy, Ay)
         call register_copy(regBz, Az)

         file_name = "uncurlAx"
         call register_assign_name(Ax, file_name)
         file_name = "uncurlAy"
         call register_assign_name(Ay, file_name)
         file_name = "uncurlAz"
         call register_assign_name(Az, file_name)
         call register_return_myname(Ax, name)
         if(ltrace) then
            write(*,97)myid,'register_curl: name: ',name
         end if

         t1   = register_return_start(regBx)
         t2   = register_return_start(regBy)
         t3   = register_return_start(regBz)
         tx   = register_return_start(Ax)
         ty   = register_return_start(Ay)
         tz   = register_return_start(Az)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     call grid_uncurl(g1,g2,g3, gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_uncurl

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_grad                                                   cc
cc           Compute the gradient of a scalar field                   cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_grad(regscal,regx, regy,regz)
      implicit none
      integer  regscal, regx, regy, regz
      !integer  reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1,         tx, ty, tz
         integer l1,         lx, ly, lz
         integer g1,         gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_grad: regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_grad: regscal: ',regscal
         end if
         if(ltrace2) then
            call reg_dump_info(regscal)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation of curl
         !
         call register_copy(regscal, regx)
         call register_copy(regscal, regy)
         call register_copy(regscal, regz)

         file_name = "gradx"
         call register_assign_name(regx, file_name)
         file_name = "grady"
         call register_assign_name(regy, file_name)
         file_name = "gradz"
         call register_assign_name(regz, file_name)
         call register_return_myname(regx, name)
         if(ltrace) then
            write(*,97)myid,'register_grad: name: ',name
         end if

         t1   = register_return_start(regscal)
         tx   = register_return_start(regx)
         ty   = register_return_start(regy)
         tz   = register_return_start(regz)
         if(ltrace)write(*,99)myid,'    start:   t1:     ',t1
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              tx .ne. NULL_TREE .and.
     *              ty .ne. NULL_TREE .and.
     *              tz .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'    start:   t1:     ',t1
            if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 lx .ne. NULL_LEVEL .and.
     *                 ly .ne. NULL_LEVEL .and.
     *                 lz .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1:     ',l1
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    gx .ne. NULL_GRID .and.
     *                    gy .ne. NULL_GRID .and.
     *                    gz .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(gx) .and.
     *                grid_is_local(gy) .and.
     *                grid_is_local(gz)       ) then
                     if(ltrace)write(*,99)myid,'      g1:     ',g1
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     call grid_grad(g1, gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

         if(ltrace) then
            write(*,97)myid,'register_grad: Done.'
         end if

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_grad

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_div:                                                   cc
cc           Compute divergence of a vector field                     cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_div(regscal,regx, regy,regz)
      implicit none
      integer  regscal, regx, regy, regz
      !integer  reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1,         tx, ty, tz
         integer l1,         lx, ly, lz
         integer g1,         gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_div: regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_div: regscal: ',regscal
         end if
         if(ltrace2) then
            call reg_dump_info(regscal)
         end if

         !
         ! copy grid structure over to the register that will hold
         ! the computation of the divergence
         !
         call register_copy(regx, regscal)
         !call register_copy(regscal, regx)

         file_name = "div"
         call register_assign_name(regscal, file_name)
         call register_return_myname(regx, name)
         if(ltrace) then
            write(*,97)myid,'register_div: name: ',name
         end if

         t1   = register_return_start(regscal)
         tx   = register_return_start(regx)
         ty   = register_return_start(regy)
         tz   = register_return_start(regz)
         if(ltrace)write(*,99)myid,'    start:   t1:     ',t1
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              tx .ne. NULL_TREE .and.
     *              ty .ne. NULL_TREE .and.
     *              tz .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'    start:   t1:     ',t1
            if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 lx .ne. NULL_LEVEL .and.
     *                 ly .ne. NULL_LEVEL .and.
     *                 lz .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1:     ',l1
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    gx .ne. NULL_GRID .and.
     *                    gy .ne. NULL_GRID .and.
     *                    gz .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(gx) .and.
     *                grid_is_local(gy) .and.
     *                grid_is_local(gz)       ) then
                     if(ltrace)write(*,99)myid,'      g1:     ',g1
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     call grid_div(g1, gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_div

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_cross:                                                 cc
cc           Compute cross product of two vectors                     cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_cross(regx,regy,regz,crossx,crossy,crossz)
      implicit none
      integer  regx, regy, regz, crossx, crossy, crossz
      !integer  reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_cross: regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_cross:crossx/y/z:',crossx,crossy,
     .                                                   crossz
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
            call reg_dump_info(regy)
            call reg_dump_info(regz)
         end if

         !
         ! Computing the cross product and sticking it in
         ! the same register as the second vector so no 
         ! copying is needed.
         !

         file_name = "crossx"
         call register_assign_name(crossx, file_name)
         file_name = "crossy"
         call register_assign_name(crossy, file_name)
         file_name = "crossz"
         call register_assign_name(crossz, file_name)
         call register_return_myname(crossx, name)
         if(ltrace) then
            write(*,97)myid,'register_cross: name: ',name
         end if

         t1   = register_return_start(regx)
         t2   = register_return_start(regy)
         t3   = register_return_start(regz)
         tx   = register_return_start(crossx)
         ty   = register_return_start(crossy)
         tz   = register_return_start(crossz)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     ! Compute and stick in second grid
                     call grid_cross(g1,g2,g3, gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_cross


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_dot:                                                   cc
cc           Compute dot   product of two vectors                     cc
cc           and put in the x-component of second vector.             cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_dot(regx,regy,regz,crossx,crossy,crossz)
      implicit none
      integer  regx, regy, regz, crossx, crossy, crossz
      !integer  reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_dot: regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_dot:crossx/y/z:',crossx,crossy,
     *                                                 crossz
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
            call reg_dump_info(regy)
            call reg_dump_info(regz)
         end if

         !
         ! Computing the cross product and sticking it in
         ! the same register as the second vector so no 
         ! copying is needed.
         !

         file_name = "dot"
         call register_assign_name(crossx, file_name)
         call register_return_myname(crossx, name)
         if(ltrace) then
            write(*,97)myid,'register_dot: name: ',name
         end if

         t1   = register_return_start(regx)
         t2   = register_return_start(regy)
         t3   = register_return_start(regz)
         tx   = register_return_start(crossx)
         ty   = register_return_start(crossy)
         tz   = register_return_start(crossz)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     ! Compute and stick in gx grid
                     call grid_dot(g1,g2,g3, gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_dot


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_vecmag:                                                cc
cc           Compute the magnitude of a vector                        cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_vecmag(regx,regy,regz,regmag)
      implicit none
      integer  regx, regy, regz, regmag
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx
         integer l1, l2, l3, lx
         integer g1, g2, g3, gx

         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_vecmag: regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_vecmag: regmag: ',regmag
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
            call reg_dump_info(regy)
            call reg_dump_info(regz)
         end if

         !
         !
         call register_copy(regx, regmag)
         !call register_copy(regmag, regx)

         file_name = "vecmag"
         call register_assign_name(regmag, file_name)
         call register_return_myname(regmag, name)
         if(ltrace) then
            write(*,97)myid,'register_vecmag: name: ',name
         end if

         t1   = register_return_start(regx)
         t2   = register_return_start(regy)
         t3   = register_return_start(regz)
         tx   = register_return_start(regmag)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace)write(*,99)myid,'    start:   tx:     ',tx
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace)write(*,99)myid,'    start:   tx:     ',tx
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               if(ltrace)write(*,99)myid,'            lx:     ',lx
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               gx = level_return_start(lx)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'      gx:     ',gx
                     ! Compute and stick in gx grid
                     call grid_vecmag(g1,g2,g3, gx)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
                  gx = grid_return_sibling(gx)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_vecmag

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_newcenter:                                             cc
cc           Transform coordinates to those w/ a new center.          cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_newcenter(regx)
      implicit none
      integer  regx
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-11)
      ! point of origin for spherical coordinates:
      real*8   center(3), tmpx,tmpy,tmpz, tmptime, time
         character*32   name, file_name
         integer prefaxis

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz
         integer ufile

         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_newcenter: regx:', regx
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
         end if

         t1   = register_return_start(regx)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE )
            if(ltrace)write(*,99)myid,'             t1: ',t1
            if (myid.eq.master) then 
               center(1) = 0.d0
               center(2) = 0.d0
               center(3) = 0.d0
               tmpx      = 0.d0
               tmpy      = 0.d0
               tmpz      = 0.d0
               tmptime   = 10000.d0
               time      = tree_return_time(t1)
               !
               ufile = 24  ! number for parameter file
               !write(*,93)myid,'newcenter: opening position.in'
               open(unit=ufile,file="position.in",status='old',err=10)
   8           read(ufile,*,end=9)   tmptime,tmpx,tmpy,tmpz
               if (abs(tmptime-time).lt.SMALLNUMBER) then
                  center(1) = tmpx
                  center(2) = tmpy
                  center(3) = tmpz
               else if (tmptime.lt.time) then
                  center(1) = tmpx
                  center(2) = tmpy
                  center(3) = tmpz
                  goto 8
               else
               end if
   9           close(ufile)
  10           continue
            end if
            call MPI_BCAST(center, 3, MPI_DOUBLE_PRECISION,
     &                                  master, MPI_COMM_WORLD, ierr)
            if(abs(tmptime-10000.d0).gt.0.0000000001.and.myid.eq.master)
     *      write(*,83)myid,'At t=',time,' use center ',
     *                         center(1),center(2),center(3)
            ! Go to start of tree
            l1 = tree_return_start(t1)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL )
               if(ltrace)write(*,99)myid,'            l1: ',l1
               g1 = level_return_start(l1)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID )
                     if(ltrace)write(*,99)myid,'A     g1: ',g1
                  if( grid_is_local(g1) ) then
                     if(ltrace)write(*,99)myid,'      g1: ',g1
                     call grid_newcenter(center,g1)
                  endif
                  g1 = grid_return_sibling(g1)
               enddo
               l1 = level_return_sibling(l1)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
         enddo 

         if (ltrace) then
            call reg_dump_info(regx)
            write(*,97)myid,'register_newcenter: Done.'
         end if

  83     format('[',I3,'] ',A,F9.2,A,3F11.4)
  93     format('[',I3,'] ',A,3F13.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_newcenter

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_polar:                                                 cc
cc           Extract the polar     anglefrom the coordinates of a fielcc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_polar(rega,reg)
      implicit none
      integer  rega, reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-11)
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz

         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_polar:reg:',reg
            write(*,99)myid,'register_polar: rega:',rega
         end if
         if(ltrace2) then
            call reg_dump_info(reg)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation
         !
         call register_copy(reg, rega )

         file_name = "polar"
         call register_assign_name(rega, file_name)
         if(ltrace) then
            call register_return_myname(rega, name)
            write(*,97)myid,'register_polar: r_a name: ',name
            call reg_dump_info(rega)
         end if

         t1   = register_return_start(reg)
         tx   = register_return_start(rega)
         if(ltrace)write(*,99)myid,'    start:   t1: ',t1
         if(ltrace)write(*,99)myid,'    start:   tx: ',tx
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE )
            if(ltrace)write(*,99)myid,'             t1: ',t1
            if(ltrace)write(*,99)myid,'             tx: ',tx
            ! Go to start of tree
            l1 = tree_return_start(t1)
            lx = tree_return_start(tx)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL )
               if(ltrace)write(*,99)myid,'            l1: ',l1
               if(ltrace)write(*,99)myid,'            lx: ',lx
               g1 = level_return_start(l1)
               gx = level_return_start(lx)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID )
                  if(ltrace)write(*,99)myid,'A     g1: ',g1
                  if(ltrace)write(*,99)myid,'A     gx: ',gx
                  if( grid_is_local(g1) ) then
                     if(ltrace)write(*,99)myid,'      g1: ',g1
                     if(ltrace)write(*,99)myid,'      gx: ',gx
                     call grid_polar(g1,gx)
                  endif
                  g1 = grid_return_sibling(g1)
                  gx = grid_return_sibling(gx)
               enddo
               l1 = level_return_sibling(l1)
               lx = level_return_sibling(lx)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            tx = tree_return_sibling(tx)
         enddo 

         if (ltrace) then
            call reg_dump_info(rega)
            write(*,97)myid,'register_polar: Done.'
         end if

  93     format('[',I3,'] ',A,3F13.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_polar

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_azimuth:                                               cc
cc           Extract the azimuthal anglefrom the coordinates of a fielcc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_azimuth(rega,reg)
      implicit none
      integer  rega, reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-11)
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz

         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_azimuth:reg:',reg
            write(*,99)myid,'register_azimuth: rega:',rega
         end if
         if(ltrace2) then
            call reg_dump_info(reg)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation
         !
         call register_copy(reg, rega )

         file_name = "azimuth"
         call register_assign_name(rega, file_name)
         if(ltrace) then
            call register_return_myname(rega, name)
            write(*,97)myid,'register_azimuth: r_a name: ',name
            call reg_dump_info(rega)
         end if

         t1   = register_return_start(reg)
         tx   = register_return_start(rega)
         if(ltrace)write(*,99)myid,'    start:   t1: ',t1
         if(ltrace)write(*,99)myid,'    start:   tx: ',tx
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE )
            if(ltrace)write(*,99)myid,'             t1: ',t1
            if(ltrace)write(*,99)myid,'             tx: ',tx
            ! Go to start of tree
            l1 = tree_return_start(t1)
            lx = tree_return_start(tx)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL )
               if(ltrace)write(*,99)myid,'            l1: ',l1
               if(ltrace)write(*,99)myid,'            lx: ',lx
               g1 = level_return_start(l1)
               gx = level_return_start(lx)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID )
                  if(ltrace)write(*,99)myid,'A     g1: ',g1
                  if(ltrace)write(*,99)myid,'A     gx: ',gx
                  if( grid_is_local(g1) ) then
                     if(ltrace)write(*,99)myid,'      g1: ',g1
                     if(ltrace)write(*,99)myid,'      gx: ',gx
                     call grid_azimuth(g1,gx)
                  endif
                  g1 = grid_return_sibling(g1)
                  gx = grid_return_sibling(gx)
               enddo
               l1 = level_return_sibling(l1)
               lx = level_return_sibling(lx)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            tx = tree_return_sibling(tx)
         enddo 

         if (ltrace) then
            call reg_dump_info(rega)
            write(*,97)myid,'register_azimuth: Done.'
         end if

  93     format('[',I3,'] ',A,3F13.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_azimuth

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_radvector:                                             cc
cc           Extract unit radial vector from the coordinates of a fielcc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_radvector(reg,regrx,regry,regrz)
      implicit none
      integer  reg, regrx, regry, regrz
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-11)
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz

         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_radvector:reg:',reg
            write(*,99)myid,'register_radvector: regrx/y/z:',
     .                       regrx,regry,regrz
         end if
         if(ltrace2) then
            call reg_dump_info(reg)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation
         !
         call register_copy(reg, regrx )
         call register_copy(reg, regry )
         call register_copy(reg, regrz )

         file_name = "rad_x"
         call register_assign_name(regrx, file_name)
         file_name = "rad_y"
         call register_assign_name(regry, file_name)
         file_name = "rad_z"
         call register_assign_name(regrz, file_name)
         if(ltrace) then
            call register_return_myname(regrx, name)
            write(*,97)myid,'register_radvector: r_x name: ',name
            call register_return_myname(regry, name)
            write(*,97)myid,'register_radvector: r_y name: ',name
            call register_return_myname(regrz, name)
            write(*,97)myid,'register_radvector: r_z name: ',name
            call reg_dump_info(regrx)
            call reg_dump_info(regry)
            call reg_dump_info(regrz)
         end if

         t1   = register_return_start(reg)
         tx   = register_return_start(regrx)
         ty   = register_return_start(regry)
         tz   = register_return_start(regrz)
         if(ltrace)write(*,99)myid,'    start:   t1: ',t1
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE )
            if(ltrace)write(*,99)myid,'             t1: ',t1
            if(ltrace)write(*,99)myid,'             tx/y/z: ',tx,ty,tz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL )
               if(ltrace)write(*,99)myid,'            l1: ',l1
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID )
                  if(ltrace)write(*,99)myid,'A     g1: ',g1
                  if(ltrace)write(*,99)myid,'A     gx/y/z: ',gx,gy,gz
                  if( grid_is_local(g1) ) then
                     if(ltrace)write(*,99)myid,'      g1: ',g1
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     call grid_radvector(g1,gx,gy,gz)
                  endif
                  g1 = grid_return_sibling(g1)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

         if (ltrace) then
            call reg_dump_info(regrx)
            call reg_dump_info(regry)
            call reg_dump_info(regrz)
            write(*,97)myid,'register_radvector: Done.'
         end if

  93     format('[',I3,'] ',A,3F13.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_radvector

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_cart2sphere:                                           cc
cc           Transform vector from Cartesian basis to Spherical one.  cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_cart2sphere(regx,regy,regz,regr,regthe,regphi)
      implicit none
      integer  regx, regy, regz, regr, regthe, regphi
      !integer  reg, dir
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-11)
      ! point of origin for spherical coordinates:
      real*8   center(3), tmpx,tmpy,tmpz, tmptime, time
         character*32   name, file_name
         integer prefaxis

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz
         integer ufile


         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_cart2sphere: regx/y/z:',
     .                       regx,regy,regz
            write(*,99)myid,'register_cart2sphere:regr/the/phi:',
     .                       regr,regthe,regphi
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
            call reg_dump_info(regy)
            call reg_dump_info(regz)
         end if

         prefaxis = register_getprefaxis(regx)
         if(ltrace)write(*,99),myid,'register_cart2sphere: prefaxis:',
     .                prefaxis

         !
         ! copy grid structure over to the registers that will hold
         ! the computation
         !
         call register_copy(regx, regr  )
         call register_copy(regy, regthe)
         call register_copy(regz, regphi)

         file_name = "compr"
         call register_assign_name(regr,   file_name)
         file_name = "compthe"
         call register_assign_name(regthe, file_name)
         file_name = "compphi"
         call register_assign_name(regphi, file_name)
         if(ltrace) then
            call register_return_myname(regr,   name)
            write(*,97)myid,'register_cart2sphere: r name: ',name
            call register_return_myname(regthe, name)
            write(*,97)myid,'register_cart2sphere: the name: ',name
            call register_return_myname(regphi, name)
            write(*,97)myid,'register_cart2sphere: phi name: ',name
            call reg_dump_info(regr)
            call reg_dump_info(regthe)
            call reg_dump_info(regphi)
         end if

         t1   = register_return_start(regx)
         t2   = register_return_start(regy)
         t3   = register_return_start(regz)
         tx   = register_return_start(regr)
         ty   = register_return_start(regthe)
         tz   = register_return_start(regphi)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace)write(*,99)myid,'             tx/y/z: ',tx,ty,tz
            if (myid.eq.master) then 
               center(1) = 0.d0
               center(2) = 0.d0
               center(3) = 0.d0
               tmpx      = 0.d0
               tmpy      = 0.d0
               tmpz      = 0.d0
               tmptime   = 10000.d0
               time      = tree_return_time(t1)
               !write(*,*) 'looking for time: ',time
               !
               ufile = 24  ! number for parameter file
               write(*,93)myid,'cart2sphere: opening .cart2sphere'
               open(unit=ufile,file=".cart2sphere",status='old',err=10)
   8           read(ufile,*,end=9)   tmptime,tmpx,tmpy,tmpz
               !write(*,*) '   entry:',tmptime,tmpx,tmpy,tmpz
               !write(*,93)myid,'cart2sphere: entry:',tmptime, time
               if (abs(tmptime-time).lt.SMALLNUMBER) then
                  !write(*,93)myid,'cart2sphere: entry found'
                  center(1) = tmpx
                  center(2) = tmpy
                  center(3) = tmpz
               else if (tmptime.lt.time) then
                  !write(*,93)myid,'cart2sphere: time is less:',tmptime
                  center(1) = tmpx
                  center(2) = tmpy
                  center(3) = tmpz
                  goto 8
               else
                  !write(*,*)'cart2sphere:time more',abs(tmptime-time)
               end if
   9           close(ufile)
  10           continue
            end if
            !write(*,93)myid,'cart2sphere:',center(1),center(2),center(3)
            call MPI_BCAST(center, 3, MPI_DOUBLE_PRECISION,
     &                                  master, MPI_COMM_WORLD, ierr)
            if(abs(tmptime-10000.d0).gt.0.0000000001.and.myid.eq.master)
     *      write(*,93)myid,'cart2sphere:',center(1),center(2),center(3)
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               if(ltrace)write(*,99)myid,'            lx/y/z: ',lx,ly,lz
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               gx = level_return_start(lx)
               gy = level_return_start(ly)
               gz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                     if(ltrace)write(*,99)myid,'A     g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'A     gx/y/z: ',gx,gy,gz
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     if(ltrace)write(*,99)myid,'      gx/y/z: ',gx,gy,gz
                     call grid_cart2sphere(center,g1,g2,g3, gx,gy,gz,
     .                                       prefaxis)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
                  gx = grid_return_sibling(gx)
                  gy = grid_return_sibling(gy)
                  gz = grid_return_sibling(gz)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
         enddo 

         if (ltrace) then
            call reg_dump_info(regr)
            call reg_dump_info(regthe)
            call reg_dump_info(regphi)
            write(*,97)myid,'register_cart2sphere: Done.'
         end if

  93     format('[',I3,'] ',A,3F13.4)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_cart2sphere


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_divide:                                                cc
cc           Divide one register by another (and store in place):     cc
cc              regnum = regnum / regdenom                            cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_divide(regnum, regdenom)
      implicit none
      integer  regnum, regdenom
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2
         integer l1, l2
         integer g1, g2


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_divide: regnum:   ',regnum
            write(*,99)myid,'register_divide: regdenom: ',regdenom
         end if
         if(ltrace2) then
            call reg_dump_info(regnum)
            call reg_dump_info(regdenom)
         end if

         !
         !
         !call register_copy(regmag, regx)

         file_name = "divide"
         call register_assign_name(regnum, file_name)
         call register_return_myname(regnum, name)
         if(ltrace) then
            write(*,97)myid,'register_divide: name: ',name
         end if

         t1   = register_return_start(regnum)
         t2   = register_return_start(regdenom)
         if(ltrace)write(*,99)myid,'    start:   t1/2:   ',t1,t2
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2:   ',t1,t2
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2:   ',l1,l2
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2:   ',g1,g2
                     ! Compute and stick in g1 grid
                     call grid_divide(g1,g2)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_divide

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_multiply:                                              cc
cc           Multiplies two registers and places result in third      cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_multiply(reg1,reg2,regout)
      implicit none
      integer  reg1, reg2, regout
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_multiply: reg1/2:',reg1,reg2
            write(*,99)myid,'register_multiply: regout:',regout
         end if
         if(ltrace2) then
            call reg_dump_info(reg1)
            call reg_dump_info(reg2)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation 
         !
         if (regout.ne.reg2) then
            call register_copy(reg1, regout)
         else
            if(ltrace)write(*,99)myid,'register_multiply: skipping copy'
         end if

         file_name = "mult"
         call register_assign_name(regout, file_name)
         call register_return_myname(regout, name)
         if(ltrace) then
            !write(*,97)myid,'register_multiply: name: ',name
            if(ltrace2) call reg_dump_info(regout)
         end if

         t1   = register_return_start(reg1)
         t2   = register_return_start(reg2)
         t3   = register_return_start(regout)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     call grid_multiply(g1,g2,g3)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_multiply

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_multdv:                                                cc
cc           Multiply a field by the product of its grid spacings     cc
cc            (ie the volume element appropriate for its dimension)   cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_multdv(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering multdv..reg: ', reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_multdv(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_multdv

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_addconstant:                                           cc
cc           Add   to a field    a given constant.                    cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_addconstant(scalar,reg)
      implicit none
      real*8  scalar
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_addconstant',reg,scalar
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_addconstant(scalar,gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_addconstant

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_multconstant:                                          cc
cc           Multiply a field by a given constant.                    cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_multconstant(scalar,reg)
      implicit none
      real*8  scalar
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_multdv..reg: ',reg,scalar
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_multconstant(scalar,gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_multconstant

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_add:                                                   cc
cc           Adds       two registers and places result in third      cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_add(reg1,reg2,regout)
      implicit none
      integer  reg1, reg2, regout
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_add: reg1/2:',reg1,reg2
            write(*,99)myid,'register_add: regout:',regout
         end if
         if(ltrace2) then
            call reg_dump_info(reg1)
            call reg_dump_info(reg2)
            call reg_dump_info(regout)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation 
         !
         call register_copy(reg1, regout)

         file_name = "add"
         call register_assign_name(regout, file_name)
         call register_return_myname(regout, name)
         if(ltrace) then
            write(*,97)myid,'register_add: name: ',name
         end if

         t1   = register_return_start(reg1)
         t2   = register_return_start(reg2)
         t3   = register_return_start(regout)
         if(ltrace)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/2/3: ',l1,l2,l3
               g1 = level_return_start(l1)
               g2 = level_return_start(l2)
               g3 = level_return_start(l3)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g2) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/2/3: ',g1,g2,g3
                     call grid_add(g1,g2,g3)
                  endif
                  g1 = grid_return_sibling(g1)
                  g2 = grid_return_sibling(g2)
                  g3 = grid_return_sibling(g3)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_add

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_mask:                                                  cc
cc           Take a scalar register and convert to all 0s and 1s      cc
cc           based on the value of the scalar.                        cc
cc           If scalar = targetval +/-epsilon                         cc
cc           Then mask = 1 else mask = 0                              cc
cc       NB: adapted from register_mask.                              cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_mask(tarval,epsval,reg)
      implicit none
      integer reg
      real*8  tarval, epsval
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_mask..reg: ',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_maskOP(tarval,epsval,gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_mask

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_mask2:                                                 cc
cc           Take a scalar register and convert to all 0s and 1s      cc
cc           based on the value of the scalar.                        cc
cc           If scalar = targetval +/-epsilon                         cc
cc           Then mask = 1 else mask = 0                              cc
cc       NB: adapted from register_mask.                              cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_mask2(tarval,reg)
      implicit none
      integer reg
      real*8  tarval
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_mask..reg: ',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_maskOP2(tarval,gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_mask2


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_sqrt:                                                  cc
cc           Take the squareroot of a register.                       cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_sqrt(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_sqrt..reg: ',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_sqrt(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_sqrt


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_square:                                                cc
cc           Take the square of a register.                           cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_square(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_square..reg: ',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_square(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_square


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_arcsin:                                                cc
cc           Take the arcsin of a register.                           cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_arcsin(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_arcsin..reg: ',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_arcsin(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_arcsin


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_abs:                                                   cc
cc           Take the absolute value of a register.                   cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_abs(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_abs..reg:',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_abs(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_abs


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_floor:                                                 cc
cc           Convert values less than or equal to the floor to the    cc
cc           floor value                                              cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_floor(floor,reg)
      implicit none
      real*8  floor
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_floor..reg:',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_floor(floor,gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_floor


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_unfloor:                                               cc
cc           Convert values less than or equatl to the floor to zero. cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_unfloor(floor,reg)
      implicit none
      real*8  floor
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_unfloor..reg:',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_unfloor(floor,gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_unfloor



cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_inverse:                                               cc
cc           Take the inverse of a register.                          cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_inverse(reg)
      implicit none
      integer reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'

         integer rank, ti, li, gi, li2, gi2, shape(3)
         integer oi, oj, ok, i, j, k, nx1, nx2, ny1, ny2, nz1, nz2
         real*8  h, real_integral

         logical     ltrace
         parameter ( ltrace = .false. )

          
         if(ltrace) then
            write(*,99)myid,'Entering register_inverse..reg:',reg
            call reg_dump_info(reg)
         end if

         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            if(ltrace)write(*,99)myid,'                     ti: ',ti
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace)write(*,99)myid,'               gi: ',gi
                     call grid_inverse(gi)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            !
            ti = tree_return_sibling(ti)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,I5)

         return
      end             ! END: register_inverse


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_r:                                                     cc
cc           Extracts the radius of each point.                       cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_r(reg1,regout)
      implicit none
      integer  reg1, regout
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
         character*32   name, file_name

         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gx, gy, gz


         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace) then
            write(*,99)myid,'register_r: reg1:  ',reg1
            write(*,99)myid,'register_r: regout:',regout
         end if
         if(ltrace2) then
            call reg_dump_info(reg1)
            call reg_dump_info(regout)
         end if

         !
         ! copy grid structure over to the registers that will hold
         ! the computation 
         !
         call register_copy(reg1, regout)

         file_name = "radius"
         call register_assign_name(regout, file_name)
         call register_return_myname(regout, name)
         if(ltrace) then
            write(*,97)myid,'register_add: name: ',name
         end if
         if(ltrace2) then
            call reg_dump_info(regout)
         end if

         t1   = register_return_start(reg1)
         t3   = register_return_start(regout)
         if(ltrace)write(*,99)myid,'    start:   t1/3: ',t1,t3
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace)write(*,99)myid,'             t1/3: ',t1,t3
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l3 = tree_return_start(t3)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace)write(*,99)myid,'            l1/3: ',l1,l3
               g1 = level_return_start(l1)
               g3 = level_return_start(l3)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  if( grid_is_local(g1) .and.
     *                grid_is_local(g3)       ) then
                     if(ltrace)write(*,99)myid,'      g1/3: ',g1,g3
                     call grid_r(g1,g3)
                  endif
                  g1 = grid_return_sibling(g1)
                  g3 = grid_return_sibling(g3)
               enddo
               l1 = level_return_sibling(l1)
               l3 = level_return_sibling(l3)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t3 = tree_return_sibling(t3)
         enddo 

  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_r

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_helicity:                                              cc
cc           Compute the helicity of a vector (magnetic) field.       cc
cc           Using second equations from the following talk:          cc
cc      http://www.tp4.ruhr-uni-bochum.de/vw/Pdfs/kis.pdf             cc
cc           Relatedly one should also look at Wikipedia:             cc
cc   http://en.wikipedia.org/wiki/Fundamental_theorem_of_vector_analysis
cc           Actually computing helical density as per:               cc
cc      rho_h(B(x))= \int [B(x') x (x-x')/|x-x'|] dot B d^3x'         cc
cc           Its integral over all space should be the helicity.      cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_helicity(regx,regy,regz,reghel,
     .                 regAx,   regAy,   regAz,
     .                 regrhox, regrhoy, regrhoz)
      implicit none
      integer  regx, regy, regz, reghel
      integer  regAx,   regAy,   regAz
      integer  regrhox, regrhoy, regrhoz
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
      integer shape(3)
      real*8   coords(3), tmpA(3)
         character*32   name, file_name

         integer trhox, trhoy, trhoz
         integer t1, t2, t3, tx, ty, tz
         integer l1, l2, l3, lx, ly, lz
         integer g1, g2, g3, gAx, gAy, gAz
         integer i,j,k

         logical     ltrace
         parameter ( ltrace = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )

          
         if(ltrace2) then
            write(*,99)myid,'register_helicity:regx/y/z:',regx,regy,regz
            write(*,99)myid,'register_helicity:reghel: ',reghel
            write(*,99)myid,'register_helicity:regAx/y/z:   ',
     .                                           regAx,  regAy,  regAz
            write(*,99)myid,'register_helicity:regrhox/y/z: ',
     .                                           regrhox,regrhoy,regrhoz
         end if
         if(ltrace2) then
            call reg_dump_info(regx)
            call reg_dump_info(regy)
            call reg_dump_info(regz)
         end if

         !
         ! Give target registers the same structure as input:
         !
         call register_copy(regx, reghel)
         call register_copy(regx, regAx)
         call register_copy(regx, regAy)
         call register_copy(regx, regAz)
         call register_copy(regx, regrhox)
         call register_copy(regx, regrhoy)
         call register_copy(regx, regrhoz)
         !
         file_name = "Ax"
         call register_assign_name(regAx, file_name)
         call register_return_myname(regAx, name)
         file_name = "Ay"
         call register_assign_name(regAy, file_name)
         call register_return_myname(regAy, name)
         file_name = "Az"
         call register_assign_name(regAz, file_name)
         call register_return_myname(regAz, name)
         file_name = "rhox"
         call register_assign_name(regrhox, file_name)
         call register_return_myname(regrhox, name)
         file_name = "rhoy"
         call register_assign_name(regrhoy, file_name)
         call register_return_myname(regrhoy, name)
         file_name = "rhoz"
         call register_assign_name(regrhoz, file_name)
         call register_return_myname(regrhoz, name)
         if(ltrace2)then
            !write(*,97)myid,'register_helicity: name: ',name
            call reg_dump_info(reghel)
            call reg_dump_info(regAx)
            call reg_dump_info(regAy)
            call reg_dump_info(regAz)
            call reg_dump_info(regrhox)
            call reg_dump_info(regrhoy)
            call reg_dump_info(regrhoz)
         end if

         t1   = register_return_start(regx)
         t2   = register_return_start(regy)
         t3   = register_return_start(regz)
         tx   = register_return_start(regAx)
         ty   = register_return_start(regAy)
         tz   = register_return_start(regAz)
         trhox= register_return_start(regrhox)
         trhoy= register_return_start(regrhoy)
         trhoz= register_return_start(regrhoz)
         if(ltrace2)write(*,99)myid,'    start:   t1/2/3: ',t1,t2,t3
         if(ltrace2)write(*,99)myid,'    start:   tx/y/z: ',tx,ty,tz
         if(ltrace2)write(*,99)myid,'    start:trhox/y/z: ',
     .                                        trhox,trhoy,trhoz
         ! Loop over all trees
         do while ( t1 .ne. NULL_TREE .and.
     *              t2 .ne. NULL_TREE .and.
     *              t3 .ne. NULL_TREE       )
            if(ltrace2)write(*,99)myid,'             t1/2/3: ',t1,t2,t3
            if(ltrace2)write(*,99)myid,'             tx/y/z: ',tx,ty,tz
            if(ltrace2)write(*,99)myid,'          trhox/y/z: ',
     .                                        trhox,trhoy,trhoz
            ! Go to start of tree
            l1 = tree_return_start(t1)
            l2 = tree_return_start(t2)
            l3 = tree_return_start(t3)
            lx = tree_return_start(tx)
            ly = tree_return_start(ty)
            lz = tree_return_start(tz)
            !lrx= tree_return_start(trhox)
            !lry= tree_return_start(trhoy)
            !lrz= tree_return_start(trhoz)
            ! Loop over all levels
            do while ( l1 .ne. NULL_LEVEL .and.
     *                 l2 .ne. NULL_LEVEL .and.
     *                 l3 .ne. NULL_LEVEL      )
               if(ltrace2)write(*,99)myid,'           l1/2/3: ',l1,l2,l3
               if(ltrace2)write(*,99)myid,'           lx/y/z: ',lx,ly,lz
               g1  = level_return_start(l1)
               g2  = level_return_start(l2)
               g3  = level_return_start(l3)
               gAx = level_return_start(lx)
               gAy = level_return_start(ly)
               gAz = level_return_start(lz)
               ! Loop over all grids
               do while ( g1 .ne. NULL_GRID .and.
     *                    g2 .ne. NULL_GRID .and.
     *                    g3 .ne. NULL_GRID       )
                  !
                  ! For each finest point, must:
                  !     (1) calculate rho for all x' w/r/t x:
                  !     (2) calculate A
                  !
                  call grid_return_shape(g1, shape)
                  do k = 1, shape(3)
                  do j = 1, shape(2)
                  do i = 1, shape(1)
                     ! Find coords(3):
                     call grid_getspeccoords(coords,g1,i,j,k)
                     if(ltrace2)write(*,96)myid,'   coords: ',coords(1),
     .                    coords(2), coords(3)
                     !
                     ! Calculate components of rho_A:
                    call tree_calcrho(coords,trhox,trhoy,trhoz,t1,t2,t3)
                     ! Calculate integral to get components of A:
                     tmpA(1)= tree_getintegral(trhox)
                     tmpA(2)= tree_getintegral(trhoy)
                     tmpA(3)= tree_getintegral(trhoz)
                     if( grid_is_local(g1) .and.
     *                   grid_is_local(g2) .and.
     *                   grid_is_local(g3)       ) then
                     if(ltrace2)write(*,99)myid,'  g1/2/3: ',g1,g2,g3
                     if(ltrace2)write(*,99)myid,'  gAx/y/z:',gAx,gAy,gAz
                     if(ltrace )write(*,90)myid,i,j,k,
     .                                 tmpA(1),tmpA(2),tmpA(3)
                     ! Stick vec{A} into appropriate storage on local grid:
                     call grid_setspecpoint(tmpA(1),gAx,i,j,k)
                     call grid_setspecpoint(tmpA(2),gAy,i,j,k)
                     call grid_setspecpoint(tmpA(3),gAz,i,j,k)
                     !call grid_helicity(g1,g2,g3, gx)
                     endif
                  end do
                  end do
                  end do
                  !
                  g1  = grid_return_sibling(g1)
                  g2  = grid_return_sibling(g2)
                  g3  = grid_return_sibling(g3)
                  gAx = grid_return_sibling(gAx)
                  gAy = grid_return_sibling(gAy)
                  gAz = grid_return_sibling(gAz)
               enddo
               l1 = level_return_sibling(l1)
               l2 = level_return_sibling(l2)
               l3 = level_return_sibling(l3)
               lx = level_return_sibling(lx)
               ly = level_return_sibling(ly)
               lz = level_return_sibling(lz)
            enddo
            !
            !
            t1 = tree_return_sibling(t1)
            t2 = tree_return_sibling(t2)
            t3 = tree_return_sibling(t3)
            tx = tree_return_sibling(tx)
            ty = tree_return_sibling(ty)
            tz = tree_return_sibling(tz)
            trhox = tree_return_sibling(trhox)
            trhoy = tree_return_sibling(trhoy)
            trhoz = tree_return_sibling(trhoz)
         enddo 

         !
         ! Now that we have vec{A} calculate:
         !           vec{A} cdot vec{B}
         ! to get helical density
         !
         !  (NB: register_dot() sticks its result in the x-component
         !       of the second vector, so we just copy Ax to hel and proceed)
         call register_copy(regAx, reghel)
         call register_dot(regx,regy,regz,reghel,regAy,regAz)
         ! Change name from dot:
         file_name = "helicity"
         call register_assign_name(reghel, file_name)
         call register_return_myname(reghel, name)
         if(ltrace)call reg_dump_info(reghel)

  90     format('[',I3,'] ','  hel:',3I6,3G10.2)
  96     format('[',I3,'] ',A,3F13.5)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_helicity

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_flines:                                                cc
cc           Compute field lines of a vector field.                   cc
cc           Input:  the vector field defined over all space:         cc
cc                    (Vx, Vy, Vz)                                    cc
cc           Output: the field lines are output parameterized         cc
cc                   in terms of some lambda:                         cc
cc                    (Lx, Ly, Lz)                                    cc
cc                   And the magnitude of the field is also output:   cc
cc                    LM                                              cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_flines(regVx,regVy,regVz,
     .                 regLx,   regLy,   regLz,
     .                 regLM)
      implicit none
      integer  regVx, regVy, regVz
      integer  regLx, regLy, regLz, regLM
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
      include 'grids.inc'
      integer shape(3)
      character*32   name, file_name

         integer tree, grid
         integer t1, t2, t3, tx, ty, tz, tm, ttx,tty,ttz
         integer l1, l2, l3, lx, ly, lz, lm
         integer             gx, gy, gz, gm
         integer tgx, tgy, tgz
         integer i,j,k
         ! "pointers" into storage for the field line points:
         integer lx_ptr, ly_ptr, lz_ptr, lm_ptr, ufile
         ! Max number of points in each field line:
         integer     maxpts
         parameter ( maxpts = 500000 )
         ! Step-size for parameter "lambda" w/r/t fieldlines:
         real*8      dlambda
         parameter ( dlambda = 0.01d0 )
         !   1 --> first order forward differencing
         !   2 --> second order Runge-Kutte
         integer     orderaccuracy 
         parameter ( orderaccuracy = 2)
         logical keepgoing
         real*8      magB, Bx, By, Bz, hatBx, hatBy, hatBz
         real*8      x,y,z,  xprev,yprev,zprev, bbox(2), h
         real*8      time, coords(3)
         real*8      seedx, seedy, seedz
         real*8      myh, distance, mindistance
         logical     reg_outofdomain
         external    reg_outofdomain
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-13)


         logical     ltrace
         parameter ( ltrace  = .true. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )
         logical     ltrace3
         parameter ( ltrace3 = .false. )

          
      if(ltrace) then
         write(*,99)myid,'register_flines: regVx/y/z:',regVx,regVy,regVz
         write(*,99)myid,'register_flines: regLx/y/z:',regLx,regLy,regLz
         write(*,99)myid,'register_flines: regLM:    ',regLM
         if (ltrace3) then
            call reg_dump_info(regVx)
            call reg_dump_info(regVy)
            call reg_dump_info(regVz)
         end if
      end if

      ! Setup registers which hold the 1D field lines:
      !
      call register_set_rank(regLx,1)
      call register_set_rank(regLy,1)
      call register_set_rank(regLz,1)
      call register_set_rank(regLM,1)
      !
      ! Fix naming:
      !
      file_name = "Lx"
      call register_assign_name(regLx, file_name)
      call register_return_myname(regLx, name)
      if(ltrace2)write(*,97)myid,'register_flines: Name: ',name(1:20)
      file_name = "Ly"
      call register_assign_name(regLy, file_name)
      call register_return_myname(regLy, name)
      if(ltrace2)write(*,97)myid,'register_flines: Name: ',name(1:20)
      file_name = "Lz"
      call register_assign_name(regLz, file_name)
      call register_return_myname(regLz, name)
      if(ltrace2)write(*,97)myid,'register_flines: Name: ',name(1:20)
      file_name = "LM"
      call register_assign_name(regLM, file_name)
      call register_return_myname(regLM, name)
      if(ltrace2)write(*,97)myid,'register_flines: Name: ',name(1:20)
      if(ltrace3)then
         call reg_dump_info(regLx)
      end if
  
      !
      ttx = register_return_start(regVx)
      tty = register_return_start(regVy)
      ttz = register_return_start(regVz)
      if (ltrace) then
         write(*,99)myid,'register_flines: ttx/y/z: ',ttx,tty,ttz
      end if
      !
  2   if (ttx.ne.NULL_TREE  .and.
     .    tty.ne.NULL_TREE  .and.
     .    ttz.ne.NULL_TREE         ) then
         !
         time = tree_return_time(ttx)
         if (ltrace) then
            write(*,99)myid,'register_flines: * * *',ttx
            write(*,98)myid,'register_flines: Time= ',time
         end if
         ! Open seeds file:
         ufile = 71
         open(unit=ufile,file="seeds",status='old',err=10)
         if(ltrace)write(*,99)myid,'register_flines: seeds opened'
         !
  7      read(ufile,*,end=9)   seedx, seedy, seedz
         if(ltrace)write(*,96)myid,'register_flines:',seedx,seedy,seedz
         !
         ! Create storage for field lines:
         !
         gx = grid_get_free_number()
         gy = grid_get_free_number()
         gz = grid_get_free_number()
         gm = grid_get_free_number()
         if(ltrace3)then
            write(*,99)myid,'register_flines: gx = ',gx
            write(*,99)myid,'register_flines: gy = ',gy
            write(*,99)myid,'register_flines: gz = ',gz
            write(*,99)myid,'register_flines: gm = ',gm
         end if
         call grid_createB(gx, 1, maxpts, myid)
         call grid_createB(gy, 1, maxpts, myid)
         call grid_createB(gz, 1, maxpts, myid)
         call grid_createB(gm, 1, maxpts, myid)
         bbox(1) = 0.d0
         bbox(2) = dlambda*(maxpts-1.d0)
         call grid_set_bbox(gx, bbox)
         call grid_set_bbox(gy, bbox)
         call grid_set_bbox(gz, bbox)
         call grid_set_bbox(gm, bbox)
         tx  = register_add_tree(regLx, time)
         ty  = register_add_tree(regLy, time)
         tz  = register_add_tree(regLz, time)
         tm  = register_add_tree(regLm, time)
         if (ltrace2) then
            if (ltrace3) then
               call tree_dump(tx)
               call tree_dump(ty)
               call tree_dump(tz)
               call tree_dump(tm)
            end if
            write(*,99)myid,'register_flines: tx = ',tx
            write(*,99)myid,'register_flines: ty = ',ty
            write(*,99)myid,'register_flines: tz = ',tz
            write(*,99)myid,'register_flines: tm = ',tm
         end if
         lx = tree_add_level(tx, dlambda)
         ly = tree_add_level(ty, dlambda)
         lz = tree_add_level(tz, dlambda)
         lm = tree_add_level(tm, dlambda)
         if (ltrace2) then
            if (ltrace3) then
               call level_dump(lx)
               call level_dump(ly)
               call level_dump(lz)
               call level_dump(lm)
            end if
            write(*,99)myid,'register_flines: lx = ',lx
            write(*,99)myid,'register_flines: ly = ',ly
            write(*,99)myid,'register_flines: lz = ',lz
            write(*,99)myid,'register_flines: lm = ',lm
         end if
         call grid_set_level(gx, lx)
         call grid_set_level(gy, ly)
         call grid_set_level(gz, lz)
         call grid_set_level(gm, lm)
         if (ltrace2) then
            if (ltrace3) then
               call grid_dump(gx)
               call grid_dump(gy)
               call grid_dump(gz)
               call grid_dump(gm)
            end if
            write(*,99)myid,'register_flines: gx = ',gx
            write(*,99)myid,'register_flines: gy = ',gy
            write(*,99)myid,'register_flines: gz = ',gz
            write(*,99)myid,'register_flines: gm = ',gm
         end if
         if(ltrace3)write(*,99)myid,'register_flines: add grid to level'
         call level_add_gridB(gx, lx)
         call level_add_gridB(gy, ly)
         call level_add_gridB(gz, lz)
         call level_add_gridB(gm, lm)
         if (ltrace2) then
            if (ltrace3) then
               call level_dump(lx)
               call level_dump(ly)
               call level_dump(lz)
               call level_dump(lm)
            end if
            write(*,99)myid,'register_flines: lx = ',lx
            write(*,99)myid,'register_flines: ly = ',ly
            write(*,99)myid,'register_flines: lz = ',lz
            write(*,99)myid,'register_flines: lm = ',lm
         end if
         if(ltrace3)call reg_dump_info(regLx)
         if(ltrace2)then 
            call reg_dump_info(regLx)
            call reg_dump_info(regLy)
            call reg_dump_info(regLz)
            call reg_dump_info(regLM)
         end if
         !
         lx_ptr  = grid_field(gx)
         ly_ptr  = grid_field(gy)
         lz_ptr  = grid_field(gz)
         lm_ptr  = grid_field(gm)
         !
         ! Start with seed:
         q(lx_ptr) =  seedx
         q(ly_ptr) =  seedy
         q(lz_ptr) =  seedz
         !q(lx_ptr) =  5.0d0
         !q(ly_ptr) =  0.0d0
         !q(lz_ptr) = +5.0d0
         !q(lx_ptr) =  .1d0
         !q(ly_ptr) =  0.0d0
         !q(lz_ptr) = +0.1d0
         ! Boolean as to when to terminate:
         keepgoing = .true.
         coords(1) = q(lx_ptr)
         coords(2) = q(ly_ptr)
         coords(3) = q(lz_ptr)
         !if ( reg_outofdomain(regVx, coords) ) then
            !write(*,99)myid,'register_flines: Seed out of domain'
            !return
         !end if
         mindistance = 1d99
         i = 0
         if(ltrace2)write(*,99)myid,'register_flines:  * * * * * * * *'
         do while (keepgoing)
            coords(1) = q(lx_ptr+i)
            coords(2) = q(ly_ptr+i)
            coords(3) = q(lz_ptr+i)
            tgx =  tree_findgridwpoint(ttx,coords)
            tgy =  tree_findgridwpoint(tty,coords)
            tgz =  tree_findgridwpoint(ttz,coords)
            if (tgx.ne.NULL_GRID.and.tgy.ne.NULL_GRID.and.
     .          tgz.ne.NULL_GRID) then
               if (grid_is_local(tgx)) then
                  call grid_interp_pt(tgx, Bx, coords)
                  call grid_interp_pt(tgy, By, coords)
                  call grid_interp_pt(tgz, Bz, coords)
               else
                  write(*,*)'register_flines:Not equipped for multiproc'
               end if
            else
               if (ltrace) then
                  write(*,99)myid,'register_flines:grids not found',
     .                    tgx,tgy,tgz
               end if
               write(*,99)myid,'register_flines: Out of domain',i
               if (i.eq.0) return
               keepgoing = .false.
            end if
            if(ltrace3) then
               write(*,99)myid,'flines: At point ',i
               write(*,96)myid,'flines: At coordinates: ',
     .                                  coords(1),coords(2),coords(3)
            end if
            ! Determine |B| and hatB at the current point on fieldline:
            magB = sqrt( Bx**2+By**2+Bz**2 )
            q(lm_ptr+i) = magB
            if(ltrace3)write(*,99)myid,'register_flines: i = ',i
            if (magB.gt.SMALLNUMBER) then
               hatBx = Bx/magB
               hatBy = By/magB
               hatBz = Bz/magB
            else
               hatBx = 0.d0
               hatBy = 0.d0
               hatBz = 0.d0
               keepgoing = .false.
               if(ltrace3)write(*,99)myid,'register_flines: magB=0',magB
            end if
            if (keepgoing) then
               if(ltrace2)write(*,99)myid,'register_flines: Stepping',i
               ! Step the curve:
               if(ltrace2)then
                  write(*,98)myid,'register_flines:   MagB = ',magB
                  write(*,96)myid,'register_flines:    Bx/y/z = ',
     .                                                   Bx,   By,   Bz
                  write(*,96)myid,'register_flines: hatBx/y/z = ',
     .                                                hatBx,hatBy,hatBz
               write(*,96)myid,'register_flines: Coords = ',coords(1),
     .                              coords(2), coords(3)
               end if
               if (orderaccuracy.eq.1) then
                  q(lx_ptr+i+1) = q(lx_ptr+i) + dlambda*hatBx
                  q(ly_ptr+i+1) = q(ly_ptr+i) + dlambda*hatBy
                  q(lz_ptr+i+1) = q(lz_ptr+i) + dlambda*hatBz
               else if (orderaccuracy.eq.2) then
                  !
                  !  y_{n+1/2} = y_n + (1/2) h f(lambda_n,y_n)
                  !
                  q(lx_ptr+i+1) = q(lx_ptr+i) + 0.5d0*dlambda*hatBx
                  q(ly_ptr+i+1) = q(ly_ptr+i) + 0.5d0*dlambda*hatBy
                  q(lz_ptr+i+1) = q(lz_ptr+i) + 0.5d0*dlambda*hatBz
                  !
                  !
                  coords(1) = q(lx_ptr+i+1)
                  coords(2) = q(ly_ptr+i+1)
                  coords(3) = q(lz_ptr+i+1)
                  tgx =  tree_findgridwpoint(ttx,coords)
                  tgy =  tree_findgridwpoint(tty,coords)
                  tgz =  tree_findgridwpoint(ttz,coords)
                  if (tgx.ne.NULL_GRID.and.tgy.ne.NULL_GRID.and.
     .                tgz.ne.NULL_GRID) then
                     if (grid_is_local(tgx)) then
                        call grid_interp_pt(tgx, Bx, coords)
                        call grid_interp_pt(tgy, By, coords)
                        call grid_interp_pt(tgz, Bz, coords)
                     else
                        write(*,99)myid,'register_flines:No multiproc'
                     end if
                  else
                     if (ltrace) then
                       write(*,99)myid,'register_flines:grids notfound',
     .                                                  tgx,tgy,tgz
                     end if
                     write(*,99)myid,'register_flines: Out of domain',i
                     if (i.eq.0) return
                     keepgoing = .false.
                  end if
                  magB = sqrt( Bx**2+By**2+Bz**2 )
                  if(ltrace3)write(*,99)myid,'register_flines: i = ',i
                  if (magB.gt.SMALLNUMBER) then
                     hatBx = Bx/magB
                     hatBy = By/magB
                     hatBz = Bz/magB
                  else
                     hatBx = 0.d0
                     hatBy = 0.d0
                     hatBz = 0.d0
                     keepgoing = .false.
                     if(ltrace3)write(*,99)myid,'register_flines:maB=0',
     .                                       magB
                  end if
                  !
                  !  y_{n+1}   = y_n + h f(lambda_n+(1/2)h,y_{n+1/2})
                  !
                  q(lx_ptr+i+1) = q(lx_ptr+i) + 1.0d0*dlambda*hatBx
                  q(ly_ptr+i+1) = q(ly_ptr+i) + 1.0d0*dlambda*hatBy
                  q(lz_ptr+i+1) = q(lz_ptr+i) + 1.0d0*dlambda*hatBz
                  !
               else 
                  write(*,99)myid,'register_flines: Unknown order'
                  return
               end if
               ! Evaluate if field line should be terminated:
               if(ltrace3)write(*,99)myid,'register_flines: Stop now?'
               i = i + 1
               myh = grid_return_resolution(tgx)
               ! In units of h:
               distance = sqrt( ( q(lx_ptr+i)-q(lx_ptr) )**2 +
     .                          ( q(ly_ptr+i)-q(ly_ptr) )**2 +
     .                          ( q(lz_ptr+i)-q(lz_ptr) )**2   )
               distance = distance /(1.d0*myh)
               if(ltrace3)write(*,96)myid,'register_flines: begin:',
     .                    q(lx_ptr),q(ly_ptr),q(lz_ptr)
               if (distance.lt.mindistance) mindistance = distance
               if(ltrace3)write(*,96)myid,'register_flines: begin:',
     .                    q(lx_ptr),q(ly_ptr),q(lz_ptr)
               if(ltrace2)write(*,96)myid,'register_flines: distances=',
     .                    distance, mindistance
               if ( distance .lt. 10.d0 .and. i.gt.1550 ) then
               !if ( distance .lt. 5.6 .and. i.gt.550 ) then
                 write(*,98)myid,'register_flines: Line looped back',
     .                             distance
                 write(*,98)myid,'register_flines: lambda=',
     .                             (i-1.d0)*dlambda
                 write(*,99)myid,'register_flines: i = ',i
                 write(*,96)myid,'register_flines: Coords =',
     .                              coords(1),coords(2), coords(3)
                  keepgoing = .false.
               end if
               if ( i.ge.maxpts ) then
                  if(ltrace)then
                     write(*,98)myid,'register_flines:Reached maxpoin',i
                     write(*,98)myid,'register_flines:   MagB =',magB
                     write(*,96)myid,'register_flines:Bx/y/z =',Bx,By,Bz
                     write(*,96)myid,'register_flines: Coords =',
     .                              coords(1),coords(2), coords(3)
                    write(*,96)myid,'register_flines:Distance:',distance
                    write(*,96)myid,'register_flines:Mindis',mindistance
                  end if
                  keepgoing = .false.
               end if
               coords(1) = q(lx_ptr+i+1)
               coords(2) = q(ly_ptr+i+1)
               coords(3) = q(lz_ptr+i+1)
               !if ( tree_outofdomain(ttx, coords) ) then
               if ( reg_outofdomain(regVx, coords) ) then
                  if(ltrace)then
                     write(*,99)myid,'register_flines:Out domain'
                     write(*,98)myid,'register_flines:   MagB =',magB
                     write(*,96)myid,'register_flines:Bx/y/z =',Bx,By,Bz
                     write(*,96)myid,'register_flines: Coords =',
     .                              coords(1),coords(2), coords(3)
                  end if
                  keepgoing = .false.
               end if
            else
               if(ltrace)then
                  !write(*,99)myid,'register_flines: MagB equals zero ',i
                  write(*,98)myid,'register_flines:   MagB =',magB
                  write(*,96)myid,'register_flines: Bx/y/z =',Bx,By,Bz
                  write(*,96)myid,'register_flines: Coords =',coords(1),
     .                              coords(2), coords(3)
!                 write(*,96)myid,'register_flines: Coords =',
!    .                  q(lx_ptr+i),q(ly_ptr+i),q(lz_ptr+i),q(lm_ptr+i)
                  !write(*,99)myid,'register_flines:tgx/y/z:',tgx,tgy,tgz
               end if
            end if
            if(ltrace2)write(*,99)myid,'register_flines: Done w/ i = ',i
            !
         end do
         !
         ! Re-set the size of these 1D arrays if we didn't
         ! compute all steps
         if (i.lt.maxpts) then
            shape(1) = i+1
            !call grid_dump(gx)
            call grid_set_shape(gx,shape)
            !call grid_dump(gx)
            call grid_set_shape(gy,shape)
            call grid_set_shape(gz,shape)
            call grid_set_shape(gm,shape)
            !
            ! Also need to adjust their bounding box:
            bbox(1) = 0.d0
            bbox(2) = dlambda*(i-1.d0)
            call grid_set_bbox(gx, bbox)
            call grid_set_bbox(gy, bbox)
            call grid_set_bbox(gz, bbox)
            call grid_set_bbox(gm, bbox)
            !
            if (ltrace3) then
               call grid_return_name(gz,  name)
               write(*,97)myid,"register_flines: gz name  = ",name(1:20)
               call grid_return_name(gm,  name)
               write(*,97)myid,"register_flines: gm name  = ",name(1:20)
            end if
         end if
         !
         ! Continue with other seeds
         !
  8      goto 7
         !
         ! Continue with other times (e.g. trees)
         !
  9      continue
         ttx = tree_return_sibling(ttx)
         tty = tree_return_sibling(tty)
         ttz = tree_return_sibling(ttz)
         if(ltrace2) then 
            write(*,99)myid,'register_flines: Look at siblings: tx=',ttx
            write(*,99)myid,'register_flines: Look at siblings: ty=',tty
            write(*,99)myid,'register_flines: Look at siblings: tz=',ttz
         end if
         !
         ! Close the seeds file
         close(ufile)
         !
         goto 2
         !
      end if

      !
      ! Fix naming:
      !
      file_name = "Lx"
      call register_assign_name(regLx, file_name)
      file_name = "Ly"
      call register_assign_name(regLy, file_name)
      file_name = "Lz"
      call register_assign_name(regLz, file_name)
      file_name = "LM"
      call register_assign_name(regLM, file_name)
      if (ltrace3) then
         call register_return_myname(regLx, name)
         write(*,97)myid,'register_flines: Name: ',name(1:20)
         call register_return_myname(regLy, name)
         write(*,97)myid,'register_flines: Name: ',name(1:20)
         call register_return_myname(regLz, name)
         write(*,97)myid,'register_flines: Name: ',name(1:20)
         call register_return_myname(regLM, name)
         write(*,97)myid,'register_flines: Name: ',name(1:20)
      end if

      if(ltrace3) then
         call reg_dump_info(regLx)
         call reg_dump_info(regLy)
         call reg_dump_info(regLz)
         call reg_dump_info(regLM)
      end if

      if(ltrace) then
         write(*,99)myid,'register_flines: Done.'
         return
      end if

 10   continue
      write(*,99)myid,'register_flines: No seeds file found.'
      write(*,99)myid,'register_flines: Create file: seeds'
      write(*,99)myid,'register_flines: w/ seedx seedy seedz'
      write(*,99)myid,'register_flines: on each line for each seed.'

  90     format('[',I3,'] ','  flines:',3I6,3G10.2)
  96     format('[',I3,'] ',A,4G13.5)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,G18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_flines

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_interpline:                                            cc
cc           Interpolate a scalar onto a 1D curve (ie field line,etc) cc
cc           Input:  the scalar field S                               cc
cc           Input:  the coordinates of the curve in terms of some    cc
cc                      parameter:  (Lx,Ly,Lz)                        cc
cc           Output: the scalar as a function of the parameter, lambdacc
cc                      regLS                                         cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_interpline(regS, regLx,regLy,regLz, regLS)
      implicit none
      integer  regS, regLx, regLy, regLz, regLS
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
      integer shape(3)
      character*32   name, file_name
      integer treels,levells, gridls
      integer treex, levelx,  gridx
      integer treey, levely,  gridy
      integer treez, levelz,  gridz, nlambda
      integer xptr,  yptr,    zptr, lsptr
      integer scalartree, scalargrid
      real*8  x,     y,       z, coords(3)
      real*8  time, flinetime, mytmp
      integer i, j, k, myindex
      ! For comparing the time of the scalar and the time of the field lines:
      real*8  fractionaldiff, reltimethresh
      parameter            ( reltimethresh = 0.01d0 )

         logical     ltrace
         parameter ( ltrace  = .true. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )
         logical     ltrace3
         parameter ( ltrace3 = .false. )

          
      if(ltrace)write(*,99)myid,'register_interpline: Enter: * * * * * '
      if(ltrace2) then
      write(*,99)myid,'register_interpline:regS:     ',regS
      write(*,99)myid,'register_interpline:regLx/y/z:',regLx,regLy,regLz
      write(*,99)myid,'register_interpline:regLS:    ',regLS
      end if

      ! Make sure registers exist:
      if (regS.eq.NULL_REGISTER) then
         write(*,99)myid,'register_interpline: Nonexistent scalar reg.'
         return
      end if
      if (regLx.eq.NULL_REGISTER) then
         write(*,99)myid,'register_interpline: Nonexistent X reg.'
         return
      end if
      if (regLy.eq.NULL_REGISTER) then
         write(*,99)myid,'register_interpline: Nonexistent Y reg.'
         return
      end if
      if (regLz.eq.NULL_REGISTER) then
         write(*,99)myid,'register_interpline: Nonexistent Z reg.'
         return
      end if
      if (regLS.eq.NULL_REGISTER) then
         write(*,99)myid,'register_interpline: Nonexistent S reg.'
         return
      end if

      ! Set things up to store the interpolated values in regLs
      call register_copy(regLx, regLS)
      file_name = "scalalongline"
      call register_assign_name(regLS, file_name)
      call register_return_myname(regLS, name)

      ! Loop over trees (different times):
      if(ltrace2)call reg_dump_info(regS)
      scalartree = register_return_start(regS)
      ! Loop over all trees:
  3   if (scalartree.ne.NULL_TREE) then
         !
         if(ltrace2)call tree_dump(scalartree)
         time = tree_return_time(scalartree)
         !
         treex  = register_return_start(regLx)
         treey  = register_return_start(regLy)
         treez  = register_return_start(regLz)
         treels = register_return_start(regLs)
         if (treex.eq.NULL_TREE.or.treey.eq.NULL_TREE.or.
     .          treez.eq.NULL_TREE.or.treels.eq.NULL_TREE ) goto 6
         !
         if(ltrace)then
            write(*,98)myid,'register_interpline: Scalar@time',time
         end if
  4      flinetime = tree_return_time(treex)
         !
         ! Make sure time is the same (or close)
         !
         !if (.not.double_equal(time, flinetime)) then
         if (flinetime .ne.0) then
            fractionaldiff = abs( (time-flinetime)/flinetime)
         else if (time.ne.0) then
            fractionaldiff = abs( (time-flinetime)/time)
         else 
            fractionaldiff = 0.d0
         end if
         if(ltrace)then
            write(*,96)myid,'register_interpline: fractionaldiff =',
     .                                fractionaldiff,reltimethresh
         end if
         ! How close to two times have to be to carryout the interpolation?
         !if (fractionaldiff .gt. 0.02d0) then
         if (fractionaldiff .gt. reltimethresh) then
            if(ltrace)then
               write(*,98)myid,'register_interpline: Not time',flinetime
            end if
            treex  = tree_return_sibling(treex)
            treey  = tree_return_sibling(treey)
            treez  = tree_return_sibling(treez)
            treels = tree_return_sibling(treels)
            if (treex.ne.NULL_TREE.and.treey.ne.NULL_TREE.and.
     .          treez.ne.NULL_TREE.and.treels.ne.NULL_TREE ) goto 4
         else
            if(ltrace)then
             write(*,98)myid,'register_interpline:Interp@time',flinetime
            end if
            levelx = tree_return_start(treex)
            levely = tree_return_start(treey)
            levelz = tree_return_start(treez)
            levells= tree_return_start(treels)
            gridx  = level_return_start(levelx)
            gridy  = level_return_start(levely)
            gridz  = level_return_start(levelz)
            gridls = level_return_start(levells)
            !
  5         if (grid_compatible(gridx,gridy) .and.
     .          grid_compatible(gridx,gridz)       ) then
                !
                ! These should all have same structure (gridx/y/z/ls)
                !
                call grid_return_shape(gridx, shape)
                xptr = grid_get_field(gridx)
                yptr = grid_get_field(gridy)
                zptr = grid_get_field(gridz)
                lsptr = grid_get_field(gridls)
                ! Field lines are parameterized in terms of some
                !      (arbitrary) parameter  lambda:
                nlambda = shape(1)
                do i = 1, nlambda
                   x = q(xptr+i-1)
                   y = q(yptr+i-1)
                   z = q(zptr+i-1)
                   if (ltrace3) then
                      write(*,91)myid,'register_interpline: i/x/y/z:',
     .                                                      i,x,y,z
                   end if
                   coords(1) = x
                   coords(2) = y
                   coords(3) = z
                   scalargrid =  tree_findgridwpoint(scalartree,coords)
                   if (scalargrid.ne.NULL_GRID) then
                      if (grid_is_local(scalargrid)) then
                         call grid_interp_pt(scalargrid, q(lsptr+i-1),
     .                                                      coords)
                      else
                         write(*,99)myid,'register_interpline:No MPIyet'
                      end if
                   else
                     write(*,99)myid,'register_interpline:Grid w/point'
                     write(*,99)myid,'register_interpline:  not found.'
                     write(*,96)myid,'register_interpline:Coords:',x,y,z
                   end if
                end do
                !
                ! Computer other field lines:
                !
                gridx = grid_return_sibling(gridx)
                gridy = grid_return_sibling(gridy)
                gridz = grid_return_sibling(gridz)
                gridls= grid_return_sibling(gridls)
                if (gridx.ne.NULL_GRID.and.gridy.ne.NULL_GRID.and.
     .              gridz.ne.NULL_GRID.and.gridls.ne.NULL_GRID) goto 5
            end if
         end if
         scalartree = tree_return_sibling(scalartree)
         goto 3
      end if

      if (ltrace3) then
          ! Just for testing interpolation:
          !     Feed in exact grid point of grid:
          call grid_return_shape(scalargrid, shape)
          i = 10
          j = 5
          k = 20
          coords(1)=q(grid_get_coords(scalargrid)+i-1)
          coords(2)=q(grid_get_coords(scalargrid)+shape(1)+j-1)
          coords(3)=q(grid_get_coords(scalargrid)+shape(1)+shape(2)+k-1)
          call grid_interp_pt(scalargrid, mytmp, coords)
          myindex = (k-1)*shape(1)*shape(2)+(j-1)*shape(1)+i-1
          write(*,*)'@i/j/k:',i,j,k
          write(*,*)'Comp:',mytmp,q(grid_get_field(scalargrid)+myindex)
      end if

 6    continue

      if(ltrace2) then
         call reg_dump_info(regLS)
      end if
      if(ltrace) then
         write(*,99)myid,'register_interpline:  Done.'
      end if

  90     format('[',I3,'] ','  interpline:',3I6,3G10.2)
  91     format('[',I3,'] ',A,I5,4F13.5)
  96     format('[',I3,'] ',A,4F13.5)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,F18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_interpline


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    reg_outofdomain:                                                cc
cc           Determine if a point is outside the domain of a register cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function reg_outofdomain(reg, coords)
      implicit none
      integer  reg
      real*8   coords(*)
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'methods.inc'
      include 'mem.inc'
      integer  ti, li, gi, shape(3), i, rank
      real*8   bbox(6)

      logical        ltrace
      parameter (    ltrace = .false. )
      logical        ltrace2
      parameter (    ltrace2 = .false. )
    
      reg_outofdomain = .true.

      if (ltrace) then
         write(*,99) myid,'reg_outofdomain: Reg: ',reg
         write(*,88) myid,'reg_outofdomain: Coords: ',coords(1),
     .                coords(2), coords(3)
      end if
      !
      ! Dump whole structure:
      !
      ti = register_return_start(reg)
 10   if (ti .ne. NULL_TREE) then
         if(ltrace2)write(*,85) myid,'Tree: ',ti, tree_return_time(ti)
         li = tree_return_start(ti)
 20      if (li .ne. NULL_LEVEL) then
            if(ltrace2)write(*,96)myid,'   Level: ',li, 
     .                           level_return_resolution(li)
            gi = level_return_start(li)
 30         if (gi .ne. NULL_GRID) then
               if(ltrace2)write(*,96)myid,'   Grid: ',gi
               call grid_return_bbox(gi, bbox)
               rank = grid_return_rank(gi)
               do i = 1, rank
                  if(ltrace2)write(*,94)myid,'   i=',i,bbox(2*i-1),
     .                     bbox(2*i)
                  if (coords(i).gt.bbox(2*i-1) .or.
     .                coords(i).lt.bbox(2*i  )      ) then
                     if(ltrace2)write(*,94)myid,'   Switching flag'
                     reg_outofdomain = .false.
                  end if
               end do
               !write(*,89)myid,'    L2norm:', grid_l2norm(gi)
               gi = grid_return_sibling(gi)
               goto 30
            end if
            li = level_return_sibling(li)
            goto 20
         end if
         ! Only search first tree right now...this should really
         ! be called tree_outofdomain
         !ti = tree_return_sibling(ti)
         !goto 10
      end if
 
      if (ltrace) then
         if (reg_outofdomain) then
            write(*,99)myid,'reg_outofdomain: Done: TRUE'
         else
            write(*,99)myid,'reg_outofdomain: Done: FALSE'
         end if
      end if
      
  83  format('[',I3,'] ',A,I4,A,I4,' (',I4,',',I4,',',I4,')')
  73  format('[',I3,'] ','             ','(',F8.3,',',F8.3,',',F8.3,
     *        ') (',F8.3,',',F8.3,',',F8.3,')')
  82  format('[',I3,'] ',A,I4,A,I4,' (',I4,',',I4,')')
  72  format('[',I3,'] ','             ','(',F8.3,',',F8.3,
     *        ') (',F8.3,',',F8.3,')')
  81  format('[',I3,'] ',A,I4,A,I4,' (',I4,')')
  71  format('[',I3,'] ','             ','(',F8.3,
     *        ') (',F8.3,')')

  88        format('[',I3,'] ',A,5G11.3)
  89        format('[',I3,'] ',A,G11.3)
  90        format('[',I3,'] ',A,I16)
  91        format('[',I3,'] ',A,F5.2,'%')
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  85        format('[',I3,'] ',A,I5,' Time: ',F10.5)
  96        format('[',I3,'] ',A,I5,' Res:  ',F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)
      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_genspheregrid                                          cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_genspheregrid(rmax,ntheta,nphi,nr,reg_num)
      implicit none
      integer        reg_num, ntheta,nphi,nr
      real*8         rmax
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'

         integer        tree
         integer        level 
         integer        grid
         integer        step
         integer        rank
         integer        gft_rc, coordsptr
         integer        owner
         integer        shape(3), i
         character*64   name
         logical        file_exists

         logical        ltrace2
         parameter (    ltrace2 = .false. )

         ! Functions
         integer        gft_read_shape
         integer        register_read
   
         real*8       bbox(6), sdf_time, h
         real*8       cpi
         parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279 d0)

         if (ltrace2) then
            write(*,99) myid, 'register_genspheregrid: reg = ',reg_num
         end if

         ! Assign name and rank to register variables
         !call register_assign_proc(reg_num, myid)
         
         ! Create new grid:
         grid      = grid_get_free_number()
         rank      = 3
         call register_set_rank(reg_num, rank)
         shape(1)  = ntheta
         shape(2)  = nphi
         shape(3)  = nr
         sdf_time  = 0.d0
         call grid_createB(grid, rank, shape, myid)

         coordsptr = grid_get_coords(grid)

         if (ltrace2) then
         write(*,99)myid,'register_gensph:read grid=',grid
         write(*,89)myid,'register_gensph:coordsptr=',coordsptr
         write(*,99)myid,'register_gensph:coords=',grid_get_coords(grid)
         write(*,89)myid,'register_gensph:field =',grid_get_field(grid)
         write(*,99)myid,'register_gensph:step = ',step
         write(*,99)myid,'register_gensph:rank = ',rank
         !write(*,98)myid,'register_gensph:name = ',name
         end if

         !
         ! Store the bounding box:
         !
         bbox(1) = 0.d0
         bbox(2) = cpi
         bbox(3) = 0.d0
         bbox(4) = 2.d0*cpi
         bbox(5) = 0.d0
         bbox(6) = rmax
         call grid_set_bbox(grid, bbox)
         if (ltrace2) then
            do i = 1, rank
             write(*,94)myid,'register_gensph: bbox:',
     *              shape(i),bbox(2*i-1),bbox(2*i)
            end do
         !write(*,99)myid,'register_gensph:Arank = ',
         write(*,*)'grid:',                  grid
         !write(*,*)'rank:', grid_return_rank(grid)
         end if

         ! Now create the tree, level, and grid
         if (ltrace2) then
!           write(*,99)myid,'register_gensph:rank = ',
!    .                         grid_return_rank(grid)
            write(*,89)myid,'register_gensph: sdf_time:', sdf_time
            write(*,91)myid,'register_gensph: Calling register_add_tree'
         end if
         tree        = register_add_tree(reg_num, sdf_time)
           
         call  register_rename( reg_num, "genspheregrid" )
         ! What happens if data set is not uniform resolution?
         h           = ( bbox(2) - bbox(1) )/( shape(1)*1.d0 - 1.d0 )
         level       = tree_add_level(tree, h)
            !call grid_dump(grid)
         call grid_set_level(grid, level)

            !call grid_dump(grid)
            !write(*,99)myid,'register_gensph:Arank = ',
!    .                         grid_return_rank(grid)
         ! Add new grid to the level
         call level_add_gridB(grid, level)
         if (ltrace2) then
            write(*,92)myid,'register_gensph: tree/level=',tree,level
            write(*,89)myid,'register_gensph:           h=',h
!           write(*,99)myid,'register_gensph:rank = ',
!    .                         grid_return_rank(grid)
!           call grid_dump(grid)
            call level_dump(level)
            write(*,91)myid,'register_gensph: Calling tree dump:'
            call tree_dump(tree)
            !write(*,91)myid,'register_gensph: Calling grid dump:'
            !call grid_dump(grid)
         end if

         if (ltrace2) then
         write(*,97)myid,'register_genspheregrid:About to call reg_comm'
         end if

         call register_communicate(reg_num)

         if (ltrace2) then
            call reg_dump_info(reg_num)
            write(*,97)myid,'register_genspheregrid:Done.'
         end if

  89        format('[',I3,'] ',A,G11.3)
  90        format('[',I3,'] ',A,I16)
  91        format('[',I3,'] ',A,F5.2,'%')
  92        format('[',I3,'] ',A,2I5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  85        format('[',I3,'] ',A,I5,' Time: ',F10.5)
  96        format('[',I3,'] ',A,I5,' Res:  ',F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

         return
      end              ! END: register_genspheregrid


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_interptogrid                                           cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_interptogrid(reg_from,reg_to)
      implicit none
      integer        reg_from, reg_to
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'
         integer        tree
         logical        ltrace2
         parameter (    ltrace2 = .true. )

         if (ltrace2) then
            write(*,99)myid,'register_interptogrid: reg_from:',reg_from
            write(*,99)myid,'register_interptogrid: reg_to  :',reg_to
         end if

         if (ltrace2) call reg_dump_info(reg_to)

  89        format('[',I3,'] ',A,G11.3)
  90        format('[',I3,'] ',A,I16)
  91        format('[',I3,'] ',A,F5.2,'%')
  92        format('[',I3,'] ',A,2I5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  85        format('[',I3,'] ',A,I5,' Time: ',F10.5)
  96        format('[',I3,'] ',A,I5,' Res:  ',F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

         return
      end              ! END: register_interptogrid

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_quadrupole                                             cc
cc          Analyze, for example, the density of a binary to find     cc
cc          the quadrupole moments of the two objects as functions    cc
cc          of time.  Steps necessary:                                cc
cc             0) Set integration mask                                cc
cc             1) Loop over all grids and                             cc
cc               1a) Determine where the maximum densities are        cc
cc               1b) Choose the "natural" dividing line between       cc
cc                   maximum densities.                               cc
cc               1c) Integrate over half-planes to find COMs          cc
cc             2) Loop over all grids and integrate                   cc
cc                w/r/t COMS to find the moments.                     cc
cc      Compute the moments of a given field D, in particular,        cc
cc      the first:                                                    cc
cc                I^j  = \int D x^j d^3x                              cc
cc      and second moments:                                           cc
cc                I^jk = \int D x^j x^k d^3x                          cc
cc      Moments assumed to be packed into a single array:             cc
cc                     (  I^xx    I^xy   I^xz  )                      cc
cc                     (  I^x     I^yy   I^yz  )                      cc
cc                     (  I^y     I^z    I^zz  )                      cc
cc      Adapted from HAD:src/amr/grid3.F->grid_momentfind()           cc
cc                                                                    cc
cc      option    ==1 compute w/r/t origin                            cc
cc      option    ==2 compute w/r/t two COMs                          cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_quadrupole(option,reg)
      implicit none
      integer        option, reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'registers.inc'
      include 'methods.inc'
      !
      integer        ti, li, gi, i, j, halfplane, axis
      real*8         integral, real_integral
      real*8         temp_max(6),temp_coords(18)
      real*8         true_max(6),true_coords(18)
      real*8         com_p(3), com_n(3), mp, mn
      real*8         momentsn(3,3),momentsp(3,3)
cc         NB: See the following two references:                              cc
cc             http://arxiv.org/abs/astro-ph/0010201                          cc
cc             http://arxiv.org/abs/astro-ph/0609473                          cc
      real*8         etaplusn, etacrossn, etan
      real*8         etaplusp, etacrossp, etap
      ! Levels of tracing:
      logical        ltrace
      parameter (    ltrace  = .false. )
      logical        ltrace2
      parameter (    ltrace2 = .false. )
      logical        ltrace3
      parameter (    ltrace3 = .false. )

         if (ltrace) then
            write(*,99)myid,'register_quadrupole: reg:    ',reg
            write(*,99)myid,'register_quadrupole: option: ',option
         end if
         if (ltrace3) call reg_dump_info(reg)


         ti   = register_return_start(reg)
         ! Loop over all trees
         do while ( ti .ne. NULL_TREE ) 
            ! if computing w/r/t origin, skip finding COM stuff
            if (option.eq.1) goto 101
            if(ltrace3)write(*,99)myid,'                     ti: ',ti
            call tree_set_mask(ti,MASK_INTEGRAL)
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace3)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace3)write(*,99)myid,'               gi: ',gi
                     ! compute all the maxima:
                     halfplane = +1.
                     do i = 1, 6
                        if(.false.)write(*,*)'find max:',i,
     .                        (i-1)*3+1,INT((i-1)/2)+1,halfplane
                        call grid_find_halfmax(gi,
     *                          temp_max(i),temp_coords((i-1)*3+1),
     *                              INT((i-1)/2)+1,halfplane)
                        halfplane = -halfplane
                        ! Keep track of global maxima:
                        if ( temp_max(i) .gt. true_max(i) ) then
                         true_max(i)            = temp_max(i)
                         true_coords((i-1)*3+1) = temp_coords((i-1)*3+1)
                         true_coords((i-1)*3+2) = temp_coords((i-1)*3+2)
                         true_coords((i-1)*3+3) = temp_coords((i-1)*3+3)
                        end if
                     end do
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            !
            ! Find most "natural" maxima such that
            !   the distance of the maxima from the dividing line is the largest
            !
            axis = 1
            if (abs(true_coords( 8)).gt.abs(true_coords(1)))axis=2
            if (abs(true_coords(14)).gt.abs(true_coords(8)) .and.
     .          abs(true_coords(14)).gt.abs(true_coords(8)) ) axis=3
            !
!           call MPI_Reduce(integral, real_integral, 1, 
!    *                    MPI_DOUBLE_PRECISION, MPI_SUM, master,
!    *                    MPI_COMM_WORLD, ierr)
            if ( myid .eq. master .and. ltrace2) then
                write(*,88)myid,'quad:Max ',tree_return_time(ti), axis,
     .          true_coords((axis-1)*3+1),true_coords((axis-1)*3+2),
     .          true_coords((axis-1)*3+3),true_max(axis)
            endif
            !
            ! Now integrate to find COM over halfplane:
            !
            do i = 1, 3
               com_p(i) = 0.d0
               com_n(i) = 0.d0
            end do
            mp = 0.d0
            mn = 0.d0
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace3)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace3)write(*,99)myid,'               gi: ',gi
                     !write(*,99)myid,' grid_comhalfp gi: ',gi
                     call grid_comhalfplanes(gi,com_n,com_p,
     .                                            mn,mp, axis)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            ! Normalize by total mass of each halfplane:
            com_n(1) = com_n(1) / mn
            com_n(2) = com_n(2) / mn
            com_n(3) = com_n(3) / mn
            com_p(1) = com_p(1) / mp
            com_p(2) = com_p(2) / mp
            com_p(3) = com_p(3) / mp
            ! Output center of masses:
            if ( myid .eq. master .and. ltrace) then
                write(*,87)myid,'COM: N ',tree_return_time(ti),
     .          com_n(1),   com_n(2),   com_n(3),    mn
                write(*,87)myid,'COM: P ',tree_return_time(ti),
     .          com_p(1),   com_p(2),   com_p(3),    mp
            endif
 101        continue
            if (option.eq.1) then
               ! compute w/r/t origin:
               com_n(1) = 0.d0
               com_n(2) = 0.d0
               com_n(3) = 0.d0
               com_p(1) = 0.d0
               com_p(2) = 0.d0
               com_p(3) = 0.d0
               mn       = 1.d0
               mp       = 1.d0
               axis = 0
            end if
            !
            ! Now integrate again to compute moments w/r/t COMs:
            !
            do j = 1, 3
            do i = 1, 3
               momentsn(i,j) = 0.d0
               momentsp(i,j) = 0.d0
            end do
            end do
            ! Go to start of tree
            li = tree_return_start(ti)
            ! Loop over all levels
            do while ( li .ne. NULL_LEVEL )   
               if(ltrace3)write(*,99)myid,'                     li: ',li
               gi = level_return_start(li)
               ! Loop over all grids
               do while ( gi .ne. NULL_GRID ) 
                  if( grid_is_local(gi) ) then
                     if(ltrace3)write(*,99)myid,'               gi: ',gi
                     call grid_moments(gi,momentsn,momentsp,com_n,com_p,
     .                                            mn,mp, axis)
                  endif
                  gi = grid_return_sibling(gi)
               enddo
               li = level_return_sibling(li)
            enddo
            ! compute distortion parameters:
            etaplusn  = (momentsn(1,1)-momentsn(2,2))
     *                 /(momentsn(1,1)+momentsn(2,2))
            etacrossn = 2.d0*momentsn(1,2)/(momentsn(1,1)+momentsn(2,2))
            etan      = sqrt(etaplusn**2+etacrossn**2)
            etaplusp  = (momentsp(1,1)-momentsp(2,2))
     *                 /(momentsp(1,1)+momentsp(2,2))
            etacrossp = 2.d0*momentsp(1,2)/(momentsp(1,1)+momentsp(2,2))
            etap      = sqrt(etaplusp**2+etacrossp**2)
            ! Output Moments:
            if ( myid .eq. master .and. ltrace) then
!               write(*,87)myid,'1st mom: N ',tree_return_time(ti),
!    .          momentsn(2,1)/mn,momentsn(3,1)/mn,momentsn(3,2)/mn
!               write(*,87)myid,'1st mom: P ',tree_return_time(ti),
!    .          momentsp(2,1)/mp,momentsp(3,1)/mp,momentsp(3,2)/mp
                write(*,87)myid,'Etas: N ',tree_return_time(ti),
     .          etan,etaplusn,etacrossn
                write(*,87)myid,'Etas: P ',tree_return_time(ti),
     .          etap,etaplusp,etacrossp
            endif
            ! Output
            if ( myid .eq. master) then
                write(*,86)tree_return_time(ti),
     .          com_n(1),   com_n(2),   com_n(3),  
     .          etan,etaplusn,etacrossn
                write(*,86)tree_return_time(ti),
     .          com_p(1),   com_p(2),   com_p(3),  
     .          etap,etaplusp,etacrossp
            endif
            !
            ! --------------------------------
            ! Advance to next tree (ie compute for the next time present):
            !
            ti = tree_return_sibling(ti)
            ! Reset maxima for next "tree":
            do i = 1, 6
               true_max(i)            = 0.d0
               true_coords((i-1)*3+1) = 0.d0
               true_coords((i-1)*3+2) = 0.d0
               true_coords((i-1)*3+3) = 0.d0
            end do
            do i = 1, 3
               com_p(i) = 0.d0
               com_n(i) = 0.d0
            end do
            mp = 0.d0
            mn = 0.d0
            !
         enddo 
         !
         if (ltrace) then
            write(*,99)myid,'register_quadrupole: Done w/ reg: ',reg
         end if

  86        format(7G15.7)
  87        format('[',I3,'] ',A,F10.3,6G14.2)
  88        format('[',I3,'] ',A,F10.3,I3,4G14.4)
  89        format('[',I3,'] ',A,G11.3)
  90        format('[',I3,'] ',A,I16)
  91        format('[',I3,'] ',A,F5.2,'%')
  92        format('[',I3,'] ',A,2I5)
  93        format('[',I3,'] ',A,I5,A,I5)
  94        format('[',I3,'] ',A,I5,2F10.5)
  95        format('[',I3,'] ',A,I5,F10.5)
  85        format('[',I3,'] ',A,I5,' Time: ',F10.5)
  96        format('[',I3,'] ',A,I5,' Res:  ',F10.5)
  97        format('[',I3,'] ',A)
  98        format('[',I3,'] ',A,A30)
  99        format('[',I3,'] ',A,I5)

         return
      end              ! END: register_quadrupole

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    register_atpoint:                                               cc
cc           Interpolate field value at a point specified by user.    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine register_atpoint(xpt,ypt,zpt,reg)
      implicit none
      real*8   xpt,ypt,zpt
      integer  reg
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'registers.inc'
      include 'variables.inc'
      include 'mem.inc'
      include 'methods.inc'
      include 'grids.inc'
      integer shape(3)
      character*32   name, file_name

         integer tree, grid
         integer t1, t2, t3, tx, ty, tz, tm, ttx,tty,ttz
         integer l1, l2, l3, lx, ly, lz, lm
         integer             gx, gy, gz, gm
         integer tgx, tgy, tgz
         integer i,j,k
         ! "pointers" into storage for the field line points:
         integer lx_ptr, ly_ptr, lz_ptr, lm_ptr, ufile
         ! Max number of points in each field line:
         integer     maxpts
         parameter ( maxpts = 500000 )
         ! Step-size for parameter "lambda" w/r/t fieldlines:
         real*8      dlambda
         parameter ( dlambda = 0.01d0 )
         !   1 --> first order forward differencing
         !   2 --> second order Runge-Kutte
         integer     orderaccuracy 
         parameter ( orderaccuracy = 2)
         logical keepgoing
         real*8      magB, Bx, By, Bz, hatBx, hatBy, hatBz
         real*8      x,y,z,  xprev,yprev,zprev, bbox(2), h
         real*8      time, coords(3)
         real*8      seedx, seedy, seedz
         real*8      myh, distance, mindistance
         logical     reg_outofdomain
         external    reg_outofdomain
         real(kind=8)      SMALLNUMBER
         parameter       ( SMALLNUMBER = 1.0d-13)


         logical     ltrace
         parameter ( ltrace  = .false. )
         logical     ltrace2
         parameter ( ltrace2 = .false. )
         logical     ltrace3
         parameter ( ltrace3 = .false. )

          
      if(ltrace) then
         write(*,99)myid,'register_atpoint: reg:',reg
         write(*,96)myid,'register_flines: x/y/z:',xpt,ypt,zpt
         if (ltrace3) then
            call reg_dump_info(reg)
         end if
      end if

  
      !
      ttx = register_return_start(reg)
      if (ltrace) then
         write(*,99)myid,'register_atpoint: ttx: ',ttx
      end if
      !
  2   if (ttx.ne.NULL_TREE ) then
         !
         time = tree_return_time(ttx)
         if (ltrace) then
            write(*,99)myid,'register_atpoint: * * *',ttx
            write(*,98)myid,'register_atpoint: Time= ',time
         end if
            coords(1) = xpt
            coords(2) = ypt
            coords(3) = zpt
            tgx =  tree_findgridwpoint(ttx,coords)
            if (tgx.ne.NULL_GRID) then
               if (grid_is_local(tgx)) then
                  if(ltrace2)write(*,*)'register_atpoint:calling grid..'
                  call grid_interp_pt(tgx, Bx, coords)
                  ! Main output of this routine:
                  write(*,*) time, Bx
               else
                  if (ltrace) then
                   write(*,*)'register_atpoint:Not equipped 4 multiproc'
                  end if
               end if
            else
               if (ltrace) then
                  write(*,99)myid,'register_atpoint:grids not found',
     .                    tgx
                  write(*,99)myid,'register_atpoint: Out of domain',i
               end if
               !if (i.eq.0) return
               !keepgoing = .false.
            end if
            if(ltrace3) then
               write(*,99)myid,'atpoint: At point ',i
               write(*,96)myid,'atpoint: At coordinates: ',
     .                                  coords(1),coords(2),coords(3)
            end if
            if(ltrace2)write(*,99)myid,'register_atpoint:Done w/ i = ',i
         !
         ! Continue with other times (e.g. trees)
         !
         ttx = tree_return_sibling(ttx)
         if(ltrace2) then 
            write(*,99)myid,'register_atpoint:Look at siblings ttx=',ttx
         end if
         !
         goto 2
         !
      end if


      if(ltrace) then
         write(*,99)myid,'register_atpoint: Done.'
         return
      end if

  90     format('[',I3,'] ','  atpoint:',3I6,3G10.2)
  96     format('[',I3,'] ',A,4G13.5)
  97     format('[',I3,'] ',A,A)
  98     format('[',I3,'] ',A,G18.11)
  99     format('[',I3,'] ',A,5I5)

         return
      end             ! END: register_atpoint


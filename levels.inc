      !
      ! Level.inc
      !

      !Arrays
      integer           level_counter            !Counts what level is free
      integer           level_num_grids(MAX_NUM_LEVELS) !How many grids in level
      double precision  level_resolution(MAX_NUM_LEVELS) !What res the level is
      integer           level_on_tree(MAX_NUM_LEVELS) !What tree level is on
      integer           level_number(MAX_NUM_LEVELS) !The number of the level on a certain tree

      !Pointers:      
      integer           level_grid(MAX_NUM_LEVELS) !Points to first grid on a level
      integer           level_sibling(MAX_NUM_LEVELS) !Points to sibling of a level

      COMMON          / mem_level / 
     *                level_counter,
     *                level_grid,
     *                level_sibling,
     *                level_num_grids,
     *                level_on_tree,
     *                level_number

      COMMON          / mem_level_real /
     *                level_resolution

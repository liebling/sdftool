      !
      ! Levels.f
      !

      subroutine level_init()
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer li

         level_counter    = 1
         level_number(1)  = 1

         do li = 1, MAX_NUM_LEVELS
            level_grid(li)    = NULL_LEVEL
            level_sibling(li) = NULL_LEVEL
         enddo 

         return
      end

      subroutine level_add_grid(gi, level)
         write(*,*) 'do not call level_add_grid'
         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    level_free:                                                             cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine level_free(level)
      implicit none
      integer level
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'levels.inc'
      include 'methods.inc'
      include 'variables.inc'

         integer   owner
         integer   last_grid, grid2, nextgi
         integer   grid_start, last, grid1_owner, grid2_owner

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         if (ltrace2)write(*,99)myid,'level_free: level = ',level
         if (ltrace2) call level_dump(level)

         !
         ! Organize by grid owner (descending order):
         !
         last   = NULL_GRID
         grid2  = level_return_start(level)
  5      if (grid2 .ne. NULL_GRID) then
            nextgi = grid_return_sibling(grid2)
            if(ltrace2)write(*,99)myid,'level_free: free grid2: ',grid2
            call grid_free(grid2)
            grid2  = nextgi
            goto 5
         end if

         call level_set_start(level, NULL_GRID)

         if (ltrace2) call level_dump(level)
         if (ltrace2)write(*,99)myid,'level_free: Done. ',level

         return

  99     format('[',I3,'] ',A,I5)

      end      ! level_free

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc    level_add_gridB:                                                        cc
cc                      Adds an existent grid to a level.                     cc
cc                      It is important to add the grid in such               cc
cc                      a way that does not depend on the order               cc
cc                      in which the grids are added so that all processor    cc
cc                      will have the grids on a given level in the same      cc
cc                      order independent of who owns them.                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine level_add_gridB(gi, level)
      implicit none
      integer gi, level
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'levels.inc'
      include 'methods.inc'
      include 'variables.inc'

         integer   owner
         integer   last_grid, grid2
         integer   grid_start, last, grid1_owner, grid2_owner

         logical     ltrace2
         parameter ( ltrace2 = .false. )

         grid1_owner = grid_return_owner(gi)
         if (ltrace2)write(*,*) 'level_add_gridB: gi = ',gi
!        if (ltrace2)write(*,*) 'level_add_gridB: rank = ',
!    .            grid_return_rank(gi)
         if (ltrace2)write(*,*) 'level_add_gridB: own= ',grid1_owner
         if (ltrace2) call level_dump(level)
         if (ltrace2)write(*,*) 'level_add_gridB: Adding grid'

         !
         ! Organize by grid owner (descending order):
         !
         last   = NULL_GRID
         grid2  = level_return_start(level)
  5      if (grid2 .ne. NULL_GRID) then
            grid2_owner = grid_return_owner(grid2)
            if (ltrace2)write(*,*) 'level_add_gridB: grid2/owner=',
     *                     grid2, grid2_owner
            if( grid2_owner .lt. grid1_owner ) then
               ! Current grid points to grid2:
               call grid_set_sibling(gi, grid2)
               if( last .eq. NULL_GRID) then
                  ! Current grid goes at beginning:
                  call level_set_start(level, gi)
                  if(ltrace2)write(*,*)'level_add_gridB:Set start level'
               else
                  ! Otherwise, set sibling of previous:
                  call grid_set_sibling(last, gi)
                  if(ltrace2)write(*,*)'level_add_gridB:Put in middle'
               endif
               ! All done:
               goto 10
            end if
            ! Continue looking
            last  = grid2
            if (ltrace2)write(*,*)'level_add_gridB: last = ',last
            grid2 = grid_return_sibling(grid2)
            goto 5
         end if

         if (last .eq. NULL_GRID) then
            if (ltrace2)write(*,*)'level_add_gridB: Level empty'
            ! Level is empty
            call level_set_start(level, gi)
            call grid_set_sibling(gi, NULL_GRID)
         else
            ! At the end of the level:
            if (ltrace2)write(*,*)'level_add_gridB: Placing grid at end'
            call grid_set_sibling(last, gi)
            call grid_set_sibling(gi, NULL_GRID)
         end if

 10      continue

         !if (ltrace2) call reg_dump_info(1)
         if (ltrace2) call level_dump(level)
         if (ltrace2)write(*,*)'level_add_gridB: Done.'

         return
      end      ! level_add_gridB

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111
      integer function level_get_free_number()
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'

      if (level_counter.eq.MAX_NUM_LEVELS) then
         write(*,*) 'level_get_free_number: No levels left.'
         write(*,*) 'level_get_free_number: Quitting here.'
         stop
      else
         level_get_free_number = level_counter
         level_counter         = level_counter + 1
      end if

         return
      end

      subroutine level_set_sibling(level, sibling)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         integer sibling

         level_sibling(level) = sibling

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function level_return_maxradius(level)
      implicit none
      integer  level
      include 'class_constants.inc'
      include 'levels.inc'
      integer  gi
      real*8   mytmp
      !Functions
      integer  grid_return_sibling
      external grid_return_sibling
      real*8   grid_return_maxradius
      external grid_return_maxradius

      gi = level_grid(level) !Sets gi equal to first grid in that level
      do while( gi .ne. NULL_GRID )
         mytmp = grid_return_maxradius(gi)
         level_return_maxradius = max(mytmp,level_return_maxradius)
         gi = grid_return_sibling(gi)
      end do


      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      !Return the last grid on a certain level
      integer function level_return_end(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'

         integer level

         integer gi

         !Functions
         integer grid_return_sibling

         gi = level_grid(level) !Sets gi equal to first grid in that level
!          write(*,*) "level_grid(level) =", level_grid(level)
         !Then if you found a grid then increment the sibling
         !pointer till you get to NULL_GRID then gi
         !will be the last grid in the level 
         if( gi .ne. NULL_GRID ) then
            do while( grid_return_sibling(gi) .ne. NULL_GRID )
               gi = grid_return_sibling(gi)
            enddo

            level_return_end = gi
         else
            level_return_end = NULL_GRID
         endif

         return
      end


      subroutine level_set_tree(level, tree)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         integer tree

         level_on_tree(level) = tree

         return
      end

      integer function level_return_tree(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
  
         level_return_tree = level_on_tree(level)
         return
      end

      subroutine level_set_start(level, grid)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer grid
         integer level

         level_grid(level) = grid

         return
      end


      subroutine level_set_resolution(level, resolution)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         real*8  resolution

         level_resolution(level) = resolution

         return
      end


      double precision function level_return_resolution(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level

         level_return_resolution = level_resolution(level)

         return
      end

      integer function level_return_sibling(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         logical  level_valid
         external level_valid
   
         if (level_valid(level)) then
            level_return_sibling = level_sibling(level)
         else
            level_return_sibling = NULL_LEVEL
         end if

         return
      end

      integer function level_return_start(level)
      implicit none
      include 'class_constants.inc' 
      include 'levels.inc'
         integer level
         
         level_return_start = level_grid(level)
         return
      end

      subroutine level_output(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         
         integer grid
         
         !Functions
         integer level_return_start
         integer grid_return_sibling
       
         !Gives first grid on level
         grid = level_return_start(level)
         do while( grid .ne. NULL_GRID )
            call grid_output(grid)
            
            !Increment grid
            grid = grid_return_sibling(grid)
         enddo
        
         return
      end

      subroutine amira_level_output(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         
         integer grid
         
         !Functions
         integer level_return_start
         integer grid_return_sibling
       
         !Gives first grid on level
         grid = level_return_start(level)
         do while( grid .ne. NULL_GRID )
            call amira_grid_output(grid)
            
            !Increment grid
            grid = grid_return_sibling(grid)
         enddo
        
         return
      end

      integer function level_get_number_grids(level)
      implicit none
      include 'class_constants.inc'
      include 'registers.inc'
         integer   level

         integer   gi
         integer   num_grids

         !Functions
         integer   level_return_start
         integer   grid_return_sibling

         gi = level_return_start( level )

         if ( gi .ne. NULL_GRID ) then
            num_grids = 1
         endif

         do while ( grid_return_sibling(gi) .ne. NULL_GRID )
            num_grids = num_grids + 1

            !Increment tree counter
            gi = grid_return_sibling(gi)
         enddo

         level_get_number_grids = num_grids

         return
      end

      subroutine level_dump(level)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'class_constants.inc'
      include 'variables.inc'
      include 'levels.inc'
         integer   level

         integer   num_grids
         integer   gi
         real*8    res

         !Functions
         integer   level_get_number_grids
         double precision level_return_resolution
         integer   grid_return_sibling
         integer   level_return_start
         integer   level_return_number

         num_grids  = level_get_number_grids(level)
         res        = level_return_resolution(level)
         
         write(*,'(A,I3.1,A,A,I3.1,A,I3.1,A,F7.4)') "[",myid,"]",
     *              "   Level: ", level_return_number(level), 
     *              "   NumGrids: ", num_grids, 
     *              "   Res: ", res

         gi = level_return_start(level)

         do while( gi .ne. NULL_GRID )
            call grid_dump(gi)

            gi = grid_return_sibling(gi)
         enddo

         return
      end

      ! What is this?
      subroutine level_set_number( level, number ) 
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
         integer number 

         level_number(level) = number

         return
      end

      integer function level_return_number(level)
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
         integer level
 
         level_return_number = level_number(level)
         return
      end

      subroutine level_copy( old_level, new_level )
      implicit none
      include 'class_constants.inc'
      include 'levels.inc'
      include 'methods.inc'
         integer old_level
         integer new_level

         integer old_gi, new_grid
         integer shape(3)
         real*8  bbox(6)
         integer grid_owner,rank
   
         write(*,*) "Entering level copy..."
         write(*,*) "old_level = ", old_level     
         ! Find first grid
         old_gi = level_grid(old_level)

         do while( old_gi .ne. NULL_LEVEL )
            ! Get bbox and shape from old_grid
            call grid_return_bbox( old_gi, bbox )
            call grid_return_shape(old_gi, shape)
            grid_owner = grid_return_owner(old_gi)
           rank        = grid_return_rank(old_gi)
 
            ! Create new grid 
!           new_grid = level_add_grid( new_level, shape, bbox, 
!    *                                 grid_owner, .false. )
            new_grid = grid_get_free_number()
            call grid_set_level(new_grid, new_level)
            call grid_createB(new_grid, rank, shape, grid_owner)
            call grid_set_bbox(new_grid, bbox)
            call level_add_gridB(new_grid, new_level)
     
            ! Copy grid
            call grid_copy( old_gi, new_grid )
 
            ! Increment
            old_gi = grid_return_sibling( old_gi )
         enddo

         return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                    cc
cc    level_valid:  Tests whether a level  number is valid or not.    cc
cc                                                                    cc
cc                                                                    cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function level_valid(lev)
      implicit none
      integer  lev
      include 'class_constants.inc'

      level_valid = .true.
      if (lev .gt. MAX_NUM_LEVELS) level_valid = .false.
      if (lev .le. 0             ) level_valid = .false.

      return
      end


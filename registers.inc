      !
      ! Registers.inc
      !
      

      !Variables:
      integer         reg_counter
      integer         reg_rank(MAX_NUM_REG)   ! Stores rank of data on reg
      integer         reg_proc(MAX_NUM_REG)   ! Stores process prefix     
      integer         reg_axis(MAX_NUM_REG)   ! Stores the preferred axis
      character*128   reg_name(MAX_NUM_REG)   ! Array storing the name 
      character*128   reg_myname(MAX_NUM_REG) ! Name w/ processor prefix

      !Pointers:
      integer         reg_tree(MAX_NUM_REG)   ! Points to first tree in reg 


      COMMON          / mem_registers / 
     *                reg_name, 
     *                reg_myname, 
     *                reg_tree,
     *                reg_rank,
     *                reg_proc,
     *                reg_axis,
     *                reg_counter

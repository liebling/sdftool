      !!!!!!!!!!!!!!!!!!!!!!
      !!  Maximum Finder  !!
      !!!!!!!!!!!!!!!!!!!!!! 
      double precision function find_maximum( field, shape, abs_flag )
      implicit none
         integer shape(3)
         real*8  field(shape(1), shape(2), shape(3))
         logical abs_flag

         integer i, j, k
         real*8  local_maximum
         
         
         local_maximum = field(1,1,1)

         do k = 1, shape(3)
            do j = 1, shape(2)
               do i = 1, shape(1)
                  if( abs_flag ) then
                     if( abs(field(i,j,k)) .gt. local_maximum) then
                        local_maximum = abs(field(i,j,k))
                     endif
                  else
                     if( field(i,j,k) .gt. local_maximum) then 
                        local_maximum = field(i,j,k)
                     endif
                  endif
               enddo
            enddo
         enddo
        
         find_maximum = local_maximum 

         return
      end

      !!!!!!!!!!!!!!!!!!!!!!
      !!  Minimum Finder  !!
      !!!!!!!!!!!!!!!!!!!!!!
      ! This assumes 3 dimensions
      ! This is same logic as maximum, very sloppy
      double precision function find_minimum( field, shape )
      implicit none
         integer shape(3)
         real*8  field(shape(1), shape(2), shape(3))

         integer i, j, k
         real*8  local_minimum

         local_minimum = field(1,1,1)

         do k = 1, shape(3)
            do j = 1, shape(2)
               do i = 1, shape(1)
                  if( field(i,j,k) .gt. local_minimum) then
                     local_minimum = field(i,j,k)
                  endif
               enddo
            enddo
         enddo
        
         find_minimum = local_minimum 

         return
      end


      !!!!!!!!!!!!!!!!!!!!!!!!!
      !!  Generate Gaussian  !!
      !!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine generate_gaussian(gft_shape, bbox,
     a                             amplitude, field  )
      implicit none
      include 'class_constants.inc'
      include 'mem.inc'
         integer        gft_shape(3)
         real*8         bbox(6)
         real*8         amplitude
         real*8         field( gft_shape(1), 
     *                         gft_shape(2), 
     *                         gft_shape(3) )
         
         integer        i, j, k, counter
         real*8         x, y, z, dx, dy, dz
         integer        nx, ny, nz

         logical        ltrace
         parameter    ( ltrace = .false. )

         nx    = gft_shape(1) 
         ny    = gft_shape(2) 
         nz    = gft_shape(3) 
            
         dx    = ( bbox(2) - bbox(1) ) / ( nx - 1 )
         dy    = ( bbox(4) - bbox(3) ) / ( ny - 1 )
         dz    = ( bbox(6) - bbox(5) ) / ( nz - 1 )



         do i = 1, nx
            x = bbox(1) + dx * (i - 1.d0)
            do j = 1, ny
               y = bbox(3) + dy * (j - 1.d0)
               do k = 1, nz
                  z = bbox(5) + dz * (k - 1.d0) 
                     
                  field(i,j,k) = amplitude *  
     a                 exp( -(x**2 + y**2 + z**2)
     a                            / 0.2d0**2 ) 
               enddo
            enddo
         enddo
         
         return
      end    

      !Gets the name of the parameter file
      subroutine get_params_file( params_file )
      implicit none
         character*(*)  params_file
    
         integer        argc
         character*100  params, temp_command

         !Functions
         integer        iargc 
         logical        ltrace
         parameter (    ltrace = .false. )

         argc = iargc()
         if (ltrace) write(*,*) 'get_params_file: argc=',argc
         if (argc .lt. 1) then
          write(*,*)'Usage: sdftool <script filename>'
          write(*,*)'Or:    mpirun -np <numprocs> sdftool <script name>'
          write(*,*)' '
          write(*,*)'An example script which (1) reads a parallel'
          write(*,*)'data set with filenames of the form:'
          write(*,*)'            <procnum>phi0.sdf,'
          write(*,*)'and then (2) selects every third time'
          write(*,*)'and then (3) merges them into a single uniform'
          write(*,*)'mesh on the master processor, and finally'
          write(*,*)'(4) outputs the new data set with name 0phi0merged'
          write(*,*)' '
          write(*,*)'      read'
          write(*,*)'           phi0'
          write(*,*)'           1'
          write(*,*)'      filtert'
          write(*,*)'           1-*/3'
          write(*,*)'           1'
          write(*,*)'      uni_merge'
          write(*,*)'           1'
          write(*,*)'           phi0merged'
          write(*,*)'           2'
          write(*,*)'      output'
          write(*,*)'           2'
          write(*,*)' '
          write(*,*)'*** For further help: sdftool help ***'
          write(*,*)' '
          write(*,*)'Copyright 2006 by Jason Williams and S.L. Liebling'
          stop
         else
            call getarg(1, params)
            read(params, *) params_file
            if(ltrace)write(*,*)'get_params_file: params_file=',
     *            params_file
         endif

         if (params_file .eq. 'help' .and. argc .eq. 1) then
           write(*,*)"For more help on a particular command, type:"
           write(*,*)"     sdftool  help <command> "
           write(*,*)' '
           write(*,*)"List of commands:"
           write(*,*)"    ---Input/Output:"
           write(*,*)"       read     - Reads in an sdf file"
           write(*,*)"       readone  - Reads in one dataset at a time"
           write(*,*)"       readonetime-Reads in all grids for a given"
           write(*,*)"                   to process, and then continues"
           write(*,*)"       output   - Outputs a register"
           write(*,*)"       amira    - Outputs an amira file"
           write(*,*)"       rename   - Renames target register"
           write(*,*)"       regdump  - Dumps a register to screen"
           write(*,*)"       reglist  - List all non-empty registers"
           write(*,*)"       memstat  - Show memory usage statistics"
           write(*,*)"    ---Filters:"
           write(*,*)"       filterh  - Filters out by resolution"
           write(*,*)"       filtert  - Filter by time (index vector)"
           write(*,*)"       filterl  - Filters out by level"
           write(*,*)"    ---Reduce:"
           write(*,*)"       min      - Finds the minimum of a register"
           write(*,*)"       max      - Finds the maximum of a register"
           write(*,*)"       maxabs   - Find the max of absolute value"
           write(*,*)"       maxhalfplane - Find max over half domain"
           write(*,*)"       integrate- Returns spatial integral"
           write(*,*)"       coarsen  - Selects every nth point"
           write(*,*)"    ---Binary operations:"
           write(*,*)"       subtract - Subtracts  2 registers"
           write(*,*)"       subtractuno-Subtracts  single tree fromall"
           write(*,*)"       subtractgen-Subtracts 2 regs generally"
           write(*,*)"       add      - Adds       2 registers"
           write(*,*)"       multiply - Multiplies 2 registers"
           write(*,*)"       divide   - Divides    2 registers"
           write(*,*)"       locmax   - Take max at each point"
           write(*,*)"       histo    - Produce a histogram from 2 regs"
           write(*,*)"    ---Coordinate operations:"
           write(*,*)"       times    - Multiplies by its coordinate"
           write(*,*)"       multDV   - Multiplies by the dx,dx*dy,dV"
           write(*,*)"       r        - Extracts coordinate radius"   
           write(*,*)"       newcenter- Transforms coords w/r/t center"
           write(*,*)"       radvector- Extract unit radial vector"
           write(*,*)"       azimuth  - Extract azimuthal angle"
           write(*,*)"       polar    - Extract polar angle"
           write(*,*)"    ---Unary operations:"
           write(*,*)"       gen      - Generates a gaussian"
           write(*,*)"       genspheregrid - Generates a spherical grid"
           write(*,*)"       multiplyconstant - Multiplies by a const"
           write(*,*)"       addconstant      - Adds a const"
           write(*,*)"       arcsin   - Computes arcsin  of reg"
           write(*,*)"       sqrt     - Computes sqrt    of reg"
           write(*,*)"       square   - Computes square  of reg"
           write(*,*)"       inverse  - Computes inverse of reg"
           write(*,*)"       abs      - Computes absolute value of reg"
           write(*,*)"       floor    - All small values go to floorval"
           write(*,*)"       unfloor  - All small values go to 0"
           write(*,*)"       mask     - Create mask [0,1] from scalar"
           write(*,*)"       mask2    - Create mask [0,1] from scalar"
           write(*,*)"       atpoint  - Get value on finest level@point"
           write(*,*)"    ---Compact Object Binary specific operations:"
           write(*,*)"       quadrupole-Compute quad. moments of binary"
           write(*,*)"    ---Misc/Controls:"
           write(*,*)"       setprefaxis Sets preferred axis for a reg"
           write(*,*)"       uni_merge- Combine domains into one grid"
           write(*,*)"       loopproc - Loop over files 0..proc"
           write(*,*)"       copy     - Copies a register to a new one"
           write(*,*)"       erase    - Erase register and free memory"
           write(*,*)"  --------- vector ops -----------"
           write(*,*)"  curl     - Computes curl of a vector field"
           write(*,*)"  uncurl   - Compute vec pot. ala Louis Pisha"
           write(*,*)"  div      - Computes divergence of vector field"
           write(*,*)"  grad     - Computes gradient of a scalar field"
           write(*,*)"  cross    - Cross product between two vectors"
           write(*,*)"  dot      - Dot/scalar product of two vectors"
           write(*,*)"  vecmag   - (flatspace) magnitude of a vector"
           write(*,*)"  cart2sphere-transforms vector from Cartesian"
           write(*,*)"              to Spherical coordinate basis"
           write(*,*)"  helicity - Computes hel. of a vector field"
           write(*,*)"  flines   - Computes field lines of vector field"
           write(*,*)"  interpline-Interpolates scalar to pts of line"
           write(*,*)"  --------- untested -----------"
           write(*,*)"  filter   - Filters out target time and/or level"
           write(*,*)"  restrict - Restricts a register"
           write(*,*)"  l2norm   - Compute l2norm for each time/tree"
           write(*,*)"  --------- to be implemented -----------"
           write(*,*)"  cvfactor - Compute convergence factor for 3regs"
           write(*,*)"  gplotout - Output script/data for gnuplot out"
           write(*,*)"  1dcut    - select a single line through data"
           write(*,*)"       interptogrid - Interpolate to another grid"
           stop
         elseif( argc .eq. 2 ) then
           call getarg(2, params)
           read(params,*) temp_command
           if(ltrace)write(*,*)'get_params_file: temp_command=',
     *                    temp_command
         if(     temp_command .eq. 'read' ) then
            write(*,*) "Read- Reads in an sdf file into a register"
            write(*,*) "Usage:"
            write(*,*) "  read"
            write(*,*) "   filename base"
            write(*,*) "   register number"
            stop
         else if(temp_command .eq. 'radvector' ) then
            write(*,*) "Radvector- Extracts the radial unit vector"
            write(*,*) "           from the coordinates of a field."
            write(*,*) "Usage:"
            write(*,*) "  radvector"
            write(*,*) "   register number from which to extract coords"
            write(*,*) "   target reg num. for X-component of vector"  
            write(*,*) "   target reg num. for Y-component of vector"  
            write(*,*) "   target reg num. for Z-component of vector"  
            stop
         else if(temp_command .eq. 'quadrupole' ) then
            write(*,*) "Quadrupole-Computes quad. moment w/r/t a center"
            write(*,*) "           found via the maximum of the field."
            write(*,*) "           Assumes a binary and looks for two"
            write(*,*) "           centers on opposite domain halves."
            write(*,*) ""
            write(*,*) "    Outputs ascii data: "
        write(*,*)"  <time> <x_COM> <y_COM> <z_COM> <eta> <eta+> <eta*>"
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  quadrupole"
            write(*,*) "   1 or 2 w/r/t origin (==1) or w/r/t COMs(==2)"
            write(*,*) "   register number of density"
            stop
         else if(temp_command .eq. 'newcenter' ) then
            write(*,*) "Newcenter- Transforms coordinates of a field"
            write(*,*) "           to a new center defined by a file:"
            write(*,*) "           position.in of form:"
            write(*,*) "           <time> <x0> <y0> <z0>"
            write(*,*) "Usage:"
            write(*,*) "  newcenter"
            write(*,*) "   register number"
            stop
         else if(temp_command .eq. 'interptogrid' ) then
            write(*,*) "interptogrid - Interpolate to the coordinates"
            write(*,*) "           of a different register"
            write(*,*) "   ***NOT IMPLEMENTED YET****"
            write(*,*) "Usage:"
            write(*,*) "  interptogrid"
            write(*,*) "   register of field"
            write(*,*) "   register with coordinates"
            stop
         else if(temp_command .eq. 'genspheregrid' ) then
            write(*,*) "genspheregrid - Generate spherical grid"
            write(*,*) "                loaded w/ zeroes"
            write(*,*) "Usage:"
            write(*,*) "  genspheregrid"
            write(*,*) "   rmax"
            write(*,*) "   ntheta -- polar [0-pi]"
            write(*,*) "   nphi -- azimuth [0-2pi]"
            write(*,*) "   nr"
            write(*,*) "   register"
            stop
         else if(temp_command .eq. 'readone' ) then
            write(*,*) "Readone- Reads in an sdf file into a register"
            write(*,*) "Usage:"
            write(*,*) "  readone"
            write(*,*) "   filename base"
            write(*,*) "   register number"
            write(*,*) "NB: This works in a mode in which each SDF"
            write(*,*) "    is read in one set at a time so that"
            write(*,*) "    as little memory as possible is used"
            stop
         else if(temp_command .eq. 'readonetime' ) then
            write(*,*) "Readonetime- Reads in all grids for a time"
            write(*,*) "Usage:"
            write(*,*) "  readonetime"
            write(*,*) "   filename base"
            write(*,*) "   register number"
            write(*,*) "NB: This works in a mode in which each SDF"
            write(*,*) "    is read in one set at a time so that"
            write(*,*) "    as little memory as possible is used"
            stop
         elseif( temp_command .eq. 'output' ) then
            write(*,*) "Ouput- Outputs the given register"
            write(*,*) "Usage:"
            write(*,*) "  output"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'amira' ) then
            write(*,*) "amira- Output amira readable register"
            write(*,*) "Usage:"
            write(*,*) "  amira"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'histo' ) then
            write(*,*) "histo- Produce a histogram"
            write(*,*) "Usage:"
            write(*,*) "  histo"
            write(*,*) "   numbins"
            write(*,*) "   min"
            write(*,*) "   max"
            write(*,*) "   register # describing amount"  
            write(*,*) "   register # from which to be binned"
            stop
         elseif( temp_command .eq. 'gen' ) then
            write(*,*) "Gen- Generates a gaussian pulse"
            write(*,*) "Usage:"
            write(*,*) "  gen"
            write(*,*) "   File Name"
            write(*,*) "   Register Number"
            write(*,*) "   nx"
            write(*,*) "   ny"
            write(*,*) "   nz"
            write(*,*) "   x-length"  
            write(*,*) "   y-length"  
            write(*,*) "   z-length"
            write(*,*) "   start time"
            write(*,*) "   end time"
            write(*,*) "   dt"
            write(*,*) "   amplitude"
            stop
         elseif( temp_command .eq. 'regdump' ) then
            write(*,*)"Regdump- Dumps the heirarchy of a given register"
            write(*,*) "Usage:"
            write(*,*) "  regdump"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'rename' ) then
            write(*,*) "Rename- Renames a register"
            write(*,*) "Usage:"
            write(*,*) "  rename"
            write(*,*) "   new file name"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'maxhalfplane' ) then
            write(*,*)"Maxhalfplane-Outputs the max for each time"
            write(*,*)"     over half the domain for x_i>0 or x_i<0"
            write(*,*)"     where the user chooses i and either < or >."
            write(*,*)"     Also outputs the coordinate location of max"
            write(*,*) "Usage:"
            write(*,*) "  maxhalfplane"
            write(*,*) "   axis number (1, 2, or 3)"
            write(*,*) "   -1 or +1 (for either < or >=, respectively)"
            write(*,*) "   register number" 
            stop
         elseif( temp_command .eq. 'max' ) then
            write(*,*)"Max- Outputs the maximum value for each time"
            write(*,*)"     Also outputs the coordinate location"
            write(*,*) "Usage:"
            write(*,*) "  max"
            write(*,*) "   register number" 
            stop
         elseif( temp_command .eq. 'maxabs' ) then
            write(*,*)"Max - Outputs the maximum value for each time"
            write(*,*) "Usage:"
            write(*,*) "  maxabs"
            write(*,*) "   register number"
            stop
         elseif( temp_command .eq. 'min'
     *                           .or. temp_command .eq. 'minimum') then
            write(*,*)"Minimum- Outputs the minimum value for each time"
            write(*,*) "Usage:"
            write(*,*) "  min(imum)"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'mask' ) then
            write(*,*)"Mask- Creates mask function[0,1] based on scalar"
            write(*,*) "Usage:"
            write(*,*) "  mask"
            write(*,*) "   value which gets sent to 1"
            write(*,*) "   epsilon -- value +/- epsilon"
            write(*,*) "   source/output register (masks in place)"  
            stop
         elseif( temp_command .eq. 'mask2' ) then
            write(*,*)"Mask2-Creates mask function[0,1] based on scalar"
            write(*,*) "Usage:"
            write(*,*) "  mask2"
            write(*,*) "   value above which gets sent to 1, below to 0"
            write(*,*) "   source/output register (masks in place)"  
            stop
         elseif( temp_command .eq. 'filterl' ) then
            write(*,*)"Filterl- Retain a given level(referenced from 0)"
            write(*,*) "Usage:"
            write(*,*) "  filterl"
            write(*,*) "   level (0,1, ...)"
            write(*,*) "   source/output register (filters in place)"  
            stop
         elseif( temp_command .eq. 'filterh' ) then
            write(*,*)"Filterh- Filter to allow only a given resolution"
            write(*,*) "Usage:"
            write(*,*) "  filterh"
            write(*,*) "   resolution to filter"
            write(*,*) "   source/output register (filters in place)"  
            stop
         elseif( temp_command .eq. 'filtert' ) then
            write(*,*)"Filtert- Filter in time w/ index vector notation"
            write(*,*) "Usage:"
            write(*,*) "  filtert"
            write(*,*) "   index vector (e.g. 1-*/4)"
            write(*,*) "   source/output register (filters in place)"  
            stop
         elseif( temp_command .eq. 'filter' ) then
            write(*,*)"Filter- Filters a certain time/resolution"
            write(*,*) "Usage:"
            write(*,*) "  filter"
            write(*,*) "   target register" 
            write(*,*) "   time to filter"
            write(*,*) "   resolution to filter"
            write(*,*) "   filename for new register" 
            write(*,*) "   new register number for filtered register"
            stop
         elseif( temp_command .eq. 'uni_merge' ) then
            write(*,*)"Uni_merge- Combines domain decomposed grids from"
            write(*,*)"           from different procs into a single,  "
            write(*,*)"           uniform grid.                        "
            write(*,*) "Usage:"
            write(*,*) "  uni_merge"
            write(*,*) "   register to merge"  
            write(*,*) "   file name for new register"
            write(*,*) "   register for merged data"
            stop
         elseif( temp_command .eq. 'restrict' ) then
            write(*,*)"Restrict- Restricts a reg to a lower resolution"
            write(*,*) "Usage:"
            write(*,*) "  restrict"
            write(*,*) "   register to restrict"  
            write(*,*) "   new register for restricted data" 
            write(*,*) "   filename for new register"
            stop
         elseif( temp_command .eq. 'coarsen' ) then
            write(*,*)"Coarsen- Selects every nth point in place"
            write(*,*) "Usage:"
            write(*,*) "  coarsen"
            write(*,*) "   n"
            write(*,*) "   input register"
            stop
         elseif( temp_command .eq. 'subtract' ) then
            write(*,*)"Subtract- Reg2 - Reg1 = Reg2 (in place)"
            write(*,*) "Usage:"
            write(*,*) "  subtract"
            write(*,*) "   register1"   
            write(*,*) "   register2"
            stop
         elseif( temp_command .eq. 'subtractgen' ) then
            write(*,*)"Subtractgen- Reg2 - Reg1 = Reg3"
            write(*,*)"     subtracts only in intersection for each lev"
            write(*,*) "Usage:"
            write(*,*) "  subtractgen"
            write(*,*) "   register1"   
            write(*,*) "   register2"
            write(*,*) "   register3"
            stop
         elseif( temp_command .eq. 'subtractuno' ) then
            write(*,*)"Subtract- Reg2 - Reg1 = Reg2 (in place)"
            write(*,*) "Usage:"
            write(*,*) "  subtract"
            write(*,*) "   register1--with only data @ only 1 time"
            write(*,*) "   register2"
            stop
         elseif( temp_command .eq. 'integrate' ) then
            write(*,*)"Integrate- Outputs the integral for each time"
            write(*,*) "Usage:"
            write(*,*) "  integrate"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'times' ) then
            write(*,*)"Times- Multiplies a field by its coord"
            write(*,*) "Usage:"
            write(*,*) "  times"
            write(*,*) "   register number"  
            write(*,*) "   dir (1,2, or 3)"
            stop
         elseif( temp_command .eq. 'divide' ) then
            write(*,*)"Divide- Divides one register by another(inplace)"
            write(*,*) "Usage:"
            write(*,*) "  divide"
            write(*,*) "   input/output number of numerator"  
            write(*,*) "   register     number of denominator"  
            stop
         elseif( temp_command .eq. 'l2norm' ) then
            write(*,*)"L2norm   - Computes the L2 norm of a field"
            write(*,*) "Usage:"
            write(*,*) "  l2norm"
            write(*,*) "   register number"  
            stop
         elseif( temp_command .eq. 'curl' ) then
            write(*,*)"Curl     - Computes the curl of a vector field"
            write(*,*) "Usage:"
            write(*,*) "  curl"
            write(*,*) "   register number for X-component"  
            write(*,*) "   register number for Y-component"  
            write(*,*) "   register number for Z-component"  
            write(*,*) "   target reg num. for X-component"  
            write(*,*) "   target reg num. for Y-component"  
            write(*,*) "   target reg num. for Z-component"  
            stop
         elseif( temp_command .eq. 'uncurl' ) then
            write(*,*)"Uncurl    Computes the uncurl of a vector field"
            write(*,*) "Usage:"
            write(*,*) "  uncurl"
            write(*,*) "   input field number for X-component"  
            write(*,*) "   input field number for Y-component"  
            write(*,*) "   input field number for Z-component"  
            write(*,*) "   output target reg num. for X-component"  
            write(*,*) "   output target reg num. for Y-component"  
            write(*,*) "   output target reg num. for Z-component"  
            stop
         elseif( temp_command .eq. 'flines' ) then
            write(*,*)"flines      Compute field lines for some vector"
            write(*,*)"            field. Uses seeds as defined"
            write(*,*)"            in file .flines_seeds"
            write(*,*)"            Assumes Cartesian domain"
            write(*,*)"     NB: Need to have a file: seeds"
            write(*,*)"         which contains any and all seeds for"
            write(*,*)"         the field lines w/ format:"
            write(*,*)"         <x_seed> <y_seed> <z_seed>"
            write(*,*)"         with any number of lines."
            write(*,*) "Usage:"
            write(*,*) "  flines"
            write(*,*) "   register containing x-component of vector"
            write(*,*) "   register containing y-component of vector"
            write(*,*) "   register containing z-component of vector"
            write(*,*) "   register to contain x-coordinates of lines"
            write(*,*) "   register to contain y-coordinates of lines"
            write(*,*) "   register to contain z-coordinates of lines"
            write(*,*) "   register to contain vector-mag. along lines"
         elseif( temp_command .eq. 'interpline' ) then
            write(*,*)"interpline  Interpolate a scalar field to the"
            write(*,*)"            location of given field lines"
            write(*,*) "Usage:"
            write(*,*) "  interpline"
            write(*,*) "   register containing scalar field"
            write(*,*) "   register containing x-coordinates of lines"
            write(*,*) "   register containing y-coordinates of lines"
            write(*,*) "   register containing z-coordinates of lines"
            write(*,*) "   register to contain 1D interpolated field"
         elseif( temp_command .eq. 'setprefaxis' ) then
            write(*,*)"SetPrefAxis Set the preferred axis for a reg."
            write(*,*)"            For 2D data, one defines the axis"
            write(*,*)"            perpendicular to the plane."
            write(*,*)"            For 1D data, one defines the axis"
            write(*,*)"            on which the data is defined"
            write(*,*)"            For 3D data, no use yet"
            write(*,*) "Usage:"
            write(*,*) "  setprefaxis"
            write(*,*) "   number for axis (1-x,2-y,3-z)"
            write(*,*) "   register number"
         elseif( temp_command .eq. 'grad' ) then
            write(*,*)"grad      - Computes gradient of a scalar field"
            write(*,*) "Usage:"
            write(*,*) "  grad"
            write(*,*) "   register number for scalar field"
            write(*,*) "   target reg num. for X-component"  
            write(*,*) "   target reg num. for Y-component"  
            write(*,*) "   target reg num. for Z-component"  
            stop
         elseif( temp_command .eq. 'div' ) then
            write(*,*)"Div      - Computes divergence of a vector field"
            write(*,*) "Usage:"
            write(*,*) "  div"
            write(*,*) "   target reg num. for divergence"
            write(*,*) "   input  reg num. for X-component"  
            write(*,*) "   input  reg num. for Y-component"  
            write(*,*) "   input  reg num. for Z-component"  
            stop
         elseif( temp_command .eq. 'cross' ) then
            write(*,*)"Cross    - Computes cross product of two vectors"
            write(*,*) "Usage:"
            write(*,*) "  cross"
            write(*,*) "   register number for X-component"  
            write(*,*) "   register number for Y-component"  
            write(*,*) "   register number for Z-component"  
            write(*,*) "   input/output reg num. for X-component"  
            write(*,*) "   input/output reg num. for Y-component"  
            write(*,*) "   input/output reg num. for Z-component"  
            stop
         elseif( temp_command .eq. 'dot' ) then
            write(*,*)"Dot      - Computes dot product of two vectors"
            write(*,*) "Usage:"
            write(*,*) "  dot"
            write(*,*) "   register number for X-component"  
            write(*,*) "   register number for Y-component"  
            write(*,*) "   register number for Z-component"  
            write(*,*) "   input/output reg num. for X-component"  
            write(*,*) "   input        reg num. for Y-component"  
            write(*,*) "   input        reg num. for Z-component"  
            stop
         elseif( temp_command .eq. 'vecmag' ) then
            write(*,*)"Vecmag   - Computes magnitude of vector"       
            write(*,*) "Usage:"
            write(*,*) "  vecmag"
            write(*,*) "   register number for X-component"  
            write(*,*) "   register number for Y-component"  
            write(*,*) "   register number for Z-component"  
            write(*,*) "   output reg. number"
            stop
         elseif( temp_command .eq. 'cart2sphere' ) then
            write(*,*)"cart2sphere- Transforms Cartesian vector comp."
            write(*,*) "            to a spherical coord. basis"
            write(*,*) "           *If the file .cart2sphere exists"
            write(*,*) "            in form:  <time> <x> <y> <z>"
            write(*,*) "            then computes relative to"
            write(*,*) "            dynamic origin."
            write(*,*) "        NB: see setprefaxis for use with 2D"
            write(*,*) "Usage:"
            write(*,*) "  cart2sphere"
            write(*,*) "   register number for X-component"  
            write(*,*) "   register number for Y-component"  
            write(*,*) "   register number for Z-component"  
            write(*,*) "   output reg for r-component"  
            write(*,*) "   output reg for theta-component (polar 0-pi)"
            write(*,*) "   output reg for phi-component (azimuth 0-2pi)"
            stop
         elseif( temp_command .eq. 'helicity' ) then
            write(*,*)"helicity- Computes hel. w/r/t vector field."
            write(*,*) "            Generally applicab. to magnetic"
            write(*,*) "Usage:"
            write(*,*) "  helicity"
            write(*,*) "   register number for X-component"  
            write(*,*) "   register number for Y-component"  
            write(*,*) "   register number for Z-component"  
            write(*,*) "   output   number for helicity"
            write(*,*) "   output   number for Ax component"
            write(*,*) "   output   number for Ay component"
            write(*,*) "   output   number for Az component"
            write(*,*) "   register number for workspace"
            write(*,*) "   register number for workspace"
            write(*,*) "   register number for workspace"
            stop
         elseif( temp_command .eq. 'arcsin' ) then
            write(*,*)"arcsin- Takes the arcsin of a register"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  arcsin"
            write(*,*) "   input register number"
            stop
         elseif( temp_command .eq. 'erase' ) then
            write(*,*)"erase- Removes a register"
            write(*,*) "Usage:"
            write(*,*) "  erase"
            write(*,*) "   input register number"
            stop
         elseif( temp_command .eq. 'abs' ) then
            write(*,*)"abs- Takes the absolute value of a register"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  abs"
            write(*,*) "   input register number"
         elseif( temp_command .eq. 'inverse' ) then
            write(*,*)"inverse- Takes the inverse of a register"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  inverse"
            write(*,*) "   input register number"
            stop
         elseif( temp_command .eq. 'square' ) then
            write(*,*)"square- Squares a register"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  square"
            write(*,*) "   input register number"
            stop
         elseif( temp_command .eq. 'sqrt' ) then
            write(*,*)"sqrt- Takes the squareroot of a register"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  sqrt"
            write(*,*) "   input register number"
            stop
         elseif( temp_command .eq. 'copy' ) then
            write(*,*)"copy- Copies a register to a new one."
            write(*,*) " "
            write(*,*) "Usage:"
            write(*,*) "  copy"
            write(*,*) "   input register number"
            write(*,*) "   output register number"
            stop
         elseif( temp_command .eq. 'multdv' ) then
            write(*,*)"multdv- Multiplies a register by the product"
            write(*,*)"          of its grid resolutions"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  multdv"
            write(*,*) "   input register number"
            stop
         elseif( temp_command .eq. 'r' ) then
            write(*,*)"r- Extracts the radius of each point"
            write(*,*)"          and sticks in a second register"
            write(*,*) "Usage:"
            write(*,*) "  r"
            write(*,*) "   input register number"
            write(*,*) "   output register number"
            stop
         elseif( temp_command .eq. 'add' ) then
            write(*,*)"add- Adds two registers"
            write(*,*)"          and sticks in a third"
            write(*,*) "Usage:"
            write(*,*) "  add"
            write(*,*) "   input register number"
            write(*,*) "   input register number"
            write(*,*) "   output register number"
            stop
         elseif( temp_command .eq. 'memstat' ) then
            write(*,*)"memstat- Output memory usage statistics"
            write(*,*) " "
            write(*,*) "Usage:"
            write(*,*) "  memstat"
            stop
         elseif( temp_command .eq. 'locmax' ) then
            write(*,*)"locmax- Take maximum at each point from"
            write(*,*)"          two fields defined there"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  locmax"
            write(*,*) "   input register number"
            write(*,*) "   input/output register number"
            stop
         elseif( temp_command .eq. 'multiply' ) then
            write(*,*)"multiply- Multiplies two registers"
            write(*,*)"          and sticks in a third"
            write(*,*) "Usage:"
            write(*,*) "  multiply"
            write(*,*) "   input register number"
            write(*,*) "   input register number"
            write(*,*) "   output register number"
            stop
         elseif( temp_command .eq. 'addconstant' ) then
            write(*,*)"addconstant- Adds a constant"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  addconstant"
            write(*,*) "   constant (real)"
            write(*,*) "   register number"
            stop
         elseif( temp_command .eq. 'multiplyconstant' ) then
            write(*,*)"multiplyconstant- Multiplies by constant"
            write(*,*)"                  In place."
            write(*,*) "Usage:"
            write(*,*) "  multiplyconstant"
            write(*,*) "   constant (real)"
            write(*,*) "   register number"
            stop
         elseif( temp_command .eq. 'loopproc' ) then
            write(*,*)"loopproc- For single proces execution,"
            write(*,*) "         loop over files 0...proc"
            write(*,*) "Usage:"
            write(*,*) "  loopproc"
            write(*,*) "   process number"
            stop
         elseif( temp_command .eq. 'floor' ) then
            write(*,*)"floor- Convert all values less than floor"
            write(*,*)"       to the floor value.               "
            write(*,*)"       In place.                         "
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  floor"
            write(*,*) "     floorvalue (real)"
            write(*,*) "     register number"
            stop
         elseif( temp_command .eq. 'unfloor' ) then
            write(*,*)"unfloor- Convert all values less or equal to "
            write(*,*)"       the floor value to 0."
            write(*,*)"       In place.                         "
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  unfloor"
            write(*,*) "     floorvalue (real)"
            write(*,*) "     register number"
            stop
         elseif( temp_command .eq. 'reglist' ) then
            write(*,*)"reglist- List all non-empty registers"
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  reglist"
            stop
         elseif( temp_command .eq. 'polar' ) then
            write(*,*)"polar-  Extract polar angle from "
            write(*,*)"         coords...ignore field value  "
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  polar"
            write(*,*) "     register number"
            write(*,*) "     target register"
            stop
         elseif( temp_command .eq. 'azimuth' ) then
            write(*,*)"azimuth- Extract azimuthal angle from "
            write(*,*)"         coords...ignore field value  "
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  azimuth"
            write(*,*) "     register number"
            write(*,*) "     target register"
            stop
         elseif( temp_command .eq. 'atpoint' ) then
            write(*,*)"atpoint- Print value at particular point"
            write(*,*) ""
            write(*,*) "Usage:"
            write(*,*) "  atpoint"
            write(*,*) "     x-value"
            write(*,*) "     y-value"
            write(*,*) "     z-value"
            write(*,*) "     register number"
            stop
         else
            write(*,*) "get_params_file: No help for command: ",
     *                  temp_command(1:20)
            stop
         endif
         stop
           !
         else
           !
           if(ltrace)write(*,*)'get_params_file: No help requested.'
           if(ltrace)write(*,*)'get_params_file: Returning name of file'
         endif
            
         
         return
      end 

      !!!!!!!!!!!!!!!!!!!
      !!  Params Read  !!
      !!!!!!!!!!!!!!!!!!!
      subroutine params_read( params_real, params_int, params_chr,
     *                        operations, file_name, params_file ) 
      implicit none
      !include 'mpif.h'
      include 'class_constants.inc'
      include 'variables.inc'

        character      file_name(MAX_OPERATIONS)*64
        !character*20   file_name(MAX_OPERATIONS)
        !character*(*)   file_name(MAX_OPERATIONS)
        character*64    tmpfname
        character*(*)   params_file
        character*128   params_chr
        real*8          params_real(MAX_OPERATIONS, num_params)
        integer         params_int(MAX_OPERATIONS, num_params)
        integer         operations(MAX_OPERATIONS)

        character*100   temp_op
        character       firstchar
        integer         p_file, i
        integer         op_counter
         logical        ltrace
         parameter    ( ltrace = .false. )
       
        p_file = 7  ! number for parameter file
        open(unit=p_file, file=params_file, status='old')
     
        if (ltrace) write(*,*) 'params_read: Starting:'
        if (ltrace) write(*,*) 'params_read: file_name(1):',file_name(1)
        op_counter = 0
        params_int(1, P_num_ops) = 0
        do while(op_counter .ne. -1) 
           op_counter = op_counter + 1
           if (op_counter .gt. MAX_OPERATIONS) then
              write(*,*) 'params_read:'
              write(*,*) 'params_read: Exceeded MAX_OPERATIONS:',
     .                            MAX_OPERATIONS
              write(*,*) 'params_read: Increase in variables.inc and'
              write(*,*) 'params_read: recompile.'
              write(*,*) 'params_read: Quitting here.'
              stop
           end if
           if (ltrace) write(*,*) 'params_read: op_counter =',op_counter
           read(p_file, *, END=10)  temp_op
           if (ltrace) write(*,*) 'params_read: temp_op =',temp_op(1:10)
           !
           if( temp_op .eq. 'gen' ) then
              operations(op_counter) = OP_GENERATE
               
              read(p_file, *)  file_name(op_counter)
              read(p_file, *)  params_int(op_counter, P_register)
              read(p_file, *)  params_int(op_counter, P_nx)
              read(p_file, *)  params_int(op_counter, P_ny)
              read(p_file, *)  params_int(op_counter, P_nz)
              read(p_file, *)  params_real(op_counter, P_x_length)
              read(p_file, *)  params_real(op_counter, P_y_length)
              read(p_file, *)  params_real(op_counter, P_z_length)
              read(p_file, *)  params_real(op_counter, P_time)
              read(p_file, *)  params_real(op_counter, P_max_time)
              read(p_file, *)  params_real(op_counter, P_dt)
              read(p_file, *)  params_real(op_counter, P_amplitude)

           elseif( temp_op .eq. 'output' ) then
              operations(op_counter) = OP_OUTPUT
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'quadrupole' ) then
              operations(op_counter) = OP_QUADRUPOLE
              read(p_file, *) params_int(op_counter, P_nx)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'amira' ) then
              operations(op_counter) = OP_AMIRA
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'read' ) then
              operations(op_counter) = OP_INPUT
              read(p_file, *) file_name(op_counter)
              read(p_file, *) params_int(op_counter, P_register)
            
           elseif( temp_op .eq. 'regdump' ) then
              operations(op_counter) = OP_REG_DUMP
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'rename' ) then
              operations(op_counter) = OP_RENAME
              read(p_file, *) file_name(op_counter)
              !read(p_file, '(A19)') tmpfname
              !file_name(op_counter) = tmpfname
              read(p_file, *) params_int(op_counter, P_register)
 
           elseif( temp_op .eq. 'maxhalfplane' ) then
              operations(op_counter) = OP_MAXHALFPLANE
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'max' ) then
              operations(op_counter) = OP_MAXIMUM
              read(p_file, *) params_int(op_counter, P_register)
              params_int(op_counter, P_absolute_value_flag) = 0

           elseif( temp_op .eq. 'max' ) then
              operations(op_counter) = OP_MAXIMUM
              read(p_file, *) params_int(op_counter, P_register)
              params_int(op_counter, P_absolute_value_flag) = 0

           elseif( temp_op .eq. 'maxabs'  ) then
              operations(op_counter) = OP_MAXABS
              read(p_file, *) params_int(op_counter, P_register)
              params_int(op_counter, P_absolute_value_flag) = 1

           elseif( temp_op .eq. 'minimum' .or.
     *             temp_op .eq. 'min'  ) then
              operations(op_counter) = OP_MINIMUM
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'mask' ) then
              operations(op_counter) = OP_MASK
              read(p_file, *) params_real(op_counter, P_x_length)
              read(p_file, *) params_real(op_counter, P_y_length)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'mask2' ) then
              operations(op_counter) = OP_MASK2
              read(p_file, *) params_real(op_counter, P_x_length)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'atpoint' ) then
              operations(op_counter) = OP_ATPOINT
              read(p_file, *) params_real(op_counter, P_x_length)
              read(p_file, *) params_real(op_counter, P_y_length)
              read(p_file, *) params_real(op_counter, P_z_length)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'azimuth' ) then
              operations(op_counter) = OP_EXTRACTAZI
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'polar' ) then
              operations(op_counter) = OP_EXTRACTPOLAR
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'filterl' ) then
              operations(op_counter) = OP_FILTERl
              read(p_file, *) params_int(op_counter, P_level)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'filterh' ) then
              operations(op_counter) = OP_FILTERH
              read(p_file, *) params_real(op_counter, P_resolution)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'filtert' ) then
              operations(op_counter) = OP_FILTERT
              read(p_file,'(A)') params_chr
              !read(p_file,'(A)') params_chr(op_counter, 1)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'filter' ) then
              operations(op_counter) = OP_FILTER
              params_real(op_counter, P_resolution) = 0.d0
              read(p_file, *) params_int(op_counter, P_reg_target)
              read(p_file, *) params_real(op_counter, P_time)
              read(p_file, *) params_real(op_counter, P_resolution)
              read(p_file, *) file_name(op_counter)
              read(p_file, *) params_int(op_counter, P_register)
 
           elseif( temp_op .eq. 'uni_merge' ) then
              operations(op_counter) = OP_UNI_MERGE
              read(p_file, *) params_int(op_counter, P_reg_target)
              read(p_file, *) file_name(op_counter)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'interptogrid' ) then
              operations(op_counter) = OP_INTERPTOGRID
              read(p_file, *) params_int(op_counter, P_reg_target)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'genspheregrid' ) then
              operations(op_counter) = OP_GENSPHEREGRID
              read(p_file, *) params_real(op_counter, P_time)
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target)

           elseif( temp_op .eq. 'restrict' ) then
              operations(op_counter) = OP_RESTRICT
              read(p_file, *) params_int(op_counter, P_reg_target)
              read(p_file, *) params_int(op_counter, P_register)
              read(p_file, *) file_name(op_counter)

           elseif( temp_op .eq. 'subtractgen' ) then
              operations(op_counter) = OP_SUBTRACTGEN
              read(p_file, *) params_int(op_counter, P_register)
              read(p_file, *) params_int(op_counter, P_reg_target)
              read(p_file, *) params_int(op_counter, P_reg_target2)

           elseif( temp_op .eq. 'subtract' ) then
              operations(op_counter) = OP_SUBTRACT
              read(p_file, *) params_int(op_counter, P_register)
              read(p_file, *) params_int(op_counter, P_reg_target)

           elseif( temp_op .eq. 'subtractuno' ) then
              operations(op_counter) = OP_SUBTRACTUNO
              read(p_file, *) params_int(op_counter, P_register)
              read(p_file, *) params_int(op_counter, P_reg_target)

           elseif( temp_op .eq. 'coarsen' ) then
              operations(op_counter) = OP_COARSEN
              read(p_file, *) params_int(op_counter, P_n)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'integrate' ) then
              operations(op_counter) = OP_INTEGRATE
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'times' ) then
              operations(op_counter) = OP_TIMES
              read(p_file, *) params_int(op_counter, P_register)
              read(p_file, *) params_int(op_counter, P_n)

           elseif( temp_op .eq. 'divide' ) then
              operations(op_counter) = OP_DIVIDE
              read(p_file, *) params_int(op_counter, P_register)
              read(p_file, *) params_int(op_counter, P_register2  )

           elseif( temp_op .eq. 'l2norm' ) then
              operations(op_counter) = OP_L2NORM
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'readone' ) then
              operations(op_counter) = OP_READONE
              read(p_file, *) file_name(op_counter)
              read(p_file, *) params_int(op_counter, P_register)

           elseif( temp_op .eq. 'curl' ) then
              operations(op_counter) = OP_CURL
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'grad' ) then
              operations(op_counter) = OP_GRAD
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'div' ) then
              operations(op_counter) = OP_DIV
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'cross' ) then
              operations(op_counter) = OP_CROSS
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'dot' ) then
              operations(op_counter) = OP_DOT
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'vecmag' ) then
              operations(op_counter) = OP_VECMAG
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )

           elseif( temp_op .eq. 'helicity' ) then
              operations(op_counter) = OP_HELICITY
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)
              read(p_file, *) params_int(op_counter, P_nx)
              read(p_file, *) params_int(op_counter, P_ny)
              read(p_file, *) params_int(op_counter, P_nz)
              read(p_file, *) params_int(op_counter, P_n)

           elseif( temp_op .eq. 'cart2sphere' ) then
              operations(op_counter) = OP_CART2SPHERE
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'copy' ) then
              operations(op_counter) = OP_COPY
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_reg_target )

           elseif( temp_op .eq. 'loopproc' ) then
              operations(op_counter) = OP_LOOPPROC
              read(p_file, *) params_int(op_counter, P_register2  )

           elseif( temp_op .eq. 'multdv' ) then
              operations(op_counter) = OP_MULTDV
              read(p_file, *) params_int(op_counter, P_register   )
              if (ltrace) write(*,*)'params_read:',op_counter,
     .                        params_int(op_counter,P_register)

           elseif( temp_op .eq. 'multiply' ) then
              operations(op_counter) = OP_MULTIPLY
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )

           elseif( temp_op .eq. 'multiplyconstant' ) then
              operations(op_counter) = OP_MULTIPLYCONSTANT
              read(p_file, *) params_real(op_counter, P_resolution)
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'sqrt' ) then
              operations(op_counter) = OP_SQRT
              read(p_file, *) params_int(op_counter, P_register   )

           elseif( temp_op .eq. 'arcsin' ) then
              operations(op_counter) = OP_ARCSIN
              read(p_file, *) params_int(op_counter, P_register   )

           elseif( temp_op .eq. 'add' ) then
              operations(op_counter) = OP_ADD
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )

           elseif( temp_op .eq. 'addconstant' ) then
              operations(op_counter) = OP_ADDCONSTANT
              read(p_file, *) params_real(op_counter, P_resolution)
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'r' ) then
              operations(op_counter) = OP_R
              read(p_file, *) params_int(op_counter,  P_register  )
              read(p_file, *) params_int(op_counter, P_register2  )

           elseif( temp_op .eq. 'square' ) then
              operations(op_counter) = OP_SQUARE
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'inverse' ) then
              operations(op_counter) = OP_INVERSE
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'erase' ) then
              operations(op_counter) = OP_ERASE
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'memstat' ) then
              operations(op_counter) = OP_MEMSTAT

           elseif( temp_op .eq. 'reglist' ) then
              operations(op_counter) = OP_REGLIST

           elseif( temp_op .eq. 'floor' ) then
              operations(op_counter) = OP_FLOOR
              read(p_file, *) params_real(op_counter, P_time      )
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'unfloor' ) then
              operations(op_counter) = OP_UNFLOOR
              read(p_file, *) params_real(op_counter, P_time      )
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'histo' ) then
              operations(op_counter) = OP_HISTO
              read(p_file, *) params_int(op_counter,  P_register2 )
              read(p_file, *) params_real(op_counter, P_time      )
              read(p_file, *) params_real(op_counter, P_resolution)
              read(p_file, *) params_int(op_counter,  P_register  )
              read(p_file, *) params_int(op_counter,  P_reg_target3)

           elseif( temp_op .eq. 'locmax' ) then
              operations(op_counter) = OP_LOCMAX
              read(p_file, *) params_int(op_counter,  P_register  )
              read(p_file, *) params_int(op_counter,  P_register2 )

           elseif( temp_op .eq. 'abs' ) then
              operations(op_counter) = OP_ABS
              read(p_file, *) params_int(op_counter,  P_register  )

           elseif( temp_op .eq. 'uncurl' ) then
              operations(op_counter) = OP_UNCURL
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)

           elseif( temp_op .eq. 'flines' ) then
              operations(op_counter) = OP_FLINES
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
              read(p_file, *) params_int(op_counter, P_register3  )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)
              read(p_file, *) params_int(op_counter, P_n)
    
           elseif( temp_op .eq. 'interpline' ) then
              operations(op_counter) = OP_INTERPLINE
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)
              read(p_file, *) params_int(op_counter, P_register2  )
    
           elseif( temp_op .eq. 'setprefaxis' ) then
              operations(op_counter) = OP_SETPREFAXIS
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_register2  )
    
           elseif( temp_op .eq. 'radvector' ) then
              operations(op_counter) = OP_RADVECTOR
              read(p_file, *) params_int(op_counter, P_register   )
              read(p_file, *) params_int(op_counter, P_reg_target )
              read(p_file, *) params_int(op_counter, P_reg_target2)
              read(p_file, *) params_int(op_counter, P_reg_target3)
    
           elseif( temp_op .eq. 'newcenter' ) then
              operations(op_counter) = OP_NEWCENTER
              read(p_file, *) params_int(op_counter, P_register   )
    
           else
              ! See if we should ignore or complain:
              firstchar = temp_op(1:1)
              if(ltrace)write(*,*)'params_read: First char:',firstchar
              if (firstchar .eq. '#') then
                 if(ltrace)write(*,*)'params_read:Ignoring as comment ',
     .                op_counter, temp_op(1:8)
                 op_counter = op_counter - 1
                 goto 99
              else
                 ! Complain bitterly:
                 write(*,*)'params_read:  * * * * *'
                 write(*,*)'params_read: Unrecognized operation: ',
     .                  temp_op(1:25)
                 write(*,*)'params_read: This going to really mess',
     .                  ' the parsing of the rest of your script.'
                 write(*,*)'params_read:  * * * * *'
              end if
           endif
           !
           ! Check validity of user options:
           !
           if (params_int(op_counter,P_register).gt. MAX_NUM_REG) then
              write(*,*)'params_read: Register cannot be greater than ',
     *                  MAX_NUM_REG
              write(*,*)'params_read: Ignoring operation ',temp_op
              op_counter = op_counter - 1
           else
              params_int(1, P_num_ops) = params_int(1, P_num_ops) + 1
!             write(*,*) 'params_read: ',op_counter,
!    .                 operations(op_counter),
!    .                params_int(op_counter,P_register)
           end if
 99        continue
           if(ltrace)write(*,*)'params-read: C op_counter =',op_counter
        enddo

10      close(p_file)
        if (ltrace) write(*,*) 'params-read: op_counter =',op_counter

        if (ltrace) then
           do i = 1, op_counter
              write(*,*) 'params_read: ',i,operations(i),
     .                                      params_int(i,P_register),
     .                                      params_int(i,P_level)
           end do
        end if

        if (ltrace) write(*,*) 'params-read: done.'
        return
      end


      !!!!!!!!!!!!!!!!!!!!!!!!
      !!  Params Broadcast  !!
      !!!!!!!!!!!!!!!!!!!!!!!!
      subroutine params_bcast( params_real, params_integer, params_chr,
     a                         operations,  file_name      )
      implicit none
      include 'mpif.h'
      include 'variables.inc'
      include 'mympi.inc'
      include 'class_constants.inc'

        integer         params_integer(MAX_OPERATIONS, num_params)
        real*8          params_real(MAX_OPERATIONS, num_params)
        character*128   params_chr
        integer         operations(MAX_OPERATIONS)
        character*(*)   file_name

        call MPI_BCAST( file_name, 100, MPI_CHARACTER, master,
     *                  MPI_COMM_WORLD,       ierr ) 

        call MPI_BCAST( params_real, MAX_OPERATIONS * num_params, 
     *                  MPI_DOUBLE_PRECISION,
     *                  master, MPI_COMM_WORLD, ierr ) 
         
        call MPI_BCAST( params_integer, MAX_OPERATIONS * num_params, 
     *                  MPI_INTEGER, 
     *                  master, MPI_COMM_WORLD, ierr ) 

        call MPI_BCAST( params_chr, 128,
     *                  MPI_CHARACTER, 
     *                  master, MPI_COMM_WORLD, ierr ) 

        call MPI_BCAST( operations, MAX_OPERATIONS, MPI_INTEGER, 
     a                  master, MPI_COMM_WORLD, ierr )  
    
        return
      end 


      !!!!!!!!!!!!!!!!!!!!!!!!!
      !!  Execute Operation  !!
      !!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine execute_op(operation, params_real, params_int, 
     *                      params_chr, file_name, counter, iter,
     *                      process, returncode)
      implicit none

      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      include 'variables.inc'
      include 'class_constants.inc'

         integer        operation, iter, returncode, process
         integer        params_int(MAX_OPERATIONS, num_params)
         real*8         params_real(MAX_OPERATIONS, num_params)
         character*128  params_chr
         character*(*)  file_name
         integer        counter

         character*2    my_rank_string
         character*4    str_myid
         integer        step
         integer        i, j

         real*8         old_time

         real*8         bbox(6)
         integer        gft_shape(3)
         integer        rank
         integer        gft_rc
        
         !Function
         integer        gft_read_shape
         integer        gft_read_rank
         integer        gft_out_bbox
         integer        gft_output_sdf 

         logical        ltrace
         parameter    ( ltrace = .false. )
        
         ! Successful until shown not to be:
         returncode = 1

         if (ltrace) then
            write(*,*)'execute_op: operation = ',operation
         end if
         
         !
         ! Command: Generate
         !
         if(operation .eq. OP_GENERATE) then
c          call get_bbox( bbox, params_int, params_real, 
c    *                    counter )

           !Setup gft_shape
           gft_shape(1) = params_int(counter, P_nx)
           gft_shape(2) = params_int(counter, P_ny)
           gft_shape(3) = params_int(counter, P_nz)
           !
           bbox(1) = params_real(counter, P_x_length)*(-0.5)
           bbox(2) = params_real(counter, P_x_length)*(+0.5)
           bbox(3) = params_real(counter, P_y_length)*(-0.5)
           bbox(4) = params_real(counter, P_y_length)*(+0.5)
           bbox(5) = params_real(counter, P_z_length)*(-0.5)
           bbox(6) = params_real(counter, P_z_length)*(+0.5)
         
           if(ltrace) then
          write(*,*) "exe_op: filename:", 
     .       params_int(counter, P_register), file_name
          write(*,*) "exe_op: id: ",myid," gft_shape(1) = ",gft_shape(1)
          write(*,*) "exe_op: id: ",myid," gft_shape(2) = ",gft_shape(2)
          write(*,*) "exe_op: id: ",myid," gft_shape(3) = ",gft_shape(3)
           endif

           !Setup register and create gaussian:

           call register_generate( params_int(counter, P_register), 
     *                             file_name,
     *                             bbox, 
     *                             gft_shape, 
     *                             params_real(counter, P_time), 
     *                             params_real(counter, P_dt),
     *                             params_real(counter, P_max_time),
     *                             params_real(counter, P_amplitude) )

         !
         ! Command: Max on halfplane
         !
         elseif ( operation .eq. OP_MAXHALFPLANE ) then
            call register_maxhalfplane(
     *                             params_int(counter, P_register   ),
     *                             params_int(counter, P_reg_target ),
     *                             params_int(counter, P_reg_target2) )

         !
         ! Command: extract azimuth
         !
         elseif ( operation .eq. OP_EXTRACTAZI ) then
            call register_azimuth(
     *                             params_int(counter, P_register   ),
     *                             params_int(counter, P_reg_target ) )

         !
         ! Command: extract polar  
         !
         elseif ( operation .eq. OP_EXTRACTPOLAR ) then
            call register_polar(
     *                             params_int(counter, P_register   ),
     *                             params_int(counter, P_reg_target ) )

         !
         ! Command: Maximum
         !
         elseif ( operation .eq. OP_MAXIMUM ) then
            call register_maximum( params_int(counter, P_register))
  
         !
         ! Command: Max Absolute
         !
         elseif ( operation .eq. OP_MAXABS ) then
            call register_maxabs( params_int(counter, P_register)) 

         !
         ! Command: Minimum
         !
         elseif ( operation .eq. OP_MINIMUM ) then
            call register_minimum( params_int(counter, P_register) )
         !
         ! Command: 
         ! 
         elseif( operation .eq. OP_INTERPTOGRID ) then
           call register_interptogrid(
     *                             params_int( counter, P_reg_target ), 
     *                             params_int( counter, P_register   ) )
         !
         ! Command: 
         ! 
         elseif( operation .eq. OP_GENSPHEREGRID ) then
           call register_genspheregrid(
     *                             params_real(counter, P_time), 
     *                             params_int( counter, P_register   ),
     *                             params_int( counter, P_register2  ),
     *                             params_int( counter, P_register3  ),
     *                             params_int( counter, P_reg_target ) )
         !
         ! Command: Input
         ! 
         elseif( operation .eq. OP_INPUT ) then
           call register_readsdf( params_int( counter, P_register ), 
     *                             file_name )
         !
         ! Command: Readone
         ! 
         elseif( operation .eq. OP_READONE ) then
           !write(*,*) 'execute_op: OP_READONE'
           !write(*,*) 'execute_op: process = ',process
           call register_readonesdf( params_int( counter, P_register ), 
     *                             file_name, iter, process,
     .                             returncode)
         !
         ! Command: Output
         !
         elseif( operation .eq. OP_OUTPUT ) then
            call register_output( params_int( counter, P_register) )
         !
         ! Command: Quadrupole
         !
         elseif( operation .eq. OP_QUADRUPOLE ) then
            call register_quadrupole( 
     .                           params_int( counter, P_nx), 
     .                           params_int( counter, P_register) )
         !
         ! Command: Amira 
         !
         elseif( operation .eq. OP_AMIRA ) then
            call register_amira( params_int( counter, P_register) )

         !
         ! Command: Register Dump
         !
         elseif( operation .eq. OP_REG_DUMP ) then
            if (myid.eq.master)
     *         call reg_dump_info( params_int( counter, P_register) )

         !
         ! Command: Rename
         !
         elseif( operation .eq. OP_RENAME ) then
            call register_rename( params_int( counter, P_register ),
     *                            file_name ) 

         ! 
         ! Command: Filtert
         !
         elseif( operation .eq. OP_FILTERT ) then
            call register_filtert( params_int(counter, P_register),
     *                            params_chr)

         !
         ! Command: Mask
         !
         elseif( operation .eq. OP_MASK ) then
            call register_mask( params_real(counter, P_x_length),
     *                          params_real(counter, P_y_length),
     *                          params_int( counter, P_register)  )

         !
         ! Command: AtPoint
         !
         elseif( operation .eq. OP_ATPOINT ) then
            call register_atpoint( params_real(counter, P_x_length),
     *                             params_real(counter, P_y_length),
     *                             params_real(counter, P_z_length),
     *                             params_int( counter, P_register)  )

         !
         ! Command: Mask2
         !
         elseif( operation .eq. OP_MASK2 ) then
            call register_mask2( params_real(counter, P_x_length),
     *                           params_int( counter, P_register)  )

         !
         ! Command: Filterl
         !
         elseif( operation .eq. OP_FILTERL ) then
            call register_filterl( params_int(counter, P_register),
     *                             params_int(counter, P_level))

         !
         ! Command: Filterh
         !
         elseif( operation .eq. OP_FILTERH ) then
            call register_filterh( params_int(counter, P_register),
     *                            params_real(counter, P_resolution))

         ! 
         ! Command: Filter
         !
         elseif( operation .eq. OP_FILTER ) then
            call register_filter( params_int(counter, P_reg_target),
     *                            params_int(counter, P_register),
     *                            params_real(counter, P_time),
     *                            params_real(counter, P_resolution),
     *                            file_name )

         !
         ! Command: Unigrid Merge
         !
         elseif( operation .eq. OP_UNI_MERGE ) then
           call register_unigrid_merge(params_int(counter,P_reg_target),
     *                                file_name,
     *                                params_int(counter, P_register)  )
 
         !
         ! Command: Subtractgen
         ! 
         elseif( operation .eq. OP_SUBTRACTGEN ) then
            call register_subtractgen(params_int(counter, P_register), 
     *                               params_int(counter, P_reg_target),
     *                               params_int(counter, P_reg_target2))
 
         !
         ! Command: Subtract
         ! 
         elseif( operation .eq. OP_SUBTRACT ) then
            call register_subtract( params_int(counter, P_register), 
     *                              params_int(counter, P_reg_target))
 
         !
         ! Command: Subtract Single
         ! 
         elseif( operation .eq. OP_SUBTRACTUNO ) then
            call register_subtractuno(params_int(counter, P_register), 
     *                                params_int(counter, P_reg_target))

         !
         ! Command: Coarsen
         ! 
         elseif( operation .eq. OP_COARSEN  ) then
            call register_coarsen( params_int(counter, P_n),
     *                              params_int(counter, P_register) )

         !
         ! Command: Restrict
         !
         elseif( operation .eq. OP_RESTRICT ) then
            call register_restrict( params_int(counter, P_reg_target),
     *                              params_int(counter, P_register),
     *                              file_name ) 
         
         ! 
         ! Command: Integrate
         !
         elseif( operation .eq. OP_INTEGRATE ) then
            call register_integrate( params_int(counter, P_register) ) 

         ! 
         ! Command: Times
         !
         elseif( operation .eq. OP_TIMES ) then
            call register_times( params_int(counter, P_register),
     *                           params_int(counter, P_n)  ) 

         ! 
         ! Command: Divide
         !
         elseif( operation .eq. OP_DIVIDE ) then
            call register_divide( params_int(counter, P_register),
     *                            params_int(counter, P_register2)  ) 

         !
         ! Command: L2norm
         !
         elseif( operation .eq. OP_L2NORM   ) then
            call register_l2norm( params_int(counter, P_register))

         !
         ! Command: curl
         !
         elseif( operation .eq. OP_CURL     ) then
            call register_curl( params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )

         !
         ! Command: uncurl
         !
         elseif( operation .eq. OP_UNCURL   ) then
            call register_uncurl(params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )

         !
         ! Command: flines      
         !
         elseif( operation .eq. OP_FLINES  ) then
            call register_FLINES(    params_int(counter, P_register ),
     *                               params_int(counter, P_register2),
     *                               params_int(counter, P_register3),
     *                               params_int(counter, P_reg_target ),
     *                               params_int(counter, P_reg_target2),
     *                               params_int(counter, P_reg_target3),
     *                               params_int(counter, P_n)          )

         !
         ! Command: interpline
         !
         elseif( operation .eq. OP_INTERPLINE   ) then
            call register_interpline(params_int(counter, P_register),
     *                               params_int(counter, P_reg_target ),
     *                               params_int(counter, P_reg_target2),
     *                               params_int(counter, P_reg_target3),
     *                               params_int(counter, P_register2)  )

         !
         ! Command: setprefaxis
         !
         elseif( operation .eq. OP_SETPREFAXIS  ) then
            call register_setprefaxis(params_int(counter, P_register2),
     *                                params_int(counter, P_register) )

         !
         ! Command: div
         !
         elseif( operation .eq. OP_DIV      ) then
            call register_div(  params_int(counter, P_register   ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )

         !
         ! Command: grad
         !
         elseif( operation .eq. OP_GRAD     ) then
            call register_grad( params_int(counter, P_register   ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )

         !
         ! Command: cross
         !
         elseif( operation .eq. OP_CROSS    ) then
            call register_cross(params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )

         !
         ! Command: dot
         !
         elseif( operation .eq. OP_DOT    ) then
            call register_dot(  params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )

         !
         ! Command: vecmag
         !
         elseif( operation .eq. OP_VECMAG ) then
            call register_vecmag(params_int(counter, P_register   ),
     *                           params_int(counter, P_register2  ),
     *                           params_int(counter, P_register3  ),
     *                           params_int(counter, P_reg_target ) )


         !
         ! Command: helicity
         !
         elseif( operation .eq. OP_HELICITY ) then
            call register_helicity(params_int(counter, P_register ),
     *                           params_int(counter, P_register2  ),
     *                           params_int(counter, P_register3  ),
     *                           params_int(counter, P_reg_target ),
     *                           params_int(counter, P_reg_target2),
     *                           params_int(counter, P_reg_target3),
     *                           params_int(counter, P_nx),     
     *                           params_int(counter, P_ny),     
     *                           params_int(counter, P_nz),     
     *                           params_int(counter, P_n)        )

         !
         ! Command: cart2sphere
         !
         elseif( operation .eq. OP_CART2SPHERE ) then
            !write(*,*)' calling register_cart2sphere:'
            call register_cart2sphere(
     *                          params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ),
     *                          params_int(counter, P_reg_target ),
     *                          params_int(counter, P_reg_target2),
     *                          params_int(counter, P_reg_target3) )
            !write(*,*)' Done calling register_cart2sphere'

         !
         ! Command: copy
         !
         elseif( operation .eq. OP_COPY ) then
            call register_copy(
     *                          params_int(counter, P_register   ),
     *                          params_int(counter, P_reg_target ) )

         !
         ! Command: multdv
         !
         elseif( operation .eq. OP_MULTDV ) then
!           write(*,*) 'execute_op: calling register_multDV: ',counter,
!    *                          params_int(counter, P_register   )
            call register_multDV(
     *                          params_int(counter, P_register   ))
         !
         ! Command: multiply
         !
         elseif( operation .eq. OP_MULTIPLY ) then
!           write(*,*)'execute_op: call register_multiply',
!    *                          params_int(counter, P_register   ),
!    *                          params_int(counter, P_register2  ),
!    *                          params_int(counter, P_register3  )
            call register_multiply(
     *                          params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ))

         !
         ! Command: multiplyconstant
         !
         elseif( operation .eq. OP_MULTIPLYCONSTANT ) then
!           write(*,*)'execute_op call register_multconst',
!    *                          params_real(counter, P_resolution),
!    *                          params_int(counter, P_register   )
            call register_multconstant(
     *                          params_real(counter, P_resolution),
     *                          params_int(counter, P_register   ) )

         !
         ! Command: erase
         !
         elseif( operation .eq. OP_ERASE ) then
            call register_free(params_int(counter,P_register) )

         !
         ! Command: add
         !
         elseif( operation .eq. OP_ADD ) then
            call register_add(
     *                          params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ),
     *                          params_int(counter, P_register3  ))

         !
         ! Command: addconstant
         !
         elseif( operation .eq. OP_ADDCONSTANT ) then
            call register_addconstant(
     *                          params_real(counter, P_resolution),
     *                          params_int(counter, P_register   ) )

         !
         ! Command: sqrt
         !
         elseif( operation .eq. OP_SQRT ) then
            call register_sqrt(
     *                          params_int(counter, P_register   ))

         !
         ! Command: r
         !
         elseif( operation .eq. OP_R ) then
!           write(*,*)'execute_op call register_r',
!    *                          params_int(counter, P_register   ),
!    *                          params_int(counter, P_register2  )
            call register_r(
     *                          params_int(counter, P_register   ),
     *                          params_int(counter, P_register2  ))
         !
         ! Command: inverse
         !
         elseif( operation .eq. OP_INVERSE ) then
            call register_inverse( params_int(counter, P_register   ))

         !
         ! Command: square
         !
         elseif( operation .eq. OP_SQUARE ) then
            call register_square( params_int(counter, P_register   ))

         !
         ! Command: arcsin
         !
         elseif( operation .eq. OP_ARCSIN ) then
            call register_arcsin( params_int(counter, P_register   ))

         !
         ! Command: abs
         !
         elseif( operation .eq. OP_ABS ) then
            call register_abs( params_int(counter, P_register   ))

         !
         ! Command: memstat
         !
         elseif( operation .eq. OP_MEMSTAT ) then
            call mem_stat()

         !
         ! Command: locmax
         !
         elseif( operation .eq. OP_ABS ) then
            call register_locmax( params_int(counter, P_register   ))

         !
         ! Command: reglist
         !
         elseif( operation .eq. OP_REGLIST ) then
            call register_list()
         !
         ! Command: unfloor
         !
         elseif( operation .eq. OP_UNFLOOR ) then
            call register_unfloor(params_real(counter, P_time),
     *                            params_int( counter, P_register   ))
         !
         ! Command:floor
         !
         elseif( operation .eq. OP_FLOOR ) then
            call register_floor(  params_real(counter, P_time),
     *                            params_int( counter, P_register   ))

         !
         ! Command: histo
         !
         elseif( operation .eq. OP_HISTO ) then
            call register_histo( params_int( counter, P_register2  ),
     *                           params_real(counter, P_time),
     *                           params_real(counter, P_resolution),
     *                           params_int( counter, P_register   ),
     *                           params_int( counter, P_reg_target3)   )
         !
         ! Command: radvector
         !
         elseif( operation .eq. OP_RADVECTOR ) then
            call register_radvector(params_int( counter, P_register  ),
     *                           params_int(counter, P_reg_target ),
     *                           params_int(counter, P_reg_target2),
     *                           params_int(counter, P_reg_target3)   )

         !
         ! Command: newcenter
         !
         elseif( operation .eq. OP_NEWCENTER ) then
            call register_newcenter(params_int( counter, P_register  ))

         endif 

         if (ltrace) write(*,*) 'exe_op: returncode = ',returncode

         return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc int2str:                                                                   cc
cc           Converts a non-negative integer to a string.                     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine int2str( myint, mystring )
      implicit none
      character*(*) mystring
      integer       myint
      integer       tmp, numdigits, the_int, poweroften, digit
      character     dig2char
      external      dig2char

      mystring = ''
      if (myint .lt. 0) then
         write(*,*) 'int2str: Cannot convert negative integer.'
         return
      else if (myint .lt. 10) then
         mystring = dig2char(myint)
         return
      end if

      tmp       = 10
      numdigits = 1
 10   if (myint .ge. tmp) then
         tmp       = 10 * tmp
         numdigits = numdigits + 1
         goto 10
      end if

      tmp               = 1
      the_int           = myint
      poweroften        = 10**(numdigits-1)

 20   digit             = the_int / poweroften
      mystring(tmp:tmp) = dig2char(digit)
      tmp               = tmp + 1
      the_int           = the_int - digit * poweroften
      poweroften        =  poweroften / 10
      if (tmp.le.numdigits) goto 20

      mystring(tmp:tmp) = ''

      return
      end        ! END: subroutine int2str


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc dig2char:                                                                  cc
cc           Converts a single digit [0..9] to a character.                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      character function dig2char( digit )
      implicit none
      integer       digit

      if      (digit .eq. 0) then
              dig2char = '0'
      else if (digit .eq. 1) then
              dig2char = '1'
      else if (digit .eq. 2) then
              dig2char = '2'
      else if (digit .eq. 3) then
              dig2char = '3'
      else if (digit .eq. 4) then
              dig2char = '4'
      else if (digit .eq. 5) then
              dig2char = '5'
      else if (digit .eq. 6) then
              dig2char = '6'
      else if (digit .eq. 7) then
              dig2char = '7'
      else if (digit .eq. 8) then
              dig2char = '8'
      else if (digit .eq. 9) then
              dig2char = '9'
      end if

      return
      end        ! END: function dig2char

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mem_stat()
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      include 'variables.inc'
  
        logical ltrace
  
        parameter ( ltrace = .false. )

           !write(*,80) myid, 'my_malloc: --------------'
           !write(*,99) myid, "my_malloc: mem       = ", mem
           !write(*,99) myid, "my_malloc: arena     = ", arena
        write(*,77) myid, 'my_malloc: ',mem,' / ',arena, '   ',
     .                      100.*mem/arena,'%'

77      format('[',I3,'] ',A,I10,A,I12,A,F7.2,A)
80      format('[',I3,'] ',A)
99      format('[',I3,'] ',A,I10)
 
      return
      end      ! END: mem_stat


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function my_malloc(len)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      include 'variables.inc'
  
        integer len
        logical ltrace
  
        parameter ( ltrace = .false. )

       
        if(mem+len+1 .gt. arena) then
           write(*,80) myid, 'my_malloc: --------------'
           write(*,99) myid, 'my_malloc: my_malloc = ', my_malloc
           write(*,99) myid, "my_malloc: mem       = ", mem
           write(*,99) myid, "my_malloc: len       = ", len
           write(*,99) myid, "my_malloc: arena     = ", arena
           write(*,80) myid, 'my_malloc: --------------'
           write(*,80) myid, "my_malloc: Insufficient memory in malloc"
           write(*,80) myid, "           closing SDFtool. . ."
           stop
        endif

        my_malloc = mem + 1
        mem       = mem + len

        if (ltrace) then
           write(*,80) myid, 'my_malloc: --------------'
           write(*,99) myid, 'my_malloc: my_malloc = ', my_malloc
           write(*,99) myid, "my_malloc: mem       = ", mem
           write(*,99) myid, "my_malloc: len       = ", len
           write(*,99) myid, "my_malloc: arena     = ", arena
        end if

80      format('[',I3,'] ',A)
99      format('[',I3,'] ',A,I10)
 
      return
      end      ! END: my_malloc

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine my_free(ptr, num)
      implicit none
      integer ptr, num
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      include 'variables.inc'
  
      return
      end      ! END: my_free

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine erase_allregisters()
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'mem.inc'
      !include 'grids.inc'
      !include 'levels.inc'
      !include 'trees.inc'
      !include 'registers.inc'
      !include 'variables.inc'
      include 'class_constants.inc'
      integer i
  
      logical     ltrace
      parameter ( ltrace = .false. )

      !do i = 1, MAX_NUM_REG
      do i = MAX_NUM_REG, 1, -1
         call register_free(i)
      end do

      !
      ! A little scary, but let us
      ! re-set memory so that it's all free:
      !
      if (ltrace) write(*,*)myid,' erase_allregisters: Resetting memory'
      mem              = 0
      !grid_counter     = 0
      !level_counter    = 0
      !tree_counter     = 0
      !register_counter = 0

      call grid_init()
      call level_init()
      call tree_init()
      call reg_init()

      return
      end      ! END: erase_allregisters


      subroutine vector_copy( vector1, vector2, nx )
      implicit none
         integer nx
         real*8  vector1(nx)
         real*8  vector2(nx)

         integer i

         do i = 1, nx
            vector2(i) = vector1(i)
         enddo

         return
      end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  double_equal:                                                             cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function double_equal( first, second )
      implicit    none
      real(kind=8)      first, second
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-12)

      double_equal = abs(first-second) .lt. SMALLNUMBER

      return
      end    ! END: double_equal

 
      logical function my_double_equal( first, second, factor )
      implicit none
      real*8     first, second, factor
     
         my_double_equal = abs(first-second) .lt. factor

         return
      end

      subroutine trans_array_setup( array, num_grids, i, gi )
      implicit none
         real*8  array(9,num_grids)
         !real*8  array(num_grids, 9)
         integer num_grids, gi, i

         integer rank
         real*8  bbox(6), time, h
         logical ltrace
         parameter (ltrace = .false.)

         ! Functions
         integer grid_return_rank
         real*8  grid_return_time
         real*8  grid_return_resolution

         time     = grid_return_time(gi)
         rank     = grid_return_rank(gi)
         h        = grid_return_resolution(gi)
         call       grid_return_bbox(gi, bbox)
         if(ltrace) then
          write(*,*) "TRANS SETUP, gi=",gi
          write(*,*) "rank    = ", rank
          write(*,*) "bbox(1) = ", bbox(1)
          write(*,*) "bbox(2) = ", bbox(2)
          write(*,*) "bbox(3) = ", bbox(3)
          write(*,*) "bbox(4) = ", bbox(4)
          write(*,*) "bbox(5) = ", bbox(5)
          write(*,*) "bbox(6) = ", bbox(6)
          write(*,*) "time    = ", time
          write(*,*) "h       = ", h
         endif
         !array(i, 1) = 1.d0*rank
         !array(i, 2) = bbox(1)
         !array(i, 3) = bbox(2)
         !array(i, 4) = bbox(3)
         !array(i, 5) = bbox(4)
         !array(i, 6) = bbox(5)
         !array(i, 7) = bbox(6)
         !array(i, 8) = time
         !array(i, 9) = h
         array(1,i) = 1.d0*rank
         array(2,i) = bbox(1)
         array(3,i) = bbox(2)
         array(4,i) = bbox(3)
         array(5,i) = bbox(4)
         array(6,i) = bbox(5)
         array(7,i) = bbox(6)
         array(8,i) = time
         array(9,i) = h
         return
      end          ! END: trans_array_setup
      
      subroutine trans_array_return( array, rank, bbox, sdf_time, h, 
     *                               gi, ng)
      implicit none
      !include 'variables.inc'
         integer rank, gi, ng
         real*8  array(9,ng)
         !real*8  array(ng, 9)
         real*8  bbox(6), sdf_time, h
         
         logical ltrace2
         parameter (ltrace2 = .false.)

       
         if(ltrace2) write(*,*) "Entering trans_array_return..."
          
         !rank    = NINT(array(gi, 1))
         !bbox(1) = array(gi, 2)
         !bbox(2) = array(gi, 3)
         !bbox(3) = array(gi, 4)
         !bbox(4) = array(gi, 5)
         !bbox(5) = array(gi, 6)
         !bbox(6) = array(gi, 7)
         !sdf_time= array(gi, 8)
         !h       = array(gi, 9)
         rank    = NINT(array(1,gi))
         bbox(1) = array(2,gi)
         bbox(2) = array(3,gi)
         bbox(3) = array(4,gi)
         bbox(4) = array(5,gi)
         bbox(5) = array(6,gi)
         bbox(6) = array(7,gi)
         sdf_time= array(8,gi)
         h       = array(9,gi)
         if(ltrace2) then
          write(*,*) "gi      = ", gi
          write(*,*) "rank    = ", rank
          write(*,*) "bbox(1) = ", bbox(1)
          write(*,*) "bbox(2) = ", bbox(2)
          write(*,*) "bbox(3) = ", bbox(3)
          write(*,*) "bbox(4) = ", bbox(4)
          write(*,*) "bbox(5) = ", bbox(5)
          write(*,*) "bbox(6) = ", bbox(6)
          write(*,*) "time    = ", sdf_time
          write(*,*) "h       = ", h
         endif
         if(ltrace2) write(*,*) "Leaving trans_array_return..."
         return
      end

      subroutine trans_array_send(array, proc)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'variables.inc'
         real*8      array(9)

         integer     proc

         logical ltrace2
         parameter (ltrace2 = .false.)
         if(ltrace2) write(*,*) "[", myid, "] Entering array send..."
         call MPI_BCAST( array, 9, MPI_DOUBLE_PRECISION, proc,
     *                   MPI_COMM_WORLD, ierr )
         if(ltrace2) write(*,*) "[", myid, "] Leaving array send..."
         return
      end


      subroutine trans_array_recv(array)
      implicit none
      include 'mpif.h'
      include 'mympi.inc'
      include 'variables.inc'
         real*8      array(9)

         logical ltrace2
         parameter (ltrace2 = .false.)
 
         if(ltrace2) write(*,*) "[", myid, "] ","Entering array recv..."
         call MPI_RECV( array, 9, MPI_DOUBLE_PRECISION, MPI_ANY_SOURCE, 
     *                  tag, MPI_COMM_WORLD, status, ierr )
         if(ltrace2) write(*,*) "[", myid, "] ","Leaving array recv..."
         return
      end



      !
      ! Matrix_Subtract: new_matrix = matrix1 - matrix2
      !
      subroutine matrix_subtract( matrix_new, shape_new, matrix1, 
     *                            shape1, matrix2, shape2 )
      implicit none
         integer shape_new(3), shape1(3), shape2(3)
         real*8  matrix_new(shape_new(1), shape_new(2), shape_new(3))
         real*8  matrix_res(shape_new(1), shape_new(2), shape_new(3))
         real*8  matrix1(shape1(1), shape1(2), shape1(3))
         real*8  matrix2(shape2(1), shape2(2), shape2(3))
       
         integer i,j,k
         logical ltrace
         parameter (ltrace=.false.)

         if(ltrace) then
            write(*,*) "Matrix_Sub: shape1(1)    = ", shape1(1)
            write(*,*) "Matrix_Sub: shape1(2)    = ", shape1(2)
            write(*,*) "Matrix_Sub: shape1(3)    = ", shape1(3)
            write(*,*) "Matrix_Sub: shape2(1)    = ", shape2(1)
            write(*,*) "Matrix_Sub: shape2(2)    = ", shape2(2)
            write(*,*) "Matrix_Sub: shape2(3)    = ", shape2(3)
            write(*,*) "Matrix_Sub: shape_new(1) = ", shape_new(1)
            write(*,*) "Matrix_Sub: shape_new(2) = ", shape_new(2)
            write(*,*) "Matrix_Sub: shape_new(3) = ", shape_new(3)
         endif
 
         ! First restrict so the resolutions are the same 
         ! Store restriction in matrix_new
         if ( shape1(1) .gt. shape2(1) ) then ! Restrict matrix1
            call restriction_operator(matrix1, shape1, 
     *                                matrix_res, shape_new)
           ! Subtraction
            do k = 1, shape_new(3)
             do j = 1, shape_new(2)
              do i = 1, shape_new(1)
               matrix_new(i,j,k) = matrix_res(i,j,k) - matrix2(i,j,k)
              enddo
             enddo
            enddo

         elseif ( shape1(1) .lt. shape2(1) ) then ! Restrict matrix2
            call restriction_operator(matrix2, shape2, 
     *                                matrix_res, shape_new)
           ! Subtraction
            do k = 1, shape_new(3)
             do j = 1, shape_new(2)
              do i = 1, shape_new(1)
               matrix_new(i,j,k) = matrix1(i,j,k) - matrix_res(i,j,k)
              enddo
             enddo
            enddo

         elseif (shape1(1) .eq. shape2(2)) then
           ! Subtraction
            do k = 1, shape_new(3)
             do j = 1, shape_new(2)
              do i = 1, shape_new(1)
               matrix_new(i,j,k) = matrix1(i,j,k) - matrix2(i,j,k)
              enddo
             enddo
            enddo
         endif 
         return
      end


      subroutine restriction_operator(phi_fine, shape_fine, 
     *                                phi_coarse, shape_coarse)
         implicit none
         integer shape_fine(3), shape_coarse(3)
         integer nx, ny, nz
         integer nx_coarse, ny_coarse, nz_coarse
         real*8  phi_fine(shape_fine(1),
     *                    shape_fine(2),
     *                    shape_fine(3))
         real*8  phi_coarse(shape_coarse(1),  
     *                      shape_coarse(2), 
     *                      shape_coarse(3))

         integer i_coarse, i_fine, j_coarse, j_fine, k_coarse, k_fine

         nx        = shape_fine(1)
         ny        = shape_fine(2)
         nz        = shape_fine(3)
         nx_coarse = shape_coarse(1)
         ny_coarse = shape_coarse(2)
         nz_coarse = shape_coarse(3)

         !
         !Using half-weighting we will set up phi on the coarse grid
         !On Interior points
         !
         do k_coarse = 2, nz_coarse - 1
            k_fine = 2 * k_coarse - 1

            do j_coarse = 2, ny_coarse - 1
               j_fine = 2 * j_coarse - 1

               do i_coarse = 2, nx_coarse - 1
                  i_fine = 2 * i_coarse - 1

                  phi_coarse(i_coarse, j_coarse, k_coarse) = 0.5d0 *
     *                phi_fine(i_fine, j_fine, k_fine) + (0.5d0/6.d0) *(
     *                    phi_fine(i_fine + 1, j_fine,     k_fine    ) +
     *                    phi_fine(i_fine - 1, j_fine,     k_fine    ) +
     *                    phi_fine(i_fine,     j_fine + 1, k_fine    ) +
     *                    phi_fine(i_fine,     j_fine - 1, k_fine    ) +
     *                    phi_fine(i_fine,     j_fine,     k_fine + 1) +
     *                    phi_fine(i_fine,     j_fine,     k_fine - 1) )

               enddo

            enddo

         enddo

         !Boundary Conditions

         do k_coarse = 1, nz_coarse
            do i_coarse = 1, nx_coarse
               phi_coarse(i_coarse, 1, k_coarse) =
     a            phi_fine(2 * i_coarse - 1, 1, 2 * k_coarse - 1)
               phi_coarse(i_coarse, ny_coarse, k_coarse) =
     a            phi_fine(2 * i_coarse - 1, ny, 2 * k_coarse - 1)
            enddo

            do j_coarse = 1, ny_coarse
               phi_coarse(1, j_coarse, k_coarse) =
     a            phi_fine(1, 2 * j_coarse - 1, 2 * k_coarse - 1)
               phi_coarse(nx_coarse, j_coarse, k_coarse) =
     a            phi_fine(nx, 2 * j_coarse - 1, 2 * k_coarse - 1)
            enddo
         enddo

         do j_coarse = 1, ny_coarse
            do i_coarse = 1, nx_coarse
               phi_coarse(i_coarse, j_coarse, 1) =
     *            phi_fine(2 * i_coarse - 1, 2 * j_coarse - 1, 1)
               phi_coarse(i_coarse, j_coarse, nz_coarse) =
     *            phi_fine(2 * i_coarse - 1, 2 * j_coarse - 1, nz)
            enddo
         enddo

      end

      subroutine matrix_fill( field, nx, ny, nz, value )
      implicit none
         integer nx, ny, nz
         real*8 field(nx, ny, nz), value

         integer i,j,k
 
         do k = 1, nz
          do j = 1, ny
           do i = 1, nx
              field(i,j,k) = value
           enddo
          enddo
         enddo
   
         return
      end

      subroutine matrix_set_value( field, nx, ny, nz, value, 
     *                             i, j, k )
      implicit none
      integer nx, ny, nz, i, j, k
      real*8  field(nx, ny, nz), value
   
         field(i,j,k) = value 
         return
      end

      real*8 function integrate(field, mask, shape, dx, dy, dz)
      implicit none
      integer shape(3)
      real*8  field(shape(1), shape(2), shape(3))
      real*8  mask(shape(1), shape(2), shape(3))
      real*8  dx, dy, dz

      real*8  integral
      integer i, j, k
      logical  double_equal
      external double_equal

         integral = 0.d0

         do k = 1, shape(3) - 1
           do j = 1, shape(2) - 1 
             do i = 1, shape(1) - 1 
                if(double_equal(mask(i,j,k), 0.d0)) then
                    integral = integral + dx*dy*dz*0.125d0*
     *                        ( field(i,   j,   k  ) +
     *                          field(i+1, j,   k  ) + 
     *                          field(i,   j+1, k  ) + 
     *                          field(i,   j,   k+1) +
     *                          field(i+1, j+1, k  ) +
     *                          field(i+1, j,   k+1) +
     *                          field(i,   j+1, k+1) +
     *                          field(i+1, j+1, k+1) ) 
                endif
             enddo
           enddo
         enddo
         integrate = integral

         return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc mystringlength:                                                            cc
cc                 Returns length of beginning non-space characters.          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function mystringlength( the_string )
      implicit none
      character*(*) the_string
      integer       the_string_length

      the_string_length = len(the_string)
      mystringlength    = index(the_string,' ') - 1
      if (mystringlength .lt. 0) mystringlength = the_string_length

      return
      end        ! END: function mystringlength

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  load_scal1d:                                                              cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine load_scal1d( field, scalar, nx)
      implicit   none
      integer    nx
      real(kind=8)     field(nx), scalar
      integer    i     
      
            do i = 1, nx
               field(i) = scalar
            end do 
            
      return   
      end    ! END: load_scal1d
